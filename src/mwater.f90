!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
        SUBROUTINE WATER
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MOLEC_M
	USE MUMBO_DATA_M
	USE MAX_DATA_M
	USE WATER_DATA
!$      USE OMP_LIB
!!
	IMPLICIT NONE
!
	integer, dimension(4)                         :: set1, set2, set3, set4, setold,setnew
	integer                                       :: numWATERpos
!
	integer,dimension(:,:),         allocatable   :: DATAsetsglobal
	real,   dimension(:,:),         allocatable   :: DATAcordglobal
	character(len=4), dimension(:), allocatable   :: DATAnameglobal
!
	integer                                       :: numWATERglobal_old, numWATERglobal_new
!	
	integer :: i, j, k, counter, natm, ndelwat, npos, ntot, ntid, nthreads
	integer,   dimension (mol(1)%mol_nrs)         ::  mutvec
	logical ::                      NOT_Hydrated_flag,unique_flag
	CHARACTER(LEN=10) :: echo
	real::test1e,test1s,test2e,test2s,test1,test2,test3e,test3s
!
!	
	 PRINT*, '        '
	 PRINT*, '##################################'
	 PRINT*, '#         SUMMARY WATER          #'
	 PRINT*, '##################################'
	 PRINT*, '          '
         
         PRINT*,MUMBO_PROC,' BUILDING WATERS BETWEEN MUTATED AS AND LIGAND               ?       ',echo(mumbo_lig_flag)  
         PRINT*,MUMBO_PROC,' BUILDING WATERS BETWEEN MUTATED AS AND MUTATED AS SIDECHAINS?       ',echo(Water_between_AS_SC_flag)
         PRINT*,MUMBO_PROC,' BUILDING WATERS BETWEEN MUTATED AS AND MUTATED AS MAINCHAINS?       ',echo(Water_between_AS_MC_flag)
         PRINT*,''                                                                               
         PRINT*,MUMBO_PROC,' MINIMAL ENERGY NECESSARY TO BUILD WATER                             ',H20_build_ene_min
         PRINT*,''
!         PRINT*,MUMBO_PROC,' MINIMAL DISTANCE BETWEEN TWO ATOMS    , SO THAT THEY ARE CALLED UNIQUE',DEL_eps              
         PRINT*,MUMBO_PROC,' MINIMAL DISTANCE BETWEEN TWO WATER OXIGENS  , SO THAT THEY ARE CALLED UNIQUE',Ox_min_dist      
         PRINT*,MUMBO_PROC,' MINIMAL DISTANCE BETWEEN TWO WATER HYDROGENS, SO THAT THEY ARE CALLED UNIQUE',Hy_min_dist                      
         PRINT*,''           
! 
         ndelwat=0
         counter=1
         numWATERglobal_new=0
         CALL CPU_TIME(test3s)
!        
        do i=1,mol(1)%mol_nrs
         if (mol(1)%res(i)%res_mut) then
	   mutvec(counter)=i
	   counter=counter+1 	   
	 end if
	end do 
!
!        counter-1 = total number of positions to be mumboed 
!        mutvec is kind of strange... mutvec(j) is identical to mol(j+1)%res(1)%res_num 
!
!        do i=1,(counter-1)
!          print*, mutvec(i), mol(i+1)%res(1)%res_num 
!        end do
!
!        some notes regarding the arrays that are being used:  
!
!        with respect to DATAsetspos, DATAcordpos, DATAnameglobal:  
!
!            let nn be a running index for the water molecule that has been built, then:
!
!            DATAsetspos(1,nn)=m1,DATAsetspos(2,nn)=m2,DATAsetspos(3,nn)=m3 and DATAsetspos(4,nn) =m4 identifies 
!                the rotamer to which it belongs namely the molecule, residue, amminoacid and rotamer as in: 
!                mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)
!
!            DATAcordpos(1:9,nn)  denotes the x,y,z-coordinates of the watermolecule (O, H1, H2 ?)
!
!            DATAnamepos(nn) = name of the solvated AA such as ASW, GLW etc.. present at nn 
!
!            DATAxxxglobal a global list of all bilt water molecules is stored... 
!
!
!        We also need some backup arrays... BACKsetsglobal
!                                           BACKcordglobal
!                                           BACKnameglobal
!
!        CALL GLOB_ADDWATER(mutvec,ntot,npos,numWATERpos,Datasetspos,DATAcordpos,Datanamepos) calls for building all 
!        water molecules related to a certain position. The position is defined by npos, which is nothing else then
!        i+1 relating to molecule mol(i+1). 
!        Not sure why mutvec and ntot are required in the subroutine
!
!       if working with allocatable arrays then we have to start with calling call GLOB_ADDWATER once FIRST 
!       before allowing for parallel calls. 
!
         if (allocated(DATAsetspos)) deallocate(DATAsetspos)
         if (allocated(DATAcordpos)) deallocate(DATAcordpos)
         if (allocated(DATAnamepos)) deallocate(DATAnamepos)
!
!        for the first position, initializse DATAsetsglobal, DATAcordglobal and DATAnameglobal 
!        before entering the parallelized section.. 
!
         i=2
!	 
	 PRINT*,MUMBO_PROC,' BUILDING WATERS AT MUTATION NUMBER     ',i-1
!	    
	 npos=i
	 ntot=counter
	 call GLOB_ADDWATER(mutvec,ntot,npos) 
!
         numWATERpos = size(DATAnamepos)
!
         if (allocated(DATAsetsglobal)) deallocate(DATAsetsglobal)
         allocate(DATAsetsglobal(4,numWATERpos))
!
         if (allocated(DATAcordglobal)) deallocate(DATAcordglobal)
         allocate(DATAcordglobal(9,numWATERpos))
!
         if (allocated(DATAnameglobal)) deallocate(DATAnameglobal)
         allocate(DATAnameglobal(numWATERpos))
!          
         do j=1,numWATERpos 
!          
            DATAsetsglobal(1,j)=DATAsetspos(1,j)
            DATAsetsglobal(2,j)=DATAsetspos(2,j)
            DATAsetsglobal(3,j)=DATAsetspos(3,j)
            DATAsetsglobal(4,j)=DATAsetspos(4,j)
!
            DATAnameglobal(j)  =DATAnamepos(j)
!            
            DATAcordglobal(1,j)=DATAcordpos(1,j)
            DATAcordglobal(2,j)=DATAcordpos(2,j)
            DATAcordglobal(3,j)=DATAcordpos(3,j)
            DATAcordglobal(4,j)=DATAcordpos(4,j)
            DATAcordglobal(5,j)=DATAcordpos(5,j)
            DATAcordglobal(6,j)=DATAcordpos(6,j)
            DATAcordglobal(7,j)=DATAcordpos(7,j)
            DATAcordglobal(8,j)=DATAcordpos(8,j)
            DATAcordglobal(9,j)=DATAcordpos(9,j)
!          
         end do
!         
         numWATERglobal_new=numWATERpos              
!
	 PRINT*,MUMBO_PROC,' WATERS BUILT AT POSITION NUMBER ',i-1, ' = ', numWATERpos 
	 PRINT*, '   '
!
!       now entering the parallel section 
!
!
!$     nthreads = omp_get_max_threads()
!
!$    PRINT*,MUMBO_PROC,' WILL USE ', nthreads,' THREADS ON ',omp_get_num_procs(), &
!$           ' CPUs; NUMBER OF MUTATIONS + Ligand',counter
!$    print*,'  '		
!
!$omp  parallel      &
!$omp& default(none) &
!$omp& shared(mol,counter,water_between_as_sc_flag,water_between_as_mc_flag,mumbo_lig_flag,ASWNAMES,      &
!$omp&        MUMBO_PROC, ntot, mutvec,                                                                   &
!$omp&        DATAsetsglobal, DATAcordglobal, DATAnameglobal)                                             &
!$omp& private(i, j, npos, ntid, numWATERpos, numWATERglobal_old, numWATERglobal_new)
!
!$omp  do schedule(guided,1)
!
         do i=3,counter
!      
            if (allocated(BACKsetsglobal)) deallocate(BACKsetsglobal)
            if (allocated(BACKcordglobal)) deallocate(BACKcordglobal)
            if (allocated(BACKnameglobal)) deallocate(BACKnameglobal)
!            
            if (allocated(DATAsetspos)) deallocate(DATAsetspos)
            if (allocated(DATAcordpos)) deallocate(DATAcordpos)
            if (allocated(DATAnamepos)) deallocate(DATAnamepos)    
!
            ntid = OMP_GET_THREAD_NUM() 
!
            PRINT*,MUMBO_PROC,' BUILDING WATERS AT MUTATION NUMBER ',i-1, ' THREAD= ', ntid
!   
	    npos=i
	    ntot=counter
!	    
            call GLOB_ADDWATER(mutvec,ntot,npos) 
            numWATERpos = size(DATAnamepos)
!           print*, numWATERpos
!
!    
!$omp  critical(write)
!
!            backing up existing     DATAsetsglobal  first...
!                                    DATAcordglobal
!                                    DATAnameglobal
!
            numWATERglobal_old = size(DATAnameglobal)
!
            allocate(BACKsetsglobal(4,numWATERglobal_old))
            allocate(BACKcordglobal(9,numWATERglobal_old))
            allocate(BACKnameglobal(numWATERglobal_old))
!            
            do j=1,numWATERglobal_old
!
                BACKsetsglobal(1,j) = DATAsetsglobal(1,j)
                BACKsetsglobal(2,j) = DATAsetsglobal(2,j)
                BACKsetsglobal(3,j) = DATAsetsglobal(3,j)
                BACKsetsglobal(4,j) = DATAsetsglobal(4,j)
!                               
                BACKnameglobal(j)   = DATAnameglobal(j) 
!                                
                BACKcordglobal(1,j) = DATAcordglobal(1,j)
                BACKcordglobal(2,j) = DATAcordglobal(2,j)
                BACKcordglobal(3,j) = DATAcordglobal(3,j)
                BACKcordglobal(4,j) = DATAcordglobal(4,j)
                BACKcordglobal(5,j) = DATAcordglobal(5,j)
                BACKcordglobal(6,j) = DATAcordglobal(6,j)
                BACKcordglobal(7,j) = DATAcordglobal(7,j)
                BACKcordglobal(8,j) = DATAcordglobal(8,j)
                BACKcordglobal(9,j) = DATAcordglobal(9,j)
!
            end do
!
            numWATERglobal_new = numWATERglobal_old + numWATERpos
!
            deallocate(DATAsetsglobal)
            allocate(DATAsetsglobal(4,numWATERglobal_new))
!
            deallocate(DATAcordglobal)
            allocate(DATAcordglobal(9,numWATERglobal_new))
!
            deallocate(DATAnameglobal)
            allocate(DATAnameglobal(numWATERglobal_new))
!          
            do j=1,numWATERglobal_old
!            
                DATAsetsglobal(1,j)  =  BACKsetsglobal(1,j)
                DATAsetsglobal(2,j)  =  BACKsetsglobal(2,j)
                DATAsetsglobal(3,j)  =  BACKsetsglobal(3,j)
                DATAsetsglobal(4,j)  =  BACKsetsglobal(4,j)
!                                                          
                DATAnameglobal(j)    =  BACKnameglobal(j)  
!                                                           
                DATAcordglobal(1,j)  =  BACKcordglobal(1,j)
                DATAcordglobal(2,j)  =  BACKcordglobal(2,j)
                DATAcordglobal(3,j)  =  BACKcordglobal(3,j)
                DATAcordglobal(4,j)  =  BACKcordglobal(4,j)
                DATAcordglobal(5,j)  =  BACKcordglobal(5,j)
                DATAcordglobal(6,j)  =  BACKcordglobal(6,j)
                DATAcordglobal(7,j)  =  BACKcordglobal(7,j)
                DATAcordglobal(8,j)  =  BACKcordglobal(8,j)
                DATAcordglobal(9,j)  =  BACKcordglobal(9,j)
!                
            end do
!
            do j=numWATERglobal_old+1,numWATERglobal_new 
!
                DATAsetsglobal(1,j)  =  DATAsetspos(1,j-numWATERglobal_old)
                DATAsetsglobal(2,j)  =  DATAsetspos(2,j-numWATERglobal_old)
                DATAsetsglobal(3,j)  =  DATAsetspos(3,j-numWATERglobal_old)
                DATAsetsglobal(4,j)  =  DATAsetspos(4,j-numWATERglobal_old)
!                                                     
                DATAnameglobal(j)    =  DATAnamepos(j-numWATERglobal_old)
!                                                     
                DATAcordglobal(1,j)  =  DATAcordpos(1,j-numWATERglobal_old)
                DATAcordglobal(2,j)  =  DATAcordpos(2,j-numWATERglobal_old)
                DATAcordglobal(3,j)  =  DATAcordpos(3,j-numWATERglobal_old)
                DATAcordglobal(4,j)  =  DATAcordpos(4,j-numWATERglobal_old)
                DATAcordglobal(5,j)  =  DATAcordpos(5,j-numWATERglobal_old)
                DATAcordglobal(6,j)  =  DATAcordpos(6,j-numWATERglobal_old)
                DATAcordglobal(7,j)  =  DATAcordpos(7,j-numWATERglobal_old)
                DATAcordglobal(8,j)  =  DATAcordpos(8,j-numWATERglobal_old)
                DATAcordglobal(9,j)  =  DATAcordpos(9,j-numWATERglobal_old)
!
            end do
!
!$omp  end critical (write)
!
	 PRINT*,MUMBO_PROC,' WATERS BUILT AT POSITION NUMBER ',i-1, ' = ', numWATERpos, ' THREAD= ', ntid
!
            if (allocated(BACKsetsglobal)) deallocate(BACKsetsglobal)
            if (allocated(BACKcordglobal)) deallocate(BACKcordglobal)
            if (allocated(BACKnameglobal)) deallocate(BACKnameglobal)
!            
            if (allocated(DATAsetspos)) deallocate(DATAsetspos) 
            if (allocated(DATAcordpos)) deallocate(DATAcordpos) 
            if (allocated(DATAnamepos)) deallocate(DATAnamepos)     
!
        end do
!$omp  end do
!$omp  end parallel
!
       numWATERglobal_new=size(DATAnameglobal) 
!
       test1=0.
       test2=0.     
       
       PRINT*,MUMBO_PROC,''
       PRINT*,MUMBO_PROC,' START WRITING WATERS                   '
       PRINT*,MUMBO_PROC,' NUMBER OF WATERMOLECULES FOUND         ',numWATERglobal_new
       
       do i=1,numWATERglobal_new
       
       test1s=0.
       test1e=0.
       test1s=0.
       test2e=0.
       
         setold=DATAsetsglobal(:,i)
         !print*,'setold',setold
         setnew=(/1,1,1,1/)
         
         natm =  mol(setold(1))%res(setold(2))%res_aas(setold(3))%aa_rots(setold(4))%rot_nat
         
            unique_flag=.FALSE.

            CALL CPU_TIME(test2s)
!            
            call test_if_unique_hydrated_ROTAMER_new(setold,natm,unique_flag,DATAcordglobal(:,i),DATAnameglobal(i))
!
            CALL CPU_TIME(test2e)
         
                  
         if (unique_flag) then
         
           CALL CPU_TIME(test1s)
           CALL EXPAND_SINGLE_AA(setold,setnew,DATAnameglobal(i))
           CALL CPU_TIME(test1e)
           
           !print*,'setnew',setnew
              
           mol(setnew(1))%res(setnew(2))%res_aas(setnew(3))%aa_rots(setnew(4))%rot_ats(natm+1)%at_xyz(1)=DATAcordglobal(1,i)
           mol(setnew(1))%res(setnew(2))%res_aas(setnew(3))%aa_rots(setnew(4))%rot_ats(natm+1)%at_xyz(2)=DATAcordglobal(2,i)
           mol(setnew(1))%res(setnew(2))%res_aas(setnew(3))%aa_rots(setnew(4))%rot_ats(natm+1)%at_xyz(3)=DATAcordglobal(3,i)
           mol(setnew(1))%res(setnew(2))%res_aas(setnew(3))%aa_rots(setnew(4))%rot_ats(natm+1)%at_bfa=1.0 
           mol(setnew(1))%res(setnew(2))%res_aas(setnew(3))%aa_rots(setnew(4))%rot_ats(natm+1)%at_occ=1.0
                                                                                         
                                                                                         
           mol(setnew(1))%res(setnew(2))%res_aas(setnew(3))%aa_rots(setnew(4))%rot_ats(natm+2)%at_xyz(1)=DATAcordglobal(4,i)
           mol(setnew(1))%res(setnew(2))%res_aas(setnew(3))%aa_rots(setnew(4))%rot_ats(natm+2)%at_xyz(2)=DATAcordglobal(5,i)
           mol(setnew(1))%res(setnew(2))%res_aas(setnew(3))%aa_rots(setnew(4))%rot_ats(natm+2)%at_xyz(3)=DATAcordglobal(6,i)
           mol(setnew(1))%res(setnew(2))%res_aas(setnew(3))%aa_rots(setnew(4))%rot_ats(natm+2)%at_bfa=1.0
           mol(setnew(1))%res(setnew(2))%res_aas(setnew(3))%aa_rots(setnew(4))%rot_ats(natm+2)%at_occ=1.0
                                                                                        
                                                                                        
           mol(setnew(1))%res(setnew(2))%res_aas(setnew(3))%aa_rots(setnew(4))%rot_ats(natm+3)%at_xyz(1)=DATAcordglobal(7,i)
           mol(setnew(1))%res(setnew(2))%res_aas(setnew(3))%aa_rots(setnew(4))%rot_ats(natm+3)%at_xyz(2)=DATAcordglobal(8,i)
           mol(setnew(1))%res(setnew(2))%res_aas(setnew(3))%aa_rots(setnew(4))%rot_ats(natm+3)%at_xyz(3)=DATAcordglobal(9,i)
           mol(setnew(1))%res(setnew(2))%res_aas(setnew(3))%aa_rots(setnew(4))%rot_ats(natm+3)%at_bfa=1.0
           mol(setnew(1))%res(setnew(2))%res_aas(setnew(3))%aa_rots(setnew(4))%rot_ats(natm+3)%at_occ=1.0
         else     
           ndelwat=ndelwat+1   
         end if
       
            test1=test1+test1e-test1s
            test2=test2+test2e-test2s
        end do
        CALL CPU_TIME(test3e)
        
        PRINT*,MUMBO_PROC,' NUMBER OF WATERMOLECULES DELETED       ',ndelwat
        PRINT*,MUMBO_PROC,' NUMBER OF WATERMOLECULES KEPT          ',numWATERglobal_new - ndelwat
        PRINT*,MUMBO_PROC,''                                             
        PRINT*,MUMBO_PROC,' NECESSARY TIME TO TEST   THE ROTAMERES',test2,'s'
        PRINT*,MUMBO_PROC,' NECESSARY TIME TO EXPAND THE ROTAMERES',test1,'s'
        PRINT*,MUMBO_PROC,' NECESSARY TIME FOR ADDING WATER       ',test3e-test3s,'s'
!!
!!
	deallocate(DATAsetsglobal)
	deallocate(DATAcordglobal)
	deallocate(DATAnameglobal)
!        
        END SUBROUTINE WATER
!!
!!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!!
        SUBROUTINE GLOB_ADDWATER(mutvec,ntot,npos)
!        SUBROUTINE GLOB_ADDWATER(mutvec,ntot,npos,numWATERpos,DATAsetspos,DATAcordpos,DATAnamepos)
!!
!!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
!!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!!NOTIZEN: - UNTERES MUSS NOCH SAUBER AUSGETÜFTELT WERDEN, NÄMLICH, das Erweiteren des pos datensatzes.. 
!!           und gleichzeitiges BACKUPen.                                                       
!!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
	USE MOLEC_M
	USE MUMBO_DATA_M
	USE MAX_DATA_M
	USE WATER_DATA
!
	IMPLICIT NONE
!
	integer, dimension(4)                          :: set1, set2, set3, set4
	integer :: i, j, k, l, m, n, o, p,q, counter,natm,ndelwat,npos,ntot
	integer,   dimension (mol(1)%mol_nrs)         ::  mutvec
	logical ::                      NOT_Hydrated_flag, unique_flag
!
           i=npos
!	 
	   set1(1)=i    
	   set1(2)=1   
!	    
	   set3(1)=1
	   set3(2)=mutvec(i-1)	  
!	   
	   first_DATApos=.true.
!	    
!	 PRINT*,MUMBO_PROC,' NUMBER OF DIFFERENT AS AT THIS POSITION',mol(set1(1))%res(set1(2))%res_naa   
!	  	    
	    do o=1,mol(set1(1))%res(set1(2))%res_naa	     
	     set1(3)=o
	     
	     set3(3)=1	     
	     set3(4)=1
!	     
!	     
	     do j=1,mol(set1(1))%res(set1(2))%res_aas(set1(3))%aa_nrt	      
	       set1(4)=j
!	       
	       if (Water_between_AS_SC_flag .OR. Water_between_AS_MC_flag) then! WATER BETWEEN MUTATED AS AND MUTATED AS
	         do l=2,ntot
	          if (i .NE.l) then	          	         	         
!	          
	           set2(1)=l
	           set2(2)=1
	           set4(1)=1
	           set4(2)=mutvec(l-1)
!	           	           	           	           	           	           
	           do p=1,mol(set2(1))%res(set2(2))%res_naa
	            set2(3)=p	            
!	            
	            set4(3)=1
	            set4(4)=1
!	            	          
	            NOT_Hydrated_flag=.TRUE. 
!	           
	            do m=1,size(ASWNAMES)
	             if (mol(set1(1))%res(set1(2))%res_aas(set1(3))%aa_nam == ASWNAMES(m)) then
	              NOT_Hydrated_flag=.FALSE.
	             end if
	             if (mol(set2(1))%res(set2(2))%res_aas(set2(3))%aa_nam == ASWNAMES(m)) then
	              NOT_Hydrated_flag=.FALSE.
	             end if	            
	            end do
!	            
	            if(NOT_Hydrated_flag) then
	             do m=1,mol(set2(1))%res(set2(2))%res_aas(set2(3))%aa_nrt
	              set2(4)=m	              
	              if (Water_between_AS_SC_flag)then
	               call ADDWATER(set1,set2,set3)
	               call SUMWATER
	               first_DATApos=.false.
	              end if	              
	               
	              if (Water_between_AS_MC_flag) then
	               call ADDWATER(set3,set2,set1)
	               call SUMWATER
	               first_DATApos=.false.
	              ! WENN MIT RESTLICHEN PROTEIN NICHT NÖTIG SONST DOPPELT?
	               call ADDWATER(set1,set4,set3)
	               call SUMWATER
	               call ADDWATER(set3,set4,set1)
	               call SUMWATER
	              end if 
!	              
                    end do
	            end if
	           end do
	          end if
	         end do
	       end if
	       
	        ! WASSER ZWISCHEN REST UND LIEGANDEN
	       if (mumbo_lig_flag) then
	        do k=1,mol(ntot+1)%res(1)%res_aas(1)%aa_nrt
	              
	         set2(1)=ntot+1
	         set2(2)=1
	         set2(3)=1
	         set2(4)=k	      	   
!                          
	         call ADDWATER(set1,set2,set3)
	         call SUMWATER
	         first_DATApos=.false.
	         call ADDWATER(set3,set2,set1)
	         call SUMWATER
                end do	
               end if
	     end do  
	    end do	        
!	           
        END SUBROUTINE GLOB_ADDWATER
!!
!!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!!
        SUBROUTINE SUMWATER
!!
!!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MOLEC_M
	USE MUMBO_DATA_M
	USE MAX_DATA_M
	USE WATER_DATA
!
	IMPLICIT NONE
!
	integer                                     :: numWATERpos_old, numWATERpos_new
        integer                                     :: numWATERlocal
!
        integer                                     :: i
!
        numWATERlocal=size(DATAnamelocal)
!
        if (first_DATApos) then 
!
        numWATERpos_new=size(DATAnamelocal)
!        
        if (allocated(DATAsetspos)) deallocate(DATAsetspos)
        allocate(DATAsetspos(4,numWATERpos_new))
        if (allocated(DATAcordpos)) deallocate(DATAcordpos)
        allocate(DATAcordpos(9,numWATERpos_new))
        if (allocated(DATAnamepos)) deallocate(DATAnamepos)
        allocate(DATAnamepos(numWATERpos_new))
!
        do i=1,numWATERpos_new
!        
          DATAsetspos(1,i)=DATAsetslocal(1,i)
          DATAsetspos(2,i)=DATAsetslocal(2,i)
          DATAsetspos(3,i)=DATAsetslocal(3,i)
          DATAsetspos(4,i)=DATAsetslocal(4,i)
!
          DATAnamepos(i)  =DATAnamelocal(i)
!          
          DATAcordpos(1,i)=DATAcordlocal(1,i)
          DATAcordpos(2,i)=DATAcordlocal(2,i)
          DATAcordpos(3,i)=DATAcordlocal(3,i)
          DATAcordpos(4,i)=DATAcordlocal(4,i)
          DATAcordpos(5,i)=DATAcordlocal(5,i)
          DATAcordpos(6,i)=DATAcordlocal(6,i)
          DATAcordpos(7,i)=DATAcordlocal(7,i)
          DATAcordpos(8,i)=DATAcordlocal(8,i)
          DATAcordpos(9,i)=DATAcordlocal(9,i)
!          
        end do
!        
        else 
!        
          numWATERpos_old = size(DATAnamepos)
!          
          if (allocated(BACKsetspos)) deallocate(BACKsetspos)
          if (allocated(BACKcordpos)) deallocate(BACKcordpos)
          if (allocated(BACKnamepos)) deallocate(BACKnamepos)
!                    
          allocate(BACKsetspos(4,numWATERpos_old))
          allocate(BACKcordpos(9,numWATERpos_old))
          allocate(BACKnamepos(numWATERpos_old))
!          
          do i=1,numWATERpos_old
          
            BACKsetspos(1,i)=DATAsetspos(1,i)
            BACKsetspos(2,i)=DATAsetspos(2,i)
            BACKsetspos(3,i)=DATAsetspos(3,i)
            BACKsetspos(4,i)=DATAsetspos(4,i)
!
            BACKnamepos(i)  =DATAnamepos(i)  
!          
            BACKcordpos(1,i)=DATAcordpos(1,i)
            BACKcordpos(2,i)=DATAcordpos(2,i)
            BACKcordpos(3,i)=DATAcordpos(3,i)
            BACKcordpos(4,i)=DATAcordpos(4,i)
            BACKcordpos(5,i)=DATAcordpos(5,i)
            BACKcordpos(6,i)=DATAcordpos(6,i)
            BACKcordpos(7,i)=DATAcordpos(7,i)
            BACKcordpos(8,i)=DATAcordpos(8,i)
            BACKcordpos(9,i)=DATAcordpos(9,i)
!            
          end do  
!
          numWATERpos_new=numWATERpos_old+numWATERlocal
!
          if (allocated(DATAsetspos)) deallocate(DATAsetspos)
          allocate(DATAsetspos(4,numWATERpos_new))
          if (allocated(DATAcordpos)) deallocate(DATAcordpos)
          allocate(DATAcordpos(9,numWATERpos_new))
          if (allocated(DATAnamepos)) deallocate(DATAnamepos)
          allocate(DATAnamepos(numWATERpos_new))
!
          do i=1,numWATERpos_old
!          
            DATAsetspos(1,i)  =    BACKsetspos(1,i)
            DATAsetspos(2,i)  =    BACKsetspos(2,i)
            DATAsetspos(3,i)  =    BACKsetspos(3,i)
            DATAsetspos(4,i)  =    BACKsetspos(4,i)
!
            DATAnamepos(i)    =    BACKnamepos(i)  
!          
            DATAcordpos(1,i)  =    BACKcordpos(1,i)
            DATAcordpos(2,i)  =    BACKcordpos(2,i)
            DATAcordpos(3,i)  =    BACKcordpos(3,i)
            DATAcordpos(4,i)  =    BACKcordpos(4,i)
            DATAcordpos(5,i)  =    BACKcordpos(5,i)
            DATAcordpos(6,i)  =    BACKcordpos(6,i)
            DATAcordpos(7,i)  =    BACKcordpos(7,i)
            DATAcordpos(8,i)  =    BACKcordpos(8,i)
            DATAcordpos(9,i)  =    BACKcordpos(9,i)
!          
          end do  
!        
          do i=numWATERpos_old+1, numWATERpos_new
!          
            DATAsetspos(1,i)=DATAsetslocal(1,i-numWATERpos_old)
            DATAsetspos(2,i)=DATAsetslocal(2,i-numWATERpos_old)
            DATAsetspos(3,i)=DATAsetslocal(3,i-numWATERpos_old)
            DATAsetspos(4,i)=DATAsetslocal(4,i-numWATERpos_old)
!
            DATAnamepos(i)  =DATAnamelocal(i-numWATERpos_old)
!          
            DATAcordpos(1,i)=DATAcordlocal(1,i-numWATERpos_old)
            DATAcordpos(2,i)=DATAcordlocal(2,i-numWATERpos_old)
            DATAcordpos(3,i)=DATAcordlocal(3,i-numWATERpos_old)
            DATAcordpos(4,i)=DATAcordlocal(4,i-numWATERpos_old)
            DATAcordpos(5,i)=DATAcordlocal(5,i-numWATERpos_old)
            DATAcordpos(6,i)=DATAcordlocal(6,i-numWATERpos_old)
            DATAcordpos(7,i)=DATAcordlocal(7,i-numWATERpos_old)
            DATAcordpos(8,i)=DATAcordlocal(8,i-numWATERpos_old)
            DATAcordpos(9,i)=DATAcordlocal(9,i-numWATERpos_old)
!            
          end do
!
          deallocate(BACKsetspos)
          deallocate(BACKcordpos)
          deallocate(BACKnamepos)
!
        end if
!
        deallocate(DATAsetslocal)
        deallocate(DATAnamelocal)
        deallocate(DATAcordlocal)
!
        END SUBROUTINE SUMWATER
!
!!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!!
        SUBROUTINE ADDWATER(set1,set2,setmc)
!!
!!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
!!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!!NOTIZEN: -
!!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
	USE MOLEC_M
	USE MUMBO_DATA_M
	USE MAX_DATA_M
	USE WATER_DATA
!!	
	IMPLICIT NONE
!!
	integer, dimension(4)                      ::  set1, set2, setmc
	real,      dimension (:),     allocatable  ::  xyz1, xyz2, xyz3
	
	integer,   dimension (:,:),   allocatable  ::  hd
	integer,   dimension (:,:),   allocatable  ::  ab
	integer,   dimension (:),     allocatable  ::  mlc
	integer,   dimension (:),     allocatable  ::  AOD !1 wenn Akzeptor oder Donor
	integer                                    ::  n1, n2, n3, n4, m1, m2, m3, m4
	real                                       ::  pi=3.1415926535
	integer                                    ::  na1,  na2, ntot, n, i ,j ,k,l,q,s,t
	character (len=1)                          ::  c_hb
	real,      dimension(3)                    ::  dist_xyz, dist_temp                                          
 	real                                       ::  dist
 	!real                                       ::  H2O_mindistAOD=3.09,H2O_maxdistAOD=5.2
 	!VEREINFACHT. RICHTIG: 6.85 ! min: OMEGA 80°,Watet-Polar-Dist=2.8 ,max: OMEGA 150°,Watet-Polar-Dist=3.5 Theta schwankung 
 	!theoretisch OMEGA+-7° bei betrachtung bis 150° für Theta 	
        ! Muss noch sortiert und umbenannt werden
	character (len=4) :: c_at, c_ab
	integer           :: o1,o2,o3,o4,o5
	integer, dimension(5)          :: o
        integer, dimension(:,:), allocatable :: con_at
	real :: r,nlen,ulen,alpha,alphastep,angle1,angle2,B1disti,B1distj,WDdisti,WDdistj
	real, dimension(3)::M,W1,u,norm,un
	real, dimension(3,3)::RM
	real,dimension(100,3)::WT
	real::TEMP1,TEMP2,TEMP3,TEMP4
	real,dimension(3)::TEMPin,TEMPout,TEMP1in,TEMP2in
	real::TEMP1inlen,TEMP2inlen,TEMPinlen,GP1len,GP2len
	integer,dimension(:,:), allocatable:: HS, AS	!Bis jetzt nur ein H20 möglich
	real,dimension(3,4)::G1,G2,GP1,GP2
	real,dimension(3)::V,Vec1,Vec2,Vec3,Vec
	real::G1len,G2len,Vlen,NVElen,NVE2len,TEMP1len,TEMP2len,Vec1len,angle,TEMPoutlen,Veclen,T71len,T72len,Vec3len,Vec2len
	integer,dimension(3)::H,A
	integer             ::nH,nA,nB,nG1,nG2,nHG1,nHG2
	real,dimension(3)::T1,T2,T3,T4,T5,T6,MP,T71,T72,T7,Wfinal,Wtemp,T8,Ox,Hy1,Hy2,Hy3
	real,dimension(3,3)::E
	real,dimension(3)::NVE,B1xyzdisti,B1xyzdistj, WDxyzdisti, WDxyzdistj
	logical :: hd1_flag, hd2_flag, ab1_flag, ab2_flag
	integer, dimension(3) :: B223
	integer, dimension(4) :: B2223,set3,set4
	real,dimension(3):: Din,B2in,B3in 
	real,dimension(3,3)::Bout
	logical::mindistflag,H20twoflag
	integer::counter 
	character (len=4)::aname1,aname1out  
        real::ehb1,ehb2,ehb3,ehb4
        logical::asp_flag
        real,dimension(3)::A1,A2
        logical::unique_flag
        real,dimension(3)::rotvec
        real             ::rotveclen    
        real             ::aa, bb, cc, AMlength, ABlength
        real,dimension(3)::AABB
        character,dimension(:), allocatable::atm_names
!        
        integer :: numWATERlocal, numwaterlocal_max
!
!       I am afraid, but we need some kind of fixed array here before packing it into an allocatable array
!
        integer, dimension(:,:), allocatable                     :: temp_DATAsetslocal
        real, dimension(:,:), allocatable                        :: temp_DATAcordlocal
        character(len=4),dimension(:), allocatable               :: temp_DATAnamelocal
!        
        numwaterlocal_max= 1000
!       
        allocate(temp_DATAsetslocal(4,numwaterlocal_max))
        allocate(temp_DATAcordlocal(9,numwaterlocal_max))
        allocate(temp_DATAnamelocal(numwaterlocal_max))
!
        numWATERlocal=0
!
	m1=set1(1)
	m2=set1(2)
	m3=set1(3)
	m4=set1(4)
!
	n1=set2(1)
	n2=set2(2)
	n3=set2(3)
	n4=set2(4)
!
	na1    =  mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_nat	
	na2    =  mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_nat
	ntot   =  na1+na2
!
        if (set1(1) .NE. 1) then
          aname1 =  mol(m1)%res(m2)%res_aas(m3)%aa_nam
        else if (set1(1) == 1) then
         aname1 =  mol(setmc(1))%res(setmc(2))%res_aas(setmc(3))%aa_nam
        end if
!
	if (allocated(mlc))   deallocate(mlc)
	allocate(mlc(ntot))
	if (allocated(hd))    deallocate(hd)
	allocate(hd(ntot,3))
	if (allocated(ab))    deallocate(ab)
	allocate(ab(ntot,3))
	if (allocated(xyz1))  deallocate(xyz1)
	allocate(xyz1(ntot))
	if (allocated(xyz2))  deallocate(xyz2)
	allocate(xyz2(ntot))
	if (allocated(xyz3))  deallocate(xyz3)
	allocate(xyz3(ntot))
        if (allocated(AOD))   deallocate(AOD)
	allocate(AOD(ntot))
!	
	if (allocated(con_at)) deallocate(con_at)
	allocate(con_at(ntot,ntot))
!	
	if (allocated(HS)) deallocate(HS)
	allocate(HS(ntot,2))
	if (allocated(AS)) deallocate(AS)
	allocate(AS(ntot,2))
	if (allocated(atm_names)) deallocate(atm_names)
	allocate(atm_names(ntot))
!
!		**ÜBERTRAGEN VON NÖTIGEN INFOS**  
! 
 	n=0       
	do i=1, na1       
	  n= n+1       
	  mlc(n)=   m1       
     	  xyz1(n)=  mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_xyz(1)       
	  xyz2(n)=  mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_xyz(2)       
	  xyz3(n)=  mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_xyz(3)            
	end do        
!       
	do i=1, na2       
	  n= n+1 
	  mlc(n)=   n1	  
	  xyz1(n)=  mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(i)%at_xyz(1)       
	  xyz2(n)=  mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(i)%at_xyz(2)       
	  xyz3(n)=  mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(i)%at_xyz(3)          
	end do        
!
!		HIER MÜSSEN NOCH KONNEKTIVITÄTEN BERECHNET WERDEN      
!
!		**HBONDS**
!			
!		ÜBERTRAGEN DER AKZEPTOR UND DONOR EIGENSCHAFTEN 
!
 	do i=1,ntot
	  if (i<=na1) then
	    atm_names(i)= mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_nam(1:1)
	  else
	    atm_names(i)= mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(i-na1)%at_nam(1:1)
	  end if
	end do
!
!	
	do i=1,ntot
	  if (i<=na1) then
	    c_hb= mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_hb
!	    c_ab = mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_ab
!	    c_at = mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_nam
!	    print*,'i=',i,'C_HB = ',c_hb,': C_AB = ',c_ab,'C_AT = ',c_at
	  else
	    c_hb= mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(i-na1)%at_hb
!	    c_ab = mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(i-na1)%at_ab
!	    c_at = mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(i-na1)%at_nam
!	    print*,'i=',i,'C_HB = ',c_hb,': C_AB = ',c_ab,'C_AT = ',c_at

	  end if	
	  
	  AOD(i)=0

	  if (c_hb == h_sp_name .or. c_hb == h2_sp_name .or. c_hb == h3_sp_name ) then
	    hd(i,1) = 1
	    hd(i,2) = 0
	    hd(i,3) = 0
	    ab(i,1) = 0
	    ab(i,2) = 0
	    ab(i,3) = 0
	  else if (c_hb == d_sp3_name) then 
	    hd(i,1) = 0 
	    hd(i,2) = 1
	    hd(i,3) = 0
	    ab(i,1) = 0
	    ab(i,2) = 0
	    ab(i,3) = 0
	    AOD(i)  = 1
	  else if (c_hb == d_sp2_name) then
            hd(i,1) = 0 
	    hd(i,2) = 1
	    hd(i,3) = 1
	    ab(i,1) = 0
	    ab(i,2) = 0
	    ab(i,3) = 0
	    AOD(i)  = 1
	  else if (c_hb == a_sp3_name) then 
	    hd(i,1) = 0
	    hd(i,2) = 0
	    hd(i,3) = 0
	    ab(i,1) = 1
	    ab(i,2) = 0
	    ab(i,3) = 0
	    AOD(i)  = 1
	  else if (c_hb == a_sp2_name) then 
	    hd(i,1) = 0
	    hd(i,2) = 0
	    hd(i,3) = 0
	    ab(i,1) = 1
	    ab(i,2) = 1
	    ab(i,3) = 0
	    AOD(i)  = 1
	  else if (c_hb == b_sp3_name) then 
	    hd(i,1) = 0 
	    hd(i,2) = 1
	    hd(i,3) = 0
	    ab(i,1) = 1
	    ab(i,2) = 0
	    ab(i,3) = 0
	    AOD(i)  = 1
	  else if (c_hb == b_sp2_name) then 
	    hd(i,1) = 0 
	    hd(i,2) = 1
	    hd(i,3) = 1
	    ab(i,1) = 1
	    ab(i,2) = 1
	    ab(i,3) = 0
	    AOD(i)  = 1
	  else if (c_hb .EQ. '-') then 
	    hd(i,1) = 0 
	    hd(i,2) = 0
	    hd(i,3) = 0
	    ab(i,1) = 0
	    ab(i,2) = 0
	    ab(i,3) = 0
	   else
	    print*,c_hb,'UNKNOWN AKZEPTOR/DONOR GROUP daher übergang en'
	    hd(i,1) = 0	 !BUMMER MELDUNG HINZUFÜGEN    
	    hd(i,2) = 0	     
	    hd(i,3) = 0	     
	    ab(i,1) = 0	     
	    ab(i,2) = 0	     
	    ab(i,3) = 0	
	  end if
	 end do 
	 
	 
!Erstellen der Konnektivitätstabelle

     	do i=1, ntot
       	 do j=1, ntot
       	   con_at(i,j) = 9
       	   if (i==j) con_at(i,j) = 1 
       	 end do
       	end do


       	do i=1, ntot
       	 if (i <= na1) then
          do k=1,mol(m1)%res(m2)%res_aas(m3)%aa_rots(1)%rot_ats(i)%at_n14
	    do l=1,5
	      o(l)= mol(m1)%res(m2)%res_aas(m3)%aa_rots(1)%rot_ats(i)%at_c14(k,l)
	    end do
       	    if (o(1)==m1.and.o(2)==m2.and.o(3)==m3) then
       	       con_at(i,o(4)) = o(5)
       	    end if 
       	    if (o(1)==n1.and.o(2)==n2.and.o(3)==n3) then
       	       con_at(i,(na1+o(4))) = o(5) 
       	    end if
       	   end do
         else 
           do k=1,mol(n1)%res(n2)%res_aas(n3)%aa_rots(1)%rot_ats(i-na1)%at_n14
            do l=1,5
	     o(l)= mol(n1)%res(n2)%res_aas(n3)%aa_rots(1)%rot_ats(i-na1)%at_c14(k,l)
       	    end do 	    
            if (o(1)==m1.and.o(2)==m2.and.o(3)==m3) then
             con_at(i,o(4)) = o(5)
            end if 
            if (o(1)==n1.and.o(2)==n2.and.o(3)==n3) then
             con_at(i,(na1+o(4))) = o(5) 
            end if
       	   end do
       	 end if 
       	end do
      
       
 !      do i=1,ntot
 !       PRINT*, con_at!(i,2)
 !      end do
 !     
 !
 !   
 !        	do i=1, ntot
 !        	if (i <= na1) then 
 !        	  do k=1,mol(m1)%res(m2)%res_aas(m3)%aa_rots(1)%rot_ats(i)%at_n14
 !        	   o1= mol(m1)%res(m2)%res_aas(m3)%aa_rots(1)%rot_ats(i)%at_c14(k,1)
 !        	   o2= mol(m1)%res(m2)%res_aas(m3)%aa_rots(1)%rot_ats(i)%at_c14(k,2)
 !        	   o3= mol(m1)%res(m2)%res_aas(m3)%aa_rots(1)%rot_ats(i)%at_c14(k,3)
 !        	   o4= mol(m1)%res(m2)%res_aas(m3)%aa_rots(1)%rot_ats(i)%at_c14(k,4)
 !        	   o5= mol(m1)%res(m2)%res_aas(m3)%aa_rots(1)%rot_ats(i)%at_c14(k,5)
 !        	   if (o1==m1.and.o2==m2.and.o3==m3) then
 !        	      con_at(i,o4) = o5
 !        	   end if 
 !        	   if (o1==n1.and.o2==n2.and.o3==n3) then
 !        	      con_at(i,(na1+o4)) = o5 
 !        	   end if
 !        	  end do
 !        	else 
 !        	  do k=1,mol(n1)%res(n2)%res_aas(n3)%aa_rots(1)%rot_ats(i-na1)%at_n14
 !        	   o1= mol(n1)%res(n2)%res_aas(n3)%aa_rots(1)%rot_ats(i-na1)%at_c14(k,1)
 !        	   o2= mol(n1)%res(n2)%res_aas(n3)%aa_rots(1)%rot_ats(i-na1)%at_c14(k,2)
 !        	   o3= mol(n1)%res(n2)%res_aas(n3)%aa_rots(1)%rot_ats(i-na1)%at_c14(k,3)
 !        	   o4= mol(n1)%res(n2)%res_aas(n3)%aa_rots(1)%rot_ats(i-na1)%at_c14(k,4)
 !        	   o5= mol(n1)%res(n2)%res_aas(n3)%aa_rots(1)%rot_ats(i-na1)%at_c14(k,5)
 !        	   if (o1==m1.and.o2==m2.and.o3==m3) then
 !        	      con_at(i,o4) = o5
 !        	   end if 
 !        	   if (o1==n1.and.o2==n2.and.o3==n3) then
 !        	      con_at(i,(na1+o4)) = o5 
 !        	   end if
 !        	  end do
 !        	end if 
 !        	end do
 !       
 !       
 !        do i=1,ntot
 !         PRINT*, con_at(i,2)
 !        end do
     
     
       
!       PRINT*, 'CON_ALL', set1, set2
!       do i=1,ntot
!         write(*,'(<ntot>i2)')(con_at(i,j),j=1,ntot)
!       end do 
!Vermutlich richtig, aber nochmal überprüfen!!!!
!ENDE erstellen der Konnektivitätstabelle

!! No need for further processing if only one or less polar groups are present
!! Figuring out the position of donor, donor-1, acceptor, acceptor-1, acceptor-2
!! IST UNTERSCHEIDUNG ZWISCHEN SP2 UND SP3 BEI WASSER ÜBERHAUPT SINNVOLL???


	if (sum(AOD)>1) then 
	  do i=1,ntot
	   if (hd(i,1)==1) then 
	    hd1_flag=.true.
	    hd2_flag=.true.
	    do j=1,ntot
	     if (hd(j,1)==0.and.hd(j,2)==1.and.con_at(i,j)==2) then
		  if (hd(j,3)==1) then
		    if (j<= na1) then 
		      c_ab = mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(j)%at_ab
		      do k=1,na1 
			  c_at = mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(k)%at_nam
			  if (con_at(i,k)==3.and.c_ab==c_at) then
			    hd(i,3)= k
			    hd2_flag=.false.
			  end if 
			end do
		    else 
		      c_ab = mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(j-na1)%at_ab
		      do k=1,na2 
			  c_at = mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(k)%at_nam
			  if (con_at(i,(na1+k))==3.and.c_ab==c_at) then
			    hd(i,3)= k+na1
			    hd2_flag=.false.
			  end if
		      end do
		    end if
		    hd(i,2)=j
		    hd1_flag=.false.
		  else 
		    hd(i,2)=j
		    hd(i,3)=0
		    hd1_flag=.false.
		    hd2_flag=.false.
		  end if
	     end if
	    end do
!
	    if (hd1_flag.or.hd2_flag) then 
	  PRINT*, '          '
	  PRINT*, ' >>>>>>> BUMMER >>>>>>>>>>>>>>>>>>>>>>>>>>>>>    '
	  PRINT*, '   FAILED TO FIND DONOR, DONOR-1 FOR HYDROGEN    '
	  PRINT*, '   ATOM: ', i, ' EITHER IN ' 
	  PRINT*, '   ',m1, m2, m3, m4  , ' OR '
	  PRINT*, '   ',n1, n2, n3, n4 , 'IN RESIDUE:  '
	  PRINT*, '   ',mol(m1)%res(m2)%res_aas(m3)%aa_nam, ' OR '
	  PRINT*, '   ',mol(n1)%res(n2)%res_aas(n3)%aa_nam
 	  PRINT*, '          '
 	  PRINT*, '   CHECK CONNECTIVITY OR RESIDUE PARAMETER FILE '
	  PRINT*, ' >>>>>>> BUMMER >>>>>>>>>>>>>>>>>>>>>>>>> '
 	  PRINT*, '          '
		STOP 
           end if
!
	   end if
	   if (ab(i,1) == 1) then 
	     ab1_flag=.true.
	     ab2_flag=.true.
	     if (ab(i,2) == 0) then 
		  do j=1,ntot
		    if (con_at(i,j) == 2 .and. hd(j,1)==0) then 
		      ab(i,2) = j
			ab1_flag=.false.
		    end if
		  end do
		  ab(1,3) = 0
		  ab2_flag=.false.
	     else if (ab(i,2) == 1) then
		  if(i<=na1) then 
		    c_ab = mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_ab
!		    print*, c_ab, ' C_AB'
		    do k=1,na1 
			  c_at = mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(k)%at_nam
!		          print*, c_at, c_ab, con_at(i,k)
			  if (con_at(i,k)==2.and.c_ab==c_at) then
			    ab(i,2)= k
			    ab1_flag=.false.
			  end if 
		    end do
		  else 
		    c_ab = mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(i-na1)%at_ab
!		    print*, c_ab, ' C_AB'
		    do k=1,na2 
			  c_at = mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(k)%at_nam
!		          print*, c_at, c_ab, con_at(i,na1+k)
			  if (con_at(i,(na1+k))==2.and.c_ab==c_at) then
			    ab(i,2)= na1+k
			    ab1_flag=.false.
			  end if
		    end do
		  end if
		  do k=1,ntot
		    if (con_at(i,k)==3.and.hd(k,1)==0) then
			  ab(i,3) = k
			  ab2_flag=.false.
		    end if
		  end do
	     end if
	     if (ab1_flag.or.ab2_flag) then 
	  PRINT*, '          '
	  PRINT*, ' >>>>>>> BUMMER >>>>>>>>>>>>>>>>>>>>>>>>>>>>>    '
	  PRINT*, '   FAILED TO FIND ACCEPTOR-1, ACCEPTOR-2 FOR     '
	  PRINT*, '   ATOM: ', i, ' EITHER IN ' 
	  PRINT*, '   ',m1, m2, m3, m4  , ' OR '
	  PRINT*, '   ',n1, n2, n3, n4, 'IN RESIDUE:  '
	  PRINT*, '   ',mol(m1)%res(m2)%res_aas(m3)%aa_nam, ' OR '
	  PRINT*, '   ',mol(n1)%res(n2)%res_aas(n3)%aa_nam
 	  PRINT*, '          '
 	  PRINT*, '   CHECK CONNECTIVITY OR RESIDUE PARAMETER FILE '
	  PRINT*, ' >>>>>>> BUMMER >>>>>>>>>>>>>>>>>>>>>>>>> '
 	  PRINT*, '          '
		  STOP 
	     end if
	  end if
	  end do
        end if  
        
!        do i=1,ntot
!         print*, i, ab(i,1), ab(i,2), ab(i,3),hd(i,1),hd(i,2),hd(i,3)
!        end do

!!
!!

!!  INTRAMOLEKULARE WASSER SOLLEN BLOS EINMAL PRO REST BETRACHTET WERDEN, EVENTUELL EXTRA DURCHLAUF
  	
	if (sum(AOD)>1)then
	 do i=2,ntot
	   if (AOD(i) .EQ. 1) then
	    k=i-1
	    do j=1,k
	     !print*,'i=',i,'j=',j
	     dist=0.
	     if (AOD(j) .EQ. 1 .and. con_at(i,j) .NE. 2 )then  
	       !print*,i,j      
	       dist_xyz(1) = xyz1(i) - xyz1(j)	      
	       dist_xyz(2) = xyz2(i) - xyz2(j)	      
	       dist_xyz(3) = xyz3(i) - xyz3(j)	      
	       do l=1,3	      
		 dist_temp(l)= dist_xyz(l)**2	      
		 dist= dist + dist_temp(l)	      
	       end do
	       dist = sqrt(dist)	       
!	       print*,'DIST= ',dist
		if (H2O_mindistAOD<dist .and. dist <H2O_maxdistAOD) then
!		print*,i,j,dist,'H2Odist möglich'
		
		 G1(1,1)=xyz1(i)
		 G1(2,1)=xyz2(i)
		 G1(3,1)=xyz3(i)
		 G2(1,1)=xyz1(j)
		 G2(2,1)=xyz2(j)
		 G2(3,1)=xyz3(j)
		 nG1=0
		 nG2=0
		 nHG1=0
		 nHG2=0

		
		 if (hd(i,2).EQ.1) then
		   
		   H(1)=0
		   H(2)=0
		   H(3)=0
		   nH=0
		   		   
		   
		   do l=1,ntot
		    if (hd(l,1)==1 .and. hd(l,2)==i) then
		     nH=nH+1
		     H(nH)=l		     
		     G1(1,nH+1)=xyz1(H(nH))-xyz1(i)
		     G1(2,nH+1)=xyz2(H(nH))-xyz2(i)
		     G1(3,nH+1)=xyz3(H(nH))-xyz3(i)
		    end if
		   end do
		    nG1=nH
		    nHG1=nH
		   if (H(1)==0) then
		    print*, 'ERROR H NOT FOUND i=',i
		   end if
		 end if
		 
		 
		 
		 if (hd(j,2).EQ.1) then
		   
		   H(1)=0
		   H(2)=0
		   H(3)=0
		   nH=0
		   
		   do l=1,ntot
		    if (hd(l,1)==1 .and. hd(l,2)==j) then
		     nH=nH+1
		     H(nH)=l
		     G2(1,nH+1)=xyz1(H(nH))-xyz1(j)
		     G2(2,nH+1)=xyz2(H(nH))-xyz2(j)
		     G2(3,nH+1)=xyz3(H(nH))-xyz3(j)
		    end if
		   end do
		   nG2=nH
		   nHG2=nH
		   if (H(1)==0) then
		    print*, 'ERROR H NOT FOUND j=',j
		   end if
		 end if	 	 
	                                  
		 
		 if (ab(i,1).EQ.1) then
		  if (ab(i,3) .NE. 0) then
                   
                   nB=1
                   B223(1)=ab(i,2)
                   B223(2)=0
                   B223(3)=0
                   
                   do l=1,ntot
		    if (con_at(i,l)==2 .and. l .NE. B223(1)) then
		     nB=nB+1
		     B223(2)=l		     
		    end if                                
                    if (con_at(B223(1),l)==2 .and. l .NE. i) then ! EVENTUELL UMGEHEN
                     B223(3)=l		    
                    end if                  
                   end do
                   
!                   print*,'i=',i,'nB=',nB,'B223=',B223 !erst raus wenn getestet mit H und 2 fach gebunden
		   		   
		   if (nB==1) then
		   
		    do q=1,3
	             E(q,1)=G1(q,1)       
	            end do
		   	
		    E(1,2)=xyz1(B223(1))-E(1,1)
                    E(2,2)=xyz2(B223(1))-E(2,1)
                    E(3,2)=xyz3(B223(1))-E(3,1)
	            E(1,3)=xyz1(B223(3))-E(1,1)
                    E(2,3)=xyz2(B223(3))-E(2,1)
                    E(3,3)=xyz3(B223(3))-E(3,1)
	      
	            NVE(1)=E(2,2)*E(3,3)-E(3,2)*E(2,3)
	            NVE(2)=E(3,2)*E(1,3)-E(1,2)*E(3,3)
	            NVE(3)=E(1,2)*E(2,3)-E(2,2)*E(1,3)
	            
	            NVElen=sqrt(NVE(1)**2+NVE(2)**2+NVE(3)**2)
	       
	            do q=1,3
	             NVE(q)=NVE(q)/NVElen
	            end do	
 

                    TEMPin(1)=xyz1(B223(1))
                    TEMPin(2)=xyz2(B223(1))
                    TEMPin(3)=xyz3(B223(1))
                    
   	            alpha=pi/180*130  !120 oder 130 Grad???
                
                    call ROTATEV(NVE,E(:,1),TEMPin,TEMPout,alpha)		    
!		    Print*,i,' sp2=',TEMPout

                    G1(1,nG1+2)=TEMPout(1)-G1(1,1)
                    G1(2,nG1+2)=TEMPout(2)-G1(2,1)
                    G1(3,nG1+2)=TEMPout(3)-G1(3,1)
!                    print*,nG1,G1(:,nG1+2)
                    
		    call ROTATEV(NVE,E(:,1),TEMPin,TEMPout,-alpha)
!		    Print*,i,' sp2=',TEMPout
		    G1(1,nG1+3)=TEMPout(1)-G1(1,1)
		    G1(2,nG1+3)=TEMPout(2)-G1(2,1)
		    G1(3,nG1+3)=TEMPout(3)-G1(3,1)
!		    print*,i,nG1
		    nG1=nG1+2
!		    print*,nG1
		    
		   end if
		   
		   if (nB==2) then
                     
                     TEMPin(1)=xyz1(B223(1))-G1(1,1)
                     TEMPin(2)=xyz2(B223(1))-G1(2,1)
                     TEMPin(3)=xyz3(B223(1))-G1(3,1)
                     
                     TEMPinlen=sqrt(TEMPin(1)**2+TEMPin(2)**2+TEMPin(3)**2)
                     
                     do q=1,3
                      TEMPin(q)=TEMPin(q)/TEMPinlen
                     end do
                                        
                                                                                                    
                     TEMP1in(1)=G1(1,1)
                     TEMP1in(2)=G1(2,1)
                     TEMP1in(3)=G1(3,1)
                     
                     TEMP2in(1)=xyz1(B223(2))              
                     TEMP2in(2)=xyz2(B223(2))
                     TEMP2in(3)=xyz3(B223(2))
                     
                     alpha=pi
                     
                     call ROTATEV(TEMPin,TEMP1in,TEMP2in,TEMPout,alpha)
                     
                     
                     G1(1,nG1+2)=TEMPout(1)-G1(1,1)
                     G1(2,nG1+2)=TEMPout(2)-G1(2,1)
                     G1(3,nG1+2)=TEMPout(3)-G1(3,1)
                     
!                     print*,TEMPout
                     nG1=nG1+1
                   end if
		     
		  end if
		  

		  if (ab(i,3) == 0) then
		  
		   nB=1
                   B2223(1)=ab(i,2)
                   B2223(2)=0
                   B2223(3)=0
                   B2223(4)=0
                   
                   do l=1,ntot
		    if (con_at(i,l)==2 .and. l .NE. B2223(1)) then
		     nB=nB+1
		     B2223(nB)=l		     
		    end if 
		   end do 
		   
		   if (nB==1)then !BIS JETZT NICHT GETESTET
		    do l=1,ntot
                     if (con_at(B2223(1),l)==2 .and. l .NE. i) then 
                      B2223(4)=l	
                     end if                  
                    end do
                   end if
		   
 
		   
		  if (nB==1)then  !Noch nicht Getestet!!!
		   		   
		   Din(1)=G1(1,1)
		   Din(2)=G1(2,1)
		   Din(3)=G1(3,1)
		   
		   B2in(1)=xyz1(B2223(1))
		   B2in(2)=xyz2(B2223(1))
		   B2in(3)=xyz3(B2223(1))
		   
		   B3in(1)=xyz1(B2223(4))
		   B3in(2)=xyz2(B2223(4))
		   B3in(3)=xyz3(B2223(4))
		   
		   call BUILDSP3B1(Din,B2in,B3in,Bout)
		   
		   do q=1,3
		    G1(1,nG1+q+1)=Bout(1,q)-G1(1,1)
		    G1(2,nG1+q+1)=Bout(2,q)-G1(2,1)
		    G1(3,nG1+q+1)=Bout(3,q)-G1(3,1)
		   end do
		   
		   G1=nG1+3
		   
		  end if
		  
		  if (nB==2)then
!		   print*,'B2',i,nG1
		   
		   TEMPin(1)=xyz1(B2223(1))-G1(1,1)
		   TEMPin(2)=xyz2(B2223(1))-G1(2,1)
		   TEMPin(3)=xyz3(B2223(1))-G1(3,1)
		   
                   TEMP1in(1)=xyz1(B2223(2)) 
                   TEMP1in(2)=xyz2(B2223(2))
                   TEMP1in(3)=xyz3(B2223(2))
                   
                    
                   TEMPinlen=sqrt(TEMPin(1)**2+TEMPin(2)**2+TEMPin(3)**2) 
                    
                   do q=1,3 
                    TEMPin(q)=TEMPin(q)/TEMPinlen 
                   end do 
                              
                   
                   angle=pi/180*120
                   
                   call ROTATEV(TEMPin,G1(:,1),TEMP1in,TEMPout,angle)
                   
                   
                   G1(1,nG1+2)=TEMPout(1)-G1(1,1)
                   G1(2,nG1+2)=TEMPout(2)-G1(2,1)
                   G1(3,nG1+2)=TEMPout(3)-G1(3,1)                   
                   
                   call ROTATEV(TEMPin,G1(:,1),TEMPout,TEMPout,angle)

                   
                   G1(1,nG1+3)=TEMPout(1)-G1(1,1)
                   G1(2,nG1+3)=TEMPout(2)-G1(2,1)
                   G1(3,nG1+3)=TEMPout(3)-G1(3,1)
                   
                   nG1=nG1+2
	   		   
		  end if
		  
		  if (nB==3)then  !Noch nicht Getestet!!!
		  
		  
		   TEMPin(1)=xyz1(B2223(1))-G1(1,1)
		   TEMPin(2)=xyz2(B2223(1))-G1(2,1)
		   TEMPin(3)=xyz3(B2223(1))-G1(3,1)
		   
                   TEMP1in(1)=xyz1(B2223(2)) 
                   TEMP1in(2)=xyz2(B2223(2))
                   TEMP1in(3)=xyz3(B2223(2))
                   
                   TEMP2in(1)=xyz1(B2223(3)) 
                   TEMP2in(2)=xyz2(B2223(3))
                   TEMP2in(3)=xyz3(B2223(3))
                   
                    
                   TEMPinlen=sqrt(TEMPin(1)**2+TEMPin(2)**2+TEMPin(3)**2) 
                    
                   do q=1,3 
                    TEMPin(q)=TEMPin(q)/TEMPinlen 
                   end do 
                              
                   
                   angle=pi/180*120
                   
                   call ROTATEV(TEMPin,G1(:,1),TEMP1in,TEMPout,angle)
                   
                   
                   G1(1,nG1+2)=TEMPout(1)-G1(1,1)
                   G1(2,nG1+2)=TEMPout(2)-G1(2,1)
                   G1(3,nG1+2)=TEMPout(3)-G1(3,1)                   
                   
                   Dist=sqrt((TEMPout(1)-TEMP2in(1))**2+(TEMPout(2)-TEMP2in(2))**2+(TEMPout(3)-TEMP2in(3))**2)
!                   print*,'NOCH NICHT GETESTET' 
                   if (Dist<0.5)then                    
                    call ROTATEV(TEMPin,G1(:,1),TEMPout,TEMPout,angle)

                   
                    G1(1,nG1+2)=TEMPout(1)-G1(1,1)
                    G1(2,nG1+2)=TEMPout(2)-G1(2,1)
                    G1(3,nG1+2)=TEMPout(3)-G1(3,1)		  
		   end if
		   
		   G1=nG1+1
		   
		  end if
		  		  
!		  print*,'SP3 = ',i,'B2223=',B2223, 'nB=',nB 
	  
		  end if


		 end if
		 
		 
		 
		 !JETZT J EVENTUELL FUNKTIONEN ODER EIN VEKTOR 
		 
		 !HIER GEHTS WEITER nG2!!!!! IRGENDWAS LÄUFT FALSCH. AUSGABE KONTROLLIEREN!!!!!!
		 
!		 
		 if (ab(j,1).EQ.1) then
		  if (ab(j,3) .NE. 0) then
                   
                   nB=1
                   B223(1)=ab(j,2)
                   B223(2)=0
                   B223(3)=0
                   
                   do l=1,ntot
		    if (con_at(j,l)==2 .and. l .NE. B223(1)) then
		     nB=nB+1
		     B223(2)=l		     
		    end if                                
                    if (con_at(B223(1),l)==2 .and. l .NE. j) then ! EVENTUELL UMGEHEN
                     B223(3)=l		    
                    end if                  
                   end do
                   
!                   print*,'j=',j,'nB=',nB,'B223=',B223 !erst raus wenn getestet mit H und 2 fach gebunden
		   		   
		   if (nB==1) then
		   
		    do q=1,3
	             E(q,1)=G2(q,1)       
	            end do
		   	
		    E(1,2)=xyz1(B223(1))-E(1,1)
                    E(2,2)=xyz2(B223(1))-E(2,1)
                    E(3,2)=xyz3(B223(1))-E(3,1)
	            E(1,3)=xyz1(B223(3))-E(1,1)
                    E(2,3)=xyz2(B223(3))-E(2,1)
                    E(3,3)=xyz3(B223(3))-E(3,1)
	      
	            NVE(1)=E(2,2)*E(3,3)-E(3,2)*E(2,3)
	            NVE(2)=E(3,2)*E(1,3)-E(1,2)*E(3,3)
	            NVE(3)=E(1,2)*E(2,3)-E(2,2)*E(1,3)
	            
	            NVElen=sqrt(NVE(1)**2+NVE(2)**2+NVE(3)**2)
	       
	            do q=1,3
	             NVE(q)=NVE(q)/NVElen
	            end do	
 

                    TEMPin(1)=xyz1(B223(1))
                    TEMPin(2)=xyz2(B223(1))
                    TEMPin(3)=xyz3(B223(1))
                    
   	            alpha=pi/180*130  !120 oder 130 Grad???
                
                    call ROTATEV(NVE,E(:,1),TEMPin,TEMPout,alpha)		    
!		    Print*,j,' sp2=',TEMPout
                    G2(1,nG2+2)=TEMPout(1)-G2(1,1)
                    G2(2,nG2+2)=TEMPout(2)-G2(2,1)
                    G2(3,nG2+2)=TEMPout(3)-G2(3,1)
                    	    
		    call ROTATEV(NVE,E(:,1),TEMPin,TEMPout,-alpha)
!		    Print*,j,' sp2=',TEMPout
		    G2(1,nG2+3)=TEMPout(1)-G2(1,1)
		    G2(2,nG2+3)=TEMPout(2)-G2(2,1)
		    G2(3,nG2+3)=TEMPout(3)-G2(3,1)
		    nG2=nG2+2
		   end if
		   
		   if (nB==2) then
                     
                     TEMPin(1)=xyz1(B223(1))-G2(1,1)
                     TEMPin(2)=xyz2(B223(1))-G2(2,1)
                     TEMPin(3)=xyz3(B223(1))-G2(3,1)
                     
                     TEMPinlen=sqrt(TEMPin(1)**2+TEMPin(2)**2+TEMPin(3)**2)
                     
                     do q=1,3
                      TEMPin(q)=TEMPin(q)/TEMPinlen
                     end do
                                        
                                                                                                    
                     TEMP1in(1)=G2(1,1)
                     TEMP1in(2)=G2(2,1)
                     TEMP1in(3)=G2(3,1)
                     
                     TEMP2in(1)=xyz1(B223(2))              
                     TEMP2in(2)=xyz2(B223(2))
                     TEMP2in(3)=xyz3(B223(2))
                     
                     alpha=pi
                     
                     call ROTATEV(TEMPin,TEMP1in,TEMP2in,TEMPout,alpha)
                     
                     
                     G2(1,nG2+2)=TEMPout(1)-G2(1,1)
                     G2(2,nG2+2)=TEMPout(2)-G2(2,1)
                     G2(3,nG2+2)=TEMPout(3)-G2(3,1)
                     nG2=nG2+1
!                     print*,TEMPout
                     
                   end if
!		  
		  end if
		  

		  if (ab(j,3) == 0) then
		  
		   nB=1
                   B2223(1)=ab(j,2)
                   B2223(2)=0
                   B2223(3)=0
                   B2223(4)=0
                   
                   do l=1,ntot
		    if (con_at(j,l)==2 .and. l .NE. B2223(1)) then
		     nB=nB+1
		     B2223(nB)=l		     
		    end if 
		   end do 
		   
		   if (nB==1)then !BIS JETZT NICHT GETESTET
		    do l=1,ntot
                     if (con_at(B2223(1),l)==2 .and. l .NE. j) then 
                      B2223(4)=l	
                     end if                  
                    end do
                   end if
		  
		  if (nB==1)then  !Noch nicht Getestet!!!
		   		   
		   Din(1)=G2(1,1)
		   Din(2)=G2(2,1)
		   Din(3)=G2(3,1)
		   
		   B2in(1)=xyz1(B2223(1))
		   B2in(2)=xyz2(B2223(1))
		   B2in(3)=xyz3(B2223(1))
		   
		   B3in(1)=xyz1(B2223(4))
		   B3in(2)=xyz2(B2223(4))
		   B3in(3)=xyz3(B2223(4))
		   
		   call BUILDSP3B1(Din,B2in,B3in,Bout)
		   
		   do q=1,3
		    G2(1,nG2+q+1)=Bout(1,q)-G2(1,1)
		    G2(2,nG2+q+1)=Bout(2,q)-G2(2,1)
		    G2(3,nG2+q+1)=Bout(3,q)-G2(3,1)
		   end do
		    nG2=nG2+3
		  end if
		  
		  if (nB==2)then
		   
		   
		   TEMPin(1)=xyz1(B2223(1))-G2(1,1)
		   TEMPin(2)=xyz2(B2223(1))-G2(2,1)
		   TEMPin(3)=xyz3(B2223(1))-G2(3,1)
		   
                   TEMP1in(1)=xyz1(B2223(2)) 
                   TEMP1in(2)=xyz2(B2223(2))
                   TEMP1in(3)=xyz3(B2223(2))
                   
                    
                   TEMPinlen=sqrt(TEMPin(1)**2+TEMPin(2)**2+TEMPin(3)**2) 
                    
                   do q=1,3 
                    TEMPin(q)=TEMPin(q)/TEMPinlen 
                   end do 
                              
                   
                   angle=pi/180*120
                   
                   call ROTATEV(TEMPin,G2(:,1),TEMP1in,TEMPout,angle)
                   
                   
                   G2(1,nG2+2)=TEMPout(1)-G2(1,1)
                   G2(2,nG2+2)=TEMPout(2)-G2(2,1)
                   G2(3,nG2+2)=TEMPout(3)-G2(3,1)                   
                   
                   call ROTATEV(TEMPin,G2(:,1),TEMPout,TEMPout,angle)

                   
                   G2(1,nG2+3)=TEMPout(1)-G2(1,1)
                   G2(2,nG2+3)=TEMPout(2)-G2(2,1)
                   G2(3,nG2+3)=TEMPout(3)-G2(3,1)
                   nG2=nG2+2		   		   
		  end if
		  
		  if (nB==3)then  !Noch nicht Getestet!!!
		  
		  
		   TEMPin(1)=xyz1(B2223(1))-G2(1,1)
		   TEMPin(2)=xyz2(B2223(1))-G2(2,1)
		   TEMPin(3)=xyz3(B2223(1))-G2(3,1)
		   
                   TEMP1in(1)=xyz1(B2223(2)) 
                   TEMP1in(2)=xyz2(B2223(2))
                   TEMP1in(3)=xyz3(B2223(2))
                   
                   TEMP2in(1)=xyz1(B2223(3)) 
                   TEMP2in(2)=xyz2(B2223(3))
                   TEMP2in(3)=xyz3(B2223(3))
                   
                    
                   TEMPinlen=sqrt(TEMPin(1)**2+TEMPin(2)**2+TEMPin(3)**2) 
                    
                   do q=1,3 
                    TEMPin(q)=TEMPin(q)/TEMPinlen 
                   end do 
                              
                   
                   angle=pi/180*120
                   
                   call ROTATEV(TEMPin,G2(:,1),TEMP1in,TEMPout,angle)
                   
                   
                   G2(1,nG2+2)=TEMPout(1)-G2(1,1)
                   G2(2,nG2+2)=TEMPout(2)-G2(2,1)
                   G2(3,nG2+2)=TEMPout(3)-G2(3,1)                   
                   
                   Dist=sqrt((TEMPout(1)-TEMP2in(1))**2+(TEMPout(2)-TEMP2in(2))**2+(TEMPout(3)-TEMP2in(3))**2)
!                   print*,'NOCH NICHT GETESTET' 
                   if (Dist<0.5)then                    
                    call ROTATEV(TEMPin,G2(:,1),TEMPout,TEMPout,angle)

                   
                    G2(1,nG2+2)=TEMPout(1)-G2(1,1)
                    G2(2,nG2+2)=TEMPout(2)-G2(2,1)
                    G2(3,nG2+2)=TEMPout(3)-G2(3,1)		  
		   end if
		    nG2=nG2+1		  
		  end if
		  		  
!		  print*,'SP3 = ',j,'B2223=',B2223, 'nB=',nB 
	  
		  end if


		 end if
 
!             if (i==16) then 
!              do q=2,nG1+1
!                TEMP1in(1)=G1(1,1)+G1(1,q)
!                TEMP1in(2)=G1(2,1)+G1(2,q)
!                TEMP1in(3)=G1(3,1)+G1(3,q)
!!                 print*,nG1,q
!                Print*,'i',i,TEMP1in
!               end do
!                
!               do q=2,nG2+1
!                TEMP1in(1)=G2(1,1)+G2(1,q)
!                TEMP1in(2)=G2(2,1)+G2(2,q)
!                TEMP1in(3)=G2(3,1)+G2(3,q)
!!                 print*,nG2,q
!                Print*,'j',j,TEMP1in
!               end do
!!	       end if
!
!FINDEN DES KREISES ZWISCHEN POLAR UND POLAR
!	         
	           !r=sqrt((2.8**2)-((dist/2)**2)) !Ideale Abstabd Polar-H2O=2.8.POLAR-POLAR Distanzanhänige FUNKTION?
                   
                   !M(1)=(xyz1(i)+xyz1(j))/2
	           !M(2)=(xyz2(i)+xyz2(j))/2
	           !M(3)=(xyz3(i)+xyz3(j))/2
                 
	           !BAUSTELLE: ABHÄNIGKEIT VON DER ATOM-ART DER POLAREN ATOME
	           !BAUSTELLE: ABHÄNIGKEIT VON DER ATOM-ART DER POLAREN ATOME
	           !BAUSTELLE: ABHÄNIGKEIT VON DER ATOM-ART DER POLAREN ATOME
	           !BAUSTELLE: ABHÄNIGKEIT VON DER ATOM-ART DER POLAREN ATOME
	           
	           
	           aa=dist
	           bb=DIST_POLAR_WATER
	           cc=DIST_POLAR_WATER
	           
	           if (atm_names(i)=='N')bb=DIST_N_WATER
                   if (atm_names(j)=='N')cc=DIST_N_WATER 	           
	           
	           r=sqrt(2*((aa**2)*(bb**2)+(bb**2)*(cc**2)+(cc**2)*(aa**2))-(aa**4+bb**4+cc**4))/(2*aa) 
	           AMlength=sqrt(bb**2-r**2) 
	           
	           do q=1,3	           
	            AABB(q)=-dist_xyz(q)
	           end do
	           
	           ABlength=sqrt(AABB(1)**2+AABB(2)**2+AABB(3)**2)
	           

	           M(1)=xyz1(i)+(AABB(1)/ABlength)*AMlength
	           M(2)=xyz2(i)+(AABB(2)/ABlength)*AMlength
	           M(3)=xyz3(i)+(AABB(3)/ABlength)*AMlength
	           !
	           !
	           	          	           
	           
	           do q=1,3
	            u(q)=-dist_xyz(q)
	           end do
	           
	           norm(1)=1
	           norm(2)=1
	           norm(3)=(-u(1)-u(2))/u(3)
	           
	           ulen=sqrt(u(1)**2+u(2)**2+u(3)**2)
	           nlen=sqrt(1+1+norm(3)**2)
	           
	           do q=1,3
	            norm(q)=norm(q)*(1/nlen)
	            un(q)=u(q)*(1/ulen)
	            W1(q)=M(q)+norm(q)*r
	           end do
	           

	           do q=2,nG1+1
	            do s=2,nG2+1

	            
	            do t=1,3
	             GP1(t,1)=G1(t,1)
	             GP1(t,2)=G1(t,q)
	             GP2(t,1)=G2(t,1)
	             GP2(t,2)=G2(t,s)
	            end do
                     
                     
                     
!                   print*,'GP1',GP1(:,1)
!                   print*,'GP1',GP1(:,2)
!                   print*,'GP2',GP2(:,1)
!                   print*,'GP2',GP2(:,2)



!                    if (i==37 .and. j==25) then
!                      print*,'GP1',GP1(:,1)+GP1(:,2)
!                      print*,'GP11',GP1(:,1)
!                      print*,'GP2',GP2(:,1)+GP2(:,2)
 !                     print*,'GP21',GP2(:,2)
!                    end if
!                    print*,i,j
!                    print*,'GP1',GP1(:,1)+GP1(:,2)
!                    print*,'GP2',GP2(:,1)+GP2(:,2)
                    mindistflag=.FALSE.
       	            call MINDISTMP(GP1,GP2,MP,mindistflag)  	           	           	           
 !                   print*,i,j
!Schneiden der idealen Gerade mit der Kreisebene
	          
	           if (mindistflag .eqv. .TRUE.)then
	               NVElen=0.
		       do t=1,3
	                NVElen=NVElen+un(t)*(MP(t)-M(t))  !WIE HAT DAS FUNKTIONIERT?
	               end do	          	          
	             
!	              print*,'un',un
!	              print*,'NVElen',NVElen
!	              print*,'MP',MP
!	             
	             TEMP1len=0.
	             TEMP2len=0. 
	             
	             do t=1,3
	              TEMP1in(t)=MP(t)+NVElen*un(t)
	              TEMP2in(t)=MP(t)-NVElen*un(t)
	              TEMP1len=TEMP1len+un(t)*(TEMP1in(t)-M(t))
	              TEMP2len=TEMP2len+un(t)*(TEMP2in(t)-M(t))
	              
	             end do          
                   
                   !HIER ALLES NOCHMAL GENAU ANSCHUAN
                   
                   
	              if (abs(TEMP1len) < 0.1*abs(NVElen))then
                   
	              
	               do t=1,3
	                TEMPout(t)=TEMP1in(t)
	               end do
	               
	              else if (abs(TEMP2len) < 0.1*abs(NVElen))then
	              
	               do t=1,3
	                TEMPout(t)=TEMP2in(t)
	               end do
	              else
	               print*, 'BEI DER PROJEKTION DES PUNKTES IN DIE EBENE IST EIN FEHLER UNTERLAUFEN'	               	           
	           
	              end if
                   
                   
	 	     do t=1,3
	 	      Vec(t)=TEMPout(t)-M(t)
	             end do
                   
	             Veclen=sqrt(Vec(1)**2+Vec(2)**2+Vec(3)**2)
	              
                     do t=1,3
	              TEMPout(t)=M(t)+Vec(t)/Veclen*r
                     end do	           

                     GP1len=sqrt(GP1(1,2)**2+GP1(2,2)**2+GP1(3,2)**2)
                     GP2len=sqrt(GP2(1,2)**2+GP2(2,2)**2+GP2(3,2)**2)
                     
                     
                     call  ANGLEP1P2P3(GP1(:,1)+GP1(:,2)/GP1len,GP1(:,1),TEMPout,angle1)
                     call  ANGLEP1P2P3(GP2(:,1)+GP2(:,2)/GP2len,GP2(:,1),TEMPout,angle2)

                                    
                     if(angle1<pi/180*70 .or.angle1>pi/180*110) then
                      if(angle2<pi/180*70 .or.angle2>pi/180*110) then
                                                                                       
                      
                      
                      
                      
                      WDxyzdisti(1)=GP1(1,1)+GP1(1,2)/GP1len-TEMPout(1)
                      WDxyzdisti(2)=GP1(2,1)+GP1(2,2)/GP1len-TEMPout(2)
                      WDxyzdisti(3)=GP1(3,1)+GP1(3,2)/GP1len-TEMPout(3)
                      WDdisti=sqrt(WDxyzdisti(1)**2+WDxyzdisti(2)**2+WDxyzdisti(3)**2)
                      WDxyzdistj(1)=GP2(1,1)+GP2(1,2)/GP2len-TEMPout(1)
                      WDxyzdistj(2)=GP2(2,1)+GP2(2,2)/GP2len-TEMPout(2)
                      WDxyzdistj(3)=GP2(3,1)+GP2(3,2)/GP2len-TEMPout(3)
                      WDdistj=sqrt(WDxyzdistj(1)**2+WDxyzdistj(2)**2+WDxyzdistj(3)**2)

                      
!                     if (i==37 .and. j==25) then
!                      print*,'WDdisti',WDdisti 
!                      print*,'WDdistj',WDdistj 
!                      print*,TEMPout
!                      print*,GP2(:,1)+GP2(:,2)/GP2len
!                     end if
!                     
                      if (WDdisti<2.25 .AND. WDdistj<2.25)then
                       !KANN EVENTUELL UMGANGEN WERDEN
                       
                       B1xyzdisti(1)=0.
                       B1xyzdisti(2)=0.
                       B1xyzdisti(3)=0.
                       B1disti=0.      
                       B1xyzdistj(1)=0.
                       B1xyzdistj(2)=0.
                       B1xyzdistj(3)=0.
                       B1distj=0.      
                       
                      
                       do t=1,ntot                                             
                         if (con_at(i,t)==2 .and. hd(t,1) .NE. 1) then                        
                          B1xyzdisti(1)=xyz1(t)-TEMPout(1)
                          B1xyzdisti(2)=xyz2(t)-TEMPout(2)
                          B1xyzdisti(3)=xyz3(t)-TEMPout(3)
                         end if                        
                          if (con_at(j,t)==2 .and. hd(t,1) .NE. 1) then
                           B1xyzdistj(1)=xyz1(t)-TEMPout(1)
                           B1xyzdistj(2)=xyz2(t)-TEMPout(2)
                           B1xyzdistj(3)=xyz3(t)-TEMPout(3)
                          end if                        
                        end do
                      
                        B1disti=sqrt(B1xyzdisti(1)**2+B1xyzdisti(2)**2+B1xyzdisti(3)**2)
                        B1distj=sqrt(B1xyzdistj(1)**2+B1xyzdistj(2)**2+B1xyzdistj(3)**2)
                        
!                        if (i==37 .and. j==25) then
!                         print*,'B1disti',B1disti 
!                         print*,'B1distj',B1distj 
!                        end if
!                        
                        if (B1disti>2.7 .and. B1distj>2.7) then !GENAU 2.657 für abweichung =+-70°
!	                 PRINT*,i,j,'Wfinal=',TEMPout
	             	 
	             	 H20twoflag=.FALSE.
	             	 
	             	 ehb1=-99.9
	             	 ehb2=-99.9
	             	 ehb3=-99.9
	             	 ehb4=-99.9
	             	 
	             	 
	             	 ! NOCHMAL ÜBERPRÜFEN OB ES DIE BESTE LÖSUNG IST DIE POLAEN ATOME ALS AUSGANGSPUNKT 
	             	 ! DER  BERECHNUNGEN ZU NUTZEN
	                 do t=1,3
	                  Ox(t)=TEMPout(t)
	                  Vec1(t)=Ox(t)-GP1(t,1)
	                  Vec2(t)=Ox(t)-GP2(t,1)
	                  Vec3(t)=M(t)-Ox(t)
	                 end do
	                  
	                 Vec3len=sqrt(Vec3(1)**2+Vec3(2)**2+Vec3(3)**2)
	                 
	                 
	                 
                         NVE(1)=Vec1(2)*Vec2(3)-Vec1(3)*Vec2(2)
                         NVE(2)=Vec1(3)*Vec2(1)-Vec1(1)*Vec2(3)
                         NVE(3)=Vec1(1)*Vec2(2)-Vec1(2)*Vec2(1)
                         NVElen=sqrt(NVE(1)**2+NVE(2)**2+NVE(3)**2)
	                 
	                 
	                 
	                 do t=1,3
	                  NVE(t)=NVE(t)/NVElen
	                  Hy2(t)=Ox(t)+(Vec3(t)/Vec3len)*0.9584!ANSTAND H-O in Wasser
	                 end do       
	                 
	                 angle=pi/180*52.225
	                 
	                 Call ROTATEV(NVE,Ox,Hy2,Hy1,angle)
	                 
	                 angle=pi/180*(-52.225)
	                 Call ROTATEV(NVE,Ox,Hy2,Hy2,angle) 
	               	                 
	                 
	                  
	                  
	                  A1(1)= xyz1(ab(i,2))
	                  A1(2)= xyz2(ab(i,2))
	                  A1(3)= xyz3(ab(i,2))
	                  
	                  
	                  if (ab(i,3) .NE. 0) then
                           asp_flag=.TRUE.
                           A2(1)=xyz1(ab(i,3))
                           A2(2)=xyz2(ab(i,3))
                           A2(3)=xyz3(ab(i,3))
                          else
                           asp_flag=.FALSE.
                           A2=(/0,0,0/)
                          end if	          
                          
                          
	                  call calc_single_hbd_exp(Hy1,Ox,GP1(:,1),A1,A2,asp_flag,ehb1)

	                  if(ehb1>H20_build_ene_min) then 
	                    call calc_single_hbd_exp(Hy2,Ox,GP1(:,1),A1,A2,asp_flag,ehb1)
	                  end if
	                  
	                  
	                  A1(1)= xyz1(ab(j,2))
	                  A1(2)= xyz2(ab(j,2))
	                  A1(3)= xyz3(ab(j,2))
	                  
	                  
	                  if (ab(j,3) .NE. 0) then
                           asp_flag=.TRUE.
                           A2(1)=xyz1(ab(j,3))
                           A2(2)=xyz2(ab(j,3))
                           A2(3)=xyz3(ab(j,3))
                          else
                           asp_flag=.FALSE.
                           A2=(/0,0,0/)
                          end if	          
                          
                          
	                  call calc_single_hbd_exp(Hy2,Ox,GP2(:,1),A1,A2,asp_flag,ehb2) 
!
	                  if(ehb2>H20_build_ene_min) then
	                    call calc_single_hbd_exp(Hy1,Ox,GP2(:,1),A1,A2,asp_flag,ehb2)
	                  end if
	                  
	                 if (q-1<=nHG1 .and. s-1<=nHG2) then
!	                  Print*,i,j,'DONOR-DONOR'
	                  do t=1,3
	                   Hy1(t)=Ox(t)+Ox(t)-Hy1(t)
	                   Hy2(t)=Ox(t)+Ox(t)-Hy2(t)	                   
	                  end do
	                  angle=pi/180*90
	                  Call ROTATEV(Vec3/Vec3len,Ox,Hy1,Hy1,angle)
	                  Call ROTATEV(Vec3/Vec3len,Ox,Hy2,Hy2,angle)
	                 	                  
	                  	                  	                  
	                  call calc_single_hbd_exp((GP1(:,1)+GP1(:,2)/GP1len*0.9584),GP1(:,1),Ox,Hy2,(/0.,0.,0./),.FALSE.,ehb1) 
	                  call calc_single_hbd_exp((GP2(:,1)+GP2(:,2)/GP2len*0.9584),GP2(:,1),Ox,Hy2,(/0.,0.,0./),.FALSE.,ehb2)                
	                  	                  
	                   	                  	                  
	                 end if
	                 
	                  !ES WIRD UM FALSCHEN VEKTOR GEDREHT
	                 if (q-1<=nHG1 .and. s-1>nHG2) then 
!	                  Print*,i,j,'DONOR-AKZEPTOR'                                                                          
         
	                  H20twoflag=.TRUE.
	                  A2=(/0,0,0/)
	                   
	                  A1(1)= xyz1(ab(j,2))
	                  A1(2)= xyz2(ab(j,2))
	                  A1(3)= xyz3(ab(j,2))
	                  
	                  if (ab(j,3) .NE. 0) then
	                   asp_flag=.TRUE.
	                   A2(1)=xyz1(ab(j,3))
	                   A2(2)=xyz2(ab(j,3))
	                   A2(3)=xyz3(ab(j,3))
	                  else
	                   asp_flag=.FALSE.
	                  end if
	                  
	                  do t=1,3
	                   Vec1(t)=GP2(t,1)-Hy1(t)
	                   Vec2(t)=GP2(t,1)-Hy2(t)
	                  end do
	                  
	                  Vec1len=sqrt(Vec1(1)**2+Vec1(2)**2+Vec1(3)**2)
	                  Vec2len=sqrt(Vec2(1)**2+Vec2(2)**2+Vec2(3)**2)
	                  
	                  
	                  if (Vec1len<Vec2len)then
	                  
	                   do t=1,3
	                    rotvec(t)=Ox(t)-Hy1(t)
	                   end do
	                   rotveclen=sqrt(rotvec(1)**2+rotvec(2)**2+rotvec(3)**2)
	                  
	                   angle=pi/180*120
	                   Call ROTATEV(rotvec/rotveclen,Ox,Hy2,Hy3,angle)
	                   
	                   angle=pi/180*(-120)
	                   Call ROTATEV(rotvec/rotveclen,Ox,Hy2,Hy2,angle)
	                   
	                   !AKZEPTOR WASSER
	                   call calc_single_hbd_exp(Hy1,Ox,GP2(:,1),A1,A2,asp_flag,ehb2)
	                   ehb4=ehb2 
	                    
	                  else if (Vec1len>Vec2len) then
	                  
	                   do t=1,3
	                    rotvec(t)=Ox(t)-Hy2(t)
	                   end do
	                   rotveclen=sqrt(rotvec(1)**2+rotvec(2)**2+rotvec(3)**2)	                  
	                  
	                   angle=pi/180*120
	                   Call ROTATEV(rotvec/rotveclen,Ox,Hy1,Hy3,angle)
	                   
	                   angle=pi/180*(-120)
	                   Call ROTATEV(rotvec/rotveclen,Ox,Hy1,Hy1,angle)
	                   
	                   !AKZEPTOR WASSER
	                   call calc_single_hbd_exp(Hy2,Ox,GP2(:,1),A1,A2,asp_flag,ehb2)
                           ehb4=ehb2
	                   
	                  	                  	                  	                  
	                  else 
	                   Print*,'ERROR 4'
	                  end if
	                  
	                  !DONOR WASSER
	                  call calc_single_hbd_exp((GP1(:,1)+GP1(:,2)/GP1len*0.9584),GP1(:,1),Ox,Hy2,(/0.,0.,0./),.FALSE.,ehb1)
	                  call calc_single_hbd_exp((GP1(:,1)+GP1(:,2)/GP1len*0.9584),GP1(:,1),Ox,Hy3,(/0.,0.,0./),.FALSE.,ehb3)	                  
	                  
	                  
	                 end if
	                 
	                 
	                 
	                 !ES WIRD UM FALSCHEN VEKTOR GEDREHT
	                 if (q-1>nHG1 .and. s-1<=nHG2) then
!	                  Print*,i,j,'AKZEPTOR-DONOR'
	                  H20twoflag=.TRUE.
	                  
                          A2=(/0,0,0/)
                           
                          A1(1)= xyz1(ab(i,2))
                          A1(2)= xyz2(ab(i,2))
                          A1(3)= xyz3(ab(i,2))
                          
                          if (ab(i,3) .NE. 0) then
                           asp_flag=.TRUE.
                           A2(1)=xyz1(ab(i,3))
                           A2(2)=xyz2(ab(i,3))
                           A2(3)=xyz3(ab(i,3))
                          else
                           asp_flag=.FALSE.
                          end if	                  
	                  
	                  do t=1,3
	                   Vec1(t)=GP1(t,1)-Hy1(t)
	                   Vec2(t)=GP1(t,1)-Hy2(t)
	                  end do
	                  
	                  Vec1len=sqrt(Vec1(1)**2+Vec1(2)**2+Vec1(3)**2)
	                  Vec2len=sqrt(Vec2(1)**2+Vec2(2)**2+Vec2(3)**2)
	                  
	                  if (Vec1len<Vec2len)then
	                  
	                   do t=1,3
	                    rotvec(t)=Ox(t)-Hy1(t)
	                   end do
	                   rotveclen=sqrt(rotvec(1)**2+rotvec(2)**2+rotvec(3)**2)
	                  
	                   angle=pi/180*120
	                   Call ROTATEV(rotvec/rotveclen,Ox,Hy2,Hy3,angle)
	                   
	                   angle=pi/180*(-120)
	                   Call ROTATEV(rotvec/rotveclen,Ox,Hy2,Hy2,angle)
	                   
	                   !AKZEPTOR WASSER
	                   call calc_single_hbd_exp(Hy1,Ox,GP1(:,1),A1,A2,asp_flag,ehb2)
	                   ehb4=ehb2 
	                  
	                  else if (Vec1len>Vec2len) then
	                   
	                   do t=1,3
	                    rotvec(t)=Ox(t)-Hy2(t)
	                   end do
	                   rotveclen=sqrt(rotvec(1)**2+rotvec(2)**2+rotvec(3)**2)	                   
	                   
	                   angle=pi/180*120
	                   Call ROTATEV(rotvec/rotveclen,Ox,Hy1,Hy3,angle)
	                   
	                   angle=pi/180*(-120)
	                   Call ROTATEV(rotvec/rotveclen,Ox,Hy1,Hy1,angle)
	                   
	                   !AKZEPTOR WASSER
	                   call calc_single_hbd_exp(Hy2,Ox,GP1(:,1),A1,A2,asp_flag,ehb2)
                           ehb4=ehb2	                   
	                   
	                  	                  	                
	                  else 
	                   Print*,'ERROR 5'
	                  end if

	                  !DONOR WASSER
	                  call calc_single_hbd_exp((GP2(:,1)+GP2(:,2)/GP2len*0.9584),GP2(:,1),Ox,Hy2,(/0.,0.,0./),.FALSE.,ehb1)
	                  call calc_single_hbd_exp((GP2(:,1)+GP2(:,2)/GP2len*0.9584),GP2(:,1),Ox,Hy3,(/0.,0.,0./),.FALSE.,ehb3)
!
	                 end if
	                 
	                 
	                 
	                 
	                   !LÖSEN!?
	                   !Wenn keine if schleife akzeptor-akzeptur :)
	                 
	                 
	               
                         
                         
	                  CALL GIVE_HYD_AS_NAME(aname1,aname1out)

	                  
	                  set3=(/1,1,1,1/)	          	  
	          	   
	                  
	                  if (set1(1) .NE. 1) then	                 
	                   set4(1)=set1(1)
	                   set4(2)=set1(2)
	                   set4(3)=set1(3)
	                   set4(4)=set1(4)
	                  else if (set1(1) == 1) then
	                   set4(1)=setmc(1)
	                   set4(2)=setmc(2)
	                   set4(3)=setmc(3)
	                   set4(4)=setmc(4) 
	                   na1=mol(setmc(1))%res(setmc(2))%res_aas(setmc(3))%aa_rots(setmc(4))%rot_nat	
	                  end if
	                  
!	                
                           
!                           997 FORMAT(f9.5,1x,f9.5,1x,f9.5,1x,f9.5)
                           

                          if (ehb1<H20_build_ene_min .and. ehb2<H20_build_ene_min) then
                            numWATERlocal=numWATERlocal+1
!                            print*,set1,set2,setmc
!                            print*,set4
!                            print*,'in',set4,'num',numWATERlocal,'OX',OX,'H',Hy2
                           if (numWATERlocal < numwaterlocal_max) then
                            temp_DATAsetslocal(1,numWATERlocal)=set4(1)
                            temp_DATAsetslocal(2,numWATERlocal)=set4(2)
                            temp_DATAsetslocal(3,numWATERlocal)=set4(3)
                            temp_DATAsetslocal(4,numWATERlocal)=set4(4)
                              
                            temp_DATAnamelocal(numWATERlocal)=aname1out
                             
                            temp_DATAcordlocal(1,numWATERlocal)=Ox(1)
                            temp_DATAcordlocal(2,numWATERlocal)=Ox(2)
                            temp_DATAcordlocal(3,numWATERlocal)=Ox(3)
                            temp_DATAcordlocal(4,numWATERlocal)=Hy1(1)
                            temp_DATAcordlocal(5,numWATERlocal)=Hy1(2)
                            temp_DATAcordlocal(6,numWATERlocal)=Hy1(3)
                            temp_DATAcordlocal(7,numWATERlocal)=Hy2(1)
                            temp_DATAcordlocal(8,numWATERlocal)=Hy2(2)
                            temp_DATAcordlocal(9,numWATERlocal)=Hy2(3)
                           else
!                           
                            PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>    '
            	            PRINT*, '>> Too many WATERMOLECULES  > numwaterlocal_max           '
	                    PRINT*, '>> ', numWATERlocal
	                    PRINT*, '>> Increase numwaterlocal_max in xxx                     '
	                    PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>    '
		            stop
!
		            end if

                           
!	                   CALL EXPAND_SINGLE_AA(set4,set3,aname1out)
!	                   
!	                   mol(set3(1))%res(set3(2))%res_aas(set3(3))%aa_rots(set3(4))%rot_ats(na1+1)%at_xyz(1)=Ox(1)
!	                   mol(set3(1))%res(set3(2))%res_aas(set3(3))%aa_rots(set3(4))%rot_ats(na1+1)%at_xyz(2)=Ox(2)
!	                   mol(set3(1))%res(set3(2))%res_aas(set3(3))%aa_rots(set3(4))%rot_ats(na1+1)%at_xyz(3)=Ox(3)
!	                   mol(set3(1))%res(set3(2))%res_aas(set3(3))%aa_rots(set3(4))%rot_ats(na1+1)%at_bfa=1.0 
!	                   mol(set3(1))%res(set3(2))%res_aas(set3(3))%aa_rots(set3(4))%rot_ats(na1+1)%at_occ=1.0
!                                                                                
!	                   
!	                    mol(set3(1))%res(set3(2))%res_aas(set3(3))%aa_rots(set3(4))%rot_ats(na1+2)%at_xyz(1)=Hy1(1)
!                           mol(set3(1))%res(set3(2))%res_aas(set3(3))%aa_rots(set3(4))%rot_ats(na1+2)%at_xyz(2)=Hy1(2)
!                           mol(set3(1))%res(set3(2))%res_aas(set3(3))%aa_rots(set3(4))%rot_ats(na1+2)%at_xyz(3)=Hy1(3)
!                           mol(set3(1))%res(set3(2))%res_aas(set3(3))%aa_rots(set3(4))%rot_ats(na1+2)%at_bfa=1.0
!                           mol(set3(1))%res(set3(2))%res_aas(set3(3))%aa_rots(set3(4))%rot_ats(na1+2)%at_occ=1.0
!                           
!                           mol(set3(1))%res(set3(2))%res_aas(set3(3))%aa_rots(set3(4))%rot_ats(na1+3)%at_xyz(1)=Hy2(1)
!                           mol(set3(1))%res(set3(2))%res_aas(set3(3))%aa_rots(set3(4))%rot_ats(na1+3)%at_xyz(2)=Hy2(2)
!                           mol(set3(1))%res(set3(2))%res_aas(set3(3))%aa_rots(set3(4))%rot_ats(na1+3)%at_xyz(3)=Hy2(3)
!                           mol(set3(1))%res(set3(2))%res_aas(set3(3))%aa_rots(set3(4))%rot_ats(na1+3)%at_bfa=1.0
!                           mol(set3(1))%res(set3(2))%res_aas(set3(3))%aa_rots(set3(4))%rot_ats(na1+3)%at_occ=1.0
!	                   
!	                   unique_flag=.FALSE.
!	                   call test_if_unique_hydrated_ROTAMER(set3,na1,unique_flag)
!	                   mol(set3(1))%res(set3(2))%res_aas(set3(3))%aa_rots(set3(4))%rot_flag=unique_flag
	                   
	                   !print*,set3
	                   !CALL dump_pdb (set3(1),set3(2),set3(3),set3(4))
	                   
                       !998 FORMAT(f9.5)
!                           WRITE(99,998)ehb1
!                           WRITE(99,998)ehb2
                          
                          end if
                          
                          if (ehb3<H20_build_ene_min .and. ehb4<H20_build_ene_min) then
	                   if (H20twoflag .eqv. .TRUE.) then
                            
                            
	                    numWATERlocal=numWATERlocal+1
!	                    print*,set1,set2,setmc
!	                    print*,set4
	                    
!	                    print*,'in',set4,'num',numWATERlocal,'OX',OX,'H',Hy3
	                    
	                    if (numWATERlocal<numwaterlocal_max) then
                            temp_DATAsetslocal(1,numWATERlocal)=set4(1)
                            temp_DATAsetslocal(2,numWATERlocal)=set4(2)
                            temp_DATAsetslocal(3,numWATERlocal)=set4(3)
                            temp_DATAsetslocal(4,numWATERlocal)=set4(4)
                            
                            temp_DATAnamelocal(numWATERlocal)=aname1out
                            
                            temp_DATAcordlocal(1,numWATERlocal)=Ox(1)
                            temp_DATAcordlocal(2,numWATERlocal)=Ox(2)
                            temp_DATAcordlocal(3,numWATERlocal)=Ox(3)
                            temp_DATAcordlocal(4,numWATERlocal)=Hy1(1)
                            temp_DATAcordlocal(5,numWATERlocal)=Hy1(2)
                            temp_DATAcordlocal(6,numWATERlocal)=Hy1(3)
                            temp_DATAcordlocal(7,numWATERlocal)=Hy3(1)
                            temp_DATAcordlocal(8,numWATERlocal)=Hy3(2)
                            temp_DATAcordlocal(9,numWATERlocal)=Hy3(3)
                           else
!                           
                            PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>    '
            	            PRINT*, '>> Too many WATERMOLECULES  > numwaterlocal_max           '
	                    PRINT*, '>> ', numWATERlocal
	                    PRINT*, '>> Increase numwaterlocal_max in xxx                     '
	                    PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>    '
		            stop
!
                           end if
	                    
!	                   WRITE(99,998)ehb3
!                           WRITE(99,998)ehb4
	                   
	                    !print*,'HIER BIN ICH',aname1out
!	                    set3=(/1,1,1,1/)
                           
!	                    CALL EXPAND_SINGLE_AA(set4,set3,aname1out)
!	                    
!	                    mol(set3(1))%res(set3(2))%res_aas(set3(3))%aa_rots(set3(4))%rot_ats(na1+1)%at_xyz(1)=Ox(1)
!	                    mol(set3(1))%res(set3(2))%res_aas(set3(3))%aa_rots(set3(4))%rot_ats(na1+1)%at_xyz(2)=Ox(2)
!	                    mol(set3(1))%res(set3(2))%res_aas(set3(3))%aa_rots(set3(4))%rot_ats(na1+1)%at_xyz(3)=Ox(3)
!	                    mol(set3(1))%res(set3(2))%res_aas(set3(3))%aa_rots(set3(4))%rot_ats(na1+1)%at_bfa=1.0 
!	                    mol(set3(1))%res(set3(2))%res_aas(set3(3))%aa_rots(set3(4))%rot_ats(na1+1)%at_occ=1.0
!                                                                                	                 
!	                     mol(set3(1))%res(set3(2))%res_aas(set3(3))%aa_rots(set3(4))%rot_ats(na1+2)%at_xyz(1)=Hy1(1)
!                            mol(set3(1))%res(set3(2))%res_aas(set3(3))%aa_rots(set3(4))%rot_ats(na1+2)%at_xyz(2)=Hy1(2)
!                            mol(set3(1))%res(set3(2))%res_aas(set3(3))%aa_rots(set3(4))%rot_ats(na1+2)%at_xyz(3)=Hy1(3)
!                            mol(set3(1))%res(set3(2))%res_aas(set3(3))%aa_rots(set3(4))%rot_ats(na1+2)%at_bfa=1.0
!                            mol(set3(1))%res(set3(2))%res_aas(set3(3))%aa_rots(set3(4))%rot_ats(na1+2)%at_occ=1.0
!                            
!                            mol(set3(1))%res(set3(2))%res_aas(set3(3))%aa_rots(set3(4))%rot_ats(na1+3)%at_xyz(1)=Hy3(1)
!                            mol(set3(1))%res(set3(2))%res_aas(set3(3))%aa_rots(set3(4))%rot_ats(na1+3)%at_xyz(2)=Hy3(2)
!                            mol(set3(1))%res(set3(2))%res_aas(set3(3))%aa_rots(set3(4))%rot_ats(na1+3)%at_xyz(3)=Hy3(3)
!                            mol(set3(1))%res(set3(2))%res_aas(set3(3))%aa_rots(set3(4))%rot_ats(na1+3)%at_bfa=1.0
!                            mol(set3(1))%res(set3(2))%res_aas(set3(3))%aa_rots(set3(4))%rot_ats(na1+3)%at_occ=1.0
!	                    CALL dump_pdb (set3(1),set3(2),set3(3),set3(4))
!                             
!  UNTERE ZEILEN AUSGEKLAMMERT, YVES (23.03.2017)
!
!                            unique_flag=.FALSE.
!                            call test_if_unique_hydrated_ROTAMER(set3,na1,unique_flag)
!                            mol(set3(1))%res(set3(2))%res_aas(set3(3))%aa_rots(set3(4))%rot_flag=unique_flag
!	                   	              
	                  end if
	                 end if
!	                 
!                        Print*,'Ox=',Ox	                 
!	                 Print*,'Hy1=',Hy1
!	                 Print*,'Hy2=',Hy2
!	                 	        	                 	                 	                 
!	                 PRINT*,'angle1=',180*angle1/pi
!	                 PRINT*,'angle2=',180*angle2/pi
!	                 
!	                 Print*,B1disti,B1distj
!	                 Print*,'MP    =',MP
!	                 Print*,'M     =',M


!	       	        999 FORMAT(a6,i5,a3,2x,a4,1x,a,i4,4x,3f8.3,2f6.2,11x,a1)
!	                !counter fehlt 
!                        WRITE(2,999)                                                           &
!                        &      'HETATM',                                                       &
!                        &      1,                                                              &
!                        &      'O',                                                            &
!                        &      'HOH',                                                          &
!                        &      'X',                                                            &
!                        &      1,                                                              &
!                        &      Ox(1),                                                          &
!                        &      Ox(2),                                                          &
!                        &      Ox(3),                                                          &
!                        &      1.00,                                                           &
!                        &      1.00,                                                           &
!                        &      'O'								
!
!                        WRITE(2,999)                                                           &
!                        &      'HETATM',                                                       &
!                        &      2,                                                              &
!                        &      'H',                                                            &
!                        &      'HOH',                                                          &
!                        &      'X',                                                            &
!                        &      1,                                                              &
!                        &      Hy1(1),                                                         &
!                        &      Hy1(2),                                                         &
!                        &      Hy1(3),                                                         &
!                        &      1.00,                                                           &
!                        &      1.00,                                                           &
!                        &      'H'								
!                                   
!                        WRITE(2,999)                                                           &
!                        &      'HETATM',                                                       &
!                        &      3,                                                              &
!                        &      'H',                                                            &
!                        &      'HOH',                                                          &
!                        &      'X',                                                            &
!                        &      1,                                                              &
!                        &      Hy2(1),                                                         &
!                        &      Hy2(2),                                                         &
!                        &      Hy2(3),                                                         &
!                        &      1.00,                                                           &
!                        &      1.00,                                                           &
!                        &      'H'								 
!                        
!                        if (H20twoflag .eqv. .TRUE.) then
!              
!                        WRITE(2,999)                                                           &
!                        &      'HETATM',                                                       &
!                        &      1,                                                              &
!                        &      'O',                                                            &
!                        &      'HOH',                                                          &
!                        &      'X',                                                            &
!                        &      1,                                                              &
!                        &      Ox(1),                                                          &
!                        &      Ox(2),                                                          &
!                        &      Ox(3),                                                          &
!                        &      1.00,                                                           &
!                        &      1.00,                                                           &
!                        &      'O'								
!
!                        WRITE(2,999)                                                           &
!                        &      'HETATM',                                                       &
!                        &      2,                                                              &
!                        &      'H',                                                            &
!                        &      'HOH',                                                          &
!                        &      'X',                                                            &
!                        &      1,                                                              &
!                        &      Hy1(1),                                                         &
!                        &      Hy1(2),                                                         &
!                        &      Hy1(3),                                                         &
!                        &      1.00,                                                           &
!                        &      1.00,                                                           &
!                        &      'H'								
!                                   
!                        WRITE(2,999)                                                           &
!                        &      'HETATM',                                                       &
!                        &      3,                                                              &
!                        &      'H',                                                            &
!                        &      'HOH',                                                          &
!                        &      'X',                                                            &
!                        &      1,                                                              &
!                        &      Hy3(1),                                                         &
!                        &      Hy3(2),                                                         &
!                        &      Hy3(3),                                                         &
!                        &      1.00,                                                           &
!                        &      1.00,                                                           &
!                        &      'H'								
!                        end if
            
                        else if (B1disti<0.1 .or. B1distj<0.1) then
                         print*,'DIST ERROR'
                        end if                                           
                       end if
                      end if
                     end if

                    end if
                   end do
		 end do
		end if
	      end if
	    end do
	   end if 
	 end do
        end if
!
!       now copying everything into an allocatable array
!
          if (allocated(DATAsetslocal)) deallocate(DATAsetslocal)
          allocate(DATAsetslocal(4,numWATERlocal))
          if (allocated(DATAcordlocal)) deallocate(DATAcordlocal)
          allocate(DATAcordlocal(9,numWATERlocal))
          if (allocated(DATAnamelocal)) deallocate(DATAnamelocal)
          allocate(DATAnamelocal(numWATERlocal))
!
          do i=1,numWATERlocal
!
                       DATAsetslocal(1,i) = temp_DATAsetslocal(1,i)
                       DATAsetslocal(2,i) = temp_DATAsetslocal(2,i)
                       DATAsetslocal(3,i) = temp_DATAsetslocal(3,i)
                       DATAsetslocal(4,i) = temp_DATAsetslocal(4,i)
!                                                            
                       DATAnamelocal(i)   = temp_DATAnamelocal(i)
!                                                             
                       DATAcordlocal(1,i) = temp_DATAcordlocal(1,i)
                       DATAcordlocal(2,i) = temp_DATAcordlocal(2,i)
                       DATAcordlocal(3,i) = temp_DATAcordlocal(3,i)
                       DATAcordlocal(4,i) = temp_DATAcordlocal(4,i)
                       DATAcordlocal(5,i) = temp_DATAcordlocal(5,i)
                       DATAcordlocal(6,i) = temp_DATAcordlocal(6,i)
                       DATAcordlocal(7,i) = temp_DATAcordlocal(7,i)
                       DATAcordlocal(8,i) = temp_DATAcordlocal(8,i)
                       DATAcordlocal(9,i) = temp_DATAcordlocal(9,i)
!                       
         end do
!         
        if (allocated(mlc))   deallocate(mlc)
	if (allocated(hd))    deallocate(hd)
	if (allocated(ab))    deallocate(ab)
	if (allocated(xyz1))  deallocate(xyz1)
	if (allocated(xyz2))  deallocate(xyz2)
	if (allocated(xyz3))  deallocate(xyz3)
        if (allocated(AOD))   deallocate(AOD)	
	if (allocated(con_at)) deallocate(con_at)	
	if (allocated(HS)) deallocate(HS)
	if (allocated(AS)) deallocate(AS)
	if (allocated(atm_names)) deallocate(atm_names)
	
     !		           
     !		  #####  ######################################################################################         
     !		  ###########################################################################         
     !		  ####################################################################         
          
     !                       
     !                   WURDE BEI DREHUNG IMMER DER EINHEITSVEKTOR VERWENDET???? 
     ! wurden alle vektoren richtig rum deffiniert?
     !
     !############################################################################################           
     !		  ############################################################################################         
     !		  ############################################################################################         
            





	          	      
	      
!	       	       

!	            
!
!	            
          
            
!	999 FORMAT(a6,i5,a3,2x,a4,1x,a,i4,4x,3f8.3,2f6.2,11x,a1)	       
!	       open(unit=2,file='00_water.pdb',status='replace')
!	       do i=1,10
!	       TEMP1=(WT(i,1))
!	       TEMP2=(WT(i,2))
!	       TEMP3=(WT(i,3))
!		 WRITE(2,999)                                                          &
!		 &	'HETATM',                                                       &
!		 &      i,                                                              &
!		 &      'O',                                                            &
!		 &      'HOH',                                                          &
!                 &      'X',                                                            &
!                 &      1,                                                              &
!                 &      WT(i,1),                                                        &
!                 &      WT(i,2),                                                        &
!                 &      WT(i,3),                                                        &
!                 &      1.00,                                                           &
!                 &      1.00,                                                           &
!                 &      'O'
!						
!		
!	       end do
!	       close(2)            
            
!
!    
!       PRINT*, '**********************************************************'       
!       PRINT*, '********************KONTROLLAUSGABEN**********************'
!       PRINT*, '**********************************************************'
!	
!	
!	       ! FINDEN DER DONOR UND DONOR -1 ATOMEN ZU DEN Hs	
!
!!ZU EINFACH????????????
!
!	      do i=1,ntot
!	       HS(i,1)=0
!	       AS(i,1)=0
!	       AS(i,2)=0
!	      end do
!	      
!	      do i=1,ntot
!	       if (hd(i,1) .EQ. 1) then
!	        HS(i,2)=9
!!	       print*,i,hd(i,1)
!	         do j=1,ntot
!                  if (con_at(i,j) .EQ. 2) then
!!		  print*, 'H=',i,'H-1=',j
!		  HS(i,1)=j
!		  else if (con_at(i,j) .EQ. 3 .and. HS(i,2).EQ.9) then
!!		  print*, 'H=',i,'H-2=',j
!		  HS(i,2)=j
!		  end if
!		 end do     
!	        end if
!	       end do
!	       
!	       ! FINDEN DER Akzeptoer UND Akzeptor-1 und Akzeptor-2 ATOME
!	       ! NOCH FALSCH, falls Akzeptor 2 Bindungspartner hat !!! 
!
!	       do i=1,ntot
!	        if (ab(i,1) .EQ. 1 .or. ab(i,2) .EQ. 1) then
!		  AS(i,2)=9
!		  do j=1,ntot
!		   if(con_at(i,j) .EQ.2) then
!!		    print*, 'A=',i,'A-1=',j
!		    AS(i,1)=j 
!		   else if(con_at(i,j) .EQ.3 .and. AS(i,2) .EQ. 9) then
!!		    print*, 'A=',i,'A-2=',j
!		    AS(i,2)=j
!		   end if
!		  end do
!		end if 
!	       end do
!!		
!!		
!!!FINDEN VON MÖGLICHEN WASSER POSSITIONEN  (Bis jetzt nur für Spezialfall i ist DONOR 
!!
!!!FINDEN DER GERADE AUF DER WASSER IDEAL LIEGEN WÜRDE
!!
!!
!!
!               i=18
!               j=8
!!
!!    !Stützvektor
!              G1(1,1)=xyz1(i)
!              G1(2,1)=xyz2(i)
!              G1(3,1)=xyz3(i)
!              G2(1,1)=xyz1(j)
!              G2(2,1)=xyz2(j)
!              G2(3,1)=xyz3(j)
!              
!
!
!             
!              
!	      H(1)=0
!	      H(2)=0
!	      A(1)=AS(j,1)
!	      A(2)=AS(j,2)
!	      
!!	      print*,'A-1=',A(1),'A-2',A(2)
!      
!	      
!	      do l=1,ntot
!	       if(HS(l,1)==i) then
!	        H(1)=l
!	        H(2)=HS(l,2)
!!	        print*,'H=',l,'H-2',HS(l,2)
!	       end if
!	      end do
!
!	       
!	                  	      
!!	      
!!	      if (H(1)==0)then
!!	       print*,'ERROR H NOT FOUND'
!!	      end if
!!	      if (H(2)==0)then
!!	       print*,'ERROR H-2 NOT FOUND'
!!	      end if
!!	      if (A(1)==0)then
!!	       print*,'ERROR A-1 NOT FOUND'
!!	      end if
!!	      if (A(2)==0)then
!!	       print*,'ERROR A-2 NOT FOUND'
!!	      end if
!!	      
!	            
!               do l=1,3
!	       E(1,l)=G2(l,1)       
!	       end do
!	      
!	       E(2,1)=xyz1(A(1))-E(1,1)
!               E(2,2)=xyz2(A(1))-E(1,2)
!               E(2,3)=xyz3(A(1))-E(1,3)
!	       E(3,1)=xyz1(A(2))-E(1,1)
!               E(3,2)=xyz2(A(2))-E(1,2)
!               E(3,3)=xyz3(A(2))-E(1,3)
!	      
!	       NVE(1)=E(2,2)*E(3,3)-E(2,3)*E(3,2)
!	       NVE(2)=E(2,3)*E(3,1)-E(2,1)*E(3,3)
!	       NVE(3)=E(2,1)*E(3,2)-E(2,2)*E(3,1)
!	       NVElen=sqrt(NVE(1)**2+NVE(2)**2+NVE(3)**2)
!	       
!	       do l=1,3
!	        NVE(l)=NVE(l)/NVElen
!	       end do
!	       
!! Drehen von A um NVE um 120°	
!
!               T3(1)=xyz1(A(1))
!               T3(2)=xyz2(A(1))
!               T3(3)=xyz3(A(1))
! 	       alpha=3.14159/180*130
!              
!               call ROTATEV(NVE,E(1,:),T3,T4,alpha)
!              	        	          	   
!! 	       	          	   
!!	      PRINT*,'T4=',T4	   
!     	   
!  
!
!	      
!               G1(1,2)=xyz1(H(1))-xyz1(i)
!               G1(2,2)=xyz2(H(1))-xyz2(i)
!               G1(3,2)=xyz3(H(1))-xyz3(i)
!               
!               V(1)=xyz1(i)-xyz1(H(2))
!               V(2)=xyz2(i)-xyz2(H(2))
!               V(3)=xyz3(i)-xyz3(H(2))
!               
!               G1len=sqrt(G1(1,2)**2+G1(2,2)**2+G1(3,2)**2)
!               Vlen=sqrt(V(1)**2+V(2)**2+V(3)**2)
!               
!               do l=1,3
!                G1(l,2)=G1(l,2)/(G1len)
!                V(l)=V(l)/Vlen
!                T1(l)=G1(l,1)+G1(l,2)
!               end do
!               
!               print*,T1(1),T1(2),T1(3)
!               
!               
!!T1 um 120° drehen
!
!              
!
! 	       T1(1)=T1(1)-xyz1(i)
! 	       T1(2)=T1(2)-xyz2(i)
! 	       T1(3)=T1(3)-xyz3(i)
! 	       
! 	       	       
! 	       T2(1)=xyz1(i)
! 	       T2(2)=xyz2(i)
! 	       T2(3)=xyz3(i)
! 	       
! 	       alpha=3.14159*2/3*2
!	       
! 	       RM(1,1)=V(1)*V(1)*(1-cos(alpha))+cos(alpha)
! 	       RM(1,2)=V(1)*V(2)*(1-cos(alpha))+V(3)*sin(alpha)
! 	       RM(1,3)=V(1)*V(3)*(1-cos(alpha))-V(2)*sin(alpha)
! 	       RM(2,1)=V(2)*V(1)*(1-cos(alpha))-V(3)*sin(alpha)
! 	       RM(2,2)=V(2)*V(2)*(1-cos(alpha))+cos(alpha)
! 	       RM(2,3)=V(2)*V(3)*(1-cos(alpha))+V(1)*sin(alpha)
! 	       RM(3,1)=V(3)*V(1)*(1-cos(alpha))+V(2)*sin(alpha)
! 	       RM(3,2)=V(3)*V(2)*(1-cos(alpha))-V(1)*sin(alpha)
! 	       RM(3,3)=V(3)*V(3)*(1-cos(alpha))+cos(alpha)
! 	            
! 	       
! 	       do l=1,3
! 		do k=1,3
! 		  T2(l)=T2(l)+RM(k,l)*T1(k)
! 		end do              
! 	       end do
! 	        	        
! 	        	          
!! 	       	          
!	      PRINT*,'T2=',T2(1),T2(2),T2(3)
!	      
!	      
!
!
!
!	      
!	      do i=1,3
!	       G2(i,2)=T4(i)-G2(i,1)
!	       G1(i,2)=T2(i)-G1(i,1) !NUR WEIL ICH WASSERSTOFF NACHTRÄGLICH DREHE
! 	      end do
! 	      
!	      call MINDISTMP(G1,G2,MP)
! 	        print*,'1000',MP
!! 	        print*,'1000',G1
!! 	        print*,'1000',G2
! 	        
! 	        do i=1,3
! 	        T5(i)=G1(i,1)+5*G1(i,2)
! 	        T6(i)=G2(i,1)+5*G2(i,2)
!                end do
!                Print*,T5
!                Print*,T6
!!	      
!!FINDEN DES KREISES ZWISCHEN POLAR UND POLAR
!
!	       i=8
!	       j=18
!               dist=0.
!               
!	       dist_xyz(1) = xyz1(i) - xyz1(j)	      
!	       dist_xyz(2) = xyz2(i) - xyz2(j)	      
!	       dist_xyz(3) = xyz3(i) - xyz3(j)
!	       
!	       do l=1,3	      
!		 dist_temp(l)= dist_xyz(l)**2	      
!		 dist= dist + dist_temp(l)
!	       end do
!	       dist = sqrt(dist)
!!	       print*,'DIST= ',dist
!	         
!	       r=sqrt((2.8**2)-((dist/2)**2)) !Ideale Abstabd Polar-H2O=2.8.POLAR-POLAR Distanzanhänige FUNKTION?
!	       
!	       M(1)=(xyz1(i)+xyz1(j))/2
!	       M(2)=(xyz2(i)+xyz2(j))/2
!	       M(3)=(xyz3(i)+xyz3(j))/2
!	       
!	       print*,M(1),M(2),M(3),r
!	       
!	       do l=1,3
!	        u(l)=-dist_xyz(l)
!	       end do
!	       
!	       norm(1)=1
!	       norm(2)=1
!	       norm(3)=(-u(1)-u(2))/u(3)
!	       nlen=sqrt(1+1+norm(3)**2)
!	       
!	       do l=1,3
!	        norm(l)=norm(l)*(1/nlen)
!	        W1(l)=M(l)+norm(l)*r
!	       end do
!	         
!        
!	       !DREHUNG UM GERADE
!
!	       ulen=sqrt(u(1)**2+u(2)**2+u(3)**2)
!	       
!	       do l=1,3
!	        un(l)=u(l)*(1/ulen)
!	       end do
!	       
!	       alphastep=2*3.14159/10
!             	       
!	      do i=1,10
!	       
!	       alpha=alphastep*i
!	       call ROTATEV(un,M,W1,WT(i,:),alpha)
!	       
!	      end do
!         
!	      
!	999 FORMAT(a6,i5,a3,2x,a4,1x,a,i4,4x,3f8.3,2f6.2,11x,a1)	       
!	       open(unit=2,file='00_water.pdb',status='replace')
!	       do i=1,10
!	       TEMP1=(WT(i,1))
!	       TEMP2=(WT(i,2))
!	       TEMP3=(WT(i,3))
!		 WRITE(2,999)                                                          &
!		 &	'HETATM',                                                       &
!		 &      i,                                                              &
!		 &      'O',                                                            &
!		 &      'HOH',                                                          &
!                 &      'X',                                                            &
!                 &      1,                                                              &
!                 &      WT(i,1),                                                        &
!                 &      WT(i,2),                                                        &
!                 &      WT(i,3),                                                        &
!                 &      1.00,                                                           &
!                 &      1.00,                                                           &
!                 &      'O'
!						
!		
!	       end do
!	       close(2)
!	          
!	      
!!Schneiden der idealen Gerade mit der Kreisebene	      
!!ACHTUNG: HIER NOMENKLATUR DER MATRIX RICHTIG	                             
!	       	       
!	          NVE2len=0.
!		  do i=1,3
!	           NVE2len=NVE2len+un(i)*(MP(i)-M(i))
!	          end do
!	          
!	          NVE2len=abs(NVE2len) 
!	          
!	          
!	          T71len=0.
!	          T72len=0. 
!	          
!	          do i=1,3
!	           T71=MP+NVE2len*un
!	           T72=MP-NVE2len*un
!	           T71len=T71len+un(i)*(T71(i)-M(i))
!	           T72len=T72len+un(i)*(T72(i)-M(i))
!	          end do
!	            
!
!	            
!	           if (T71len < 0.1*abs(NVE2len))then
!	            do i=1,3
!	             T7(i)=T71(i)
!	            end do
!	           else if (T72len < 0.1*abs(NVE2len))then
!	            do i=1,3
!	             T7(i)=T72(i)
!	            end do
!	           else
!	            print*, 'BEI DER PROJEKTION DES PUNKTES IN DIE EBENE IST EIN FEHLER UNTERLAUFEN'
!	           end if
!	          
!	          
!	               	          
!	 	  do i=1,3
!	 	   Vec1(i)=T7(i)-M(i)
!	 	   Vec2(i)=norm(i)
!	 	   T8(i)=M(i)+r*norm(i)
!	          end do
!	           Vec1len=sqrt(Vec1(1)**2+Vec1(2)**2+Vec1(3)**2)
!	           
!	          do i=1,3
!	           Vec1(i)=Vec1(i)/Vec1len
!	          end do
!	          
!	          angle=acos(Vec1(1)*Vec2(1)+Vec1(2)*Vec2(2)+Vec1(3)*Vec2(3))
!	          
!	         
!	         call ROTATEV(un,M,T8,Wfinal,angle)
!	         PRINT*,'Wfinal1000=',Wfinal
!!       
!!!	          print*,'ANGLE=',angle	      
!!!	          print*,'T7=',T7	          
!!!	          print*,'M=',M	
!!!	          print*,'W1=',W1
!!
!!
!!!ENDE 
!!
!
!
!       
!        PRINT*, ' na1= ',na1,' na2= ',na2,' ntot= ',ntot
!       
!        PRINT*, 'ERSTER REST'
!       
!        CALL dump_pdb(m1,m2,m3,m4)
!        
!        PRINT*, 'ZWEITER REST'
!        
!        CALL dump_pdb(n1,n2,n3,n4)
        



END SUBROUTINE ADDWATER
        
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc        
!
        SUBROUTINE MINDISTMP(V1,V2,MP,mindistflag)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MOLEC_M
	USE MUMBO_DATA_M
	USE MAX_DATA_M
!!
	IMPLICIT NONE
!!
	real, intent(in),  dimension(3,2)  :: V1, V2 
	real, intent(out), dimension(3)    :: MP
        logical, intent(inout)             :: mindistflag
        
        real,              dimension(3)    ::NV,LV,P1,P2
	real,              dimension(3,4)  ::M
	integer                            ::i    
        
        NV(1)=V1(2,2)*V2(3,2)-V1(3,2)*V2(2,2)
        NV(2)=V1(3,2)*V2(1,2)-V1(1,2)*V2(3,2)
        NV(3)=V1(1,2)*V2(2,2)-V1(2,2)*V2(1,2)
        
        do i=1,3      
        M(i,1)=V1(i,2)
        M(i,2)=-V2(i,2)
        M(i,3)=-NV(i)
        M(i,4)=-V1(i,1)+V2(i,1)
        end do        
          
        call Gaus3x3(M,LV)
       
       if (LV(1)>0 .OR. LV(2)>0) then
       mindistflag=.TRUE. 
       ! print*,'ERROR MINDIST','LV=',LV
       ! print*,'V1=',V1(:,1)+V1(:,2)
       ! Print*,'V2=',V2(:,1)+V2(:,2)
       ! print*,'   ',V1(:,1)
       ! print*,'   ',V2(:,1)
       end if
       
	do i=1,3
        P1(i)=V1(i,1)+LV(1)*V1(i,2)
        P2(i)=V2(i,1)+LV(2)*V2(i,2)
        MP(i)=(P1(i)+P2(i))/2
        end do
        
        
!        PRINT*,'>>>>>>>>>>>>>MITTELPUNKT>>>>>>>>>>>>',MP
!        PRINT*,'>>>>>>>>>>>>>>>>>>>>>>P1>>>>>>>>>>>>',P1
!     PRINT*,'>>>>>>>>>>>>>>>>>>>>>>P2>>>>>>>>>>>>',P2
!
!
        END SUBROUTINE MINDISTMP
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc        
!
        SUBROUTINE GAUS3x3(M,LV) !3x3+b => M 3x4
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MOLEC_M
	USE MUMBO_DATA_M
	USE MAX_DATA_M
!!
	IMPLICIT NONE
!!
        real, intent(inout) ,dimension(3,4)  ::M
        real, intent(out),dimension(3)       ::LV
        
        integer                              ::PIV, i,j,k,z
        real                                 ::TEMP
   
              
        do j=1,2         
         PIV=j
         
         do i=j+1,3
          if (abs(M(PIV,j)) < abs(M(i,j))) then
           PIV=i
          end if
         end do         
         
         do i=1,4
          TEMP=M(j,i)
          M(j,i)=M(PIV,i)
          M(PIV,i)=TEMP
         end do
                
         do k=j+1,3
          do i=1,4
           z=5-i
           M(k,z)=M(k,z)-M(j,z)/M(j,j)*M(k,j)
          end do
         end do          
        end do
         
                          
        
        LV(3)=M(3,4)/M(3,3)
        LV(2)=(M(2,4)-LV(3)*M(2,3))/M(2,2)
        LV(1)=(M(1,4)-LV(3)*M(1,3)-LV(2)*M(1,2))/M(1,1)              
!!
!!
        END SUBROUTINE Gaus3x3
!!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc 
!
        SUBROUTINE ROTATEV(Vec,Pvec,Pin,Pout,angle)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MOLEC_M
	USE MUMBO_DATA_M
	USE MAX_DATA_M
!!
	IMPLICIT NONE
!!
        real, intent(in) ,dimension(3)       ::Vec,Pvec,Pin
        real, intent(in)                     ::angle
        real, intent(out),dimension(3)       ::Pout
        
        integer                              ::i,l,k
        real,             dimension(3)       ::Ptemp
        real,             dimension(3,3)     ::RM
        
!Vec  = Gerade um die Gedreht wird
!Pvec = Punkt auf Gerade

	         
	       do i=1,3  
	        Ptemp(i)=Pin(i)-Pvec(i)	              
	        Pout(i)=Pvec(i)             
	       end do
	       
	       RM(1,1)=Vec(1)*Vec(1)*(1-cos(angle))+cos(angle)
	       RM(1,2)=Vec(1)*Vec(2)*(1-cos(angle))+Vec(3)*sin(angle)
	       RM(1,3)=Vec(1)*Vec(3)*(1-cos(angle))-Vec(2)*sin(angle)
	       RM(2,1)=Vec(2)*Vec(1)*(1-cos(angle))-Vec(3)*sin(angle)
	       RM(2,2)=Vec(2)*Vec(2)*(1-cos(angle))+cos(angle)
	       RM(2,3)=Vec(2)*Vec(3)*(1-cos(angle))+Vec(1)*sin(angle)
	       RM(3,1)=Vec(3)*Vec(1)*(1-cos(angle))+Vec(2)*sin(angle)
	       RM(3,2)=Vec(3)*Vec(2)*(1-cos(angle))-Vec(1)*sin(angle)
	       RM(3,3)=Vec(3)*Vec(3)*(1-cos(angle))+cos(angle)
	            
	       
	       do l=1,3
		do k=1,3
		  Pout(l)=Pout(l)+RM(k,l)*Ptemp(k)		  
		end do
	       end do
                
!!
!!
        END SUBROUTINE ROTATEV
!!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc 
!
        SUBROUTINE ANGLEP1P2P3(P1,P2,P3,angle) !NOCH NICHT FERTIG, WEIL NICHT ORIENTIERT
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MOLEC_M
	USE MUMBO_DATA_M
	USE MAX_DATA_M
!!
	IMPLICIT NONE
!!
        real, intent(in) ,dimension(3)       :: P1,P2,P3
        real, intent(out)                    :: angle
        
        integer                              ::i,l,k
        real,             dimension(3)       ::V1,V2
        real                                 ::V1len,V2len
        
        do i=1,3
         V1(i)=P1(i)-P2(i)
         V2(i)=P1(i)-P3(i)
        end do
        
         V1len=sqrt(V1(1)**2+V1(2)**2+V1(3)**2)
         V2len=sqrt(V2(1)**2+V2(2)**2+V2(3)**2)
        
         angle=acos((V1(1)*V2(1)+V1(2)*V2(2)+V1(3)*V2(3))/(V1len*V2len))
         
        
        
               
!!
!!
        END SUBROUTINE ANGLEP1P2P3
!!
!!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
        SUBROUTINE BUILDSP3B1(Din,B2in,B3in,Bout) 
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MOLEC_M
	USE MUMBO_DATA_M
	USE MAX_DATA_M
!!
	IMPLICIT NONE
!!
        real, intent(in) ,dimension(3)       :: Din,B2in,B3in
        real, intent(out),dimension(3,3)     :: Bout
        
        integer                              ::i
        real,             dimension(3)       ::V1,V2,NV1,NV2
        real                                 ::NV1len,NV2len,angle,V1len,pi=3.1415926535
        
        do i=1,3
         V1(i)=Din(i)-B2in(i) 
         V2(i)=B3in(i)-B2in(i)
        end do
        
        V1len=sqrt(V1(1)**2+V1(2)**2+V1(3)**2)
        
        do i=1,3
         V1(i)=V1(i)/V1len
        end do
        
         
        NV1(1)=V1(2)*V2(3)-V1(3)*V2(2)
        NV1(2)=V1(3)*V2(1)-V1(1)*V2(3)
        NV1(3)=V1(1)*V2(2)-V1(2)*V2(1)
        
        NV1len=sqrt(NV1(1)**2+NV1(2)**2+NV1(3)**2)
        
        do i=1,3
         NV1(i)=NV1(i)/NV1len
        end do
        
        do i=1,3
         Bout(i,1)=Din(i)+NV1(i)       
        end do
        
        NV2(1)=V1(2)*NV1(3)-V1(3)*NV1(2)
        NV2(2)=V1(3)*NV1(1)-V1(1)*NV1(3)
        NV2(3)=V1(1)*NV1(2)-V1(2)*NV1(1)
        
       
        NV2len=sqrt(NV2(1)**2+NV2(2)**2+NV2(3)**2)
        
        do i=1,3
         NV2(i)=NV2(i)/NV2len
        end do
        
       angle=-pi/180*20
       call ROTATEV(NV2,Din,Bout(:,1),Bout(:,1),angle) ! MUSS NOCH ÜBERPRÜFT WERDEN OB DREHRICHTUNG IMMER RICHTIG 
       
       angle=pi/180*90   
       call ROTATEV(V1,Din,Bout(:,1),Bout(:,1),angle) ! MUSS NOCH ÜBERPRÜFT WERDEN OB DREHRICHTUNG IMMER RICHTIG
                
       
       do i=1,2 
        angle=pi/180*120
        call ROTATEV(V1,Din,Bout(:,i),Bout(:,i+1),angle) 
       end do
        
       
        
!!
        END SUBROUTINE BUILDSP3B1
!!
!!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
        SUBROUTINE GIVE_HYD_AS_NAME(ASNAMEin,ASNAMEout) 
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MOLEC_M
	USE MUMBO_DATA_M
	USE MAX_DATA_M
	USE WATER_DATA
!!
	IMPLICIT NONE
!!
        CHARACTER(LEN=4), intent(in)       :: ASNAMEin
        CHARACTER(LEN=4), intent(out)      :: ASNAMEout
        
        integer                            :: i                            
        
        ASNAMEout='    '
              
        do i=1,size(ASNAMES)
          if (ASNAMEin == ASNAMES(i)) then
           ASNAMEout=ASWNAMES(i)
          end if
        end do
    
        if (ASNAMEout=='    ') then
         print*,'AS ',ASNAMEin,' NOT KNOWEN'
        end if        
!!
        END SUBROUTINE GIVE_HYD_AS_NAME
!!
!!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
        SUBROUTINE calc_single_hbd_exp(Hy,D,A,A1,A2,asp_flag,ehb) 
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MOLEC_M
	USE MUMBO_DATA_M
	USE MAX_DATA_M
	USE WATER_DATA
	!QUELLE ANGEBEN
!!
	IMPLICIT NONE
!!
        real, dimension(3), intent(in)       :: Hy,D,A,A1,A2
        logical,            intent(in)       :: asp_flag
        real,               intent(out)      :: ehb        
        
        real                                 :: dis_ha,tet,phi,ang,pi=3.1415926535
        real, dimension(3)                   :: dis
      
        integer                              :: i                            
        
!                         

                          ehb    = 0.
                          dis_ha = 0.
                          
                          
	                  dis(1) = Hy(1) - A(1)
	                  dis(2) = Hy(2) - A(2)
	                  dis(3) = Hy(3) - A(3)
	                 	                  
	                  do i=1,3
		           dis(i)= dis(i)**2
		           dis_ha = dis_ha + dis(i)
	                  end do
	                  
 	                  dis_ha = sqrt(dis_ha)	                  	                  
	                  
	                  call angle(A,Hy,D,tet)
	                  call angle(A1,A,Hy,phi)
	                  
	                  
	                  if (asp_flag) then 
	                   call dihedral(Hy,A,A1,A2,ang)
		          end if
		          
		          tet = (tet / pi) * 180
		          phi = (phi / pi) * 180
		          
		          !print*,'dis_ha,tet,phi,ang,asp_flag,ehb',dis_ha,tet,phi,ang,asp_flag,ehb
		          !
		          ! calculated according the empirical approach from David Baker 
		          
		          call calc_hbd_exp(dis_ha,tet,phi,ang,asp_flag,ehb)        
        
!!
        END SUBROUTINE calc_single_hbd_exp
!!
!!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE WRITE_FINAL_WATER()
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MOLEC_M
	USE MUMBO_DATA_M
	USE MAX_DATA_M
	USE WATER_DATA
!
	IMPLICIT NONE
!
	CHARACTER (LEN=1)     :: achid
	CHARACTER (LEN=4)     :: anam, name
	REAL                  :: bfa, xboff
	INTEGER               :: i,j,m,n,o
	INTEGER               :: nnrs, nat, nbfa, n1, n2 , n3, n4,rot_nat_without_water,nat_water,nwater
	LOGICAL               :: Hydrated_flag
	CHARACTER (len=255)   :: cwd
!
        if (.not.water_flag) return 
!
	open(98,file='waterlist.pdb',form='formatted',status='replace')
	open(99,file='waterless.pdb',form='formatted',status='replace')
	
	
        !GFORTRAN
        n4=-1000
        n3=-1000
        n2=-1000
        n1=-1000
        !GFORTRAN	
        
        nwater    = 0
        nat       = 0
	nat_water = 0

        do j=1, mol(1)%mol_nrs
        	
        
          nnrs=  mol(1)%res(j)%res_nold
          achid= mol(1)%res(j)%res_chid
                   
          
          if (mol(1)%res(j)%res_mut) then
            do m=2, size(mol)
              if (nnrs  == mol(m)%res(1)%res_nold) then
                if (achid == mol(m)%res(1)%res_chid) then
                !
                  do n= 1,mol(m)%res(1)%res_naa
                    do o= 1,mol(m)%res(1)%res_aas(n)%aa_nrt
                      if (mol(m)%res(1)%res_aas(n)%aa_rots(o)%rot_flag2) then
                       anam = mol(m)%res(1)%res_aas(n)%aa_nam
                       n1 = m
                       n2 = 1
                       n3 = n
                       n4 = o
                      end if
                    end do
                  end do                !
                end if
              end if
            end do
	          
	    Hydrated_flag=.FALSE.
	    
	    do i=1,size(ASWNAMES)
	      if (anam == ASWNAMES(i)) then
	       anam=ASNAMES(i)
	       Hydrated_flag=.TRUE.
	      end if
	    end do
        
!
	    bfa = 0
	    nbfa = 0
!
!
!	    FIRST WRITE OUT GLYCINE-ATOMS BEFORE ADDING ROTAMER
!	    (IF GLY THEN ROTAMER ATOMS ADDED == 0)
!
	    do m=1,mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_nat
             nat = nat+1
             nbfa = nbfa+1
             !	
             xboff = mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_occ
             name=mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_nam
             !
             if (name(1:1)=='H') then
              if (.not.mumbo_out_hyd) then
               nat = nat-1
               nbfa = nbfa-1
               cycle
              end if
             end if
                          
             !
             bfa = bfa + mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_bfa
             !
             if (name(4:4)==' ') then
             !
              write(99,fmt=1010)                                                 &
              &	'ATOM  ',                                                        &
              &      nat,                                                        &
              &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_nam,      &
              &      anam,                                                       &
              &      achid,                                                      &
              &      nnrs,                                                       &
              &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(1),   &
              &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(2),   &
              &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(3),   &
              &      xboff,                                                      &
              &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_bfa
             !
             else if (name(4:4)/=' ') then
             !
              write(99,fmt=1011)                                                 &
              &	'ATOM  ',                                                        &
              &      nat,                                                        &
              &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_nam,      &
              &      anam,                                                       &
              &      achid,                                                      &
              &      nnrs,                                                       &
              &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(1),   &
              &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(2),   &
              &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(3),   &
              &      xboff,                                                      &
              &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_bfa
             !
	     end if
	    end do
	    

	    bfa = bfa/nbfa
!
!	    NOW WRITE OUT ROTAMER PART OF AMINO ACID

            if(Hydrated_flag)then 
             rot_nat_without_water=mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_nat-3
            else
             rot_nat_without_water=mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_nat
            end if
!
	    do m=1,rot_nat_without_water
              nat = nat+1
              !	
              xboff = 1.0 
              name=mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(m)%at_nam
              !
              if (name(1:1)=='H') then
               if (.not.mumbo_out_hyd) then 
                nat = nat-1
                cycle
               end if
              end if	    
            	    
              if (name(4:4)==' ') then
            !
                write(99,fmt=1010)                                                     &
                &	'ATOM  ',                                                      &
                &      nat,                                                            &
                &      mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(m)%at_nam,      &
                &      anam,                                                           &
                &      achid,                                                          &
                &      nnrs,                                                           &
                &      mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(m)%at_xyz(1),   &
                &      mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(m)%at_xyz(2),   &
                &      mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(m)%at_xyz(3),   &
                &      xboff,                                                          &
                &      bfa
            !   
              else if (name(4:4)/=' ') then
            !
                  write(99,fmt=1011)                                                &
                 &	'ATOM  ',                                                       &
                 &      nat,                                                            &
                 &      mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(m)%at_nam,      &
                 &      anam,                                                           &
                 &      achid,                                                          &
                 &      nnrs,                                                           &
                 &      mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(m)%at_xyz(1),   &
                 &      mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(m)%at_xyz(2),   &
                 &      mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(m)%at_xyz(3),   &
                 &      xboff,                                                          &
                 &      bfa
            !
              end if
	    end do
	    
	    
	    if (Hydrated_flag) then
	     do m=rot_nat_without_water+1,rot_nat_without_water+3
	     nat_water=nat_water+1
               !	
               xboff = 1.0 
               name=mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(m)%at_nam
               
               if (name==WOx_name) then
                nwater=nwater+1
                name='O   '                
               else
                name='H   '
               end if
               
               !    
               write(98,fmt=1009)                                                     &
               &	'HETATM',                                                       &
               &      nat_water,                                                      &
               &      name,                                                           &
               &      'HOH',                                                          &
               &      'W',                                                            &
               &      nwater,                                                         &
               &      mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(m)%at_xyz(1),   &
               &      mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(m)%at_xyz(2),   &
               &      mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(m)%at_xyz(3),   &
               &      xboff,                                                          &
               &      bfa
             !   
	     end do	    
	    end if
          else
!	  IF NOT MUTATED THEN KEEP ORIGINAL STRUCTURE
!
            do m=1, mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_nat
              nat = nat+1
              xboff = mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_occ
              name=   mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_nam
!           
              if (name(1:1)=='H') then
                 if (.not.mumbo_out_hyd) then 
                  nat = nat-1
                  cycle
                 end if
              end if
                   !
              if (name(4:4)==' ') then
              !
                write(99,fmt=1010)                                                &
                &	'ATOM  ',                                                       &
                &      nat,                                                        &
                &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_nam,      &
                &      mol(1)%res(j)%res_aas(1)%aa_nam,                            &
                &      achid,                                                      &
                &      nnrs,                                                       &
                &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(1),   &
                &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(2),   &
                &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(3),   &
                &      xboff,                                                      &
                &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_bfa
              !
              else 
              !
                write(99,fmt=1011)                                                &
                &	'ATOM  ',                                                       &
                &      nat,                                                        &
                &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_nam,      &
                &      mol(1)%res(j)%res_aas(1)%aa_nam,                            &
                &      achid,                                                      &
                &      nnrs,                                                       &
                &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(1),   &
                &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(2),   &
                &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(3),   &
                &      xboff,                                                      &
                &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_bfa
                   !
              end if
            end do
          end if
         end do
!
         if (mumbo_lig_flag) then 
!
           nnrs=  mol(mol_nmls)%res(1)%res_nold
           achid= mol(mol_nmls)%res(1)%res_chid
!
           do o= 1, mol(mol_nmls)%res(1)%res_aas(1)%aa_nrt
             if (mol(mol_nmls)%res(1)%res_aas(1)%aa_rots(o)%rot_flag2) then
               anam = mol(mol_nmls)%res(1)%res_aas(1)%aa_nam
               n1 = mol_nmls
               n2 = 1
               n3 = 1
               n4 = o
             end if
           end do
!
           do m=1,mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_nat
             nat = nat+1
!	
             xboff = 1.0 
             name=mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(m)%at_nam
!
             if (name(1:1)=='H') then
               if (.not.mumbo_out_hyd) then
                 nat = nat-1
                 cycle
               end if
             end if
 !
             if (name(4:4)==' ') then
 !
               write(99,fmt=1010)                                                     &
               &      'ATOM  ',                                                       &
               &      nat,                                                            &
               &      mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(m)%at_nam,      &
               &      anam,                                                           &
               &      achid,                                                          &
               &      nnrs,                                                           &
               &      mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(m)%at_xyz(1),   &
               &      mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(m)%at_xyz(2),   &
               &      mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(m)%at_xyz(3),   &
               &      xboff,                                                          &
               &      bfa
!
             else if (name(4:4)/=' ') then
!
               write(99,fmt=1011)                                                     &
               &	'ATOM  ',                                                     &
               &      nat,                                                            &
               &      mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(m)%at_nam,      &
               &      anam,                                                           &
               &      achid,                                                          &
               &      nnrs,                                                           &
               &      mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(m)%at_xyz(1),   &
               &      mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(m)%at_xyz(2),   &
               &      mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(m)%at_xyz(3),   &
               &      xboff,                                                          &
               &      bfa
!
       	     end if
       	   end do
!
         end if
!
         write(98,fmt=1012) 'END   ' 
         write(99,fmt=1012) 'END   '
         
         close(98)
         close(99)
!                
         
         
         CALL GETCWD(cwd)
         
         PRINT*, '        '
         PRINT*, '################################'
         PRINT*, '# SUMMARY WRITE_FINAL_WATER    #'
         PRINT*, '################################'
         PRINT*, '        '
         PRINT*, '        '
         PRINT*, 'STRUCTURE WITHOUT WATER'
         PRINT*, 'WRITTEN TO FILE:',trim(cwd)//'/waterlist.pdb'
         PRINT*, '        '
         PRINT*, 'WATER MOLEKULES'
         PRINT*, 'WRITTEN TO FILE:',trim(cwd)//'/waterless.pdb'
         PRINT*, '                  '
         !
         !
         
         1009	format(a6,i5,3x,a1,2x,a3,1x,a,i4,4x,3f8.3,2f6.2)
         1010	format(a6,i5,2x,a4,a4,a,i4,4x,3f8.3,2f6.2)
         1011	format(a6,i5,1x,a4,1x,a4,a,i4,4x,3f8.3,2f6.2)
         1012	format(a6,/)


	END SUBROUTINE WRITE_FINAL_WATER
!!
!!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc	                  
!!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
        SUBROUTINE test_if_unique_hydrated_ROTAMER_new(set,na,unique_flag,OH1H2cord,anam) 
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MOLEC_M
	USE MUMBO_DATA_M
	USE MAX_DATA_M
	USE WATER_DATA

!!
	IMPLICIT NONE
!!
        integer,dimension(4),intent(in)       :: set
        integer,             intent(in)       :: na
        logical,             intent(out)      :: unique_flag
        real,dimension(9),   intent(in)       :: OH1H2cord
        character(len=4),    intent(in)       :: anam
      
        integer                               :: i,j,newpos                            
        real,dimension(3)                     :: rot_old_xyz,rot_new_xyz
        real,dimension(3)                     :: Ox_old,Ox_new,Hy1_old,Hy2_old,Hy1_new,Hy2_new
        real,dimension(3)                     :: xyz_dif,dis
        logical                               :: same_ROTAMER_flag,same_WATER_Position_flag,old_AS_flag
        real                                  :: dist_Ox_Ox,dist_H1o_H1n,dist_H2o_H2n,dist_H1o_H2n,dist_H2o_H1n
!        
!       changed by Yves 28.3.2017, not anymore applying the DEL_eps criterium, using a fixed del_diff criterium instead
!
        real                                  :: del_diff = 0.001
!        
        !SUCHEN DES RICHTIGEN SETS
       
        old_AS_flag=.false.
        newpos=0
!
!       first check whether amino acid is already present or not within a specific mol/residue/aa set
!
        do i=1,mol(set(1))%res(set(2))%res_naa
           if (mol(set(1))%res(set(2))%res_aas(i)%aa_nam.eq.anam)then
           old_AS_flag=.true.
           newpos=i
           end if
        end do
!                                
       !na kann noch umschrieben werden                         
        
        unique_flag=.TRUE.
        
        if (old_AS_flag) then
        
          do i=1,mol(set(1))%res(set(2))%res_aas(newpos)%aa_nrt
           if (mol(set(1))%res(set(2))%res_aas(newpos)%aa_rots(i)%rot_flag)then
            same_ROTAMER_flag=.TRUE.
            same_WATER_Position_flag=.FALSE.
            
            dist_Ox_Ox   =9.99
            dist_H1o_H1n =9.99
            dist_H2o_H2n =9.99
            dist_H1o_H2n =9.99
            dist_H2o_H1n =9.99
                     
            do j=1,na
            
             rot_new_xyz(1)=mol(set(1))%res(set(2))%res_aas(set(3))%aa_rots(set(4))%rot_ats(j)%at_xyz(1)
             rot_new_xyz(2)=mol(set(1))%res(set(2))%res_aas(set(3))%aa_rots(set(4))%rot_ats(j)%at_xyz(2)
             rot_new_xyz(3)=mol(set(1))%res(set(2))%res_aas(set(3))%aa_rots(set(4))%rot_ats(j)%at_xyz(3)
             
             rot_old_xyz(1)=mol(set(1))%res(set(2))%res_aas(newpos)%aa_rots(i)%rot_ats(j)%at_xyz(1) 
             rot_old_xyz(2)=mol(set(1))%res(set(2))%res_aas(newpos)%aa_rots(i)%rot_ats(j)%at_xyz(2)
             rot_old_xyz(3)=mol(set(1))%res(set(2))%res_aas(newpos)%aa_rots(i)%rot_ats(j)%at_xyz(3)
             
             
             xyz_dif(1)=abs(rot_new_xyz(1)-rot_old_xyz(1))
             xyz_dif(2)=abs(rot_new_xyz(2)-rot_old_xyz(2))
             xyz_dif(3)=abs(rot_new_xyz(3)-rot_old_xyz(3))
            
!             if(xyz_dif(1) > DEL_eps .OR. xyz_dif(2) > DEL_eps .OR. xyz_dif(3) > DEL_eps) then
             if(xyz_dif(1) > del_diff .OR. xyz_dif(2) > del_diff .OR. xyz_dif(3) > del_diff) then
              same_ROTAMER_flag=.FALSE.
             end if  
            end do
            
            if (same_ROTAMER_flag) then
            
             Ox_new(1)=OH1H2cord(1)
             Ox_new(2)=OH1H2cord(2)
             Ox_new(3)=OH1H2cord(3)
            
             Ox_old(1)=mol(set(1))%res(set(2))%res_aas(newpos)%aa_rots(i)%rot_ats(na+1)%at_xyz(1)
             Ox_old(2)=mol(set(1))%res(set(2))%res_aas(newpos)%aa_rots(i)%rot_ats(na+1)%at_xyz(2)
             Ox_old(3)=mol(set(1))%res(set(2))%res_aas(newpos)%aa_rots(i)%rot_ats(na+1)%at_xyz(3)
             
             dis(1) = Ox_new(1) - Ox_old(1)
             dis(2) = Ox_new(2) - Ox_old(2)
             dis(3) = Ox_new(3) - Ox_old(3)
             
             dist_Ox_Ox=sqrt(dis(1)**2+dis(2)**2+dis(3)**2)         
             
             if (dist_Ox_Ox<Ox_min_dist) then
              same_WATER_Position_flag=.TRUE.
             end if
              
            end if   
            
            if (same_WATER_Position_flag) then
            
             Hy1_new(1)=OH1H2cord(4)
             Hy1_new(2)=OH1H2cord(5)
             Hy1_new(3)=OH1H2cord(6)
             
             Hy2_new(1)=OH1H2cord(7)
             Hy2_new(2)=OH1H2cord(8)
             Hy2_new(3)=OH1H2cord(9)
             
             Hy1_old(1)=mol(set(1))%res(set(2))%res_aas(newpos)%aa_rots(i)%rot_ats(na+2)%at_xyz(1)
             Hy1_old(2)=mol(set(1))%res(set(2))%res_aas(newpos)%aa_rots(i)%rot_ats(na+2)%at_xyz(2)
             Hy1_old(3)=mol(set(1))%res(set(2))%res_aas(newpos)%aa_rots(i)%rot_ats(na+2)%at_xyz(3)
             
             Hy2_old(1)=mol(set(1))%res(set(2))%res_aas(newpos)%aa_rots(i)%rot_ats(na+3)%at_xyz(1)
             Hy2_old(2)=mol(set(1))%res(set(2))%res_aas(newpos)%aa_rots(i)%rot_ats(na+3)%at_xyz(2)
             Hy2_old(3)=mol(set(1))%res(set(2))%res_aas(newpos)%aa_rots(i)%rot_ats(na+3)%at_xyz(3)
             
             dis(1) = Hy1_new(1) - Hy1_old(1)
             dis(2) = Hy1_new(2) - Hy1_old(2)
             dis(3) = Hy1_new(3) - Hy1_old(3)
             dist_H1o_H1n=sqrt(dis(1)**2+dis(2)**2+dis(3)**2)
             
             
             dis(1) = Hy2_new(1) - Hy2_old(1)
             dis(2) = Hy2_new(2) - Hy2_old(2)
             dis(3) = Hy2_new(3) - Hy2_old(3)         
             dist_H2o_H2n=sqrt(dis(1)**2+dis(2)**2+dis(3)**2)
             
             
             dis(1) = Hy2_new(1) - Hy1_old(1)
             dis(2) = Hy2_new(2) - Hy1_old(2)
             dis(3) = Hy2_new(3) - Hy1_old(3)         
             dist_H1o_H2n=sqrt(dis(1)**2+dis(2)**2+dis(3)**2)
             
             
             dis(1) = Hy1_new(1) - Hy2_old(1)
             dis(2) = Hy1_new(2) - Hy2_old(2)
             dis(3) = Hy1_new(3) - Hy2_old(3)         
             dist_H2o_H1n=sqrt(dis(1)**2+dis(2)**2+dis(3)**2)
             
             if (dist_H1o_H1n < Hy_min_dist .AND. dist_H2o_H2n < Hy_min_dist) then
              unique_flag=.FALSE.           
             end if
             
             if (dist_H1o_H2n < Hy_min_dist .AND. dist_H2o_H1n < Hy_min_dist) then
              unique_flag=.FALSE.
             end if                     
            end if         
           
           end if
          end do
        end if                        
!!
        END SUBROUTINE test_if_unique_hydrated_ROTAMER_new
!!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
        SUBROUTINE CHECK_ROTAMER_UNIQUENESS 
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MOLEC_M
	USE MUMBO_DATA_M
	USE MAX_DATA_M
	USE WATER_DATA
!
	IMPLICIT NONE
!      
        integer                               :: i,j,k,l,m,n,o,p   
        integer,dimension(4)                  :: set1,set2
        real,dimension(3)                     :: rot_1_xyz,rot_2_xyz
        real,dimension(3)                     :: xyz_dif
        real                                  :: rmsd, dis, dissqr, dissumsqr, maxdis
        character(len=4)                      :: atnm1, atnm2
        integer                               :: ncomp
        logical                               :: w_pres_flag, diff_flag
!
        PRINT*,MUMBO_PROC,'FOR INDIVIDUAL ROTAMERS OF THE SAME AMINO ACID TYPE TO BE KEPT,' 
        PRINT*,MUMBO_PROC,'AT LEAST ONE PAIR OF CORRESPONDING ATOMS FORM TWO COMPARED ROTAMERS'
        PRINT*,MUMBO_PROC,'HAS TO BE DISTANT BY DEL_EPS GREATER THAN: ', DEL_eps
        print*,MUMBO_PROC,''
        PRINT*,MUMBO_PROC,'IF THIS IS NOTTHE CASE, THEN ONE ROTAMER WILL BE DELETED' 
        print*,MUMBO_PROC,''
        print*,MUMBO_PROC,'NUMBER OF MOLECULES TO BE CHECKED                              ',mol_nmls-1
        print*,MUMBO_PROC,''
        print*,MUMBO_PROC,''
!
!        rmsd criterium not used anymore... 
!        PRINT*,MUMBO_PROC,'MINIMAL RMSD BETWEEN ROTAMERS     , SO THAT THEY ARE CALLED UNIQUE',RMSDmin

!
!       cycle over all molecules
        do i=2,mol_nmls
        
         print*,MUMBO_PROC,'CHECKING',i-1,'_atm_sum'
         
         set1(1)=i
         set1(2)=1
!        
         do j=1,mol(set1(1))%res(set1(2))%res_naa
           set1(3)=j
           
!          No need to go on if only one rotamer is present for a specific amino acid 
           if (mol(set1(1))%res(set1(2))%res_aas(set1(3))%aa_nrt.eq.1) cycle
!
!          Checking whether amino acid has a water molecule attached to it 
!          This is required in order to investigate whether water-hydrogen swapping helps
!          identifying identical rotamers.(see o=2 below)
!
           w_pres_flag=.false.
           do k=1,mol(set1(1))%res(set1(2))%res_aas(set1(3))%aa_rots(1)%rot_nat
              if (mol(set1(1))%res(set1(2))%res_aas(set1(3))%aa_rots(1)%rot_ats(k)%at_nam.eq.WOx_name) then
                w_pres_flag=.true.
              end if
           end do   
!                         
!          no cycle over all rotamers
           do k=1,mol(set1(1))%res(set1(2))%res_aas(set1(3))%aa_nrt
                set1(4)=k        
                if (.not.mol(set1(1))%res(set1(2))%res_aas(set1(3))%aa_rots(set1(4))%rot_flag) cycle
!               interchanging WH1_name and WH2_name atoms in o = 2 run
                do o=1,2
                   if ((o.gt.1).and.(.not.w_pres_flag)) then
                      cycle
                   else if ((o.gt.1).and.(w_pres_flag)) then
                      do p=1,mol(set1(1))%res(set1(2))%res_aas(set1(3))%aa_rots(set1(4))%rot_nat
                         if (mol(set1(1))%res(set1(2))%res_aas(set1(3))%aa_rots(set1(4))%rot_ats(p)%at_nam.eq.WH1_name) then
                             mol(set1(1))%res(set1(2))%res_aas(set1(3))%aa_rots(set1(4))%rot_ats(p)%at_nam=WH2_name
                         else if (mol(set1(1))%res(set1(2))%res_aas(set1(3))%aa_rots(set1(4))%rot_ats(p)%at_nam.eq.WH2_name) then
                               mol(set1(1))%res(set1(2))%res_aas(set1(3))%aa_rots(set1(4))%rot_ats(p)%at_nam=WH1_name
                         end if
                      end do
                   end if       
                   set2(1)=set1(1)
                   set2(2)=set1(2)
                   set2(3)=set1(3)
!                  compare each rotamer with all additional rotamers present at this position 
                   do l=(k+1),mol(set2(1))%res(set2(2))%res_aas(set2(3))%aa_nrt
                       set2(4)=l
                       if (.not.mol(set2(1))%res(set2(2))%res_aas(set2(3))%aa_rots(set2(4))%rot_flag) cycle
                       diff_flag=.false.
                       ncomp=0
                       dissqr=0
                       dissumsqr=0
                       dis=0
                       maxdis=0
!                      finding matching atoms                       
                       do m=1,mol(set1(1))%res(set1(2))%res_aas(set1(3))%aa_rots(set1(4))%rot_nat
                          atnm1= mol(set1(1))%res(set1(2))%res_aas(set1(3))%aa_rots(set1(4))%rot_ats(m)%at_nam
!                          
                          do n=1,mol(set2(1))%res(set2(2))%res_aas(set2(3))%aa_rots(set2(4))%rot_nat
                             atnm2= mol(set2(1))%res(set2(2))%res_aas(set2(3))%aa_rots(set2(4))%rot_ats(n)%at_nam
                             
                             if (atnm1.eq.atnm2) then 
!                                         
                 rot_1_xyz(1)=mol(set1(1))%res(set1(2))%res_aas(set1(3))%aa_rots(set1(4))%rot_ats(m)%at_xyz(1)
                 rot_1_xyz(2)=mol(set1(1))%res(set1(2))%res_aas(set1(3))%aa_rots(set1(4))%rot_ats(m)%at_xyz(2)
                 rot_1_xyz(3)=mol(set1(1))%res(set1(2))%res_aas(set1(3))%aa_rots(set1(4))%rot_ats(m)%at_xyz(3)
!                                                                                                  
                 rot_2_xyz(1)=mol(set2(1))%res(set2(2))%res_aas(set2(3))%aa_rots(set2(4))%rot_ats(n)%at_xyz(1) 
                 rot_2_xyz(2)=mol(set2(1))%res(set2(2))%res_aas(set2(3))%aa_rots(set2(4))%rot_ats(n)%at_xyz(2)
                 rot_2_xyz(3)=mol(set2(1))%res(set2(2))%res_aas(set2(3))%aa_rots(set2(4))%rot_ats(n)%at_xyz(3)
!                                  
                 xyz_dif(1)=abs(rot_1_xyz(1)-rot_2_xyz(1))
                 xyz_dif(2)=abs(rot_1_xyz(2)-rot_2_xyz(2))
                 xyz_dif(3)=abs(rot_1_xyz(3)-rot_2_xyz(3))
!                         
                                  dis= ((xyz_dif(1))**2) + ((xyz_dif(2))**2) + ((xyz_dif(3))**2) 
                                  dis= sqrt(dis)
                                  
                                  if (dis.gt.maxdis) then 
                                  maxdis = dis
                                  end if
!                          
                                  if (dis.gt.DEL_eps) then
                                       diff_flag=.true.
                                  end if
                                  ncomp=ncomp+1
                                  dissqr=dis**2                                  
                                  dissumsqr=dissumsqr+dissqr
!                                  
                             end if                                     
                          end do
                       end do
!                       
                       if (.not.diff_flag) then 
                         mol(set2(1))%res(set2(2))%res_aas(set2(3))%aa_rots(set2(4))%rot_flag=.false.
!                         print*, 'xxxxxDIS-CHECK',DEL_eps, maxdis, o
!                         print*, set1
!                         print*, set2
!                         print*, 'xxxxxx'
                       end if  
!
                       if (ncomp.ne.0) then
                            rmsd= dissumsqr/ncomp
                            rmsd= sqrt(rmsd)
!
!                            RMSDmin criterium not used anymore
!
!                            if (RMSDmin.gt.rmsd) then 
!                                  mol(set2(1))%res(set2(2))%res_aas(set2(3))%aa_rots(set2(4))%rot_flag=.false.
!                                  print*, rmsd, 'xxxxxRMS-CHECK',RMSDmin, 'ncomp ', ncomp , o
!                                  print*, set1
!                                  print*, set2
!                                  print*, 'xxxxxx'
!                            end if
                       end if
!
                   end do  
                   if ((o.gt.1).and.(w_pres_flag)) then
                      do p=1,mol(set1(1))%res(set1(2))%res_aas(set1(3))%aa_rots(set1(4))%rot_nat
                         if (mol(set1(1))%res(set1(2))%res_aas(set1(3))%aa_rots(set1(4))%rot_ats(p)%at_nam.eq.WH1_name) then
                             mol(set1(1))%res(set1(2))%res_aas(set1(3))%aa_rots(set1(4))%rot_ats(p)%at_nam=WH2_name
                         else if (mol(set1(1))%res(set1(2))%res_aas(set1(3))%aa_rots(set1(4))%rot_ats(p)%at_nam.eq.WH2_name) then
                                  mol(set1(1))%res(set1(2))%res_aas(set1(3))%aa_rots(set1(4))%rot_ats(p)%at_nam=WH1_name
                         end if
                      end do
                   end if                           
!                  finished comparing rotamers (before swappimg)
                 end do
!                finished with o=2 
              end do
!             finished comparing all rotamers to all rotamers               
            end do
!           finished comparing all amino acids and rotames at one position, _sum file             
          end do
!         finished looking at all molecules
!!
        END SUBROUTINE CHECK_ROTAMER_UNIQUENESS
!!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
















































































































































































