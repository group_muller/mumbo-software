!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!
	MODULE PARA_M
!
        CONTAINS
	
	SUBROUTINE PDEE_PGOLD_CLEAN(xb)
	
	USE MOLEC_M
	USE EP_DATA_M
	USE MAX_DATA_M
	USE MB_M
	USE SPLIT_M
        USE SPLIT_POS_M
!
	IMPLICIT NONE
!
	integer :: i,j,k,l, ncheck
        TYPE (EP_C_T), DIMENSION(:), allocatable :: xb
!
	if (allocated(xb)) then
	  do i=1, size(xb)
	    do j=1, size(xb(i)%ec)
	       do k=1, size(xb(i)%ec(j)%ed)
		 do l=1, size(xb(i)%ec(j)%ed(k)%ee)
		  deallocate(xb(i)%ec(j)%ed(k)%ee(l)%ef,stat=ncheck)
		  if(ncheck/=0) stop '1Fehler beim Deallokieren. Programmabbruch'
	         end do
		 deallocate(xb(i)%ec(j)%ed(k)%ee,stat=ncheck)
		 if(ncheck/=0) stop '2Fehler beim Deallokieren. Programmabbruch'
	       end do
	       deallocate(xb(i)%ec(j)%ed,stat=ncheck)
	       if(ncheck/=0) stop '3Fehler beim Deallokieren. Programmabbruch'
	    end do
	    deallocate(xb(i)%ec,stat=ncheck)
	    if(ncheck/=0) stop '4Fehler beim Deallokieren. Programmabbruch'
	  end do
	  deallocate(xb,stat=ncheck)
	  if(ncheck/=0) stop '5Fehler beim Deallokieren. Programmabbruch'
	end if
!
	END SUBROUTINE PDEE_PGOLD_CLEAN
!	
	SUBROUTINE PDEE_PGOLD_PAIRWISE_INT(npos,xb)
!
	USE MOLEC_M
	USE EP_DATA_M
	USE MAX_DATA_M
	USE MB_M
	USE SPLIT_M
        USE SPLIT_POS_M
!
	IMPLICIT NONE
!
	INTEGER :: npos
	integer :: i,j,k,l, m, n, ncnt, na, ncheck
	integer, dimension(4) :: set1, set2
	logical :: first1, first2
	real, dimension(15) :: en
      
        TYPE (EP_C_T), DIMENSION(:), allocatable :: xb
!
	   allocate(xb(mol(npos)%res(1)%res_naa),stat=ncheck)
	   if (ncheck/=0) PRINT*,'ALLOCATION ERROR IN CALC_PAIRWISE_INT' 
	   if (ncheck/=0) stop 
           do j=1,mol(npos)%res(1)%res_naa
	       allocate(xb(j)%ec(mol(npos)%res(1)%res_aas(j)%aa_nrt),stat=ncheck)
	       if (ncheck/=0) PRINT*,'ALLOCATION ERROR IN CALC_PAIRWISE_INT' 
	       if (ncheck/=0) stop 
	       do k=1, mol(npos)%res(1)%res_aas(j)%aa_nrt
	           allocate(xb(j)%ec(k)%ed(size(mol)-1),stat=ncheck)
	           if (ncheck/=0) PRINT*,'ALLOCATION ERROR IN CALC_PAIRWISE_INT' 
	           if (ncheck/=0) stop 
                   do l=1,(size(mol)-1)
	              allocate(xb(j)%ec(k)%ed(l)%ee(mol(l+1)%res(1)%res_naa),stat=ncheck)
	              if (ncheck/=0) PRINT*,'ALLOCATION ERROR IN CALC_PAIRWISE_INT' 
	              if (ncheck/=0) stop 
                      do m=1,mol(l+1)%res(1)%res_naa
		            na=mol(l+1)%res(1)%res_aas(m)%aa_nrt
		            allocate(xb(j)%ec(k)%ed(l)%ee(m)%ef(na),stat=ncheck)
	                    if (ncheck/=0) PRINT*,'ALLOCATION ERROR IN CALC_PAIRWISE_INT' 
	                    if (ncheck/=0) stop 
		      end do
	           end do
	       end do
	   end do
!
!
	ncnt=0
!
         do j=1,mol(npos)%res(1)%res_naa
            do k=1, mol(npos)%res(1)%res_aas(j)%aa_nrt
!
                set1(1)=npos
                set1(2)=1
                set1(3)=j
                set1(4)=k
!	      
              do l=1,(size(mol)-1)
                do m=1,mol(l+1)%res(1)%res_naa
                  do n=1, mol(l+1)%res(1)%res_aas(m)%aa_nrt
                  set2(1)=l+1
                  set2(2)=1
                  set2(3)=m
                  set2(4)=n
                  ncnt=ncnt+1
!
         if (npos==(l+1).and.j==m.and.k==n) then
!
		xb(j)%ec(k)%ed(l)%ee(m)%ef(n)%ep_en(1)=               &
     &    mol(npos)%res(1)%res_aas(j)%aa_rots(k)%rot_en(1)
		xb(j)%ec(k)%ed(l)%ee(m)%ef(n)%ep_en(2)=               &
     &    mol(npos)%res(1)%res_aas(j)%aa_rots(k)%rot_en(2)
		xb(j)%ec(k)%ed(l)%ee(m)%ef(n)%ep_en(3)=               &
     &    mol(npos)%res(1)%res_aas(j)%aa_rots(k)%rot_en(3)                 
		xb(j)%ec(k)%ed(l)%ee(m)%ef(n)%ep_en(4)=               &
     &    mol(npos)%res(1)%res_aas(j)%aa_rots(k)%rot_en(4)                 
		xb(j)%ec(k)%ed(l)%ee(m)%ef(n)%ep_en(5)=               &
     &    mol(npos)%res(1)%res_aas(j)%aa_rots(k)%rot_en(5)                 
		xb(j)%ec(k)%ed(l)%ee(m)%ef(n)%ep_en(6)=               &
     &    mol(npos)%res(1)%res_aas(j)%aa_rots(k)%rot_en(6)                 
		xb(j)%ec(k)%ed(l)%ee(m)%ef(n)%ep_en(7)=               &
     &    mol(npos)%res(1)%res_aas(j)%aa_rots(k)%rot_en(7)                 
    		xb(j)%ec(k)%ed(l)%ee(m)%ef(n)%ep_en(8)=               &
     &    mol(npos)%res(1)%res_aas(j)%aa_rots(k)%rot_en(8)                 
    		xb(j)%ec(k)%ed(l)%ee(m)%ef(n)%ep_en(9)=               &
     &    mol(npos)%res(1)%res_aas(j)%aa_rots(k)%rot_en(9)                 
    		xb(j)%ec(k)%ed(l)%ee(m)%ef(n)%ep_en(10)=               &
     &    mol(npos)%res(1)%res_aas(j)%aa_rots(k)%rot_en(10)                 
		xb(j)%ec(k)%ed(l)%ee(m)%ef(n)%ep_flag=.true.
		xb(j)%ec(k)%ed(l)%ee(m)%ef(n)%pair_flag= .true.
!
	       else if (npos==l+1) then
		    cycle
	       else
		    first1=.false.
		    first2=.false.
		    call energize(set1,set2,en,first1,first2)
!
!
		xb(j)%ec(k)%ed(l)%ee(m)%ef(n)%ep_en(1)= en(3)+en(4)+en(6)+en(9)
		xb(j)%ec(k)%ed(l)%ee(m)%ef(n)%ep_en(2)= en(3)+ en(4)+ en(9)
!
		xb(j)%ec(k)%ed(l)%ee(m)%ef(n)%ep_en(3)= en(3)
		xb(j)%ec(k)%ed(l)%ee(m)%ef(n)%ep_en(4)= en(4)
		xb(j)%ec(k)%ed(l)%ee(m)%ef(n)%ep_en(5)= 0
		xb(j)%ec(k)%ed(l)%ee(m)%ef(n)%ep_en(6)= en(6)
		xb(j)%ec(k)%ed(l)%ee(m)%ef(n)%ep_en(7)= 0
		xb(j)%ec(k)%ed(l)%ee(m)%ef(n)%ep_en(8)= 0
		xb(j)%ec(k)%ed(l)%ee(m)%ef(n)%ep_en(9)= en(9)
		xb(j)%ec(k)%ed(l)%ee(m)%ef(n)%ep_en(10)= en(10)
		xb(j)%ec(k)%ed(l)%ee(m)%ef(n)%ep_flag= .true.
		xb(j)%ec(k)%ed(l)%ee(m)%ef(n)%pair_flag= .true.
!
	       end if
!
		    end do
		  end do
	        end do
!
	     end do
	   end do
!
!        PRINT*, '        '
!        PRINT*, '#########################################'
!        PRINT*, '#  SUMMARY P_CALC_PAIRWISE_INTERACTIONS   #'
!        PRINT*, '#########################################'
!        PRINT*, '          '
!        PRINT*, '  TOTAL OF PAIRW. INTERACT. STORED', ncnt
!        PRINT*, '          '
!
	    END SUBROUTINE PDEE_PGOLD_PAIRWISE_INT
!
!
	END MODULE PARA_M
!
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE P_DEAD_END_ELIM()
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
!	Implementation according to equation 2' in   
!	De Maeyer, Desmet and Lasters, Methods in Molecular Biology, 
!	vol. 143, pp 265
!
	USE MOLEC_M
	USE EP_DATA_M 
	USE MUMBO_DATA_M
	USE MAX_DATA_M
	USE PARA_M
!$      USE OMP_LIB
!
	IMPLICIT NONE
!
	integer :: i, j, k, l, m, n
	integer :: ncyc, nelim
	logical :: rflag
	real, dimension (15)  :: en
	real :: en_min_sum_max, en_sum_min, en_sum_max
	real :: en_max, en_min, en_p

        TYPE (EP_C_T), DIMENSION(:), allocatable :: xb
	
!
!!$     integer :: omp_get_max_threads,omp_get_num_procs
!
	
!	
	PRINT*, '        '
	PRINT*, '##################################'
	PRINT*, '#    SUMMARY P_DEAD_END_ELIM     #'
	PRINT*, '##################################'
	PRINT*, '          '
!
	ncyc=0
!
!$    print*,' WILL USE ',omp_get_max_threads(),' THREADS ON ',omp_get_num_procs(), &
!$           ' CPUs; size(mol)=',size(mol)
!$    print*,'  '

	do
	 ncyc=ncyc+1
	 nelim=0

!$omp  parallel                                                              &
!$omp& private(i,j,k,l,m,n,en_min_sum_max,en_sum_min,en_sum_max,             &
!$omp&         en_p,en_max,en_min,rflag,xb)                                  &
!$omp& shared(nelim,mol)                                                     &
!$omp& default(none) !num_threads(1)                                          
!$omp  do schedule(guided,1)
	 
	do i=2,size(mol) 
	
	    if(mol(i)%res(1)%res_naa==1.and.mol(i)%res(1)%res_aas(1)%aa_nrt==1) then
	      cycle
	    end if 
!
	    call PDEE_PGOLD_CLEAN(xb)
	    call PDEE_PGOLD_PAIRWISE_INT(i,xb)
!
	    en_min_sum_max = size(mol)* 1000000
!
            do j=1,mol(i)%res(1)%res_naa
               do k=1,mol(i)%res(1)%res_aas(j)%aa_nrt
!
                      rflag= mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_flag
                      if (.NOT.rflag) then
		          cycle
		      end if
!
		      en_sum_min = 0
		      en_sum_max = 0
!
	       do l=2,size(mol) 
	          if (l==i) then
	              cycle
	          end if
	          en_max= -100000 * size(mol)
	          en_min=  100000 * size(mol)
            	  do m=1,mol(l)%res(1)%res_naa
                     do n=1,mol(l)%res(1)%res_aas(m)%aa_nrt
!
                  rflag= mol(l)%res(1)%res_aas(m)%aa_rots(n)%rot_flag
!
				 if (.NOT.rflag) cycle
!
		en_p= xb(j)%ec(k)%ed(l-1)%ee(m)%ef(n)%ep_en(1)
!
				 if (en_p > en_max) then
				     en_max = en_p
				 end if
!
				 if (en_p < en_min) then
				     en_min = en_p
				 end if
!
		      end do
		   end do
			     en_sum_min = en_sum_min + en_min
			     en_sum_max = en_sum_max + en_max
	       end do
		     en_sum_min = en_sum_min +                              &
     &		          xb(j)%ec(k)%ed(i-1)%ee(j)%ef(k)%ep_en(1)
 		     en_sum_max = en_sum_max +                              &
     &                    xb(j)%ec(k)%ed(i-1)%ee(j)%ef(k)%ep_en(1)
!
		     if (en_sum_max < en_min_sum_max) then
			   en_min_sum_max = en_sum_max
		     end if
!
		   mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_en(15)=en_sum_min
!			   
		   end do
	      end do
!
            do j=1,mol(i)%res(1)%res_naa
                do k=1,mol(i)%res(1)%res_aas(j)%aa_nrt
!
                 rflag= mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_flag
!
                 if (.NOT.rflag) cycle
!
	         en_sum_min=mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_en(15)
			if (en_sum_min > en_min_sum_max) then
!
			   mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_flag=.false.
!			
!$omp critical (crit_e)
			   nelim= nelim +1
			   print*,'  ROTAMER DEAD END ELIMINATED:  '
			   print*,'  RESIDUE= ', mol(i)%res(1)%res_chid,' ',   & 
     &  		    mol(i)%res(1)%res_nold 
			   print*,'  AAS=     ', mol(i)%res(1)%res_aas(j)%aa_nam
			   print*, (i-1),'   1' , j , k
			   print*,'       ' 
!$omp end critical (crit_e)
!
			end if
		    end do
		end do
!
	call PDEE_PGOLD_CLEAN(xb)
!
	end do
!
!$omp end parallel
!
	 PRINT*, '  NUM. OF ROTAMERS ELIMINATED: ', nelim,' IN CYCLE: ', ncyc  
	 PRINT*, '        '
	 
	 if (nelim == 0) then
	   exit
	 end if
	end do

	END SUBROUTINE P_DEAD_END_ELIM
	
	
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC	
!
	SUBROUTINE P_GOLDSTEIN
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
!       searches for dead ending rotamers using goldstein�s equation for singles.
!       it states that a rotamer ir at position i cannot be part of the GEMC if
!       the following condition holds true: (comparision of rotamers ir and it)
!
!               E(ir) - E(js) + sum(j)( min(s) [E(ir,js) - E(it,js)]) > 0
	
	USE MOLEC_M
	USE EP_DATA_M
	USE MUMBO_DATA_M
	USE MAX_DATA_M
	USE PARA_M
!$      USE OMP_LIB
!
	IMPLICIT NONE
	
	INTEGER :: a, b, c, d, i, j, r, s   ! some do-loop indices
	INTEGER :: elim_num
	INTEGER :: nelim, ncyc, nelimtot
	REAL    :: energy_rot1, energy_rot2 ! self-energy of rotamer 1 and 2
	REAL    :: energy_min               ! minimum energy-expression in goldstein�s equation
	REAL    :: energy_sum               ! sum of minimum energy differences 
	REAL    :: energy_difference        ! difference of pairwise energies: E(ir,js) - E(it,js)
	REAL    :: energy_criterion
        TYPE (EP_C_T), DIMENSION(:), allocatable :: xb
!
!!$     integer :: omp_get_max_threads,omp_get_num_procs
!
	energy_rot1       = 0
	energy_rot2       = 0
	energy_sum        = 0
	energy_difference = 0
	energy_min        = 1.0e12
	energy_criterion  = 0
!	
!	
	PRINT*, '        '
	PRINT*, '################################'
	PRINT*, '#    SUMMARY GOLDSTEIN         #' 
	PRINT*, '################################'
	PRINT*, '          '
!	
!
	ncyc=0
!
!$    print*,' P_GOLDSTEIN: WILL USE ',omp_get_max_threads(),' THREADS ON ', &
!$           omp_get_num_procs(),' CPUs; size(mol)=',size(mol)
!$    print*,'  '

	 do
	 ncyc=ncyc+1
	 nelim=0
!
	 energy_rot1       = 0
	 energy_rot2       = 0
	 energy_sum        = 0
	 energy_difference = 0
	 energy_min        = 1.0e12
	 energy_criterion  = 0
!
! now, at each position i, take a rotamer and ask if it's dead ending compared
! with another rotamer at i
!
!
!$omp parallel                                                          &
!$omp& private(i,j,r,a,b,c,d,energy_rot1,energy_rot2,energy_sum,        &
!$omp&         energy_criterion,energy_difference,energy_min,xb)        &                                   
!$omp& shared(nelim,mol)                                                &
!$omp& default(none) !num_threads(1)
!$omp do schedule(guided,1)
!
	do i = 2, size(mol)
	  
	  if(mol(i)%res(1)%res_naa==1.and.mol(i)%res(1)%res_aas(1)%aa_nrt==1) then
	     cycle
	  end if 

	    call PDEE_PGOLD_CLEAN(xb)
	    call PDEE_PGOLD_PAIRWISE_INT(i,xb)
	
	  do a = 1,mol(i)%res(1)%res_naa
	    dee_loop: do b = 1,mol(i)%res(1)%res_aas(a)%aa_nrt
	      
	    ! take the next, if rotamer is already eliminated  
	      if ( .NOT. (mol(i)%res(1)%res_aas(a)%aa_rots(b)%rot_flag) ) cycle

	    ! get the self-energy of rotamer 1  
	      energy_rot1 = xb(a)%ec(b)%ed(i-1)%ee(a)%ef(b)%ep_en(1)
	    
	    ! now take another rotamer at i for comparison
	      do c = 1,mol(i)%res(1)%res_naa
	        do d = 1,mol(i)%res(1)%res_aas(c)%aa_nrt
		
		! if the two rotamers are identical, take the next
		  if ( c == a .and. d == b ) cycle

		! again, don't consider rotamers already eliminated
	          if ( .NOT. (mol(i)%res(1)%res_aas(c)%aa_rots(d)%rot_flag) ) cycle

		  energy_rot2 = xb(c)%ec(d)%ed(i-1)%ee(c)%ef(d)%ep_en(1)

		 ! now, consider all rotamers at all other positions j .ne. i and
		 ! calculate the expression as stated in goldsteins equation  
!		  
		  energy_sum        = 0
		  energy_criterion  = 0
!
		  do j = 2, size(mol)
!		    
		    energy_difference = 0 
		    energy_min        = 1.0e12
!		    
		    if ( j == i ) cycle
		    do r = 1, mol(j)%res(1)%res_naa
		      do s = 1, mol(j)%res(1)%res_aas(r)%aa_nrt
	                
			if ( .NOT. (mol(j)%res(1)%res_aas(r)%aa_rots(s)%rot_flag) ) cycle
		        
			! calculate the energy difference E(ir,js) - E(it,js)

			energy_difference = xb(a)%ec(b)%ed(j-1)%ee(r)%ef(s)%ep_en(1)	&
		&	                  - xb(c)%ec(d)%ed(j-1)%ee(r)%ef(s)%ep_en(1)


		        ! if the energy difference is .lt. the actual minimum then
			! take it as new minimum
			if ( energy_difference .lt. energy_min ) then
			  energy_min = energy_difference
			end if
			    
		      end do
		    end do

		    ! now, summation of all terms: sum[min(s)[E(ir,js) - E(it, js)]
		    energy_sum = energy_sum + energy_min
		    
		  end do
!		    
		    energy_criterion = energy_rot1 - energy_rot2 + energy_sum
		    
		    if ( energy_criterion .gt. 0 ) then
!			
		      mol(i)%res(1)%res_aas(a)%aa_rots(b)%rot_flag = .false.
!
!$omp critical (crit_e)
!			
			   nelim= nelim +1
		      	   print*,'  ROTAMER DEAD END ELIMINATED:  '
			   print*,'  RESIDUE= ', mol(i)%res(1)%res_chid,' ',       &
      		&          mol(i)%res(1)%res_nold
			   print*,'  AAS=     ', mol(i)%res(1)%res_aas(a)%aa_nam
			   print*, (i-1),'   1' , a , b
			   print*,'       ' 
!
!$omp end critical (crit_e)
!
		      cycle dee_loop
		      
		      
		    end if
		
		end do
	      end do  
	    end do dee_loop
	  end do  
	  call PDEE_PGOLD_CLEAN(xb)
	end do
!
!$omp end parallel
!
!
	 PRINT*, '  NUM. OF ROTAMERS ELIMINATED: ', nelim,' IN CYCLE: ', ncyc  
	 PRINT*, '        '
	 if (nelim == 0) then
	   exit
	 end if
	end do
!
	
	END SUBROUTINE P_GOLDSTEIN
	
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
	
!
