!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE MC_INTERACTION ()
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MOLEC_M
	USE MUMBO_DATA_M
	USE MAX_DATA_M
!
	IMPLICIT NONE
!
	integer :: i, j, k, m, n, ncnt, na , nb
	integer, dimension(4)     :: set1, set2
	logical                   :: first1, first2
	real, dimension(15)       :: en, entot
	integer, save             :: ncalls = 0
!
!	LOOP OVER ALL POSITIONS TO BE MUMBOED
!
	ncnt=0
!
	ncalls = ncalls +1
!
	do i=2,size(mol)
	    set1(1)=i
	    na=mol(i)%res(1)%res_num
	    do j=1,mol(i)%res(1)%res_naa
		set1(2)=1
		set1(3)=j
!
!	LOOP OVER ALL ROTAMERS AT THE VARIOUS POSITIONS
!
		do k=1, mol(i)%res(1)%res_aas(j)%aa_nrt
		   first1=.true.
		   set1(4)=k
		   do m=1,15
		     entot(m) = 0.0
		     mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_en(m)=0
	           end do
!
!		   LOOP OVER THE CONSTANT PART OF THE STRUCTURE
!
		   do m=1,mol(1)%mol_nrs
			nb=mol(1)%res(m)%res_num
!
			set2(1)=1
			set2(2)=m
			set2(3)=1
			set2(4)=1
!
			do n=1,size(en)
			  en(n)=0
			end do
!
			first2=.false.
			call energize(set1,set2,en,first1,first2)
!
!	RETURNING ENERGIES AS FOLLOWS
!	en(1) = 0
!	en(2) = 0
!	en(3) =  en_vdw
!	en(4) =  en_elec 
!	en(5) =  en_rprob
!	en(6) =  en_solv
!	en(7) =  en_solv_ref
!	en(8) =  en_xray
!	en(9) =  en_hbond
!	en(10) = en_solv_ana    for analysis purposes
!
			first1=.false.
			ncnt = ncnt +1
			entot(3) = entot(3) + en(3)
			entot(4) = entot(4) + en(4)
			entot(5) = entot(5) + en(5)
			entot(6) = entot(6) + en(6)
			entot(7) = entot(7) + en(7)
			entot(8) = entot(8) + en(8)
			entot(9) = entot(9) + en(9)
			entot(10) = entot(10) + en(10)
!
		   end do
!
!	STORING ENERGIES AS FOLLOWS: 
!
!	entot(1) = TEMPLATE INTERACTION + SOLV + SOLVREF
!	entot(2) = TEMPLATE INTERACTION (VDW+ELEC+RPROB+XRAY+HBOND) 
!	entot(3) = VDW - MAIN CHAIN
!	entot(4) = ELEC - MAIN CHAIN
!	entot(5) = RPROB - ROTAMER PROBABILITY
!	entot(6) = SOLVATION - TEMPLATE
!	entot(7) = SOLVATION - REF
!	entot(8) = XRAY - ROTAMER XRAY
!	entot(9) = HBOND
!	entot(10) = SOLVATON FOR ANA
!
		   entot(2) = entot(3) + entot(4) + entot(5) + entot(8) + entot(9)
		   entot(1) = entot(2) + entot(6) + entot(7) 
!
!   SOLV_REF AND SOLV CAN NOT BE PART OF THE TOTAL INTERACTION ENERGY 
!   entot(1) HERE BECAUSE ATOMS CONTRIBUTED BY SURROUNDING 
!   ROTAMERS ARE NOT TAKEN INTO ACCOUNT.
!   ENTOT(1) CONTAINS en_vdw, en_elec, en_rprob and en_xray
!
!   SOLV_REF AND SOLV ADDED INTO ENTOT(2) FOR LATER USE
!
	           mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_en(1)=entot(1)
	           mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_en(2)=entot(2)
	           mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_en(3)=entot(3)
	           mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_en(4)=entot(4)
	           mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_en(5)=entot(5)
	           mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_en(6)=entot(6)
	           mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_en(7)=entot(7)
	           mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_en(8)=entot(8)
	           mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_en(9)=entot(9)
	           mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_en(10)=entot(10)
!
  	        end do 
	    end do
	end do
!
	PRINT*, '        '
	PRINT*, '###################################################'
	PRINT*, '# MC - SUMMARY INTERACTIONS WITH CONSTANT PART    #'
	PRINT*, '###################################################'
	PRINT*, '        '
!
	if (ncalls < 3 .or. mumbo_log_mc_flag) then
!
	PRINT*, '  INTERACTION WITH CONSTANT PART: '
	PRINT*, '  ------------------------------- '
!
	do i=2,size(mol)
	    do j=1,mol(i)%res(1)%res_naa
!
	PRINT*, '       '
	PRINT*, '  AMINOACID: ', mol(i)%res(1)%res_aas(j)%aa_nam,   &
     &             mol(i)%res(1)%res_chid, mol(i)%res(1)%res_nold 
	PRINT*, '     '
	PRINT*, '     E(TOT)[kcal/mol-1]   E(VDW)   E(ELEC)  E(RPROB)  E(HBOND)  E(XRAY)'
	PRINT*, '     '
!
		do k=1, mol(i)%res(1)%res_aas(j)%aa_nrt
!
	write(*,fmt='(a,6F9.3)') '   ROT ENERGY: ',      &
     &  mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_en(2),     &
     &  mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_en(3),     &
     &  mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_en(4),     &
     &  mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_en(5),     &
     &  mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_en(9),     &   
     &  mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_en(8)   
!
  	        end do 
	    end do
	end do
!
	if (solv_flag) then
!
	PRINT*, '     '
	PRINT*, '  SOLVATION ENERGY:   '
	PRINT*, '  -----------------   '
!
	do i=2,size(mol)
	    do j=1,mol(i)%res(1)%res_naa
!
	PRINT*, '       '
	PRINT*, '  AMINOACID: ', mol(i)%res(1)%res_aas(j)%aa_nam,   &
     &             mol(i)%res(1)%res_chid, mol(i)%res(1)%res_nold 
	PRINT*, '     '
	PRINT*, '       E(MCb#)[kcal/mol-1] E(TOT)   E(SOLV#) E(SOLV_REF) '
	PRINT*, '     '
!
		do k=1, mol(i)%res(1)%res_aas(j)%aa_nrt
!
	write(*,fmt='(a,4F9.3)') '   ROT ENERGY: ',      &
     &  mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_en(1),     &
     &  mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_en(2),     &
     &  mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_en(6),     &
     &  mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_en(7)    
!
  	        end do 
	    end do
	end do
!
	end if
!
	end if
!
	PRINT*, '        '
	PRINT*, '  DONE WITH CHECKING MC_INTERACTIONS       '
	PRINT*, '  TOTAL OF:', ncnt, ' INTERACTIONS CHECKED '
	PRINT*, '        '
!
	END SUBROUTINE MC_INTERACTION
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE FLAG_ROTAMER ()
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MOLEC_M
	USE MUMBO_DATA_M
	USE MAX_DATA_M
!
	IMPLICIT NONE
!
	integer :: i, j, k, tncnt, tnres, tnkeep, nx
	integer, dimension(:), allocatable :: ncnt, nres, nkeep
	logical, dimension(:), allocatable :: rec_flag
	real :: enx, en_min
	real, dimension(:), allocatable :: enm
	integer :: nrec
	real, parameter :: err4 = 0.0001
!
	enx=0
	nrec = 0
!
	nx=size(mol)
!
	if (allocated(ncnt))     deallocate(ncnt)
	if (allocated(nres))     deallocate(nres)
	if (allocated(nkeep))    deallocate(nkeep)
	if (allocated(rec_flag)) deallocate(rec_flag)
	if (allocated(enm))      deallocate(enm)
!
	allocate(ncnt(nx))
	allocate(nres(nx))
	allocate(nkeep(nx))
	allocate(rec_flag(nx))
	allocate(enm(nx))
!
	do, i=1,size(mol)
	   rec_flag(i) = .false.
	end do
!
	do i=2,size(mol)
	    ncnt(i)=0
	    nres(i)=0
            nkeep(i)=0
	    rec_flag(i)=.false.
	    enm(i) = 0
 	    en_min = 10000000
!
            do j=1,mol(i)%res(1)%res_naa
                do k=1,mol(i)%res(1)%res_aas(j)%aa_nrt
	           nres(i)=nres(i)+1
		   enx=mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_en(2)
		   if (enx < en_min) then 
                         en_min = enx
		   end if
		   if (enx >= mumbo_en_cut) then
		        mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_flag=.false.
	                ncnt(i)=ncnt(i)+1
		   else 
			nkeep(i)=nkeep(i)+1
		   end if
                end do
            end do
!
	    if (mumbo_en_mc_recover_flag.and.nkeep(i)==0) then
             do j=1,mol(i)%res(1)%res_naa
                do k=1,mol(i)%res(1)%res_aas(j)%aa_nrt
		   enx=mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_en(2)
!                  if (enx == en_min) then
		   if (abs(enx - en_min).le.err4) then
		       mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_flag=.true.
	               ncnt(i)=ncnt(i) - 1
		       nkeep(i)=nkeep(i) + 1
		       nrec= nrec + 1
		       rec_flag(i) = .true.
		       enm(i) = enx
		   end if
                end do
             end do
	    end if
!
        end do
!
	tnres=0
	tnkeep=0
	tncnt=0
!
	do i=2,size(mol)
	 	tnres=tnres+nres(i)
	 	tncnt=tncnt+ncnt(i)
	 	tnkeep=tnkeep+nkeep(i)
	end do
!
	PRINT*, '        '
	PRINT*, '################################'
	PRINT*, '#    SUMMARY FLAG_ROTAMER      #'
	PRINT*, '################################'
	PRINT*, '          '
!
	PRINT*, '  NUMBER OF AA*NROT  FOUND IN TOTAL:    ', tnres
	PRINT*, '  NUMBER FLAGGED TO GO IN TOTAL:        ', tncnt
	PRINT*, '  NUMBER KEPT IN TOTAL:                 ', tnkeep
	PRINT*, '  ENERGY CUT-OFF APPLIED FROM INPUT:    ', mumbo_en_cut
	PRINT*, '        '
!
	if (nrec /= 0) then
	PRINT*, '  **** WARNING **** '
	PRINT*, '  ****      '
	PRINT*, '  **** AUTO_RECOVERY OPTION USED FOR A TOTAL OF ',nrec,' POSITIONS'
        PRINT*, '  **** HERE E MIN > E CUT-OFF '
        PRINT*, '  **** ROTAMER WITH LOWEST ENERGY KEPT AT THIS POSITION '
	PRINT*, '  ****      '
	PRINT*, '  **** DONE SO FOR POSITIONS, E-MIN: '
	do, i=1,size(mol)
	   if (rec_flag(i)) then
	     PRINT*, '  **** ', mol(i)%res(1)%res_chid, mol(i)%res(1)%res_nold, enm(i)  
	   end if
	end do
	PRINT*, '  ****      '
	PRINT*, '  **** WARNING **** '
	PRINT*, '        '
	end if
!
	if (allocated(ncnt)) deallocate(ncnt)
	if (allocated(nres)) deallocate(nres)
	if (allocated(nkeep)) deallocate(nkeep)
	if (allocated(rec_flag)) deallocate(rec_flag)
	if (allocated(enm)) deallocate(enm)
!
!
	END SUBROUTINE FLAG_ROTAMER
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE CALC_PAIRWISE_INT()
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!	
	USE MOLEC_M
	USE EP_DATA_M
	USE MAX_DATA_M
	USE MB_M
	USE SPLIT_M
        USE SPLIT_POS_M
!
	IMPLICIT NONE
!
	integer :: a,b,c,d,i,j,k,l, m, n, ncnt, na, ncheck
	integer, dimension(4) :: set1, set2
	integer :: split_pos
	logical :: first1, first2
	real, dimension(15) :: en
!
	if (allocated(p_ij)) deallocate(p_ij)
	allocate(p_ij(size(mol),size(mol)))
!
	if (allocated(ea)) then
	do i=1,size(ea)
	  do j=1, size(ea(i)%eb)
	    do k=1, size(ea(i)%eb(j)%ec)
	       do l=1, size(ea(i)%eb(j)%ec(k)%ed)
		 do m=1, size(ea(i)%eb(j)%ec(k)%ed(l)%ee)
		  deallocate(ea(i)%eb(j)%ec(k)%ed(l)%ee(m)%ef,stat=ncheck)
		  if(ncheck/=0) stop '1Fehler beim Deallokieren. Programmabbruch'
	         end do
		 deallocate(ea(i)%eb(j)%ec(k)%ed(l)%ee,stat=ncheck)
		 if(ncheck/=0) stop '2Fehler beim Deallokieren. Programmabbruch'
	       end do
	       deallocate(ea(i)%eb(j)%ec(k)%ed,stat=ncheck)
	       if(ncheck/=0) stop '3Fehler beim Deallokieren. Programmabbruch'
	    end do
	    deallocate(ea(i)%eb(j)%ec,stat=ncheck)
	    if(ncheck/=0) stop '4Fehler beim Deallokieren. Programmabbruch'
	  end do
	  deallocate(ea(i)%eb,stat=ncheck)
	  if(ncheck/=0) stop '5Fehler beim Deallokieren. Programmabbruch'
	end do
	deallocate(ea,stat=ncheck)
        if(ncheck/=0) stop '6Fehler beim Deallokieren. Programmabbruch'
	end if
!
!	if (allocated(ea)) then
!		deallocate(ea,stat=ncheck)
!	        if (ncheck/=0) PRINT*,'DEALLOCATION ERROR IN CALC_PAIRWISE_INT' 
!	        if (ncheck/=0) STOP 
!	end if
!
	allocate(ea(size(mol)),stat=ncheck)
	if (ncheck/=0) PRINT*,'ALLOCATION ERROR IN CALC_PAIRWISE_INT' 
	if (ncheck/=0) stop 
	do i=1,size(mol)
	  if (i==1) then 
	   allocate(ea(i)%eb(1),stat=ncheck)
	   if (ncheck/=0) PRINT*,'ALLOCATION ERROR IN CALC_PAIRWISE_INT' 
	   if (ncheck/=0) stop 
	  else
	   allocate(ea(i)%eb(mol(i)%res(1)%res_naa),stat=ncheck)
	   if (ncheck/=0) PRINT*,'ALLOCATION ERROR IN CALC_PAIRWISE_INT' 
	   if (ncheck/=0) stop 
	  end if
          do j=1,mol(i)%res(1)%res_naa
	    allocate(ea(i)%eb(j)%ec(mol(i)%res(1)%res_aas(j)%aa_nrt),stat=ncheck)
	    if (ncheck/=0) PRINT*,'ALLOCATION ERROR IN CALC_PAIRWISE_INT' 
	    if (ncheck/=0) stop 
           do k=1, mol(i)%res(1)%res_aas(j)%aa_nrt
!
	      allocate(ea(i)%eb(j)%ec(k)%ed(size(mol)),stat=ncheck)
	      if (ncheck/=0) PRINT*,'ALLOCATION ERROR IN CALC_PAIRWISE_INT' 
	      if (ncheck/=0) stop 
            do l=1,size(mol)
	 	if (l==1) then 
	          allocate(ea(i)%eb(j)%ec(k)%ed(l)%ee(1),stat=ncheck)
	          if (ncheck/=0) PRINT*,'ALLOCATION ERROR IN CALC_PAIRWISE_INT' 
	          if (ncheck/=0) stop 
	        else
	       allocate(ea(i)%eb(j)%ec(k)%ed(l)%ee(mol(l)%res(1)%res_naa),stat=ncheck)
	          if (ncheck/=0) PRINT*,'ALLOCATION ERROR IN CALC_PAIRWISE_INT' 
	          if (ncheck/=0) stop 
   		end if
                do m=1,mol(l)%res(1)%res_naa
		   na=mol(l)%res(1)%res_aas(m)%aa_nrt
		   allocate(ea(i)%eb(j)%ec(k)%ed(l)%ee(m)%ef(na),stat=ncheck)
	           if (ncheck/=0) PRINT*,'ALLOCATION ERROR IN CALC_PAIRWISE_INT' 
	           if (ncheck/=0) stop 
		end do
	      end do
!
	    end do
	  end do
	end do
!
!       In SUBROUTINE CONF_SPLIT for every rotamer at i the min term from goldsteins
!       criterion have to be precalculated. Now allocate the array in which this information
!       can be stored.
!
	if (allocated(sa)) then
	    deallocate(sa, stat=ncheck)
	    if (ncheck .ne. 0) stop 'DEALLOCATION ERROR IN CALC_PAIRWISE_INT:s'
	end if
!
	allocate(sa(size(mol)), stat=ncheck)
	if (ncheck .ne. 0) stop 'ALLOCATION ERROR IN CALC_PAIRWISE_INT:s'
	do i=1, size(mol)
	  if (i==1) then
	    allocate(sa(i)%sb(1), stat=ncheck)
	    if (ncheck .ne. 0) stop 'ALLOCATION ERROR IN CALC_PAIRWISE_INT:s'
	  else
	    allocate(sa(i)%sb(mol(i)%res(1)%res_naa), stat=ncheck)
	    if (ncheck .ne. 0) stop 'ALLOCATION ERROR IN CALC_PAIRWISE_INT:s'
	  end if
	  do a=1, mol(i)%res(1)%res_naa
	    allocate(sa(i)%sb(a)%sc(mol(i)%res(1)%res_aas(a)%aa_nrt), stat=ncheck)
	    if (ncheck .ne. 0) stop 'ALLOCATION ERROR IN CALC_PAIRWISE_INT:s'
	    do b=1, mol(i)%res(1)%res_aas(a)%aa_nrt
	      allocate(sa(i)%sb(a)%sc(b)%sd(mol(i)%res(1)%res_naa), stat=ncheck)
	      if (ncheck .ne. 0) stop 'ALLOCATION ERROR IN CALC_PAIRWISE_INT:s'
	      do c=1, mol(i)%res(1)%res_naa
	        allocate(sa(i)%sb(a)%sc(b)%sd(c)%se(mol(i)%res(1)%res_aas(c)%aa_nrt))
		if (ncheck .ne. 0) stop 'ALLOCATION ERROR IN CALC_PAIRWISE_INT:s'
		do d=1, mol(i)%res(1)%res_aas(c)%aa_nrt
		  allocate(sa(i)%sb(a)%sc(b)%sd(c)%se(d)%sf(size(mol)), stat=ncheck)
		  if (ncheck .ne. 0) stop 'ALLOCATION ERROR IN CALC_PAIRWISE_INT:s'
		end do
	      end do
	    end do
	  end do
	end do
!
!       We need another data structure for usage in the SUBROUTINE CONF_SPLIT.
!       With help of an energy metric the most favorable splitting positions have to be calculated there.
!       The result for each rotamer at position i is stored in a data structure for which the 
!       memory is now allocated.
!        
        split_pos=3 !number of splitting positions needed in subroutine CONF_SPLIT
                
        if (allocated(pos)) then
          deallocate(pos,stat=ncheck)
          if (ncheck .ne. 0) stop 'DEALLOCATION ERROR IN CALC_PAIRWISE_INT: split'
        else
          allocate (pos(size(mol)), stat=ncheck)
          if (ncheck .ne. 0) stop 'ALLOCATION ERROR IN CALC_PAIRWISE_INT: split'
        end if
          do i=1,size(mol)
            if (i==1) then
              allocate (pos(i)%aa(1), stat=ncheck)
              if (ncheck .ne. 0) stop 'ALLOCATION ERROR IN CALC_PAIRWISE_INT: split'
            else
              allocate (pos(i)%aa(mol(i)%res(1)%res_naa), stat=ncheck)
              if (ncheck .ne. 0) stop 'ALLOCATION ERROR IN CALC_PAIRWISE_INT: split'
            end if
            do a=1,mol(i)%res(1)%res_naa
              allocate(pos(i)%aa(a)%ro(mol(i)%res(1)%res_aas(a)%aa_nrt), stat=ncheck)
              if (ncheck .ne. 0) stop 'ALLOCATION ERROR IN CALC_PAIRWISE_INT: split'
              do b=1,mol(i)%res(1)%res_aas(a)%aa_nrt
                allocate(pos(i)%aa(a)%ro(b)%split(split_pos), stat=ncheck)
                if (ncheck .ne. 0) stop 'ALLOCATION ERROR IN CALC_PAIRWISE_INT: split'
              end do
            end do
          end do
!
!
	ncnt=0
	do i=2,size(mol)
          do j=1,mol(i)%res(1)%res_naa
            do k=1, mol(i)%res(1)%res_aas(j)%aa_nrt
!
                set1(1)=i
                set1(2)=1
                set1(3)=j
                set1(4)=k
!	      
              do l=2,size(mol)
                do m=1,mol(l)%res(1)%res_naa
                  do n=1, mol(l)%res(1)%res_aas(m)%aa_nrt
                  set2(1)=l
                  set2(2)=1
                  set2(3)=m
                  set2(4)=n
                  ncnt=ncnt+1
!
         if (i==l.and.j==m.and.k==n) then
!
		ea(i)%eb(j)%ec(k)%ed(l)%ee(m)%ef(n)%ep_en(1)=               &
     &    mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_en(1)
		ea(i)%eb(j)%ec(k)%ed(l)%ee(m)%ef(n)%ep_en(2)=               &
     &    mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_en(2)
		ea(i)%eb(j)%ec(k)%ed(l)%ee(m)%ef(n)%ep_en(3)=               &
     &    mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_en(3)                 
		ea(i)%eb(j)%ec(k)%ed(l)%ee(m)%ef(n)%ep_en(4)=               &
     &    mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_en(4)                 
		ea(i)%eb(j)%ec(k)%ed(l)%ee(m)%ef(n)%ep_en(5)=               &
     &    mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_en(5)                 
		ea(i)%eb(j)%ec(k)%ed(l)%ee(m)%ef(n)%ep_en(6)=               &
     &    mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_en(6)                 
		ea(i)%eb(j)%ec(k)%ed(l)%ee(m)%ef(n)%ep_en(7)=               &
     &    mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_en(7)                 
    		ea(i)%eb(j)%ec(k)%ed(l)%ee(m)%ef(n)%ep_en(8)=               &
     &    mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_en(8)                 
    		ea(i)%eb(j)%ec(k)%ed(l)%ee(m)%ef(n)%ep_en(9)=               &
     &    mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_en(9)                 
    		ea(i)%eb(j)%ec(k)%ed(l)%ee(m)%ef(n)%ep_en(10)=               &
     &    mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_en(10)                 
		ea(i)%eb(j)%ec(k)%ed(l)%ee(m)%ef(n)%ep_flag=.true.
		ea(i)%eb(j)%ec(k)%ed(l)%ee(m)%ef(n)%pair_flag= .true.
!
	       else if (i==l) then
		    cycle
	       else
		    first1=.false.
		    first2=.false.
		    call energize(set1,set2,en,first1,first2)
!
!
!	ROTAMER PAIR ENERGY = EVDW(en(3)) + EELEC(en(4)) + ESOLV(en(6)) +
!                           EHBOND(en(9))
!
!	EPROB IS PART OF ENERGY TERM CONTAINING THE MC INTERACTION
!	EXRAY  ALSO PART OF MC INTERACTION TERM
!	ESOLV_REF  ALSO PART OF MC INTERACTION TERM
!
!	THE LATTER THREE WERE STORED ALREADY IN 
!		ea(i)%eb(j)%ec(k)%ed(i)%ee(j)%ef(k)%ep_en(1)
!
!	RETURNING ENERGIES AS FOLLOWS
!	en(1) =  0
!	en(2) =  0
!	en(3) =  en_vdw
!	en(4) =  en_elec 
!	en(5) =  en_rprob
!	en(6) =  en_solv
!	en(7) =  en_solv_ref
!	en(8) =  en_xray
!	en(9) =  en_hbond
!	en(10) = en_solv_ana  for analysis purposes (solvation of i in presence of j)
!
		ea(i)%eb(j)%ec(k)%ed(l)%ee(m)%ef(n)%ep_en(1)= en(3)+en(4)+en(6)+en(9)
!CHANGED
!		ea(i)%eb(j)%ec(k)%ed(l)%ee(m)%ef(n)%ep_en(2)= en(3)+en(4)+en(6)+en(9)
!TO
		ea(i)%eb(j)%ec(k)%ed(l)%ee(m)%ef(n)%ep_en(2)= en(3)+ en(4)+ en(9)
!
		ea(i)%eb(j)%ec(k)%ed(l)%ee(m)%ef(n)%ep_en(3)= en(3)
		ea(i)%eb(j)%ec(k)%ed(l)%ee(m)%ef(n)%ep_en(4)= en(4)
		ea(i)%eb(j)%ec(k)%ed(l)%ee(m)%ef(n)%ep_en(5)= 0
		ea(i)%eb(j)%ec(k)%ed(l)%ee(m)%ef(n)%ep_en(6)= en(6)
		ea(i)%eb(j)%ec(k)%ed(l)%ee(m)%ef(n)%ep_en(7)= 0
		ea(i)%eb(j)%ec(k)%ed(l)%ee(m)%ef(n)%ep_en(8)= 0
		ea(i)%eb(j)%ec(k)%ed(l)%ee(m)%ef(n)%ep_en(9)= en(9)
		ea(i)%eb(j)%ec(k)%ed(l)%ee(m)%ef(n)%ep_en(10)= en(10)
		ea(i)%eb(j)%ec(k)%ed(l)%ee(m)%ef(n)%ep_flag= .true.
		ea(i)%eb(j)%ec(k)%ed(l)%ee(m)%ef(n)%pair_flag= .true.
!
	       end if
!
		    end do
		  end do
	        end do
!
	     end do
	   end do
	end do
!
        PRINT*, '        '
        PRINT*, '#########################################'
        PRINT*, '#  SUMMARY CALC_PAIRWISE_INTERACTIONS   #'
        PRINT*, '#########################################'
        PRINT*, '          '
        PRINT*, '  TOTAL OF PAIRW. INTERACT. STORED', ncnt
        PRINT*, '          '
!
	END SUBROUTINE CALC_PAIRWISE_INT
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE DEAD_END_ELIM()
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
!	Implementation according to equation 2' in   
!	De Maeyer, Desmet and Lasters, Methods in Molecular Biology, 
!	vol. 143, pp 265
!
	USE MOLEC_M
	USE EP_DATA_M
	USE MUMBO_DATA_M
	USE MAX_DATA_M
!
	IMPLICIT NONE
!
	integer :: i, j, k, l, m, n
	integer :: ncyc, nelim
	logical :: rflag
!	real, dimension (15)  :: en
	real :: en_min_sum_max, en_sum_min, en_sum_max
	real :: en_max, en_min, en_p
!	
	PRINT*, '        '
	PRINT*, '################################'
	PRINT*, '#    SUMMARY DEAD_END_ELIM     #'
	PRINT*, '################################'
	PRINT*, '          '
!
	ncyc=0
	do
	 ncyc=ncyc+1
	 nelim=0
	do i=2,size(mol) 
!
	    en_min_sum_max = size(mol)* 1000000
!
	    if(mol(i)%res(1)%res_naa==1.and.mol(i)%res(1)%res_aas(1)%aa_nrt==1) then
	      cycle
	    end if 
!
            do j=1,mol(i)%res(1)%res_naa
               do k=1,mol(i)%res(1)%res_aas(j)%aa_nrt
!
                      rflag= mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_flag
                      if (.NOT.rflag) then
		          cycle
		      end if
!
		      en_sum_min = 0
		      en_sum_max = 0
!
	       do l=2,size(mol) 
	          if (l==i) then
	              cycle
	          end if
	          en_max= -100000 * size(mol)
	          en_min=  100000 * size(mol)
            	  do m=1,mol(l)%res(1)%res_naa
                     do n=1,mol(l)%res(1)%res_aas(m)%aa_nrt
!
                  rflag= mol(l)%res(1)%res_aas(m)%aa_rots(n)%rot_flag
!
				 if (.NOT.rflag) cycle
!
		en_p= ea(i)%eb(j)%ec(k)%ed(l)%ee(m)%ef(n)%ep_en(1)
!
				 if (en_p > en_max) then
				     en_max = en_p
				 end if
!
				 if (en_p < en_min) then
				     en_min = en_p
				 end if
!
		      end do
		   end do
			     en_sum_min = en_sum_min + en_min
			     en_sum_max = en_sum_max + en_max
	       end do
		     en_sum_min = en_sum_min +                              &
     &		          ea(i)%eb(j)%ec(k)%ed(i)%ee(j)%ef(k)%ep_en(1)
 		     en_sum_max = en_sum_max +                              &
     &                    ea(i)%eb(j)%ec(k)%ed(i)%ee(j)%ef(k)%ep_en(1)
!
		     if (en_sum_max < en_min_sum_max) then
			   en_min_sum_max = en_sum_max
		     end if
!
		   mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_en(15)=en_sum_min
!			   
		   end do
	      end do
!
            do j=1,mol(i)%res(1)%res_naa
                do k=1,mol(i)%res(1)%res_aas(j)%aa_nrt
!
                 rflag= mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_flag
                 if (.NOT.rflag) cycle
!
	         en_sum_min=mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_en(15)
!
			if (en_sum_min > en_min_sum_max) then
!			
			   nelim= nelim +1
			   mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_flag=.false.
			   print*,'  ROTAMER DEAD END ELIMINATED:  '
			   print*,'  RESIDUE= ', mol(i)%res(1)%res_chid,' ',   & 
     &  		    mol(i)%res(1)%res_nold 
			   print*,'  AAS=     ', mol(i)%res(1)%res_aas(j)%aa_nam
			   print*, (i-1),'   1' , j , k
 			   print*,'       ' 
			end if
		    end do
		end do
!
	end do
	 PRINT*, '  NUM. OF ROTAMERS ELIMINATED: ', nelim,' IN CYCLE: ', ncyc  
	 PRINT*, '        '
	 if (nelim == 0) then
	   exit
	 end if
	end do
!
	END SUBROUTINE DEAD_END_ELIM
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE GOLDSTEIN
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
!       searches for dead ending rotamers using goldstein�s equation for singles.
!       it states that a rotamer ir at position i cannot be part of the GEMC if
!       the following condition holds true: (comparision of rotamers ir and it)
!
!               E(ir) - E(js) + sum(j)( min(s) [E(ir,js) - E(it,js)]) > 0
	
	USE MOLEC_M
	USE EP_DATA_M
	USE MUMBO_DATA_M
	USE MAX_DATA_M
!
	IMPLICIT NONE
	
	INTEGER :: a, b, c, d, i, j, r, s   ! some do-loop indices
	INTEGER :: nelim, ncyc  
	REAL    :: energy_rot1, energy_rot2 ! self-energy of rotamer 1 and 2
	REAL    :: energy_min               ! minimum energy-expression in goldstein�s equation
	REAL    :: energy_sum               ! sum of minimum energy differences 
	REAL    :: energy_difference        ! difference of pairwise energies: E(ir,js) - E(it,js)
	REAL    :: energy_criterion
!
!
	energy_rot1       = 0
	energy_rot2       = 0
	energy_sum        = 0
	energy_difference = 0
	energy_min        = 1.0e12
	energy_criterion  = 0
!	
!	
	PRINT*, '        '
	PRINT*, '################################'
	PRINT*, '#    SUMMARY GOLDSTEIN         #' 
	PRINT*, '################################'
	PRINT*, '          '
!	
!
	ncyc=0
!
	 do
	 ncyc=ncyc+1
	 nelim=0
!
		energy_rot1       = 0
		energy_rot2       = 0
		energy_sum        = 0
		energy_difference = 0
		energy_min        = 1.0e12
		energy_criterion  = 0
!
	! now, at each position i, take a rotamer and ask if it�s dead ending compared
	! with another rotamer at i
!
	do i = 2, size(mol)
	  
	    if(mol(i)%res(1)%res_naa==1.and.mol(i)%res(1)%res_aas(1)%aa_nrt==1) then
	      cycle
	    end if 
	
	  do a = 1,mol(i)%res(1)%res_naa
	    dee_loop: do b = 1,mol(i)%res(1)%res_aas(a)%aa_nrt
	      
	    ! take the next, if rotamer is allready eliminated  
	      if ( .NOT. (mol(i)%res(1)%res_aas(a)%aa_rots(b)%rot_flag) ) cycle

	    ! get the self-energy of rotamer 1  
	      energy_rot1 = ea(i)%eb(a)%ec(b)%ed(i)%ee(a)%ef(b)%ep_en(1)
	    
	    ! now take another rotamer at i for comparison
	      do c = 1,mol(i)%res(1)%res_naa
	        do d = 1,mol(i)%res(1)%res_aas(c)%aa_nrt
		
		! if the two rotamers are identical, take the next
		  if ( c == a .and. d == b ) cycle

		! again, don�t consider rotamers allready eliminated
	          if ( .NOT. (mol(i)%res(1)%res_aas(c)%aa_rots(d)%rot_flag) ) cycle

		  energy_rot2 = ea(i)%eb(c)%ec(d)%ed(i)%ee(c)%ef(d)%ep_en(1)

		 ! now, consider all rotamers at all other positions j .ne. i and
		 ! calculate the expression as stated in goldsteins equation  
!		  
		  energy_sum        = 0
		  energy_criterion  = 0
!
		  do j = 2, size(mol)
!		    
		    energy_difference = 0 
		    energy_min        = 1.0e12
!		    
		    if ( j == i ) cycle
		    do r = 1, mol(j)%res(1)%res_naa
		      do s = 1, mol(j)%res(1)%res_aas(r)%aa_nrt
	                
			if ( .NOT. (mol(j)%res(1)%res_aas(r)%aa_rots(s)%rot_flag) ) cycle
		        
			! calculate the energy difference E(ir,js) - E(it,js)

			energy_difference = ea(i)%eb(a)%ec(b)%ed(j)%ee(r)%ef(s)%ep_en(1)	&
		&	                  - ea(i)%eb(c)%ec(d)%ed(j)%ee(r)%ef(s)%ep_en(1)


		        ! if the energy difference is .lt. the actual minimum then
			! take it as new minimum
			if ( energy_difference .lt. energy_min ) then
			  energy_min = energy_difference
			end if
			    
		      end do
		    end do

		    ! now, summation of all terms: sum[min(s)[E(ir,js) - E(it, js)]
		    energy_sum = energy_sum + energy_min
		    
		  end do
!		    
		    energy_criterion = energy_rot1 - energy_rot2 + energy_sum
		    if ( energy_criterion .gt. 0 ) then
		      mol(i)%res(1)%res_aas(a)%aa_rots(b)%rot_flag = .false.
!		      
			   nelim= nelim +1
!
		      	   print*,'  ROTAMER DEAD END ELIMINATED:  '
			   print*,'  RESIDUE= ', mol(i)%res(1)%res_chid,' ',       &
      		&          mol(i)%res(1)%res_nold
			   print*,'  AAS=     ', mol(i)%res(1)%res_aas(a)%aa_nam
			   print*, (i-1),'   1' , a , b
			   print*,'       ' 
!		      
		      cycle dee_loop
		    end if
		
		end do
	      end do  
	    end do dee_loop
	  end do  
	end do
!   
	 PRINT*, '  NUM. OF ROTAMERS ELIMINATED: ', nelim,' IN CYCLE: ', ncyc  
	 PRINT*, '        '
	 if (nelim == 0) then
	   exit
	 end if
	end do
!
	
	END SUBROUTINE GOLDSTEIN
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE GOLDSTEIN_DB
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
!       searches for dead ending rotamers using goldstein�s equation for singles.
!       it states that a rotamer ir at position i cannot be part of the GEMC if
!       the following condition holds true: (comparision of rotamers ir and it)
!
!               E(ir) - E(js) + sum(j)( min(s) [E(ir,js) - E(it,js)]) > 0
	
	USE MOLEC_M
	USE EP_DATA_M
	USE MUMBO_DATA_M
	USE MAX_DATA_M
!
	IMPLICIT NONE
	
	INTEGER :: a, b, c, d, i, j, r, s   ! some do-loop indices
	INTEGER :: elim_num
	REAL    :: energy_rot1, energy_rot2 ! self-energy of rotamer 1 and 2
	REAL    :: energy_min               ! minimum energy-expression in goldstein�s equation
	REAL    :: energy_sum               ! sum of minimum energy differences 
	REAL    :: energy_difference        ! difference of pairwise energies: E(ir,js) - E(it,js)
	REAL    :: energy_criterion
!
!
	energy_rot1       = 0
	energy_rot2       = 0
	energy_sum        = 0
	energy_difference = 0
	energy_min        = 1.0e12
	energy_criterion  = 0
	
	
	PRINT*, '        '
	PRINT*, '##########################################################'
	PRINT*, '#    SUMMARY DOUBLES - GOLDSTEIN DOUBLES - GOLDSTEIN     #' 
	PRINT*, '##########################################################'
	PRINT*, '          '
!
!
	elim_num = 0
			
	! now, at each position i, take a rotamer and ask if it�s dead ending compared
	! with another rotamer at i
	do i = 2, size(mol)
!
	
	  do a = 1,mol(i)%res(1)%res_naa
	    dee_loop2: do b = 1,mol(i)%res(1)%res_aas(a)%aa_nrt
	      
	    ! take the next, if rotamer is allready eliminated  
	      if ( .NOT. (mol(i)%res(1)%res_aas(a)%aa_rots(b)%rot_flag) ) cycle
	      
	    ! get the self-energy of rotamer 1  
	      energy_rot1 = ea(i)%eb(a)%ec(b)%ed(i)%ee(a)%ef(b)%ep_en(1)
	    
	    ! now take another rotamer at i for comparison
	      do c = 1,mol(i)%res(1)%res_naa
	        do d = 1,mol(i)%res(1)%res_aas(c)%aa_nrt
		
		! if the two rotamers are identical, take the next
		  if ( c == a .and. d == b ) cycle

		! again, don�t consider rotamers allready eliminated
	          if ( .NOT. (mol(i)%res(1)%res_aas(c)%aa_rots(d)%rot_flag) ) cycle

		  energy_rot2 = ea(i)%eb(c)%ec(d)%ed(i)%ee(c)%ef(d)%ep_en(1)

		 ! now, consider all rotamers at all other positions j .ne. i and
		 ! calculate the expression as stated in goldsteins equation  
		  

		  energy_sum        = 0
		  energy_criterion  = 0

		  do j = 2, size(mol)
		    
		    energy_difference = 0
		    energy_min        = 1.0e12  
		    
		    if ( j == i ) cycle
		    do r = 1, mol(j)%res(1)%res_naa
		      do s = 1, mol(j)%res(1)%res_aas(r)%aa_nrt
		      
		      ! if rotamer at j builds a dee pair with rotamer at i
		      ! then take the next
		      
		        if ( .NOT. (ea(i)%eb(a)%ec(b)%ed(j)%ee(r)%ef(s)%pair_flag) ) cycle
			if ( .NOT. (mol(j)%res(1)%res_aas(r)%aa_rots(s)%rot_flag) )  cycle
		        
			! calculate the energy difference E(ir,js) - E(it,js)

			energy_difference = ea(i)%eb(a)%ec(b)%ed(j)%ee(r)%ef(s)%ep_en(1)	&
		&	                  - ea(i)%eb(c)%ec(d)%ed(j)%ee(r)%ef(s)%ep_en(1)
		        
		        ! if the energy difference is .lt. the actual minimum then
			! take it as new minimum
			if ( energy_difference .lt. energy_min ) then
			  energy_min = energy_difference
			end if
			    
		      end do
		    end do
		    
		    ! now, summation of all terms: sum[min(s)[E(ir,js) - E(it, js)]

		    energy_sum = energy_sum + energy_min
		    
		  end do
		    
		    energy_criterion = energy_rot1 - energy_rot2 + energy_sum
		    
		    if ( energy_criterion .gt. 0 ) then
		      mol(i)%res(1)%res_aas(a)%aa_rots(b)%rot_flag = .false.
		      elim_num = elim_num + 1
!
		      	   print*,'  ROTAMER DEAD END ELIMINATED:  '
			   print*,'  RESIDUE= ', mol(i)%res(1)%res_chid,' ',       &
      		&          mol(i)%res(1)%res_nold
			   print*,'  AAS=     ', mol(i)%res(1)%res_aas(a)%aa_nam
			   print*, (i-1),'   1' , a , b
 			   print*,'       ' 
!		      
		      cycle dee_loop2
		    end if		
		
		end do
	      end do  
	    end do dee_loop2
	  end do  
	end do		
	
	  if ( elim_num == 0 ) then
            MUMBO_DB_CONV = .true.
!
	    PRINT*, '  GOLDSTEIN DOUBLES .... CONVERGED .... '
!
          end if	
	
	END SUBROUTINE GOLDSTEIN_DB
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc        
        
    SUBROUTINE MB_SPLIT()
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
!   The SUBROUTINE MB_SPLIT() searches for magic bullet splitting positions for every rotamer ir
!   as described in Pierce, N. A.; Spriet, J.A.; Desmet J.; Mayo, S.L. J. Comp. Chem. 2000, 21, 999-1009
!
        USE MOLEC_M
	USE EP_DATA_M
	USE MUMBO_DATA_M
	USE MAX_DATA_M
	USE SPLIT_M
        USE SPLIT_POS_M
!
	IMPLICIT NONE
!
	INTEGER :: i, a, b, c, d, k, v, w
	REAL    :: energy_metric, energy_min
	REAL    :: energy1, energy2, energy3
	

	PRINT*, '        '
	PRINT*, '##########################################################'
	PRINT*, '#                 SUMMARY MB_SPLIT                       #' 
	PRINT*, '##########################################################'
	PRINT*, '          '	
        
        
        ! first, for each rotamer ir rank the positions ks with which the candidate rotamers it have
	! the strongest adverse interactions and store the result.

	do i = 2, size(mol)
	  do a = 1, mol(i)%res(1)%res_naa
	    do b = 1, mol(i)%res(1)%res_aas(a)%aa_nrt

              energy1 = 1.0e15
	      energy2 = 2.0e15
	      energy3 = 3.0e15
              pos(i)%aa(a)%ro(b)%split(1) = 1000000
              pos(i)%aa(a)%ro(b)%split(2) = 1000000
              pos(i)%aa(a)%ro(b)%split(3) = 1000000

	      do k = 2, size(mol)
                if ( k==i ) cycle
                energy_min = 4.0e15
                do c = 1, mol(i)%res(1)%res_naa
	          do d = 1, mol(i)%res(1)%res_aas(c)%aa_nrt
		    if (c==a .and. d==b) cycle
		      do v = 1, mol(k)%res(1)%res_naa
		        do w = 1, mol(k)%res(1)%res_aas(v)%aa_nrt

		          energy_metric = ea(i)%eb(a)%ec(b)%ed(k)%ee(v)%ef(w)%ep_en(1)	&
		&                       - ea(i)%eb(c)%ec(d)%ed(k)%ee(v)%ef(w)%ep_en(1)

                         if ( energy_metric < energy_min ) energy_min = energy_metric

		      
		        end do
		      end do
		    end do
		  end do
                  if ( energy_min < energy1 ) then
                    energy3 = energy2
                    energy2 = energy1
                    energy1 = energy_min
                    pos(i)%aa(a)%ro(b)%split(3) = pos(i)%aa(a)%ro(b)%split(2)
                    pos(i)%aa(a)%ro(b)%split(2) = pos(i)%aa(a)%ro(b)%split(1)
                    pos(i)%aa(a)%ro(b)%split(1) = k
                  else if ( energy_min >= energy1 .and. energy_min < energy2 ) then
                    energy3 = energy2
                    energy2 = energy_min
                    pos(i)%aa(a)%ro(b)%split(3) = pos(i)%aa(a)%ro(b)%split(2)
              	    pos(i)%aa(a)%ro(b)%split(2) = k
                  else if ( energy_min >= energy2 .and. energy_min < energy3 ) then
                    energy3 = energy_min
                    pos(i)%aa(a)%ro(b)%split(3) = k
                  end if             
                end do
	      end do
	    end do
	  end do    

          
	  
!	do i=2,size(mol)
!	  do a=1,mol(i)%res(1)%res_naa
!            do b=1,mol(i)%res(1)%res_aas(a)%aa_nrt
!              print *,'fuer i,a,b: ', i,a,b
!              print *,'split positions1: ',pos(i)%aa(a)%ro(b)%split(1)
!	      print *,'split positions2: ',pos(i)%aa(a)%ro(b)%split(2)
!	      print *,'split positions3: ',pos(i)%aa(a)%ro(b)%split(3)
!            end do
!          end do
!        end do
!              
    END SUBROUTINE MB_SPLIT
!
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

    SUBROUTINE CONF_SPLIT1()                                                 

!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!c

	USE MOLEC_M
	USE EP_DATA_M
	USE MUMBO_DATA_M
	USE MAX_DATA_M
	USE SPLIT_M
        USE SPLIT_POS_M
!
	IMPLICIT NONE
!
	INTEGER :: i, j, a, b, c, d, k, l, m, r, s
	REAL    :: energy, energy_min, energy_split, energy_template
        REAL    :: energy_sum, energy_expression
	LOGICAL :: elim
!
!	GFORTRAN
	j=-1000
!
	PRINT*, '        '
	PRINT*, '##########################################################'
	PRINT*, '#                 SUMMARY CONF_SPLIT1                    #' 
	PRINT*, '##########################################################'
	PRINT*, '          '	
!
!
	do i = 2, size(mol)
	  do a = 1, mol(i)%res(1)%res_naa
	    dee_loop: do b = 1, mol(i)%res(1)%res_aas(a)%aa_nrt
              if (.not.(mol(i)%res(1)%res_aas(a)%aa_rots(b)%rot_flag)) cycle
!              print *,'questionable rotamer: '
!              print *,'i,a,b: ',i,a,b
              do c = 1, mol(i)%res(1)%res_naa
	        do d = 1, mol(i)%res(1)%res_aas(c)%aa_nrt
		  if (.not.(mol(i)%res(1)%res_aas(c)%aa_rots(d)%rot_flag)) cycle
                  if ( c==a .and. d==b ) cycle
		  do j = 2, size(mol)
                    if ( j==i ) cycle
                    energy_min = 1.0e15
                    do r = 1, mol(j)%res(1)%res_naa
		      do s = 1, mol(j)%res(1)%res_aas(r)%aa_nrt
			if (.not.(mol(j)%res(1)%res_aas(r)%aa_rots(s)%rot_flag)) cycle  
			energy = ea(i)%eb(a)%ec(b)%ed(j)%ee(r)%ef(s)%ep_en(1) &
			     & - ea(i)%eb(c)%ec(d)%ed(j)%ee(r)%ef(s)%ep_en(1)
		        if ( energy < energy_min ) energy_min = energy

                      end do
		    end do
		    sa(i)%sb(a)%sc(b)%sd(c)%se(d)%sf(j)%minima = energy_min
		  end do
		end do
	      end do

              k = 1000000
              elim = .false.

!  split_loop: do z=1,3
   split_loop:  do k=2,size(mol)
                 if ( k==i .or. k==j ) cycle
!                if ( (pos(i)%aa(a)%ro(b)%split(z) .eq. 1000000) .or. (pos(i)%aa(a)%ro(b)%split(z) .eq. k) ) exit

!                k = pos(i)%aa(a)%ro(b)%split(z)

                do l=1,mol(k)%res(1)%res_naa
        pos_loop: do m=1,mol(k)%res(1)%res_aas(l)%aa_nrt
                    if (.not.(mol(k)%res(1)%res_aas(l)%aa_rots(m)%rot_flag)) cycle

                    elim = .false.

                    do c=1,mol(i)%res(1)%res_naa
                      do d=1,mol(i)%res(1)%res_aas(c)%aa_nrt
                        if (.not.(mol(i)%res(1)%res_aas(c)%aa_rots(d)%rot_flag)) cycle
                        if (c==a .and. d==b) cycle
                   
                          energy_template = ea(i)%eb(a)%ec(b)%ed(i)%ee(a)%ef(b)%ep_en(1)  &
                                 &        - ea(i)%eb(c)%ec(d)%ed(i)%ee(c)%ef(d)%ep_en(1)

                          energy_sum = 0
                          energy = 0

                          do j = 2, size(mol)
                            if (j==i .or. j==k) cycle
                            energy_sum = energy_sum + sa(i)%sb(a)%sc(b)%sd(c)%se(d)%sf(j)%minima
                          end do

                          energy       = energy_template + energy_sum

                          energy_split = ea(i)%eb(a)%ec(b)%ed(k)%ee(l)%ef(m)%ep_en(1) &
			           &   - ea(i)%eb(c)%ec(d)%ed(k)%ee(l)%ef(m)%ep_en(1) 

                          energy_expression = energy + energy_split

                          if ( energy_expression > 0 ) then
                            elim = .true.
                            cycle pos_loop
                          else
                            elim = .false.
                          end if  
                        end do
                      end do

                      if ( elim .eqv. .false. ) then
                        cycle split_loop
                      end if
                    end do pos_loop
                  end do

                  if ( elim .eqv. .true.) then
                    
                    mol(i)%res(1)%res_aas(a)%aa_rots(b)%rot_flag = .false.
                    
                    print*,'  ROTAMER DEAD END ELIMINATED:  '
                    print*,'  RESIDUE= ', mol(i)%res(1)%res_chid,' ',       &
         &          mol(i)%res(1)%res_nold
                    print*,'  AAS=     ', mol(i)%res(1)%res_aas(a)%aa_nam
                    print*, (i-1),'   1' , a , b
                    print*,'       ' 
                    
                    cycle dee_loop
                  end if
                end do split_loop
              end do dee_loop
            end do
          end do

	END SUBROUTINE CONF_SPLIT1
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!                
        SUBROUTINE CONF_SPLIT2
!       
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
!       The SUBROUTINE CONF_SPLIT2 performs magic bullet conformational 
!       splitting of 2nd order using two splitting positions per rotamer.
!       described in Pierce, N. A.; Spriet, J.A.; Desmet J.; Mayo, S.L. 
!       J. Comp. Chem. 2000, 21, 999-1009.
	
        USE MOLEC_M
	USE EP_DATA_M
	USE MUMBO_DATA_M
	USE MAX_DATA_M
	USE SPLIT_M
        USE SPLIT_POS_M
!
	IMPLICIT NONE
!
	INTEGER :: i, j, a, b, c, d, r, s
        INTEGER :: k1, k2, m1, m2, n1, n2
	REAL    :: energy, energy_min, energy_split
        REAL    :: energy_template, energy_sum, energy_expression
	LOGICAL :: elim

	PRINT*, '        '
	PRINT*, '##########################################################'
	PRINT*, '#                 SUMMARY CONF_SPLIT2                    #' 
	PRINT*, '##########################################################'
	PRINT*, '          '	
!
!
        
	do i = 2, size(mol)
	  do a = 1, mol(i)%res(1)%res_naa
	    dee_loop: do b = 1, mol(i)%res(1)%res_aas(a)%aa_nrt
              if (.not.(mol(i)%res(1)%res_aas(a)%aa_rots(b)%rot_flag)) cycle
              do c = 1, mol(i)%res(1)%res_naa
	        do d = 1, mol(i)%res(1)%res_aas(c)%aa_nrt
		  if (.not.(mol(i)%res(1)%res_aas(c)%aa_rots(d)%rot_flag)) cycle
                  if ( c==a .and. d==b ) cycle
		  do j = 2, size(mol)
                    if ( j==i ) cycle
                    energy_min = 1.0e15
                    do r = 1, mol(j)%res(1)%res_naa
		      do s = 1, mol(j)%res(1)%res_aas(r)%aa_nrt
			if(.not.(mol(j)%res(1)%res_aas(r)%aa_rots(s)%rot_flag)) cycle  
			energy = ea(i)%eb(a)%ec(b)%ed(j)%ee(r)%ef(s)%ep_en(1) &
			     & - ea(i)%eb(c)%ec(d)%ed(j)%ee(r)%ef(s)%ep_en(1)
		        if ( energy < energy_min ) energy_min = energy
		      
                      end do
		    end do

		    sa(i)%sb(a)%sc(b)%sd(c)%se(d)%sf(j)%minima = energy_min

		  end do
		end do
	      end do
	      

              elim = .false.
              
              k1 = pos(i)%aa(a)%ro(b)%split(1)
              k2 = pos(i)%aa(a)%ro(b)%split(2)
              
              if ( k1 == 100000 .or. k2 == 100000 .or. k1==k2 ) then
                elim = .false.
                cycle dee_loop
              end if
                do m1=1,mol(k1)%res(1)%res_naa
                  do n1=1,mol(k1)%res(1)%res_aas(m1)%aa_nrt
                    do m2=1,mol(k2)%res(1)%res_naa
                      split_loop: do n2=1,mol(k2)%res(1)%res_aas(m2)%aa_nrt
                      
                        do c=1,mol(i)%res(1)%res_naa
                          do d=1,mol(i)%res(1)%res_aas(c)%aa_nrt
                            if ( c==a .and. d==b ) cycle
                            if (.not.(mol(i)%res(1)%res_aas(c)%aa_rots(d)%rot_flag)) cycle
                              
                              energy_sum = 0
                              
                              !now, calculate the sum over all minima excluding positions i,k1 and k2.
                              
                              do j=2,size(mol)
                                if ( j==i .or. j==k1 .or. j==k2 ) cycle
!
                                energy_sum = energy_sum + sa(i)%sb(a)%sc(b)%sd(c)%se(d)%sf(j)%minima  
!
                              end do
                              
                              energy_template = ea(i)%eb(a)%ec(b)%ed(i)%ee(a)%ef(b)%ep_en(1)  &
                                     &        - ea(i)%eb(c)%ec(d)%ed(i)%ee(c)%ef(d)%ep_en(1)
                              
                              energy_split    = ea(i)%eb(a)%ec(b)%ed(k1)%ee(m1)%ef(n1)%ep_en(1) &
                                     &        - ea(i)%eb(c)%ec(d)%ed(k1)%ee(m1)%ef(n1)%ep_en(1) &
                                     &        + ea(i)%eb(a)%ec(b)%ed(k2)%ee(m2)%ef(n2)%ep_en(1) & 
                                     &        - ea(i)%eb(c)%ec(d)%ed(k2)%ee(m2)%ef(n2)%ep_en(1)
                              
                              energy_expression = energy_template + energy_sum + energy_split
                              
                              if ( energy_expression > 0 ) then 
                                elim = .true.
                                cycle split_loop
                              else
                                elim = .false.
                              end if
                              
                          end do
                        end do
                        if ( elim .eqv. .false. ) then
                          cycle dee_loop
                        end if
                      end do split_loop
                    end do
                  end do
                end do
              if ( elim .eqv. .true. ) then

                mol(i)%res(1)%res_aas(a)%aa_rots(b)%rot_flag = .false.
              
                print*,'  ROTAMER DEAD END ELIMINATED:  '
                print*,'  RESIDUE= ', mol(i)%res(1)%res_chid,' ',       &
     &          mol(i)%res(1)%res_nold
                print*,'  AAS=     ', mol(i)%res(1)%res_aas(a)%aa_nam
                print*, (i-1),'   1' , a , b
                print*,'       ' 
              
              end if
            end do dee_loop
          end do
        end do
        
        END SUBROUTINE CONF_SPLIT2
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE MB()
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!       
!	calculates the min and max terms for every rotamer pair and
!	searches for "magic bullet" pairs as described in
!	Gordon and Mayo, J. Comp. Chem., 19, 13, 1505-1514 (1998)
!
	
	USE MOLEC_M
	USE EP_DATA_M
	USE MUMBO_DATA_M
	USE MAX_DATA_M
	USE MB_M
!
	IMPLICIT NONE
	
	! first of all, calculate the min max terms
	! Emax(ir,js) = E(ir)+E(js)+E(ir,js) + sum(k)max(t)[E(ir,kt)+E(js,kt)]
	! Emin(ir,js) = E(ir)+E(js)+E(ir,js) + sum(k)min(t)[E(ir,kt)+E(js,kt)]
	
	real :: energy_pair
	real :: energy_min, energy_max
	real :: energy
	real :: energy_sum
	real :: energy_min_sum
	real :: energy_max_sum
	real :: energy_max_comp
	real :: energy_pair_max
	real :: energy_pair_min
	
	integer :: aa_1, rot_1, aa_2, rot_2
	integer :: i, j, a, b, r, s, k, l, m
!
!	GFORTRAN initilialisation of parameters that might never be initialised else
	rot_1 = -1000
	rot_2 = -1000
	aa_1 = -1000
	aa_2 = -1000
!	
	energy_pair = 0
	energy_min  = 1.0e12
	energy_max  = -1.0e12
	energy      = 0
	energy_sum  = 0
	energy_min_sum = 0
	energy_max_sum = 0
	energy_max_comp = 0
	energy_pair_max = 0
	energy_pair_min = 0
!	
	do i = 2, size(mol)
	  do j = i+1, size(mol)
!	  
	    energy_max_comp = 1.0e15
	    
	    do a = 1, mol(i)%res(1)%res_naa
	      do b = 1, mol(i)%res(1)%res_aas(a)%aa_nrt
		do r = 1, mol(j)%res(1)%res_naa
		  do s = 1, mol(j)%res(1)%res_aas(r)%aa_nrt
		    
		    energy_pair = ea(i)%eb(a)%ec(b)%ed(i)%ee(a)%ef(b)%ep_en(1)  &
		  &             + ea(j)%eb(r)%ec(s)%ed(j)%ee(r)%ef(s)%ep_en(1)  &
		  &             + ea(i)%eb(a)%ec(b)%ed(j)%ee(r)%ef(s)%ep_en(1)
		  

		    energy = 0
		    energy_max_sum = 0
		    energy_min_sum = 0
		    
		    do k = 2, size(mol)
		    
		      energy_min = 1.0e12
		      energy_max = -1.0e12
		      
		      if ( k == i .or. k == j ) cycle
		            
		      do l = 1, mol(k)%res(1)%res_naa
		        do m = 1, mol(k)%res(1)%res_aas(l)%aa_nrt
			  
			    energy = ea(i)%eb(a)%ec(b)%ed(k)%ee(l)%ef(m)%ep_en(1)  &
		          &        + ea(j)%eb(r)%ec(s)%ed(k)%ee(l)%ef(m)%ep_en(1)
			  
			    if ( energy < energy_min ) energy_min = energy
			    if ( energy > energy_max ) energy_max = energy


			end do
		      end do
		      
		      energy_max_sum = energy_max_sum + energy_max
		      energy_min_sum = energy_min_sum + energy_min
		      
		    end do
		    
		    energy_pair_max = energy_pair + energy_max_sum
		    energy_pair_min = energy_pair + energy_min_sum
    
		    ea(i)%eb(a)%ec(b)%ed(j)%ee(r)%ef(s)%pair_max = energy_pair_max
		    ea(i)%eb(a)%ec(b)%ed(j)%ee(r)%ef(s)%pair_min = energy_pair_min
		    ea(j)%eb(r)%ec(s)%ed(i)%ee(a)%ef(b)%pair_max = energy_pair_max
		    ea(j)%eb(r)%ec(s)%ed(i)%ee(a)%ef(b)%pair_min = energy_pair_min
        
		    if ( energy_pair_max < energy_max_comp ) then
		      
		      aa_1  = a
		      rot_1 = b
		      aa_2  = r
		      rot_2 = s
		      energy_max_comp = energy_pair_max
		    
		    end if
		  
		  end do
		end do
	      end do
	    end do
	  
	    ! now, store the "magic bullet" pair at ij for subsequent first order doubles calculation
!
!
	    p_ij(i,j)%max_energy = energy_max_comp
	    p_ij(i,j)%pair_aa1 = aa_1
	    p_ij(i,j)%pair_rot1 = rot_1    
	    p_ij(i,j)%pair_aa2 = aa_2
	    p_ij(i,j)%pair_rot2 = rot_2
!
	  end do
	end do
!	
	END SUBROUTINE MB
!
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

	SUBROUTINE DOUBLES()

!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!c
!c   searches for dead ending pairs using dead end elimination equation for doubles
!c
	USE MOLEC_M
	USE EP_DATA_M
	USE MUMBO_DATA_M
	USE MAX_DATA_M
!
	IMPLICIT NONE
!
	integer :: i, j, a, b, c, d, k, l, m, r, s, v, w
	real    :: energy_pair1, energy_pair2, energy_min_sum, energy_max_sum
	real    :: energy, energy_criterion, energy_max, energy_min
	real    :: energy_min_temp, energy_max_temp
!
!
	energy_pair1=     0 
	energy_pair2=     0 
	energy_min_sum=   0 
	energy_max_sum=   0
	energy=           0 
	energy_criterion= 0 
	energy_max= -1.0e12
	energy_min=  1.0e12
!
!    PRINT*, '        '
!    PRINT*, '###################################'
!    PRINT*, '#         SUMMARY DOUBLES         #'
!    PRINT*, '###################################'
!    PRINT*, '          '
!    
    do i = 2, size(mol)
      do j = i+1, size(mol)
      	do a = 1, mol(i)%res(1)%res_naa ! the following 4 do-loops take one 
	  do b = 1, mol(i)%res(1)%res_aas(a)%aa_nrt  ! specific rotamer-pair a time 
	    if (.not.(mol(i)%res(1)%res_aas(a)%aa_rots(b)%rot_flag)) then
!	     print *, 'cycle i, a, b : ', i, a, b
	     cycle ! if the rotamer is allready eliminated, take the next
	    end if
	    
	    do r = 1, mol(j)%res(1)%res_naa
	      pair_loop: do s = 1, mol(j)%res(1)%res_aas(r)%aa_nrt

	        if (.not.(ea(i)%eb(a)%ec(b)%ed(j)%ee(r)%ef(s)%pair_flag)) then
	         cycle
	        end if
	        if (.not.(mol(i)%res(1)%res_aas(a)%aa_rots(b)%rot_flag)) then
!		 print *, 'cycle i, a, b : ', i, a, b
	         cycle ! if the rotamer is allready eliminated, take the next
	        end if
		
		energy_pair1 = ea(i)%eb(a)%ec(b)%ed(i)%ee(a)%ef(b)%ep_en(1)    &
	&	             + ea(j)%eb(r)%ec(s)%ed(j)%ee(r)%ef(s)%ep_en(1)    &
	&	             + ea(i)%eb(a)%ec(b)%ed(j)%ee(r)%ef(s)%ep_en(1)		

	        do c = 1, mol(i)%res(1)%res_naa
	          do d = 1, mol(i)%res(1)%res_aas(c)%aa_nrt
	            if (.not.(mol(i)%res(1)%res_aas(c)%aa_rots(d)%rot_flag)) then
!	     	     print *, 'cycle i, a, b : ', i, a, b
	             cycle ! if the rotamer is allready eliminated, take the next
		    end if
		    do v = 1, mol(j)%res(1)%res_naa
		      do w = 1, mol(j)%res(1)%res_aas(v)%aa_nrt
	                if (.not.(ea(i)%eb(c)%ec(d)%ed(j)%ee(v)%ef(w)%pair_flag)) then
	                 cycle
	                end if
	                if (.not.(mol(j)%res(1)%res_aas(v)%aa_rots(w)%rot_flag)) then
!	     	            print *, 'cycle i, a, b : ', i, a, b
	                 cycle ! if the rotamer is allready eliminated, take the next
	                end if
			
			energy_pair2 = ea(i)%eb(c)%ec(d)%ed(i)%ee(c)%ef(d)%ep_en(1)    &
	&	                     + ea(j)%eb(v)%ec(w)%ed(j)%ee(v)%ef(w)%ep_en(1)    &
	&	                     + ea(i)%eb(c)%ec(d)%ed(j)%ee(v)%ef(w)%ep_en(1)

			energy_max_sum=0
			energy_min_sum=0
			energy_max=-1.0e12
			energy_min=1.0e12
						
			do k = 2, size(mol)
			  if ( k == i .or. k == j ) then
			   cycle
			  end if
			  do l = 1, mol(k)%res(1)%res_naa
			    do m = 1, mol(k)%res(1)%res_aas(l)%aa_nrt
			      if (.not.(mol(k)%res(1)%res_aas(l)%aa_rots(m)%rot_flag)) then
			       cycle
			      end if
			      
			      energy_min_temp = ea(i)%eb(a)%ec(b)%ed(k)%ee(l)%ef(m)%ep_en(1)    &
	&	                              + ea(j)%eb(r)%ec(s)%ed(k)%ee(l)%ef(m)%ep_en(1)
	
			      energy_max_temp = ea(i)%eb(c)%ec(d)%ed(k)%ee(l)%ef(m)%ep_en(1)    &
	&	                              + ea(j)%eb(v)%ec(w)%ed(k)%ee(l)%ef(m)%ep_en(1)

			      if ( energy_min_temp < energy_min ) then
			       energy_min = energy_min_temp
			      end if
			      if ( energy_max_temp > energy_max ) then
			       energy_max = energy_max_temp
			      end if			      
			    end do
			  end do
			  
			  energy_max_sum = energy_max_sum + energy_max
			  energy_min_sum = energy_min_sum + energy_min
			
			end do
		        
			energy_criterion = energy_pair1 - energy_pair2 + energy_min_sum - energy_max_sum
			
			if ( energy_criterion > 0 ) then
!
!			 write(6,*)'Doubles Criterion fulfilled !'
!			 write(6,*)'dee pair: '
!			 write(6,"(3I4)")i,a,b
!			 write(6,"(3I4)")j,r,s
!			 write(6,*)
!			 
			 ea(i)%eb(a)%ec(b)%ed(j)%ee(r)%ef(s)%pair_flag = .false.
		        
			 cycle pair_loop
			end if
		      
		      end do
		    end do
		  end do
	        end do
	      
	      end do pair_loop
	    end do
	    
	  end do
	end do
      end do
    end do
!        
    END SUBROUTINE DOUBLES
!   
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

    SUBROUTINE MB_DOUBLES()                                                 

!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!c
!c   searches for dead ending pairs using goldsteins dead end elimination 
!c   equation for doubles:
!c   
!c           [E(ir) + E(js) + E(ir,js)] - [E(iu) + E(jv) + E(iu,jv)] 
!c        +  sum {min(t) [E(ir,kt) + E(js,kt) - E(iu,kt) - E(jv,kt)]} > 0

	USE MOLEC_M
	USE EP_DATA_M
	USE MUMBO_DATA_M
	USE MAX_DATA_M
        USE MB_M
!
	IMPLICIT NONE
!
	integer :: i, j, a, b, k, l, m, r, s
	integer :: aa1, rot1, aa2, rot2
	real :: energy_mbpair, energy_pair, energy_min, energy_min_sum
	real :: energy, energy_criterion
!
!
	energy_pair      = 0
	energy_mbpair    = 0
	energy           = 0
	energy_min       = 1.0e12
	energy_min_sum   = 0
	energy_criterion = 0
	
	do i = 2, size(mol)                              ! loop over all residue-pairs ij
	  do j = i+1, size(mol)                          ! ij = ji -> only the half-matrix has to be considered
	    
	    aa1  = p_ij(i,j)%pair_aa1
	    rot1 = p_ij(i,j)%pair_rot1
	    aa2  = p_ij(i,j)%pair_aa2
	    rot2 = p_ij(i,j)%pair_rot2
		    
	    energy_mbpair = ea(i)%eb(aa1)%ec(rot1)%ed(i)%ee(aa1)%ef(rot1)%ep_en(1) &
		        & + ea(j)%eb(aa2)%ec(rot2)%ed(j)%ee(aa2)%ef(rot2)%ep_en(1) &
		        & + ea(i)%eb(aa1)%ec(rot1)%ed(j)%ee(aa2)%ef(rot2)%ep_en(1)

	    do a = 1, mol(i)%res(1)%res_naa              ! the following 4 do-loops take one 
	      do b = 1, mol(i)%res(1)%res_aas(a)%aa_nrt  ! rotamer-pair a time 
!	        if ( .not.(mol(i)%res(1)%res_aas(a)%aa_rots(b)%rot_flag) ) cycle
	        do r = 1, mol(j)%res(1)%res_naa
                  pair_loop: do s = 1, mol(j)%res(1)%res_aas(r)%aa_nrt	
		    ! if this pair is allready eliminated, take the next
		    
		    if ( a == aa1 .and. b == rot1 .and. r == aa2 .and. s == rot2 ) cycle
		    
		    if ( .not.(ea(i)%eb(a)%ec(b)%ed(j)%ee(r)%ef(s)%pair_flag) ) then
		      cycle 
		    end if
		    
		    ! calculate the energy of pair: E(ir) + E(js) + E(ir,js)
		    
		    energy_pair = ea(i)%eb(a)%ec(b)%ed(i)%ee(a)%ef(b)%ep_en(1) &
		              & + ea(j)%eb(r)%ec(s)%ed(j)%ee(r)%ef(s)%ep_en(1) &
		              & + ea(i)%eb(a)%ec(b)%ed(j)%ee(r)%ef(s)%ep_en(1)

		    energy_min_sum   = 0
		    energy_criterion = 0

		    ! now walk through all positions k .ne. i, j and caculate the energy-criterion
		    ! according to goldsteins equation for doubles    
			    
		    do k = 2, size(mol)
		      if ( k == i .or. k == j ) then
		        cycle
		      end if
			      
		      energy_min = 1.0e12  
			    
		      do l = 1, mol(k)%res(1)%res_naa
		        do m = 1, mol(k)%res(1)%res_aas(l)%aa_nrt
			  
!			  if ( .not.(ea(i)%eb(c)%ec(d)%ed(j)%ee(v)%ef(w)%pair_flag) ) cycle
			  if ( .not.(ea(i)%eb(a)%ec(b)%ed(k)%ee(l)%ef(m)%pair_flag) ) cycle
			  if ( .not.(ea(j)%eb(r)%ec(s)%ed(k)%ee(l)%ef(m)%pair_flag) ) cycle  
			  ! calculate the energy-difference:
			  ! E(ir,kt) + E(js,kt) - E(iu,kt) - E(jv,kt)
				    
			  energy = ea(i)%eb(a)%ec(b)%ed(k)%ee(l)%ef(m)%ep_en(1) &
		&	         + ea(j)%eb(r)%ec(s)%ed(k)%ee(l)%ef(m)%ep_en(1) &
		&	         - ea(i)%eb(aa1)%ec(rot1)%ed(k)%ee(l)%ef(m)%ep_en(1) &
		&		 - ea(j)%eb(aa2)%ec(rot2)%ed(k)%ee(l)%ef(m)%ep_en(1)

				  ! search for the lowest energy-difference
				  
			  if ( energy < energy_min ) then
			    energy_min = energy
			  end if
				
			end do
		      end do
			    
		      ! now, for all positions k .ne. i,j calculate
		      ! the sum-term: sum(k){min(t)[(E(ir,kt) + E(js,kt) - E(iu,kt) - E(jv,kt)]}
		      energy_min_sum = energy_min_sum + energy_min
		    
		    end do
			    
		    ! evaluate the energy-criterion:
		    !      [E(ir) + E(js) + E(ir,js)]  -  [E(iu) + E(jv) + E(iu,jv)]
		    !   +  sum(k){min(t)[(E(ir,kt) + E(js,kt) - E(iu,kt) - E(jv,kt)]} > 0
		    
		    energy_criterion = energy_pair - energy_mbpair + energy_min_sum
		    
		    if ( energy_criterion .gt. 0 ) then
		      
		      ea(i)%eb(a)%ec(b)%ed(j)%ee(r)%ef(s)%pair_flag = .false.
		      
	!	      write(6,*) 'energy_criterion fulfilled !'
	!	      write(6,*)'dead ending pair:'
	!	      write(6,"(3I2)")i,a,b
	!	      write(6,"(3I2)")j,r,s
	!	      write(6,*)'with magic bullet pair: '
        !             write(6,"(3I2)")i,aa1,rot1
        !             write(6,"(3I2)")j,aa2,rot2
		      cycle pair_loop
		      
		    end if
	  
	  	end do pair_loop
	      end do
      	    end do
          end do
        end do
      end do
	
	
    END SUBROUTINE MB_DOUBLES
!
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

    SUBROUTINE GOLDSTEIN_DOUBLES()                                                 

!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!c
!c   searches for dead ending pairs using goldsteins dead end elimination 
!c   equation for doubles:
!c   
!c           [E(ir) + E(js) + E(ir,js)] - [E(iu) + E(jv) + E(iu,jv)] 
!c        +  sum {min(t) [E(ir,kt) + E(js,kt) - E(iu,kt) - E(jv,kt)]} > 0

	USE MOLEC_M
	USE EP_DATA_M
	USE MUMBO_DATA_M
	USE MAX_DATA_M
!
	IMPLICIT NONE
!
	integer :: i, j, a, b, c, d, k, l, m, r, s, v, w
	real :: energy_pair1, energy_pair2, energy_min, energy_min_sum
	real :: energy, energy_criterion
!
!
	energy_pair1     = 0
	energy_pair2     = 0
	energy           = 0
	energy_min       = 1.0e12
	energy_min_sum   = 0
	energy_criterion = 0
	
	do i = 2, size(mol)                              ! loop over all residue-pairs ij
	  do j = i+1, size(mol)                          ! ij = ji -> only the half-matrix has to be considered
	    do a = 1, mol(i)%res(1)%res_naa              ! the following 4 do-loops take one 
	      do b = 1, mol(i)%res(1)%res_aas(a)%aa_nrt  ! rotamer-pair a time 
!	        if ( .not.(mol(i)%res(1)%res_aas(a)%aa_rots(b)%rot_flag) ) cycle
	        do r = 1, mol(j)%res(1)%res_naa
                  pair_loop: do s = 1, mol(j)%res(1)%res_aas(r)%aa_nrt	
		    ! if this pair is allready eliminated, take the next
		    if ( .not.(ea(i)%eb(a)%ec(b)%ed(j)%ee(r)%ef(s)%pair_flag) ) then
		      cycle 
		    end if
		    
		    ! calculate the energy of pair 1: E(ir) + E(js) + E(ir,js)
		    
		    energy_pair1 = ea(i)%eb(a)%ec(b)%ed(i)%ee(a)%ef(b)%ep_en(1) &
		               & + ea(j)%eb(r)%ec(s)%ed(j)%ee(r)%ef(s)%ep_en(1) &
		               & + ea(i)%eb(a)%ec(b)%ed(j)%ee(r)%ef(s)%ep_en(1)

		    do c = 1, mol(i)%res(1)%res_naa
		      do d = 1, mol(i)%res(1)%res_aas(c)%aa_nrt
		        do v = 1, mol(j)%res(1)%res_naa
			  do w = 1, mol(j)%res(1)%res_aas(v)%aa_nrt
			    
			    ! now take another rotamer pair at ij 
			        
			    if ( c == a .and. d == b .and. v == r .and. w == s ) then
			      cycle
			    end if
			    
			    if ( .not.(ea(i)%eb(c)%ec(d)%ed(j)%ee(v)%ef(w)%pair_flag) ) then
		              cycle 
		            end if

			    ! calculate the energy of pair 2: E(iu) + E(jv) + E(iu,jv)
			    
			    energy_pair2 = ea(i)%eb(c)%ec(d)%ed(i)%ee(c)%ef(d)%ep_en(1) &
		                       & + ea(j)%eb(v)%ec(w)%ed(j)%ee(v)%ef(w)%ep_en(1) &
		                       & + ea(i)%eb(c)%ec(d)%ed(j)%ee(v)%ef(w)%ep_en(1)
			    
			    energy_min_sum   = 0
			    energy_criterion = 0

			    ! now walk through all positions k .ne. i, j and caculate the energy-criterion
			    ! according to goldsteins equation for doubles    
			    
			    do k = 2, size(mol)
			      if ( k == i .or. k == j ) then
			        cycle
			      end if
			      
			      energy_min = 1.0e12
			    
			      do l = 1, mol(k)%res(1)%res_naa
			        do m = 1, mol(k)%res(1)%res_aas(l)%aa_nrt
				  
				  if ( .not.(ea(i)%eb(c)%ec(d)%ed(j)%ee(v)%ef(w)%pair_flag) ) then
		                    cycle 
		                  end if			

				  ! calculate the energy-difference:
				  ! E(ir,kt) + E(js,kt) - E(iu,kt) - E(jv,kt)
				    
				  energy = ea(i)%eb(a)%ec(b)%ed(k)%ee(l)%ef(m)%ep_en(1) &
			&	         + ea(j)%eb(r)%ec(s)%ed(k)%ee(l)%ef(m)%ep_en(1) &
			&	         - ea(i)%eb(c)%ec(d)%ed(k)%ee(l)%ef(m)%ep_en(1) &
			&		 - ea(j)%eb(v)%ec(w)%ed(k)%ee(l)%ef(m)%ep_en(1)

				  ! search for the lowest energy-difference
				  
				  if ( energy < energy_min ) then
				    energy_min = energy
				  end if
				
				end do
			      end do
			    
			      ! now, for all positions k .ne. i,j calculate
			      ! the sum-term: sum(k){min(t)[(E(ir,kt) + E(js,kt) - E(iu,kt) - E(jv,kt)]}
			      energy_min_sum = energy_min_sum + energy_min
			    
			    end do
			    
			    ! evaluate the energy-criterion:
			    !      [E(ir) + E(js) + E(ir,js)]  -  [E(iu) + E(jv) + E(iu,jv)]
			    !   +  sum(k){min(t)[(E(ir,kt) + E(js,kt) - E(iu,kt) - E(jv,kt)]} > 0
!			    
			    energy_criterion = energy_pair1 - energy_pair2 + energy_min_sum
!			    
			    if ( energy_criterion .gt. 0 ) then
!			      
			      ea(i)%eb(a)%ec(b)%ed(j)%ee(r)%ef(s)%pair_flag = .false.
!			      
!			      write(6,*) 'energy_criterion fulfilled !'
!			      write(6,*)'dead ending pair:'
!			      write(6,"(3I2)")i,a,b
!			      write(6,"(3I2)")j,r,s
!			      
			      cycle pair_loop
			      
			    end if
			    
			  end do
			end do
		      end do
		    end do
		  
		  end do pair_loop
		end do
	      end do
	    end do
	  end do
	end do
!	
    END SUBROUTINE GOLDSTEIN_DOUBLES
!
!
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
    SUBROUTINE SIM_ANNEAL
!
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
! The SUBROUTINE SIM_ANNEAL performs a simualted annealing procedure and calls
! the SUBROUTINE MONTE_CARLO in order to find a low energy rotamer combination.
! Different cooling schedules are possible.
!
  USE MOLEC_M
  USE MUMBO_DATA_M
!
  IMPLICIT NONE

  REAL :: T_max      ! Starting temperature for simulated annealing
  REAL :: T_min      ! End temperature for simulated annealing
  REAL :: dT         ! Temperature decreasing increment
  REAL :: T          ! Current temperature during simulated annealing
!  REAL :: P_max      ! Probability for acceptance at the beginning
!  REAL :: P_min      ! Probability for acceptance at the end
  REAL :: energy_mc  ! Energy of the monte carlo energy search
  REAL :: energy_min ! Energy of the previous monte carlo energy search
!  REAL, PARAMETER :: kb=1.3806505E-23  ! Boltzmann constant
!  REAL, PARAMETER :: Na=6.0221415E+23  ! Avogadro constant
  INTEGER :: k, l
  INTEGER, DIMENSION(3*(SIZE(mol)-1))    :: conf     ! array to store the actual configuration
  INTEGER, DIMENSION(3*(SIZE(mol)-1))    :: conf_min ! array to store the minimum configuration
                                                     ! for every mutated residue 3 integers:
                                                     ! residue number, aacid, rotamer
!
  INTEGER :: n_mc            ! Number of steps in the monte carlo subroutine
  INTEGER :: sa_steps        ! Number of simulated annealing steps
  INTEGER :: count
  INTEGER :: mxxsize          ! number of mutated residues

  INTEGER :: i               ! loop index

  LOGICAL :: single_solution ! true, if only one solution left from previous steps
!
!
  mxxsize = SIZE(mol) - 1
!
! check, if only one solution is left from previous elimination steps
! if so, leave the subroutine

  single_solution = .TRUE.
    DO i=2,size(mol)
       DO k=1,mol(i)%res(1)%res_naa
         DO l=1,mol(i)%res(1)%res_aas(k)%aa_nrt
           mol(i)%res(1)%res_aas(k)%aa_rots(l)%rot_flag=.true.
           mol(i)%res(1)%res_aas(k)%aa_rots(l)%rot_flag2=.true.
        END DO
      END DO
    END DO
!
    DO i=2,size(mol)
	IF (mol(i)%res(1)%res_naa.ne.1) THEN
	  single_solution =.false.
          EXIT
        ELSE IF (mol(i)%res(1)%res_aas(1)%aa_nrt.ne.1) THEN
	  single_solution =.FALSE.
	  EXIT
	END IF
    END DO
!
  If (single_solution) then
    DO i=1,mxxsize
	conf_min(1+ 3*(i-1)) = i
	conf_min(2+ 3*(i-1)) = 1 
	conf_min(3+ 3*(i-1)) = 1
    end do
  end if
!
  IF (.not.single_solution) THEN

  T_max    = mumbo_sa_Tmax  ! maximal temperature
  T_min    = mumbo_sa_Tmin  ! minimal temperature
  sa_steps = mumbo_sa_steps ! number of simulated annealing steps
  
  T     = T_max             ! current temperature
  n_mc  = mumbo_sa_mc       ! number of monte carlo steps
  
  
  dT    = (T_max - T_min)/sa_steps
  count = 0


! DEBUG
!	print*,'T_max= ', T_max 
!	print*,'T_min= ', T_min 
!	print*,'sa_steps= ', mumbo_sa_steps
!	print*,'dT ', dT
!	print*,'n_mc', mumbo_sa_steps
!
!

  
  energy_mc     =  100000000000000.
  energy_min    = 1000000000000000.

  ! Start the simulated annealing procedure and
  ! stop, if minimal temperature is reached.

  PRINT *,'#######################################'
  PRINT *,'#   SIMULATED ANNEALING SWITCHED ON   #'
  PRINT *,'#######################################'
  PRINT *
  PRINT *,'Starting with the following parameters: '
  PRINT *
  PRINT *,'T_max= ', T_max
  PRINT *,'T_min= ', T_min
  PRINT *,'dT   = ', dT
  PRINT *,'mcsteps=', n_mc
  PRINT *
  
  DO; IF( T .LT. T_min ) EXIT
    
    ! calculate the new temperature
    ! using the linear temperature schedule
    T = TEMP_LINEAR(count)

    PRINT *
    PRINT *,'Temperature: ', T

    ! for the new temperature, perform a monte carlo simulation
    CALL MONTE_CARLO(T, n_mc, energy_mc, conf, count)
    PRINT *,'Annealing step #: ', count
    PRINT *,'Energy_min: ', energy_min
    PRINT *,'Energy_mc : ', energy_mc
    count = count + 1
    
    ! if the resulting energy of the actual monte carlo procedure
    ! is lower than the lowest energy observed so far, store the new
    ! configuration in the array conf_min and store the actual energy
    ! as new lowest energy.
    IF ( energy_mc .LT. energy_min ) THEN
      conf_min = conf
      energy_min  = energy_mc
      PRINT *,'Energy decreased !'
    ELSE
      energy_mc = energy_min
    END IF
    PRINT *,'----------------------------------------------------------'

  END DO
!
  PRINT *,'***********************************'
  PRINT *,'*  Simulated annealing completed  *'
  PRINT *,'***********************************'
  PRINT *,'  Final energy: ', energy_min
  PRINT *
  PRINT *,'###################################'
!
 END IF
!
!   STORE MINIMAL CONFORMATION FOR LATER USE
!
    if (allocated(mumbo_sim_conf_min)) deallocate(mumbo_sim_conf_min)
!    allocate(mumbo_sim_conf_min(size(conf_min)))
    allocate(mumbo_sim_conf_min(3*mxxsize))
!
    DO i=1,mxxsize
	mumbo_sim_conf_min(1+ 3*(i-1)) = i
	mumbo_sim_conf_min(2+ 3*(i-1)) = conf_min(2+ 3*(i-1)) 
	mumbo_sim_conf_min(3+ 3*(i-1)) = conf_min(3+ 3*(i-1)) 
    end do
!

  CONTAINS

  ! Several functions for different cooling schedules following

  FUNCTION TEMP_LINEAR (count) RESULT(T_new)
    ! this FUNCTION provides a linear decreasing temperature schedule
    ! the stepsize is determined by the temperature difference (final - start)
    ! and the number of steps (n_steps)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: count ! Actual step number
    REAL :: T_new
    T_new = T_max - count*dT
  END FUNCTION TEMP_LINEAR

! to be implemented:
!
!   FUNCTION TEMP_PROP () RESULT(TEMP)
!     IMPLICIT NONE
!
!   END FUNCTION TEMP_PROP
!
!
!
!   FUNCTION TEMP_EXP () RESULT(TEMP)
!
!   END FUNCTION TEMP_EXP
!
!
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
    END SUBROUTINE SIM_ANNEAL
!
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
  SUBROUTINE FLAG_ROTAM_SIM
!
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
! Flag the combination of aminoacids and rotamers representing the solution of
! the monte carlo simulation for later analyse and write steps.
!
  USE MOLEC_M
  USE EP_DATA_M
  USE MUMBO_DATA_M
  USE MAX_DATA_M
!
  IMPLICIT NONE
!
  INTEGER :: i, k, l ! loop variables
  INTEGER :: aa, ro  ! number of current aacid and rotamer
  INTEGER, DIMENSION(3*(SIZE(mol)-1)) :: conf_min ! configuration to write out
!
  conf_min = mumbo_sim_conf_min 
!
  DO i=1,(size(mol)-1)
   aa = conf_min(2+(i-1)*3)
   ro = conf_min(3+(i-1)*3)
    DO k=1,mol(i+1)%res(1)%res_naa
      DO l=1,mol(i+1)%res(1)%res_aas(k)%aa_nrt
        mol(i+1)%res(1)%res_aas(k)%aa_rots(l)%rot_flag=.false.
        mol(i+1)%res(1)%res_aas(k)%aa_rots(l)%rot_flag2=.false.
      END DO
    END DO
    mol(i+1)%res(1)%res_aas(aa)%aa_rots(ro)%rot_flag=.true.
    mol(i+1)%res(1)%res_aas(aa)%aa_rots(ro)%rot_flag2=.true.
  END DO
!
  END SUBROUTINE FLAG_ROTAM_SIM
!
!
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
     SUBROUTINE MONTE_CARLO(T, n_mc, energy_min, conf_min, call_nr)
!
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
! It�s Monte Carlos flying circus
! This subroutine performs a monte carlo search based on the
! metropololis criterion to solve the sidechain positioning problem.
! At each step 3 random numbers are needed. The first decides at
! which position the alteration is to be made. The second helps
! to choose an amino acid at this position and the third defines
! the corresponding rotamer.
!
    USE MOLEC_M
    USE EP_DATA_M
    USE MUMBO_DATA_M
    USE MAX_DATA_M

    IMPLICIT NONE

    REAL, PARAMETER        :: kb=1.3806505E-23       ! Boltzmann constant
    REAL, PARAMETER        :: Na=6.0221415E+23       ! Avogadro constant

    REAL                   :: ran1, ran2, ran3       ! random numbers
    REAL                   :: ran_metropolis         ! random number for metropolis criterion

    REAL, INTENT(IN)       :: T                      ! temperature in Kelvin
!    REAL                   :: T_max                  ! maximal temperature
!    REAL                   :: dT                     ! temperature increment

    REAL                   :: energy_old             ! energy of prev. system
!    REAL                   :: energy_new             ! energy of new system
    REAL                   :: energy_mc              ! energy of monte carlo simulation
    REAL                   :: therm_en               ! thermic energy kb*T
    REAL                   :: delta_en               ! energy difference (in metropolis criterion)
    REAL                   :: boltz_factor           ! Boltzmann factor
    REAL, INTENT(INOUT)    :: energy_min             ! minimal energy of monte carlo simulation

    REAL                   :: percent_acc            ! percentage of
                                                     ! accepted moves

    INTEGER, INTENT(IN)    :: n_mc                   ! number of steps in
                                                     ! monte carlo simulation
    INTEGER                :: rpos, raas, rrot       ! position, aacid and rotamer
                                                     ! derived from random numbers
    INTEGER                :: aas_old, rot_old       ! aacid and rotamer of last step
    INTEGER                :: num_aas, num_rot        ! number of aacids and rotamers
                                                      ! at the current position
    INTEGER                :: accepted                ! number of accepted moves
    INTEGER                :: i                       ! loop variable

    INTEGER, INTENT(IN)    :: call_nr                 ! number of monte carlo calls

    INTEGER, DIMENSION(3*(SIZE(mol)-1))                :: conf       ! contains the actual/minimum conformation
    INTEGER, DIMENSION(3*(SIZE(mol)-1)), INTENT(INOUT) :: conf_min   ! for every mutated aminoacid:
                                                      ! number of aacid, aacid, rotamer
    ! this is quite ugly, but we can only pass normal arrays to the corresponding C++ methods
    ! therefore, we need integers denoting the adress of position, aacid and rotamer in memory:
    INTEGER :: offset, pos_ad, aas_ad, rot_ad
    INTEGER :: mxsize  ! number of active residiues
!
!
!  
    ! number of mutated residues
    mxsize = SIZE(mol) - 1

    ! calculate the thermal energy corresponding to provided temperature
    therm_en = (Na*kb*T)/(1000.0000*4.1868)

    ! first of all, build an initial structure at random
    ! and calculate its energy
        
    IF ( call_nr == 0 ) THEN
      PRINT *,'first call of monte carlo routine'      
      PRINT *,' ... building initial structure'      
      CALL INIT_MONT(conf_min)
      CALL ENERGY_MONT(conf_min, energy_min)
      energy_old = energy_min
      conf       = conf_min
    ELSE
      PRINT *,' ... building initial structure'
      CALL INIT_MONT(conf)
      CALL ENERGY_MONT(conf, energy_old)
    END IF
    
    PRINT *,'energy at entry of monte carlo routine: ', energy_mc

    num_aas = 0
    num_rot = 0
!    energy_min = energy_mc
    accepted = 0.0
    percent_acc = 0.0
    therm_en = (Na*kb*T)/(1000.0000*4.1868)

    ! repeat the monte carlo procdure n_mc times
    DO i=1, n_mc

      ! decide, which position should be varied
      CALL random_number(ran1)
      
      rpos = INT((mxsize)*ran1) + 1

      ! now, calculate the location of the integers
      ! for position, aacid and rotamer in memory:
      offset = (rpos-1)*3
      pos_ad = offset + 1
      aas_ad = offset + 2
      rot_ad = offset + 3

      ! store the aminoacid and the rotamer of the chosen position:
      aas_old = conf(aas_ad)
      rot_old = conf(rot_ad)

      ! choose another aacid at this position
      num_aas = mol(rpos+1)%res(1)%res_naa ! number of aacids at rpos
      CALL random_number(ran2)
      raas = INT(num_aas*ran2) + 1

      ! choose a rotamer for this aacid
      num_rot = mol(rpos+1)%res(1)%res_aas(raas)%aa_nrt ! number of rotamers at rpos
      CALL random_number(ran3)
      rrot = INT(num_rot*ran3) + 1

      ! change the configuration corresponding to the chosen variations
      conf(aas_ad) = raas
      conf(rot_ad) = rrot

      ! calculate the energy of the new configuration
      CALL ENERGY_MONT(conf, energy_mc)
      ! if the energy is lower than the previous one, take it as new energy
      ! and accept the move
      IF ( energy_mc .LT. energy_old ) THEN
        energy_old = energy_mc
        accepted   = accepted + 1
        ! if the energy is lower than the lowest energy observed so far,
        ! take it as new lowest energy and store the configuration
        IF ( energy_mc .LT. energy_min ) THEN
          conf_min    = conf
          energy_min  = energy_mc
        END IF

      ! if the energy is higher than the previous one, apply the metropolis
      ! criterion to decide if the move is to be accepted. In this case
      ! take the energy as new energy and accept the move, otherwise take the old
      ! configuration
      ELSE
        CALL RANDOM_NUMBER( ran_metropolis )
        delta_en     = energy_mc - energy_old
        boltz_factor = exp( -(delta_en/therm_en) )
        IF ( boltz_factor .GE. ran_metropolis ) THEN
          energy_old = energy_mc
          accepted   = accepted + 1
        ELSE
          conf(aas_ad) = aas_old
          conf(rot_ad) = rot_old
        END IF
      END IF
    END DO

    ! calculate the ratio of accepted moves
    percent_acc = REAL(accepted/n_mc)*100.0

    ! the lowest energy observed during the monte carlo procedure
    ! is the return value of the SUBROUTINE


    PRINT *
    PRINT *,'ENERGY OF THE FINAL CONFORMATION: ', energy_mc
    PRINT "(A,2X,I12)",'ACCEPTED:                   : ', accepted
    PRINT "(A,2X,I12)",'NUMBER OF MC-STEPS:         : ', n_mc
    PRINT "(A,2X,F5.2)",'FINAL PERCENTAGE ACCEPTED: ', percent_acc
    PRINT *
    PRINT *,'**************************************************************'

!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

    CONTAINS

   ! SUBROUTINES directly needed by SUBROUTINE MONTE_CARLO:
   !
   ! INIT_MONT: Builds a random starting configuration
   ! ENERGY_MONT: Calculates the energy of a configuration
   ! WRITE_MONT: Writes the result of the monte carlo simulation


    SUBROUTINE ENERGY_MONT(conf, en)

    ! Given a certain conformation: a certain aminoacid and rotamer at each position
    ! Calculate the energy of the system.

      IMPLICIT NONE

      REAL, INTENT(INOUT)                        :: en       ! energy of the actual configuration
!      DOUBLE PRECISION                           :: msrout   ! result of the surface calculation
      INTEGER, DIMENSION(3*mxsize), INTENT(IN)   :: conf     ! array containing the actual configuration
      INTEGER                                    :: aa1, aa2, aa ! aminoacid 1, aminoacid 2
      INTEGER                                    :: ro1, ro2, ro ! rotamer 2, rotamer 2
      INTEGER                                    :: i, j     ! loop indices
!
!	GFORTRAN
	ro2 = -1000
	aa2 = -1000
!	GFORTRAN
!
      ! Calculate the total energy of the configuration, for pairwise interaction only
      ! half of the matrix has to be calculated (don�t calculate interactions twice).
      ! This is why j>i.

      en = 0

      ! to calculate the energy of the configuration, the following sums have to be evaluated:
      ! sum over the self energy of all active residues plus the sum over all pairwise interactions
      ! E = sum(i)[E(i)] + sum(i)sum(j>i)[E(i,j)]
      ! to evaluate correctly, the index i in the double sum goes from 1 to the
      ! maximal number of mutated residues -1 and the index j goes from i+1 to the maximal
      ! number of mutated residues
      
      IF ( mxsize == 1 ) THEN
        aa = conf(2)
	ro = conf(3)
        en = ea(2)%eb(aa)%ec(ro)%ed(2)%ee(aa)%ef(ro)%ep_en(1)
      ELSE
      
        DO i=1,(mxsize-1)
          ! get the aminoacid aa1 and rotamer ro1 at position i from configuration table conf()
          aa1 = conf(2+(i-1)*3)
          ro1 = conf(3+(i-1)*3)
	  ! get the energy of residue i, E(i): look up in the energy table
          ! the energy table starts at index 2 like the mol() array, therefore
          ! to get the entry for position i, take the (i+1)th component of the energy array
          en = en + ea(i+1)%eb(aa1)%ec(ro1)%ed(i+1)%ee(aa1)%ef(ro1)%ep_en(1)
          DO j=i+1,mxsize
            ! get the aminoacid aa2 and rotamer ro2 at position j from configuration table conf()
            aa2 = conf(2+(j-1)*3)
            ro2 = conf(3+(j-1)*3)
            ! get the pairwise interaction beteween residue i and j
	    en = en + ea(i+1)%eb(aa1)%ec(ro1)%ed(j+1)%ee(aa2)%ef(ro2)%ep_en(1)
          END DO
        END DO

        ! finally add the self energy of residue mxsize, because it�s not included
        ! in the sumation above
        en = en + ea(mxsize+1)%eb(aa2)%ec(ro2)%ed(mxsize+1)%ee(aa2)%ef(ro2)%ep_en(1)
      END IF 
      
 
    END SUBROUTINE ENERGY_MONT
!
!    SUBROUTINE WRITE_MONT(conf)
!    ! just print the aminoacid sequence of the given combination!
!
!    IMPLICIT NONE
!
!    INTEGER, DIMENSION(3*mxsize), INTENT(IN) :: conf ! actual configuration
!    INTEGER :: i      ! loop index
!    INTEGER :: aa, ro ! aacid, rotamer
!
!     DO i=1,mxsize
!       ! get the aminoacid and rotamer of position i from the configuration array
!       aa = conf(2+(i-1)*3)
!       ro = conf(3+(i-1)*3)
!       PRINT *,i, aa, ro
!       WRITE (6,"(I4,X,A,2X,A3,2X,I2)"),mol(i+1)%res(1)%res_nold, mol(i+1)%res(1)%res_chid, &
!           &                            mol(i+1)%res(1)%res_aas(aa)%aa_nam,ro
!     END DO
!
!    END SUBROUTINE WRITE_MONT
!
    SUBROUTINE INIT_MONT(conf)
    ! This SUBROUTINE builds an initial structure
    ! using 2 random numbers at each position of
    ! the chain.

    IMPLICIT NONE

    INTEGER, DIMENSION(3*mxsize)                 :: conf             ! configuration table
    INTEGER                                      :: num_aas, num_rot ! number of aacids and
                                                                     ! rotamers at current position
    INTEGER                                      :: raas, rrot       ! aminoacid and rotamer
                                                                     ! derived from random numbers
    REAL                                         :: ran1, ran2       ! random numbers
    INTEGER                                      :: i                ! loop index

    ! go through all positions to be mutated
    ! at every position, chose an aacid and a rotamer at random
    DO i=1,mxsize
      ! get the number of aminoacids for position i
      ! again, the array mol starts with component 2 (component 1 contains the backbone)
      ! therefore look at the (i+1)th component
      num_aas = mol(i+1)%res(1)%res_naa
      CALL RANDOM_NUMBER(ran1)
      ! from the random number, calculate the random aminoacid
      raas = INT((num_aas-1)*ran1+1)
      ! get the number of rotamers for position i
      num_rot = mol(i+1)%res(1)%res_aas(raas)%aa_nrt
      CALL RANDOM_NUMBER(ran2)
      !from the random number, calculate the random rotamer
      rrot = INT((num_rot-1)*ran2+1)

      ! store the configuration for position i in the conf() array
      conf(1+(i-1)*3) = i
      conf(2+(i-1)*3) = raas
      conf(3+(i-1)*3) = rrot

    END DO

    PRINT *,'done with building initial conformation'

    END SUBROUTINE INIT_MONT

!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

    END SUBROUTINE MONTE_CARLO

!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
    SUBROUTINE BOUND_DEE()
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
!   This SUBROUTINE tries to use the solution of a previous monte carlo/
!   simulated annealing procedure to eliminate rotamers which cannot
!   be part of the GEMC.
!   The criterion applied, claims that a rotamer i_r cannot be part of the
!   GMEC, if the following criterion is fulfilled:
!      E(i_r) + sum(j)[min(s) E(i_r,j_s)] > E(i_mc) + sum(j) E(i_mc,j_mc)
!   i_mc and j_mc are the rotamers at position i and j found by the 
!   mc-sa procedure.
!   If the best possible energy of rotamer i_r is higher than the energy 
!   of the rotamer chosen by the monte carlo procedure, it can be eliminated.

    USE MOLEC_M
    USE EP_DATA_M
    USE MUMBO_DATA_M
    USE MAX_DATA_M
!
    IMPLICIT NONE
!
    INTEGER, DIMENSION(3*(SIZE(mol)-1))    :: conf_mc ! array with the minimum configuration of
                                                      ! previous monte carlo/simulated annealing run
    INTEGER :: aa1_mc, aa2_mc         ! aacid 1 and 2 of monte carlo solution
    INTEGER :: ro1_mc, ro2_mc         ! rotamer 1 and 2 of monte carlo solution
    INTEGER :: i, j, a, b, m, n, k    ! loop indices
    INTEGER :: mxsize                 ! number of mutated residues + 1

    REAL    :: en, en_min        ! current energy, minimal energy
    REAL    :: en_sum, en_sum_mc ! sum of interaction energies, the same for monte carlo
    REAL    :: en_ro, en_ro_mc   ! self energy of the current rotamer, self energy of the 
                                 ! rotamer of the mc solution
!	
    PRINT*, '        '
    PRINT*, '################################'
    PRINT*, '#    SUBROUTINE BOUND_DEE      #'
    PRINT*, '################################'
    PRINT*, '          '
!
    mxsize = SIZE(mol)
!       
    conf_mc = mumbo_sim_conf_min
!
    ! scan over all mutated positions
    DO i=2,mxsize
      ! take the aacid and rotamer of the mc solution at the current position
      ! the conf_mc array starts with entry 1 !!
      aa1_mc   = conf_mc(2+(i-2)*3)
      ro1_mc   = conf_mc(3+(i-2)*3)
      ! take every aacid and rotamer at the current position
      ! and calculate it�s interaction energy
      DO a=1,mol(i)%res(1)%res_naa
        DO b=1,mol(i)%res(1)%res_aas(a)%aa_nrt
          en_sum    = 0.0
          en_sum_mc = 0.0
          ! to calculate the energy, cycle over all positions j .ne. i
          DO j=2,size(mol)
            IF (j==i) CYCLE
              en_min = 1.0E20
              DO m=1,mol(j)%res(1)%res_naa
                DO n=1,mol(j)%res(1)%res_aas(m)%aa_nrt
                  ! search for the minimal possible energy of the current rotamer
                  en = ea(i)%eb(a)%ec(b)%ed(j)%ee(m)%ef(n)%ep_en(1)
                  en_min = MIN(en,en_min)
                END DO
              END DO
             ! calculate the energy sum : sum(j)[min(s) E(i_r,j_s)]
             en_sum = en_sum + en_min
             ! for the new position, take the aacid and rotamer of the mc solution           
             aa2_mc = conf_mc(2+(j-2)*3)
             ro2_mc = conf_mc(3+(j-2)*3)  
            ! calculate the energy sum for the monte carlo solution:
            ! sum(j) E(i_mc,j_mc)
            en_sum_mc = en_sum_mc + ea(i)%eb(aa1_mc)%ec(ro1_mc)%ed(j)%ee(aa2_mc)%ef(ro2_mc)%ep_en(1)

          END DO
          ! now add the self energy of the rotamer at i to the energy sum:
          ! E(i_r) + sum(j)[min(s) E(i_r,j_s)]
          en_ro     = ea(i)%eb(a)%ec(b)%ed(i)%ee(a)%ef(b)%ep_en(1)
          en_sum    = en_sum + en_ro
          ! do the same for the mc solution:
          ! E(i_mc) + sum(j) E(i_mc,j_mc)
          en_ro_mc  = ea(i)%eb(aa1_mc)%ec(ro1_mc)%ed(i)%ee(aa1_mc)%ef(ro1_mc)%ep_en(1)
          en_sum_mc = en_sum_mc + en_ro_mc
          ! now apply the criterion:
          !   E(i_r) + sum(j)[min(s) E(i_r,j_s)] > E(i_mc) + sum(j) E(i_mc,j_mc)
          ! if the best possible energy of the current rotamer is greater than the energy
          ! of the rotamer chosen by the mc procedure, eliminate it!
          IF ( en_sum .GT. en_sum_mc ) THEN
            mol(i)%res(1)%res_aas(a)%aa_rots(b)%rot_flag=.false.
          END IF
!
        END DO
      END DO
    END DO

!
    do i=2,size(mol)
      do j=1,mol(i)%res(1)%res_naa
        do k=1,mol(i)%res(1)%res_aas(j)%aa_nrt
          if ( mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_flag .eqv..false. ) then   
            print*,'  ROTAMER DEAD END ELIMINATED:  '
            print*,'  RESIDUE= ', mol(i)%res(1)%res_chid,' ',   & 
     &      mol(i)%res(1)%res_nold 
            print*,'  AAS=     ', mol(i)%res(1)%res_aas(j)%aa_nam
            print*, (i-1),'   1' , j , k
            print*,'       ' 
          end if
        end do
      end do
    end do
!
!
    END SUBROUTINE BOUND_DEE
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
      SUBROUTINE ALL_EXPLICIT ()
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MOLEC_M
	USE MUMBO_DATA_M
	USE MAX_DATA_M
!
	IMPLICIT NONE
!
      integer, dimension(:),     allocatable     :: num_nco
      logical, dimension(size(mol)-1)    :: new
      integer(kind=INT_LONG),dimension(:),allocatable:: ncomb_plus, ncomb_minus
      integer(kind=INT_LONG) :: ncomb_tot, AAN, BBN, CCN, i, ntest, ni, nj
      integer, dimension(:,:,:), allocatable     :: nco
      integer, dimension(size(mol),4)       :: hkl      
      integer, dimension(size(mol),3:4)     :: hkl_old
      integer, dimension(:,:), allocatable  :: hkl_save
      integer ::  j, k, na, nao, nb, nbo, nmax, ncnt, nnpos
      integer ::  n1, n2, n3, n4, n7, n8
      real            ::  entot, entot_old, ri
      logical :: fflag, l1
      character(len=1000) :: temp_file
!
      integer                  :: nthreads
!$     integer :: omp_get_max_threads,omp_get_num_procs
      real, dimension(max_npos)             :: en_up_to
!     
!
      nnpos=size(mol)-1
      if (allocated(num_nco)) deallocate(num_nco)
      allocate(num_nco(nnpos))
      if (allocated(ncomb_plus)) deallocate(ncomb_plus)
      allocate(ncomb_plus(nnpos))
      if (allocated(ncomb_minus)) deallocate(ncomb_minus)
      allocate(ncomb_minus(nnpos))
      if (allocated(hkl_save)) deallocate(hkl_save)
      allocate(hkl_save((nnpos+1),4))
!
!      allocate(num_nco(size(mol)-1))
!      allocate(ncomb_plus(size(mol)-1))
!      allocate(ncomb_minus(size(mol)-1))
!      allocate(hkl_save((size(mol)),4))
!
      nmax=0
      fflag=.true.
!
      do i=1,nnpos
            num_nco(i)=0
            do j=1,mol(i+1)%res(1)%res_naa
               num_nco(i)=num_nco(i)+(mol(i+1)%res(1)%res_aas(j)%aa_nrt)
               if (nmax < num_nco(i)) then
                   nmax=num_nco(i)
               end if
               do k=1, mol(i+1)%res(1)%res_aas(j)%aa_nrt
!
         mol(i+1)%res(1)%res_aas(j)%aa_rots(k)%rot_flag=.false.
         mol(i+1)%res(1)%res_aas(j)%aa_rots(k)%rot_flag2=.false.
!
               end do
            end do
        end do
!
!      allocate(nco((size(num_nco)),nmax,2))
      allocate(nco(nnpos,nmax,2))
!
      do i=1,nnpos
            ncnt=0
            do j=1,mol(i+1)%res(1)%res_naa
                do k=1,mol(i+1)%res(1)%res_aas(j)%aa_nrt
               ncnt=ncnt+1
               nco(i,ncnt,1)=j
               nco(i,ncnt,2)=k
            end do
           end do
      end do
!
      ncomb_tot=1     
!
      do i=1, nnpos
             ncomb_minus(i)=  ncomb_tot
             ncomb_tot=     ncomb_tot*num_nco(i)
             ncomb_plus(i)= ncomb_tot
      end do
!     
      PRINT*, '        '
      PRINT*, '################################'
      PRINT*, '#    SUMMARY ALL_EXPLICIT      #'
      PRINT*, '################################'
      PRINT*, '          '
      DO i=1,nnpos
      PRINT*, '    # AA*RT AT POSITION:  ', mol(i+1)%res(1)%res_chid,    &
     &        mol(i+1)%res(1)%res_nold,                                 &
     &          ' ==  ',num_nco(i)

      END DO
      PRINT*, '  '
      PRINT*, '  NUMBER OF COMBINATIONS TO TEST IS GROWING RAPIDELY '
      PRINT*, '  '
      DO i=1, nnpos
      PRINT*, '    COMBINATION LIMITS: ',ncomb_minus(i), ncomb_plus(i)
      END DO
      PRINT*, '  ' 
      PRINT*, '  TOTAL NUMBER OF COMBINATIONS TO TEST: ', ncomb_tot 
      PRINT*, '  ' 
!
      do i=1, nnpos
         hkl_old(i,3)=0
         hkl_old(i,4)=0
      end do
!      entot     =100000
      entot_old =100000
!
      if (ncomb_tot > 1000000000) then
        nj= int(ncomb_tot/400)
      else if (ncomb_tot > 1000000) then
        nj= int(ncomb_tot/200)
      else if (ncomb_tot > 100) then
        nj= int(ncomb_tot/50)
      else
        nj= 5
      end if

!$     nthreads = omp_get_max_threads()

!$     print*,'  WILL USE ',nthreads,' THREADS ON ',omp_get_num_procs(),' CPUs'
!$     print*,'  '
!
!$omp parallel                                                            &
!$omp& private(i,j,new,aan,bbn,ccn,hkl,na,nao,nb,nbo,entot,               &
!$omp&         ntest,ni,ri)                                               &
!$omp& firstprivate(hkl_old,fflag,en_up_to)                               &
!$omp& shared(num_nco,nthreads,ncomb_tot,ncomb_plus,ncomb_minus,nco,      &
!$omp&  mumbo_en_bwr,mol,n7,n8,nj,l1,k,hkl_save,entot_old,nnpos) &
!$omp& default(none)
!$omp do schedule(static,max(1,nj/nthreads))
      do i=1,ncomb_tot
!
          do j=1,nnpos
!
              new(j)=.true.
!
              AAN = mod(i,ncomb_plus(j))
              if (AAN==0) then
                 AAN=ncomb_plus(j)
              end if
!
              BBN=int(AAN/(ncomb_minus(j)))
              CCN=mod(i,ncomb_minus(j))
                hkl(j,1)=j+1
                hkl(j,2)=1
              if (CCN > 0) then
                hkl(j,3)=nco(j,(BBN+1),1)
                hkl(j,4)=nco(j,(BBN+1),2)
              else
                hkl(j,3)=nco(j,(BBN),1)
                hkl(j,4)=nco(j,(BBN),2)
              end if
!
          end do
!
          do j=nnpos,1,-1
          na=hkl(j,3)
          nao=hkl_old(j,3)
          nb=hkl(j,4)
            nbo=hkl_old(j,4)
              if (na==nao.and.nb==nbo) then
                  new(j)=.false.
                  cycle
              else
                  goto 100
              end if
          end do
100      continue
!
          do j=1,nnpos
               hkl_old(j,3)=hkl(j,3)
               hkl_old(j,4)=hkl(j,4)
          end do      
!
          call tot_energy(hkl,new,entot,fflag,en_up_to)
          fflag=.false.
!
          if (entot < entot_old) then
!$omp critical (crit_a) ! very rarely entered
!$          if (entot < entot_old) then  ! make sure no other thread has changed
!                               entot_old since the critical section was entered
              entot_old=entot
              do j=1,nnpos
              do k=1,4
                  hkl_save(j,k)=hkl(j,k)
              end do
              end do
!$          endif
!$omp end critical (crit_a)
          end if
!
          if (entot < mumbo_en_bwr) then
!$omp critical (crit_b) ! rarely entered
!       FLAG ROTAMER FOR LATER WRITING
            do j=1,nnpos
            n7=hkl(j,3)
            n8=hkl(j,4)
            mol(j+1)%res(1)%res_aas(n7)%aa_rots(n8)%rot_flag=.true.
            end do
!
!$omp end critical (crit_b)
!
          end if
!
          ntest=mod(i,nj)
!
          if (ntest==0) then
                   ni=int(10000*i/ncomb_tot)
            ri=real(ni)/100
!$omp critical (crit_c)
            PRINT*, '  # Tested: ', i, '%age searched= ', ri,       &
     &              '    ENERGY MINIMUM= ', min(entot_old,entot)
!$omp end critical (crit_c)
           end if
!
      end do
!$omp end parallel
      close(15)
!
	PRINT*, '  DONE WITH #COMBINATIONS ', ncomb_tot
	PRINT*, '    LOWEST ENERGY= ',  entot_old
       	PRINT*, '  '
	PRINT*, '  BEST CONFIGURATION: '
!
	if (entot_old >=  mumbo_en_bwr) then
            PRINT*,  '                                                   '
            PRINT*,  ' >>>>>>>>>>>>>>>>>>>>>>>>>>  WARNING >>>>>>>>>>>>>>'
            PRINT*,  ' >>                                                '
            PRINT*,  ' >>  NO COMBINATIONS FOUND WITH E < ',mumbo_en_bwr  
            PRINT*,  ' >>  LOWEST ENERGY CONFIGURATION WILL BE KEPT      '
            PRINT*,  ' >>                                                '
            PRINT*,  ' >>>>>>>>>>>>>>>>>>>>>>>>>>  WARNING >>>>>>>>>>>>>>'
            PRINT*,  '                                                   '
!	    STOP
	end if
!
	DO j=1,nnpos
            PRINT*,(hkl_save(j,k),k=1,4)
	END DO
!
	do i=1,1000
        	temp_file(i:i)=' '
	end do
!
	call getcwd(mumbo_cwd)
	mumbo_ncwd=len_trim(mumbo_cwd)
	temp_file = mumbo_cwd(1:mumbo_ncwd) // '/mumbo.pdb'
!
        PRINT*, '  '
        PRINT*, '  COORDINATES BELOW WRITTEN TO FILE: '
        PRINT*, '     ', temp_file(1:len_trim(temp_file))
        PRINT*, '  '
!
	open(15,file=temp_file,form='formatted',status='unknown')
!
	do j=1,nnpos
	    n1=hkl_save(j,1)
	    n2=hkl_save(j,2)
	    n3=hkl_save(j,3)
	    n4=hkl_save(j,4)
	    call dump_pdb(n1,n2,n3,n4)
          mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_flag2=.true.
!          
!        keep the best rotamer at each position even if (entot_old >=  mumbo_en_bwr)
!        this ensures that new atom_sum-files will be written out even if (entot_old >=  mumbo_en_bwr) 
!
          mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_flag=.true.
!                    
	end do
!
	close(15)
!
        PRINT*, '  '
!
	END SUBROUTINE ALL_EXPLICIT
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE TOT_ENERGY (hkl,new,entot,fflag,en_up_to)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MOLEC_M
	USE EP_DATA_M
	USE MAX_DATA_M, ONLY : max_npos
!
	IMPLICIT NONE
!
	logical, dimension(size(mol)-1)       :: new
	integer, dimension(size(mol),4)       :: hkl
	real                                  :: entot, enlev
	real, dimension(max_npos)             :: en_up_to
!
	logical                    :: fflag
!
	integer ::  i, j, n1, n2, n3, n4, n5, n6, npos
!
!
!
	npos=(size(new))
!
	if (fflag) then
	  do i=1,size(en_up_to)
	    en_up_to(i)=0.0
	  end do
	end if
!
	entot=0
!
	do i= npos,1,-1
	    enlev=0
	    if (new(i)) then
!
!       CALCULATE ENERGY AT THIS LEVEL
!
		do j= npos,i,-1
!
                       n1=hkl(i,1)
                       n2=hkl(i,3)
                       n3=hkl(i,4)
                       n4=hkl(j,1)
                       n5=hkl(j,3)
                       n6=hkl(j,4)
!
!	SELF ENERGY (i=j) ELSE PAIRWISE ENERGY (i =/ j)
!
         enlev=enlev+(ea(n1)%eb(n2)%ec(n3)%ed(n4)%ee(n5)%ef(n6)%ep_en(1))
!
		end do
!
		if (i==npos) then
		    en_up_to(i)=enlev
		else
		    en_up_to(i)=enlev + en_up_to(i+1)
		end if
!
		entot = entot + enlev
!
	    else
		entot = en_up_to(i)
	    end if
	end do
!
	END SUBROUTINE TOT_ENERGY
!


