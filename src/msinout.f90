!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE WRITE_ENERGIES(toten,hkl)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MOLEC_M
	USE MUMBO_DATA_M
	USE EP_DATA_M
	USE MAX_DATA_M
!
	IMPLICIT NONE
!
	integer, dimension(size(mol),4)     :: hkl      
	logical, dimension(size(mol))       :: hkl_flag    
	real :: toten, enp(15), enhalf(15)
	logical, save :: first = .true.
	integer, save :: ncnt, nlin, nlon
	character (len=1000) :: temp_file, timp_file, tomp_file
	integer :: i, j, k, l, n1, n2, n3, n4, n5, n6
!
	integer, dimension(4)  :: set1, set2
	integer   :: nmol
	logical :: first1, first2, flag1, flag2
	real, dimension(15) :: ent       
	character (len=4) :: aanm
	real ::  envdwtot,  eneletot, enhbdtot, ensoltot, ensolrot 
	real ::  etot1, etot3, etot4, etot5, etot10, etot8, etot7, etot9
	real ::  etot11, etoi3, etoi4, etoi9
!
	if (first) then 
	    do i=1,1000
	       temp_file(i:i)=' '
	       timp_file(i:i)=' '
	       tomp_file(i:i)=' '
	    end do
	    first=.false.
	    ncnt = 0
	    nlin = 0
	    nlon = 0
	    call getcwd(mumbo_cwd)
	    mumbo_ncwd=len_trim(mumbo_cwd)
	    temp_file = mumbo_cwd(1:mumbo_ncwd) // '/mumbo.ene'
	    timp_file = mumbo_cwd(1:mumbo_ncwd) // '/mumbo.lis'
	    tomp_file = mumbo_cwd(1:mumbo_ncwd) // '/mumbo.ene2'
!
	    open(15,file=temp_file,form='formatted',status='unknown')
	    if (mumbo_log_ana_flag) then
	    open(16,file=timp_file,form='formatted',status='unknown')
	    open(17,file=tomp_file,form='formatted',status='unknown')
	    write(16,*) 'SUMMARY OF ROTAMERS PRESENT IN A GIVEN CONFIGURATION ' 
	    write(16,*) '-----------------------------------------------------'
	    write(17,*) 'SUMMARY OF RESID-RESID INTER FOR A GIVEN CONFIGURATION ' 
	    write(17,*) '------------------------------------------------------'
	    end if
!
	    write(15,*) 'SUMMARY OF RESIDUE ENERGIES FOR A GIVEN CONFIGURATION ' 
	    write(15,*) '------------------------------------------------------'
!
	end if
!
	ncnt=ncnt+1
!
	   etot1=  0
	   etot3=  0
	   etot4=  0
	   etot5=  0
	   etot7=  0
	   etot8=  0
	   etot9=  0
	   etot10= 0
!
	   etoi3=  0
	   etoi4=  0
	   etoi9=  0
!
	write(15,'(a,i5,a,f10.3)') 'CONF#: ',ncnt,' TOTAL ENERGY=', toten  
	write(15,'(16x,a63)')'     ETOT     EVDW    EELEC    ERPROB   ESOLV    EXRAY   EHBOND'
	nlin=nlin+2
!
	if (mumbo_log_ana_flag) then
!GFORTRAN		write (16,'(F10.3,\)') toten 
		write (16,'(F10.3)') toten 
		do i= 2,(size(mol))
	    	 n1=hkl(i-1,1)
	    	 n2=hkl(i-1,3)
	     	 n3=hkl(i-1,4)
! GFORTRAN	     	 write (16,'(x,a4,i3,a1,\)')                              &
	     	 write (16,'(1x,a4,i3,a1)')                              &
     &                  mol(n1)%res(1)%res_aas(n2)%aa_nam,          &
     &                  n3,','
		end do
		write (16,'(/)')                             
	end if
!
!	NOW FIGURING OUT THE DETAILS, USING ENERGIES STORED IN EP-PAIRS
!
!	..%rot_en(1)= enp1 = TEMPLATE INTERCATION + SOLV + SOLVR
!	..%rot_en(2)= enp2 = TEMPLATE INTERACTION (VDW + ELEC + RPROB + XRAY + HBOND) 
!	..%rot_en(3)= enp3 = VDW  
!	..%rot_en(4)= enp4 = ELEC 
!	..%rot_en(5)= enp5 = RPROB - ROT PROBABILITY (only for i = j)
!	..%rot_en(6)= enp6 = SOLVATION 
!	..%rot_en(7)= enp7 = SOLVATION REF(only for i = j)
!	..%rot_en(8)= enp8 = XRAY  (only for i = j)
!	..%rot_en(9)= enp9 = HBOND
!	..%rot_en(10)= enp10 = INDIVIDUEL AND NOT PAIRWISE SOLVATION CONTRIBUTIONS
!
	do i = 2, (size(mol))
	   n1=hkl(i-1,1)
	   n2=hkl(i-1,3)
	   n3=hkl(i-1,4)
!
	   do j= 1, size(enp)
		enp (j) = 0
		enhalf(j) = 0
	   end do
!
	   do j = 2, (size(mol))
		n4 = hkl(j-1,1)
		n5 = hkl(j-1,3)
		n6 = hkl(j-1,4)
		if (i==j) then
		  enp(1) = enp(1) + ea(n1)%eb(n2)%ec(n3)%ed(n4)%ee(n5)%ef(n6)%ep_en(1)
		  enp(2) = enp(2) + ea(n1)%eb(n2)%ec(n3)%ed(n4)%ee(n5)%ef(n6)%ep_en(2)
		  enp(3) = enp(3) + ea(n1)%eb(n2)%ec(n3)%ed(n4)%ee(n5)%ef(n6)%ep_en(3)
	    enhalf(3) = enhalf(3) + ea(n1)%eb(n2)%ec(n3)%ed(n4)%ee(n5)%ef(n6)%ep_en(3)
		  enp(4) = enp(4) + ea(n1)%eb(n2)%ec(n3)%ed(n4)%ee(n5)%ef(n6)%ep_en(4)
	    enhalf(4) = enhalf(4) + ea(n1)%eb(n2)%ec(n3)%ed(n4)%ee(n5)%ef(n6)%ep_en(4)
		  enp(5) = enp(5) + ea(n1)%eb(n2)%ec(n3)%ed(n4)%ee(n5)%ef(n6)%ep_en(5)
		  enp(6) = enp(6) + ea(n1)%eb(n2)%ec(n3)%ed(n4)%ee(n5)%ef(n6)%ep_en(6)
		  enp(7) = enp(7) + ea(n1)%eb(n2)%ec(n3)%ed(n4)%ee(n5)%ef(n6)%ep_en(7)
		  enp(8) = enp(8) + ea(n1)%eb(n2)%ec(n3)%ed(n4)%ee(n5)%ef(n6)%ep_en(8)
		  enp(9) = enp(9) + ea(n1)%eb(n2)%ec(n3)%ed(n4)%ee(n5)%ef(n6)%ep_en(9)
	    enhalf(9) = enhalf(9) + ea(n1)%eb(n2)%ec(n3)%ed(n4)%ee(n5)%ef(n6)%ep_en(9)
		  enp(10) = enp(10) + ea(n1)%eb(n2)%ec(n3)%ed(n4)%ee(n5)%ef(n6)%ep_en(10)
		else
!
		  enp(1) = enp(1) + ea(n1)%eb(n2)%ec(n3)%ed(n4)%ee(n5)%ef(n6)%ep_en(1)
		  enp(2) = enp(2) + ea(n1)%eb(n2)%ec(n3)%ed(n4)%ee(n5)%ef(n6)%ep_en(2)
		  enp(3) = enp(3) + ea(n1)%eb(n2)%ec(n3)%ed(n4)%ee(n5)%ef(n6)%ep_en(3)
	    enhalf(3) = enhalf(3) + (ea(n1)%eb(n2)%ec(n3)%ed(n4)%ee(n5)%ef(n6)%ep_en(3)/2)
		  enp(4) = enp(4) + ea(n1)%eb(n2)%ec(n3)%ed(n4)%ee(n5)%ef(n6)%ep_en(4)
	    enhalf(4) = enhalf(4) + (ea(n1)%eb(n2)%ec(n3)%ed(n4)%ee(n5)%ef(n6)%ep_en(4)/2)
		  enp(5) = enp(5) 
		  enp(6) = enp(6) + ea(n1)%eb(n2)%ec(n3)%ed(n4)%ee(n5)%ef(n6)%ep_en(6)
		  enp(7) = enp(7) 
		  enp(8) = enp(8) 
		  enp(9) = enp(9) + ea(n1)%eb(n2)%ec(n3)%ed(n4)%ee(n5)%ef(n6)%ep_en(9)
	    enhalf(9) = enhalf(9) + (ea(n1)%eb(n2)%ec(n3)%ed(n4)%ee(n5)%ef(n6)%ep_en(9)/2)
		  enp(10) = enp(10) + ea(n1)%eb(n2)%ec(n3)%ed(n4)%ee(n5)%ef(n6)%ep_en(10)
		end if
	   end do
!
!  solvation energy = solvref + solv-energy
!
	   enp(10) =  enp(10) +  enp(7)
	   enp(2)  =  enp(2)  +  enp(10)
!!
	   write (15,'(1X,A1,I5,2X,A4,I3,7F9.3)')                             &
     &                      mol(n1)%res(1)%res_chid,                         &
     &                      mol(n1)%res(1)%res_nold,                         &
     &                      mol(n1)%res(1)%res_aas(n2)%aa_nam,               &
     &                      n3,                                              &
     &            enp(2), enp(3), enp(4), enp(5), enp(10), enp(8), enp(9)
!
	   nlin=nlin+1
!
	   etot1=   etot1  +  enp(2)
	   etoi3=   etoi3  +  enp(3)
	   etot3=   etot3  +  enhalf(3)
	   etoi4=   etoi4  +  enp(4)
	   etot4=   etot4  +  enhalf(4)
	   etot5=   etot5  +  enp(5)
	   etot7=   etot7  +  enp(7)
	   etot8=   etot8  +  enp(8)
	   etoi9=   etoi9  +  enp(9)
	   etot9=   etot9  +  enhalf(9)
	   etot10=  etot10 +  enp(10)
!
	end do
!
	etot11= etot3 + etot4 + etot5 + etot10 + etot8 + etot9
!
	write(15,'(2x,a14,7F9.3)')  	'E-SUM-RESIDUE ',				&
     &		   	etot1,   etoi3,  etoi4,  etot5,  etot10,  etot8,  etoi9
	write(15,'(2x,a14,7F9.3)')  	'E-SUM-OVERALL ',				&
     &		   	etot11,  etot3,  etot4,  etot5,  etot10,  etot8,  etot9
!
	write(15,'(a64)')'---------------------------------------------------------------'
	nlin=nlin+1
!
	if (nlin > max_lines_output) then
	PRINT*, '    '
	PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
	PRINT*, '>>>>>>>  TO MANY LINES TO WRITE INTO FILE:           '
	PRINT*, '>>>>>>> ', temp_file(1:len_trim(temp_file)), nlin
	PRINT*, '>>>>>>>  NUMBER OF LINES IN ANY OUTPUT RESTRICTED TO:'
	PRINT*, '>>>>>>> ', max_lines_output
	PRINT*, '>>>>>>>  DURING COMPILATION (MAX_LINES_OUTPUT)       '
	PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
	PRINT*, '    '
	STOP
	end if
!
!	NOW WRITING OUT INTERACTIONS IN GREAT DETAIL IF ASKED FOR
!
	if (mumbo_log_ana_flag) then
!
	  write(17,'(a,i5,a,f10.3)') ' CONF#: ',ncnt,' TOTAL ENERGY=', toten  
	  nlon=nlon+1
!
	  do i = 2, (size(mol))
!
	  envdwtot =  0.0
          eneletot =  0.0
	  enhbdtot =  0.0
	  ensoltot =  0.0
	  ensolrot =  0.0
!
	    n1=hkl(i-1,1)
	    n2=hkl(i-1,3)
	    n3=hkl(i-1,4)
	    set1(1)=n1
	    set1(2)=1
	    set1(3)=n2
	    set1(4)=n3
!
	    do j=1,4
	     set2(j)=1
	    end do
!
	    do j=1,size(mol)
		hkl_flag(j)  =.false.
	    end do
		hkl_flag(1)  =.true.
		hkl_flag(i-1)=.true.
!
	    do j=1,size(enp)
	      enp(j)=0
	    end do
!
!  FIRST DETERMINING ROTAMER SELF - ENERGIES ...
!
	    first1=.true.
	    first2=.true.
	    call energize(set1,set2,enp,first1,first2)
	    first1=.false.
	    first2=.false.
!
!
	write(17,'(a64)')'---------------------------------------------------------------'
	    write (17,'(a,i5,2x,a1,i5,2x,a4,i3,7F9.3)')                      & 
     &                      ' CONF#: ',ncnt,                                 & 
     &                      mol(n1)%res(1)%res_chid,                         & 
     &                      mol(n1)%res(1)%res_nold,                         & 
     &                      mol(n1)%res(1)%res_aas(n2)%aa_nam,               & 
     &                      n3                                               
	    write(17,'(16x,a69)')                                            &
     &    '    ETOT     EVDW    EELEC   ERPROB ESOLV-R   ESOLV*   EXRAY   EHBOND'
	    write(17,'(a16,a8,7F9.3)') 'SELF-ENERGY    ',                    &
     &                      '    ----',                                      &
     &                      enp(3), enp(4),  enp(5),                         &
     &                      enp(7), enp(10), enp(8), enp(9) 
	    nlon=nlon+4
!
			    envdwtot =  enp(3)
			    eneletot =  enp(4)
			    enhbdtot =  enp(9)
			    ensoltot =  enp(10)
			    ensolrot =  enp(7)
!!
!   NOW CALCULATING RESIDUE - RESIDUE INTERACTIONS, ALL OVER AGAIN.....
!
		  do j=1,mol(1)%mol_nrs
		    set2(1)=1
		    set2(2)=j
		    set2(3)=1
		    set2(4)=1
!
			aanm = mol(1)%res(j)%res_aas(1)%aa_nam
!
			do k=1,size(enp)
			  enp(k)=0
			  ent(k)=0
			end do
			call energize(set1,set2,enp,first1,first2)
			do k=1,size(enp)
			  ent(k)= enp(k)
			end do
!
	           if (mol(1)%res(j)%res_mut) then
!
 		          flag1=.false.
			  flag2=.false.
			  nmol = 0
			  do k=2, size(mol)
!
		if(mol(k)%res(1)%res_num == mol(1)%res(j)%res_num .and. k /= n1) then 
		   nmol = k
		   flag1=.true.
		   exit
		end if
!
			  end do
!
			  do k=2,size(mol)
			   if (hkl(k-1,1) == nmol) then
				set2(1)=hkl(k-1,1)
				set2(2)=hkl(k-1,2)
				set2(3)=hkl(k-1,3)
				set2(4)=hkl(k-1,4)
                                aanm = mol(set2(1))%res(1)%res_aas(set2(3))%aa_nam
				hkl_flag(k-1) = .true.
		  		flag2=.true.
				exit
			    end if
			  end do
!
			  do k=1,size(enp)
			     enp(k)=0
			  end do
			  if (flag1.and.flag2) then 
			    first1=.false.
			    first2=.false.
			    call energize(set1,set2,enp,first1,first2)	
			  end if
			  do k=1,size(enp)
			    ent(k)= ent(k)+ enp(k)
			  end do
!
		       end if
!
	   write (17,'(1x,a1,i5,2x,a4,3x,a8,2F9.3,2a8,F9.3,a8,F9.3)')         &
     &                      mol(1)%res(j)%res_chid,                          &
     &                      mol(1)%res(j)%res_nold,                          &
     &                      aanm,                                            &
     &                      '    ----',                                      &
     &                      ent(3), ent(4),                                  &
     &                      '    ----',                                      &
     &                      '    ----',                                      &
     &                      ent(10),                                         &
     &                      '    ----',                                      &
     &                      ent(9)                                            
	   nlon=nlon+1
!
			    envdwtot =  envdwtot + ent(3)
			    eneletot =  eneletot + ent(4)
			    enhbdtot =  enhbdtot + ent(9)
			    ensoltot =  ensoltot + ent(10)
!
		  end do
!
!  STILL FIGURING OUT ADDITIONAL STUFF LIKE RESIDUE LIGAND INTERACTIONS ...
!
		  do k=2,size(mol)
			if (.not.hkl_flag(k-1)) then
			   set2(1)=hkl(k-1,1)
			   set2(2)=hkl(k-1,2)
			   set2(3)=hkl(k-1,3)
			   set2(4)=hkl(k-1,4)
			   aanm = mol(set2(1))%res(1)%res_aas(set2(3))%aa_nam
!
			   do l=1,size(enp)
			     enp(l)=0
			     ent(l)=0
			   end do
			   first1=.false.
			   first2=.false.
			   call energize(set1,set2,enp,first1,first2)
			   do l=1,size(enp)
			      ent(l)= enp(l)
			   end do
!
	   write (17,'(1x,a1,i5,2x,a4,3x,a8,2F9.3,2a8,F9.3,a8,F9.3)')         &
     &                      mol(set2(1))%res(1)%res_chid,                    &
     &                      mol(set2(1))%res(1)%res_nold,                    &
     &                      aanm,                                            &
     &                      '    ----',                                      &
     &                      ent(3), ent(4),                                  &
     &                      '    ----',                                      &
     &                      '    ----',                                      &
     &                      ent(10),                                         &
     &                      '    ----',                                      &
     &                      ent(9)                                            
	   nlon=nlon+1
!
			    envdwtot =  envdwtot + ent(3)
			    eneletot =  eneletot + ent(4)
			    enhbdtot =  enhbdtot + ent(9)
			    ensoltot =  ensoltot + ent(10)
!
			 end if
		   end do
!
	  if (nlon > max_lines_output) then
	    PRINT*, '    '
	    PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
	    PRINT*, '>>>>>>>  TO MANY LINES TO WRITE INTO FILE:           '
	    PRINT*, '>>>>>>> ', tomp_file(1:len_trim(tomp_file)), nlon
	    PRINT*, '>>>>>>>  NUMBER OF LINES IN ANY OUTPUT RESTRICTED TO:'
	    PRINT*, '>>>>>>> ', max_lines_output
	    PRINT*, '>>>>>>>  DURING COMPILATION (MAX_LINES_OUTPUT)       '
	    PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
	    PRINT*, '    '
	    STOP
	  end if
!
	  write(17,'(4(a,F9.3))')                                             &
     &                      ' ENVDWTOT = ',  envdwtot,                        &
     &                      ' ENELETOT = ',  eneletot,                        &
     &                      ' ENSOLTOT = ', (ensolrot + ensoltot) ,           &
     &                      ' ENHBDTOT = ',  enhbdtot 
!
	  end do
!
	end if
!
!
	END SUBROUTINE WRITE_ENERGIES
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE WRITE_FINAL()
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MOLEC_M
	USE MUMBO_DATA_M
	USE MAX_DATA_M
!
	IMPLICIT NONE
!
	CHARACTER (LEN=1) :: achid
	CHARACTER (LEN=4) :: anam, name
	REAL :: bfa, xboff
	INTEGER :: j,m,n,o
	INTEGER :: nnrs,  nat, nbfa, n1, n2 , n3, n4
!
!       GFORTRAN
	n4=-1000
	n3=-1000
	n2=-1000
	n1=-1000
!       GFORTRAN	
!
	open(15,file=mumbo_outpdb,form='formatted',status='unknown')
!
	nat=0
	do j=1, mol(1)%mol_nrs
	    nnrs=  mol(1)%res(j)%res_nold
	    achid= mol(1)%res(j)%res_chid
!
	    if (mol(1)%res(j)%res_mut) then
!
		do m=2, size(mol)
		 if (nnrs  == mol(m)%res(1)%res_nold) then
		   if (achid == mol(m)%res(1)%res_chid) then
!
		     do n= 1,mol(m)%res(1)%res_naa
		        do o= 1,mol(m)%res(1)%res_aas(n)%aa_nrt
			  if (mol(m)%res(1)%res_aas(n)%aa_rots(o)%rot_flag2) then
			     anam = mol(m)%res(1)%res_aas(n)%aa_nam
			     n1 = m
			     n2 = 1
			     n3 = n
			     n4 = o
			  end if
		        end do
		     end do
!
	           end if
		  end if
		end do
!
	bfa = 0
	nbfa = 0
!
!
!	FIRST WRITE OUT GLYCINE-ATOMS BEFORE ADDING ROTAMER
!	(IF GLY THEN ROTAMER ATOMS ADDED == 0)
!
	do m=1,mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_nat
		nat = nat+1
		nbfa = nbfa+1
!	
	xboff = mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_occ
	name=mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_nam
!
	if (name(1:1)=='H') then
	   if (.not.mumbo_out_hyd) then
		nat = nat-1
		nbfa = nbfa-1
		cycle
	   end if
	end if
!
	bfa = bfa + mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_bfa
!
	if (name(4:4)==' ') then
!
     write(15,fmt=1010)                                                &
    &	'ATOM  ',                                                   &
    &      nat,                                                        &
    &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_nam,      &
    &      anam,                                                       &
    &      achid,                                                      &
    &      nnrs,                                                       &
    &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(1),   &
    &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(2),   &
    &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(3),   &
    &      xboff,                                                      &
    &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_bfa
!
	else if (name(4:4)/=' ') then
!
      write(15,fmt=1011)                                                &
     &	'ATOM  ',                                                   &
     &      nat,                                                        &
     &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_nam,      &
     &      anam,                                                       &
     &      achid,                                                      &
     &      nnrs,                                                       &
     &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(1),   &
     &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(2),   &
     &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(3),   &
     &      xboff,                                                      &
     &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_bfa
!
	end if
	end do
!
	bfa = bfa/nbfa
!
!	NOW WRITE OUT ROTAMER PART OF AMINO ACID
!
	do m=1,mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_nat
	nat = nat+1
!	
	xboff = 1.0 
	name=mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(m)%at_nam
!
	if (name(1:1)=='H') then
	   if (.not.mumbo_out_hyd) then 
		nat = nat-1
		cycle
	   end if
	end if
!

	if (name(4:4)==' ') then
!
      write(15,fmt=1010)                                                    &
     &	'ATOM  ',                                                       &
     &      nat,                                                            &
     &      mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(m)%at_nam,      &
     &      anam,                                                           &
     &      achid,                                                          &
     &      nnrs,                                                           &
     &      mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(m)%at_xyz(1),   &
     &      mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(m)%at_xyz(2),   &
     &      mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(m)%at_xyz(3),   &
     &      xboff,                                                          &
     &      bfa
!
	else if (name(4:4)/=' ') then
!
      write(15,fmt=1011)                                                &
     &	'ATOM  ',                                                       &
     &      nat,                                                            &
     &      mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(m)%at_nam,      &
     &      anam,                                                           &
     &      achid,                                                          &
     &      nnrs,                                                           &
     &      mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(m)%at_xyz(1),   &
     &      mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(m)%at_xyz(2),   &
     &      mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(m)%at_xyz(3),   &
     &      xboff,                                                          &
     &      bfa
!
	end if
	end do
!

	    else
!
!
!	IF NOT MUTATED THEN KEEP ORIGINAL STRUCTURE
!
	        do m=1, mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_nat
	           nat = nat+1
	           xboff = mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_occ
	           name=   mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_nam
!
	if (name(1:1)=='H') then
	   if (.not.mumbo_out_hyd) then 
		nat = nat-1
		cycle
	   end if
	end if
!
	if (name(4:4)==' ') then
!
      write(15,fmt=1010)                                                &
     &	'ATOM  ',                                                       &
     &      nat,                                                        &
     &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_nam,      &
     &      mol(1)%res(j)%res_aas(1)%aa_nam,                            &
     &      achid,                                                      &
     &      nnrs,                                                       &
     &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(1),   &
     &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(2),   &
     &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(3),   &
     &      xboff,                                                      &
     &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_bfa
!
	else 
!
      write(15,fmt=1011)                                                &
     &	'ATOM  ',                                                       &
     &      nat,                                                        &
     &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_nam,      &
     &      mol(1)%res(j)%res_aas(1)%aa_nam,                            &
     &      achid,                                                      &
     &      nnrs,                                                       &
     &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(1),   &
     &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(2),   &
     &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(3),   &
     &      xboff,                                                      &
     &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_bfa
!
	end if
	end do
!
	end if
	end do
!
	if (mumbo_lig_flag) then 
!
	    nnrs=  mol(mol_nmls)%res(1)%res_nold
	    achid= mol(mol_nmls)%res(1)%res_chid
!
	    do o= 1, mol(mol_nmls)%res(1)%res_aas(1)%aa_nrt
		if (mol(mol_nmls)%res(1)%res_aas(1)%aa_rots(o)%rot_flag2) then
		    anam = mol(mol_nmls)%res(1)%res_aas(1)%aa_nam
		    n1 = mol_nmls
		    n2 = 1
		    n3 = 1
		    n4 = o
		end if
	    end do
!
	do m=1,mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_nat
	nat = nat+1
!	
	xboff = 1.0 
	name=mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(m)%at_nam
!
	if (name(1:1)=='H') then
	   if (.not.mumbo_out_hyd) then
		nat = nat-1
		cycle
	   end if
	end if
!
	if (name(4:4)==' ') then
!
      write(15,fmt=1010)                                                    &
     &	'ATOM  ',                                                       &
     &      nat,                                                            &
     &      mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(m)%at_nam,      &
     &      anam,                                                           &
     &      achid,                                                          &
     &      nnrs,                                                           &
     &      mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(m)%at_xyz(1),   &
     &      mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(m)%at_xyz(2),   &
     &      mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(m)%at_xyz(3),   &
     &      xboff,                                                          &
     &      bfa
!
	else if (name(4:4)/=' ') then
!
      write(15,fmt=1011)                                                &
     &	'ATOM  ',                                                       &
     &      nat,                                                            &
     &      mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(m)%at_nam,      &
     &      anam,                                                           &
     &      achid,                                                          &
     &      nnrs,                                                           &
     &      mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(m)%at_xyz(1),   &
     &      mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(m)%at_xyz(2),   &
     &      mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(m)%at_xyz(3),   &
     &      xboff,                                                          &
     &      bfa
!
	end if
	end do
!
	end if
!
        write(15,fmt=1012) 'END   '                                       
	close(15)
!
	PRINT*, '        '
	PRINT*, '################################'
	PRINT*, '# SUMMARY WRITE_FINAL_STRUCT   #'
	PRINT*, '################################'
	PRINT*, '        '
	PRINT*, '  GMEC - STRUCTURE WRITTEN TO FILE: ', mumbo_outpdb(1:len_trim(mumbo_outpdb))
	PRINT*, '                  '
!
!
1010	format(a6,i5,2x,a4,a4,a,i4,4x,3f8.3,2f6.2)
1011	format(a6,i5,1x,a4,1x,a4,a,i4,4x,3f8.3,2f6.2)
1012	format(a6,/)
!
	END SUBROUTINE WRITE_FINAL
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE WRITE_STRUCT()
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MOLEC_M
	USE MAX_DATA_M
	USE MUMBO_DATA_M
!
	IMPLICIT NONE
!
	CHARACTER(LEN=1000), dimension(5) :: filenm
	INTEGER :: i,j,k,l,m 
	INTEGER :: npos, natn, nres, nnrs, nskip, nkeep
	CHARACTER(LEN=4) :: name
	CHARACTER(LEN=1) :: achid= ' '
	CHARACTER(LEN=132) :: string
!
	LOGICAL :: cflag, aaflag 
	REAL :: xboff, rgly
	integer :: taas, tncnt, tnres, tnkeep, tpos
	integer :: kaas, naas
!
!
	MUMBO_CONV_FLAG=.FALSE.
!
!	check for existing files and create backups
!
	PRINT*,mumbo_proc,  '        '
	PRINT*,mumbo_proc,  ' ################################'
	PRINT*,mumbo_proc,  ' # SUMMARY WRITE_STRUCT         #'
	PRINT*,mumbo_proc,  ' ################################'
	PRINT*,mumbo_proc,  '        '
!
	cflag=.true.
	npos=size(mol)
!
!	BACK UP PREVIOUS FILES IF REQUESTED
!
	if (mumbo_log_dbase_flag) then
		call backup(npos)
	end if
!
	tncnt = 0
	tnres = 0
	tnkeep = 0
	tpos = (size(mol)) -1
	taas = 0
	kaas = 0
	rgly = 0
!
	  do i=1,size(mol)
!
	  call get_filnms (i-1,filenm)
!
	  if (i == 1) then 
	    open(unit=14,file=filenm(2),status='unknown',form='formatted')
	    open(unit=15,file=filenm(1),status='unknown',form='formatted')
	  else 
	    open(unit=14,file=filenm(3),status='unknown',form='formatted')
	  end if
!
	  natn=    0
	  nres=    0
	  naas=    0
	  nkeep=   0
	  nskip=   0
!
	  do j=1,mol(i)%mol_nrs
	    do k=1,mol(i)%res(j)%res_naa
		aaflag=.false.
		
		if (i/=1) then
		   naas = naas +1
		   taas = taas +1 
		end if
!
		if (i/=1 .and. (mol(i)%res(1)%res_aas(k)%aa_nam) == gly_name) then 
		  cflag= mol(i)%res(1)%res_aas(k)%aa_rots(1)%rot_flag
!
		  if(.not.cflag) then
		    nskip=nskip+1
		    cycle
		  end if
!
		  nkeep=nkeep+1
		  nres = nres + 1
		  natn = natn + 1
		  if (natn > max_ntmp) then
			natn = 1
		  end if
!
		  kaas = kaas+1
		  nnrs = nkeep
		  achid= ' '
		  xboff = mol(i)%res(j)%res_aas(k)%aa_rots(1)%rot_prob
		  name=ca_name 
!
		  do m=1, size(mol(i)% res(1)%res_atom_mc)
		    if (name == (mol(i)%res(1)%res_atom_mc(m)%at_nam)) then
		    
		    if (.not.backrub_flag) then
!		
		       write(14,fmt=1010)                             &
     &		             'ATOM  ',                                &
     &                       natn,                                    &
     &                       ca_name,                                 &
     &                       gly_name,                                &
     &                       achid,                                   &
     &                       nnrs,                                    &
     &                       mol(i)%res(1)%res_atom_mc(m)%at_xyz(1),  &
     &                       mol(i)%res(1)%res_atom_mc(m)%at_xyz(2),  &
     &                       mol(i)%res(1)%res_atom_mc(m)%at_xyz(3),  &
     &                       xboff,                                   &
     &                       mol(i)%res(1)%res_atom_mc(m)%at_bfa   
		    else
		      if (.not.(mol(i)%res(1)%res_bbr)) then
		      write(14,fmt=1010)                              &
     &		             'ATOM  ',                                &
     &                       natn,                                    &
     &                       ca_name,                                 &
     &                       gly_name,                                &
     &                       achid,                                   &
     &                       nnrs,                                    &
     &                       mol(i)%res(1)%res_atom_mc(m)%at_xyz(1),  &
     &                       mol(i)%res(1)%res_atom_mc(m)%at_xyz(2),  &
     &                       mol(i)%res(1)%res_atom_mc(m)%at_xyz(3),  &
     &                       xboff,                                   &
     &                       mol(i)%res(1)%res_atom_mc(m)%at_bfa   
		      else 
		      write(14,fmt=1012)                              &
     &		             'ATOM  ',                                &
     &                       natn,                                    &
     &                       ca_name,                                 &
     &                       gly_name,                                &
     &                       achid,                                   &
     &                       nnrs,                                    &
     &                       mol(i)%res(1)%res_atom_mc(m)%at_xyz(1),  &
     &                       mol(i)%res(1)%res_atom_mc(m)%at_xyz(2),  &
     &                       mol(i)%res(1)%res_atom_mc(m)%at_xyz(3),  &
     &                       xboff,                                   &
     &                       mol(i)%res(1)%res_atom_mc(m)%at_bfa,     &   
     &                       rgly
		      end if
		    end if 
!
!
		    end if
		  end do
!
		  cycle
		end if

		  do l=1, mol(i)%res(j)%res_aas(k)%aa_nrt
		    if (i/=1 .and. (mol(i)%res(1)%res_aas(k)%aa_nam) == gly_name) then 
		      exit 
		    end if
		    nres=nres+1
!
		    if (i/=1) then
			cflag= mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_flag
			if(.not.cflag) then
			   nskip=nskip+1
			   cycle
			else
			   nkeep=nkeep+1
			   aaflag=.true.
			end if
		    end if
!
		    if (i==1) then
			nnrs=  mol(i)%res(j)%res_nold
			achid= mol(i)%res(j)%res_chid
		    else
			nnrs=nkeep
			achid= ' '
		    end if
!
		    do m=1, mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_nat
			natn=natn+1
		  	if (natn > max_ntmp) then
			   natn = 1
		        end if
!
			if (i==1) then
			  xboff = mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_occ
			else
			  xboff = mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_prob
			end if
!
			name=mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_nam
!
		if (i==1.or..not.backrub_flag) then
!
			if (name(4:4)==' ') then
!
      write(14,fmt=1010)                                                &
     &	'ATOM  ',                                                       &
     &      natn,                                                       &
     &      mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_nam,      &
     &      mol(i)%res(j)%res_aas(k)%aa_nam,                            &
     &      achid,                                                      &
     &      nnrs,                                                       &
     &      mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_xyz(1),   &
     &      mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_xyz(2),   &
     &      mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_xyz(3),   &
     &      xboff,                                                      &
     &      mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_bfa
!
				else
!
      write(14,fmt=1011)                                                &
     &	'ATOM  ',                                                       &
     &      natn,                                                       &
     &	mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_nam,          &
     &      mol(i)%res(j)%res_aas(k)%aa_nam,                            &
     &      achid,                                                      &
     &      nnrs,                                                       &
     &	mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_xyz(1),       &
     &	mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_xyz(2),       &
     &	mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_xyz(3),       &
     &	xboff,                                                          &
     &	mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_bfa
!
				end if
				
		else
		  if (.not.(mol(i)%res(1)%res_bbr)) then
		  
!
			if (name(4:4)==' ') then
!
		  
      write(14,fmt=1010)                                                &
     &	'ATOM  ',                                                       &
     &      natn,                                                       &
     &      mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_nam,      &
     &      mol(i)%res(j)%res_aas(k)%aa_nam,                            &
     &      achid,                                                      &
     &      nnrs,                                                       &
     &      mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_xyz(1),   &
     &      mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_xyz(2),   &
     &      mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_xyz(3),   &
     &      xboff,                                                      &
     &      mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_bfa
!
				else
!
      write(14,fmt=1011)                                                &
     &	'ATOM  ',                                                       &
     &      natn,                                                       &
     &	mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_nam,          &
     &      mol(i)%res(j)%res_aas(k)%aa_nam,                            &
     &      achid,                                                      &
     &      nnrs,                                                       &
     &	mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_xyz(1),       &
     &	mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_xyz(2),       &
     &	mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_xyz(3),       &
     &	xboff,                                                          &
     &	mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_bfa
!
				 end if
		   else
		   
			if (name(4:4)==' ') then
!
		  
      write(14,fmt=1012)                                                &
     &	'ATOM  ',                                                       &
     &      natn,                                                       &
     &      mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_nam,      &
     &      mol(i)%res(j)%res_aas(k)%aa_nam,                            &
     &      achid,                                                      &
     &      nnrs,                                                       &
     &      mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_xyz(1),   &
     &      mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_xyz(2),   &
     &      mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_xyz(3),   &
     &      xboff,                                                      &
     &      mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_bfa,      &
     &      mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_qrot
!
				else
!
      write(14,fmt=1013)                                                &
     &	'ATOM  ',                                                       &
     &      natn,                                                       &
     &	mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_nam,          &
     &      mol(i)%res(j)%res_aas(k)%aa_nam,                            &
     &      achid,                                                      &
     &      nnrs,                                                       &
     &	mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_xyz(1),       &
     &	mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_xyz(2),       &
     &	mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_xyz(3),       &
     &	xboff,                                                          &
     &	mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_bfa,          &
     &	mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_qrot
!
				 end if
		       end if
		      end if
!		   
				
				
!					    End loop over all atoms in rotamer:
		    end do
!					    End loop over all rotamers in aa:
		  end do

		  if (aaflag) then
		     kaas = kaas+1
		  end if
!
!					    End loop over all amino acids at residue position:
	    end do
!					    End loop over all residues in molecule:
	  end do 
!
	if (i/=1) then
	    PRINT *,mumbo_proc, ' WRITING OUT DATA MOLECULE ', i-1
	    PRINT *,mumbo_proc, '        RESIDUE NUMBER=    ', mol(i)%res(1)%res_chid, &
     &              mol(i)%res(1)%res_nold  
	    PRINT *,mumbo_proc, '        FOUND AAS=       ', naas
	    PRINT *,mumbo_proc, '        FOUND AAS*NROT=  ', nres
	    PRINT *,mumbo_proc, '        DELETED=         ', nskip
	    PRINT *,mumbo_proc, '        KEPT=            ', nkeep 
!
	    if (nkeep==0) then
	PRINT*, '    '
	PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
	PRINT*, '>>>>>>>  NO ROTAMER KEPT                             '
	PRINT*, '>>>>>>>  THIS WILL CERTAINLY CAUSE TROUBLE LATER     '
	PRINT*, '>>>>>>>                                              '
	PRINT*, '>>>>>>>  POSSIBLE REMEDY:                            '
	PRINT*, '>>>>>>>                                              '
	PRINT*, '>>>>>>>  IF ERROR OCCURS AT THE END OF MC_STEP ==>   '
	PRINT*, '>>>>>>>      INCREASE    ENERGY_CUTOFF= XX           '            
	PRINT*, '>>>>>>>                                              '
	PRINT*, '>>>>>>>  IF ERROR OCCURS AT THE END OF BRUT_STEP ==> '
	PRINT*, '>>>>>>>      INCREASE   ENERGY_BRUT=  KEEP= -XX      '            
	PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
	PRINT*, '    '
	STOP
	    else if (nkeep > 9999) then
	PRINT*, '    '
	PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
	PRINT*, '>>>>>>>                                              '
	PRINT*, '>>>>>>>  TRYING TO STORE MORE THAN 9999 ROTAMERS FOR '
	PRINT*, '>>>>>>>  RESIDUE NUMBER=    ', mol(i)%res(1)%res_chid, &
     &              mol(i)%res(1)%res_nold  
	PRINT*, '>>>>>>>  THIS WILL CAUSE FORMAT PROBLEMS ( > i4) IN  '
	PRINT*, '>>>>>>>  THE CORRESPONDING xx_atom_sum FILE  AND     '
	PRINT*, '>>>>>>>  WILL CAUSE MUMBO TO CRASH IN SUBSEQUENT     '
	PRINT*, '>>>>>>>  STEPS                                       '
	PRINT*, '>>>>>>>                                              '
	PRINT*, '>>>>>>>  DECREASE NUMBER OF ROTAMERS PER POSITION    '
	PRINT*, '>>>>>>>      AAS*NROT KEPT to  below 9999            '            
	PRINT*, '>>>>>>>                                              '
	PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
	PRINT*, '    '
	STOP
	    end if
!
		tncnt=tncnt+ nskip
		tnkeep=tnkeep + nkeep
		tnres=tnres + nres
	end if
!
	close(14)
!
	end do
!
	PRINT*, mumbo_proc,'          '
	PRINT*, mumbo_proc,' NUM11 POS   (TOTAL)             :', tpos
	PRINT*, mumbo_proc,' NUM11 AAS   (TOTAL,KEPT,DELETED):', taas,  kaas, (taas-kaas)
	PRINT*, mumbo_proc,' NUM12 AAS*ROTS(TOT,KEPT,DELETED):', tnres, tnkeep, tncnt
	PRINT*, mumbo_proc,'          '
	PRINT*,'        '
!
	if (tncnt == 0) MUMBO_CONV_FLAG=.TRUE.
!
!	now write out main-chain information of positions to be mumboed
!
	natn=0
	do i=2,size(mol)
!
          do k=1,132
            string(k:k)=' '
          end do 
!
!       incorperate ligand_flag			
!
	  if (mumbo_lig_flag) then
	    if (i==size(mol)) then 
		natn = natn + 1
		if (natn > max_ntmp) then
			natn = 1
		end if
!
!
	        write (string,fmt=1010)                               &
     &        'ATOM  ',                                             &
     &        natn   ,                                              &
     &        'CA    ',lig_name,                                    &
     &        mol(i)%res(1)%res_chid,                               &
     &        mol(i)%res(1)%res_nold,                               &
     &        1.00,                     &
     &        1.00,                     &
     &        1.00,                     &
     &        1.00,                     &
     &        1.00
                write (15,fmt='(a)') string
                exit
            end if
	  end if 
!
	  do j=1,(size(mol(i)%res(1)%res_atom_mc))
                do k=1,132
                     string(k:k)=' '
                end do 
		natn=natn+1
		if (natn > max_ntmp) then
			natn = 1
		end if
!
	write (string,fmt=1015)                                         &
     &      'ATOM  ',                                                   &
     &      natn ,                                                      &
     &      mol(i)%res(1)%res_atom_mc(j)%at_nam ,                       &
     &      'ALA ',                                                     &
     &      mol(i)%res(1)%res_chid,                                     &
     &      mol(i)%res(1)%res_nold,                                     &
     &      mol(i)%res(1)%res_atom_mc(j)%at_xyz(1),                     &
     &      mol(i)%res(1)%res_atom_mc(j)%at_xyz(2),                     &
     &      mol(i)%res(1)%res_atom_mc(j)%at_xyz(3),                     &
     &      mol(i)%res(1)%res_atom_mc(j)%at_occ,                        &
     &      mol(i)%res(1)%res_atom_mc(j)%at_bfa
!     
              if (backrub_flag.and.(mol(i)%res(1)%res_bbr)) then 
                write (string(71:74),fmt='(a4)') bbr_name
              end if
!
!              if (water_flag.and.(mol(i)%res(1)%res_wtr)) then 
              if (water_flag) then 
                write (string(76:80),fmt='(a4)') wtr_name
              end if
!              
        write (15,fmt='(a)') string
!
	   end do
	end do
!
	close(15)
!
!  PLEASE BE AWARE THAT THE FORMAT BELOW DOES NOT ALLOW FOR RESIDUE NUMBERS THAT EXCEED 9999
!
1010	format(a6,i5,2x,a4,a4,a,i4,4x,3f8.3,2f6.2)
1011	format(a6,i5,1x,a4,1x,a4,a,i4,4x,3f8.3,2f6.2)
1012	format(a6,i5,2x,a4,a4,a,i4,4x,3f8.3,2f6.2,5x,f7.2)
1013	format(a6,i5,1x,a4,1x,a4,a,i4,4x,3f8.3,2f6.2,5x,f7.2)
1015	format(a6,i5,2x,a4,a4,a,i4,4x,3f8.3,2f6.2)
1016	format(a6,i5,2x,a4,a4,a,i4,4x,3f8.3,2f6.2,9x,a4)
!
	PRINT*, '  ... FINISHED WRITING STRUCTURE '
!
	END SUBROUTINE WRITE_STRUCT
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	subroutine backup(npos)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MAX_DATA_M
!
	IMPLICIT NONE
!
	CHARACTER(LEN=1000), dimension(5) :: filenm
	CHARACTER(LEN=1000) :: string
	INTEGER :: npos, na, ios
	LOGICAL :: flag
	CHARACTER(LEN=132) :: line
	INTEGER i,j,k,l,m
!	
	na=0
	do i= 1,npos
	   call get_filnms(i-1,filenm)
	   do j= 1,5
	      do k=1,1000
			string(k:k)=' '
		end do
		   na=len_trim(filenm(j))
		   string(1:na)=filenm(j)
!
	    do l= 1,8
		   flag=.false.
		   write (string(na+1:na+3),'(a2,i1)') '--',(9-l)
!		   
		   inquire (file=string(1:na+3),exist=flag)
!
		   if (flag) then
!
	open(14,file=string(1:na+3),status='unknown',form='formatted')
	write (string(na+1:na+3),'(a2,i1)') '--',((9-l)+1)
	open(15,file=string(1:na+3),status='unknown',form='formatted')
			  do m=1, max_lines_input
			      read(14,fmt='(a)',iostat=ios) line
			      if (ios==eo_file) then
!			         print*,'END-OF-FILE REACHED IN FILE: '
!			         print*, string(1:na+3) 
!			         print*,'NO HARM DONE                 '
			         exit
			      end if
			      if (m==max_lines_input) then
	PRINT*, '    '
	PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
	PRINT*, '>>>>>>>  END-OF-FILE NOT REACHED:                    '
	PRINT*, '>>>>>>> ', string(1:na+3)
	PRINT*, '>>>>>>>  INCREASE MAX_LINES_INPUT                     '
	PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
	PRINT*, '    '
	STOP
			      end if
			      write(15,fmt='(a)') line
			  end do
			  close(14)
			  close(15)
		   end if
!
		end do
!
		flag=.false.
		string(1:na)=filenm(j)
		inquire (file=string(1:na),exist=flag)
		if (flag) then
!
	open(14,file=string(1:na),status='unknown',form='formatted')
	write (string(na+1:na+3),'(a3)') '--1'              		
	open(15,file=string(1:na+3),status='unknown',form='formatted')
			do m=1, max_lines_input
			      read(14,fmt='(a)', iostat=ios) line
			      if (ios==eo_file) then
!			         print*,'END-OF-FILE REACHED IN FILE: '
!			         print*, string(1:na) 
!			         print*,'NO HARM DONE                 '
			         exit
			      end if
			      if (m==max_lines_input) then
	PRINT*, '    '
	PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
	PRINT*, '>>>>>>>  END-OF-FILE NOT REACHED:                    '
	PRINT*, '>>>>>>> ', string(1:na)
	PRINT*, '>>>>>>>  INCREASE MAX_LINES_INPUT                     '
	PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
	PRINT*, '    '
	STOP
			      end if
			      write(15,fmt='(a)') line
			end do
			close(14)
			close(15)
		end if
	   end do
	end do
!
	END SUBROUTINE BACKUP
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE LOAD_PREV_STRUCT 
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MAX_DATA_M
!
	IMPLICIT NONE
!
	INTEGER :: natmc, npos, na, nb
!
	na=max_lines_input
	nb=max_npos
!
	CALL GET_MCINFO(na,nb,natmc,npos)
!
	CALL ALLO_PREV_STRUCT(npos)
!
	CALL GET_PREV_STRUCT(npos)
!
	END SUBROUTINE LOAD_PREV_STRUCT
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE ALLO_PREV_struct(npos)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MOLEC_M
	USE MAX_DATA_M
	USE MUMBO_DATA_M
!
	IMPLICIT NONE
!
	integer :: npos
	character (LEN=1000), dimension(5):: filenam
	integer :: nresid, ncnt, i, nx, ny, nz, j, k, na
	integer, dimension(:), allocatable :: natom
	character(LEN=4), dimension(:), allocatable ::cnam
	logical, dimension(:), allocatable :: lflag
	character(LEN=4) :: old, brname, wrname
	character(LEN=1) :: cid
	logical :: first 
!
	IF (ALLOCATED(mol)) DEALLOCATE (mol)
	ALLOCATE (mol(npos+1))
	mol_nmls=npos+1
!
	ncnt=0
	call get_filnms(ncnt,filenam)
	call get_nresid(filenam(2),nresid,max_lines_input,cid)
	allocate(natom(nresid),cnam(nresid))
!
	call get_natom(filenam(2),nresid,natom,cnam,max_lines_input)
	allocate (mol(1)%res(nresid))
	mol(1)%mol_nrs=nresid
!
	do,i=1,nresid
	   allocate(mol(1)%res(i)%res_aas(1))
	   mol(1)%res(i)%res_naa=1
	   mol(1)%res(i)%res_mut= .false.
	   mol(1)%res(i)%res_lig= .false.
	   mol(1)%res(i)%res_bbr= .false.
	   mol(1)%res(i)%res_pps= .false.
!	   mol(1)%res(i)%res_wtr= .false.
	   
	   allocate(mol(1)%res(i)%res_aas(1)%aa_rots(1))
	   mol(1)%res(i)%res_aas(1)%aa_nrt=1
	   allocate(mol(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(natom(i)))
	   mol(1)%res(i)%res_aas(1)%aa_rots(1)%rot_nat=natom(i)
	end do
!
	do, i=2,npos+1
	       allocate(mol(i)%res(1))
	       mol(i)%mol_nrs=1
	       mol(i)%res(1)%res_mut= .false.
	       mol(i)%res(1)%res_lig= .false.
	       mol(i)%res(1)%res_bbr= .false.
!	       mol(i)%res(1)%res_wtr= .false.
	       mol(i)%res(1)%res_pps= .false.
	end do
!
	deallocate(natom,cnam)
!
!	CHECKING  ----- 0_mch_sum ------- AND ALLOCATING STUFF
!
	call get_nresid(filenam(1),nresid,max_lines_input,cid)
	allocate (natom(nresid),cnam(nresid),lflag(nresid))
!
	call get_natom(filenam(1),nresid,natom,cnam,max_lines_input)
!
	mumbo_lig_flag=.false.
	do, i=1,nresid
	    if (cnam(i) == lig_name) then
		mumbo_lig_flag=.true.
	    end if
	end do
!
	if (mumbo_lig_flag) then
    	       mol(npos+1)%res(1)%res_lig= .true.
	end if
!
!	HERE FINDING OUT ABOUT BACKRUB_FLAG AND WHICH RESIDUES TO BE BACKRUB(B)ED
!
	backrub_flag=.false.
	brname=bbr_name
	call get_lflag(filenam(1),nresid,natom,lflag,brname)
	do, i=2,npos+1
	       if (lflag(i-1)) backrub_flag=.true.
	       mol(i)%res(1)%res_bbr=lflag(i-1)
	end do
!
!DEBUG
!	PRINT*, 'BACKRUB_FLAG', backrub_flag
!	do, i=2,npos+1
!	       PRINT*,i-1,mol(i)%res(1)%res_bbr
!	end do
!DEBUG
!
!
!	HERE FINDING OUT ABOUT WATER_FLAG BEING PRESENT IN 0_mch_sum
!       THIS makes only sense in the none WATER steps...if JOB = WATER specified then water_flag will always remain true
!
	if (.NOT.water_flag) then 
	   wrname=wtr_name
	   call get_lflag(filenam(1),nresid,natom,lflag,wrname)
	   do, i=2,npos+1
	         if (lflag(i-1)) water_flag=.true.
!	         mol(i)%res(1)%res_wtr=lflag(i-1)
	   end do
        end if 
!
        do, i=2,npos+1
	       allocate(mol(i)%res(1)%res_atom_mc(natom(i-1)))
	end do
!
	deallocate(natom,cnam)
!
!	NOW ALLOCATING RESIDUES TO BE MUMBOED
!
	do, i=2,npos+1
!
	call get_filnms((i-1),filenam)
	call get_nresid(filenam(3),nx,max_lines_input,cid)
	allocate (natom(nx),cnam(nx))
	call get_natom(filenam(3),nx,natom,cnam,max_lines_input)
!
	old='    '
	nz=0
	first=.true.
	do j=1, nx
		if (cnam(j)/=old) then
			if (first) then
			   first=.false.
			   nz=1
			   old=cnam(j)
			else
			   nz=nz+1
			   old=cnam(j)
			end if
		end if
	end do
!
	allocate(mol(i)%res(1)%res_aas(nz))
	mol(i)%res(1)%res_naa=nz
!
	old='    '
	ny=0
	nz=0
	first=.true.
	do j=1, nx
		if (cnam(j)==old) then
 		   ny=ny+1
		else if (cnam(j)/=old) then
		  if (first) then
		 	first=.false.
			nz=1
			ny=1
			old=cnam(j)
		  else
		    allocate(mol(i)%res(1)%res_aas(nz)%aa_rots(ny))
		    mol(i)%res(1)%res_aas(nz)%aa_nrt=ny
!
		    do k=1,ny
			na=natom(j-1)
			allocate(mol(i)%res(1)%res_aas(nz)%aa_rots(k)%rot_ats(na))
			mol(i)%res(1)%res_aas(nz)%aa_rots(k)%rot_nat=na
		    end do
!
		    nz=nz+1
		    ny=1
		    old=cnam(j)
		  end if
		end if
	end do
!
	allocate(mol(i)%res(1)%res_aas(nz)%aa_rots(ny))
	mol(i)%res(1)%res_aas(nz)%aa_nrt=ny
!
	do k=1,ny
		na=natom(nx)
!		
!       line above corrected from na=natom(j-1) on March 29, 2016
!
		allocate(mol(i)%res(1)%res_aas(nz)%aa_rots(k)%rot_ats(na))
		mol(i)%res(1)%res_aas(nz)%aa_rots(k)%rot_nat=na
	end do
!
	deallocate (natom,cnam)
	end do
!
	END SUBROUTINE ALLO_PREV_STRUCT
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE GET_PREV_STRUCT(npos)
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!
	USE MOLEC_M
	USE MAX_DATA_M
!
	IMPLICIT NONE
!
	integer:: npos,na,nb,nc,nd,i,j,k,l, m
	integer:: nw, nn, ios
	real:: xbfa, xx, xy, xz, xocc, xprob, xrot
	character(LEN=1000), dimension(5):: filenam
	character(LEN=132) :: string
	character(LEN=80) :: boff, baff
	character(LEN=80), dimension(40) :: word
	character(LEN=1) :: chid
	logical :: chainid_flag
!
	do i=1,npos+1
		na=0
		nb=0
		nc=0
		nd=0
!
!	READ IN CONSTANT PART FIRST
!
	if (i==1) then
		call get_filnms((i-1),filenam)
		open(14,file=filenam(2),status='old',form='formatted')
		na=mol(i)%mol_nrs
		do j=1,na
		mol(i)%res(j)%res_num= j
		nb=mol(i)%res(j)%res_aas(1)%aa_rots(1)%rot_nat
		mol(i)%res(j)%res_aas(1)%aa_rots(1)%rot_flag=.true.
		mol(i)%res(j)%res_aas(1)%aa_rots(1)%rot_flag2=.false.
!
		do k=1,nb
			read(14,fmt='(a)',iostat=ios) string
			if (ios==eo_file) then
		PRINT*, '    '
		PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
		PRINT*, '>>>>>>>  UNEXPECTED END-OF-FILE REACHED IN           '
		PRINT*, '>>>>>>> ', filenam(2)(1:len_trim(filenam(2)))
		PRINT*, '>>>>>>>  MUST EXIT IN SUBROUTINE GET_PREV_STRUCT     '
		PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
		PRINT*, '    '
		STOP
			end if
			call parser(string, word, nw)
			call chainid_present(word(5),chainid_flag)
			if (chainid_flag) then
				baff=word(5)
				chid=baff(1:1)
				do l=6,40
				   word(l-1)=word(l)
				end do
			else if (.not.chainid_flag) then
				chid=' '
			end if
!
			if (k==1) then
				boff=word(4)
				read(word(5),*)  nn
				mol(i)%res(j)%res_aas(1)%aa_nam=boff(1:4)
				mol(i)%res(j)%res_chid = chid
				mol(i)%res(j)%res_nold = nn
			        mol(i)%res(j)%res_aas(1)%aa_rots(1)%rot_prob=1.0
			end if
			boff=word(3)
			read(word(6),*)  xx
			read(word(7),*)  xy
			read(word(8),*)  xz
			read(word(9),*)  xocc
			read(word(10),*) xbfa
	mol(i)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(k)%at_nam=boff(1:4)
	mol(i)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(k)%at_xyz(1)=xx
	mol(i)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(k)%at_xyz(2)=xy
	mol(i)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(k)%at_xyz(3)=xz
	mol(i)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(k)%at_occ=xocc
	mol(i)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(k)%at_bfa=xbfa
!
		end do
		end do
		close(14)
!
!	STILL HAVE TO READ IN FILE   ----- 0_mch_sum -------
!
	open(14,file=filenam(1),status='old',form='formatted')
!
	na=0
!
	do j=1,npos
	   do k=1,(size(mol(j+1)%res(1)%res_atom_mc))
		read(14,fmt='(a)',iostat=ios) string
		if (ios==eo_file) then
		  PRINT*, '    '
		  PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
		  PRINT*, '>>>>>>>  UNEXPECTED END-OF-FILE REACHED IN           '
		  PRINT*, '>>>>>>> ', filenam(1)(1:len_trim(filenam(1)))
		  PRINT*, '>>>>>>>  MUST EXIT IN SUBROUTINE GET_PREV_STRUCT     '
		  PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
		  PRINT*, '    '
		  STOP
		end if
		call parser(string,word,nw)
!
		call chainid_present(word(5),chainid_flag)
		if (chainid_flag) then
			baff=word(5)
			chid=baff(1:1)
			do l=6,40
			   word(l-1)=word(l)
			end do
		else if (.not.chainid_flag) then
			chid=' '
		end if
!
		read (word(5),*) na
!
		if (k == 1) then
!
	           mol(j+1)%res(1)%res_chid=chid
	           mol(j+1)%res(1)%res_nold=na
!
		end if
!
			boff=word(3)		
			read(word(6),*)  xx
			read(word(7),*)  xy
			read(word(8),*)  xz
			read(word(9),*)  xocc
			read(word(10),*) xbfa
!
	mol(j+1)%res(1)%res_atom_mc(k)%at_nam=boff(1:4)
	mol(j+1)%res(1)%res_atom_mc(k)%at_xyz(1)=xx
	mol(j+1)%res(1)%res_atom_mc(k)%at_xyz(2)=xy
	mol(j+1)%res(1)%res_atom_mc(k)%at_xyz(3)=xz
	mol(j+1)%res(1)%res_atom_mc(k)%at_occ=xocc
	mol(j+1)%res(1)%res_atom_mc(k)%at_bfa=xbfa
!
	   end do
	end do
!
	close (14)
!
!	NOW READ IN POSITIONS TO BE MUMBOED
!
	else
		call get_filnms((i-1),filenam)
		open(14,file=filenam(3),status='old',form='formatted')
		na=mol(i)%res(1)%res_naa
!
		do j=1,na
		nb=mol(i)%res(1)%res_aas(j)%aa_nrt
		do k=1,nb
		nc=mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_nat
		mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_flag=.true.
		mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_flag2=.false.
		do l=1,nc
			read(14,fmt='(a)',iostat=ios) string
			if (ios==eo_file) then
		  PRINT*, '    '
		  PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
		  PRINT*, '>>>>>>>  UNEXPECTED END-OF-FILE REACHED IN           '
		  PRINT*, '>>>>>>> ', filenam(3)(1:len_trim(filenam(3)))
		  PRINT*, '>>>>>>>  MUST EXIT IN SUBROUTINE GET_PREV_STRUCT     '
		  PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
		  PRINT*, '    '
		  STOP
			end if
!
			call parser(string, word, nw)
!
			call chainid_present(word(5),chainid_flag)
			if (chainid_flag) then
				do m=6,40
				   word(m-1)=word(m)
				end do
			end if
!
			if (l==1 .and. k==1) then
				boff=word(4)
				mol(i)%res(1)%res_aas(j)%aa_nam=boff(1:4)
			end if
			if (l==1) then
				read(word(9),*) xprob
				mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_prob=xprob
				if (mol(i)%res(1)%res_bbr) then 
				    read(word(11),*) xrot
				    mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_qrot=xrot
				else
				    mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_qrot=0.00
				end if
			end if
!
			boff=word(3)
			read(word(6),*)  xx
			read(word(7),*)  xy
			read(word(8),*)  xz
			read(word(9),*)  xocc
			read(word(10),*) xbfa
	mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_ats(l)%at_nam=boff(1:4)
	mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_ats(l)%at_xyz(1)=xx
	mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_ats(l)%at_xyz(2)=xy
	mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_ats(l)%at_xyz(3)=xz
	mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_ats(l)%at_occ=xocc
	mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_ats(l)%at_bfa=xbfa
!
		end do
		end do
		end do
!
		close(14)
!
!	CHECKING FOR GLYCINES AT POSITIONS TO BE MUMBOED
!
		do j=1, mol(i)%res(1)%res_naa 
		  if (mol(i)%res(1)%res_aas(j)%aa_nam == gly_name) then 
			mol(i)%res(1)%res_aas(j)%aa_nrt=1
			mol(i)%res(1)%res_aas(j)%aa_rots(1)%rot_nat = 0 
			mol(i)%res(1)%res_aas(j)%aa_rots(1)%rot_prob= 1.0
		  end if 
		end do
!
	end if
	end do
!
	do i=2,npos+1
	   do j=1, mol(1)%mol_nrs
		if (mol(1)%res(j)%res_nold == mol(i)%res(1)%res_nold) then
		   if (mol(1)%res(j)%res_chid == mol(i)%res(1)%res_chid) then
!
			    mol(i)%res(1)%res_num = mol(1)%res(j)%res_num
			    mol(1)%res(j)%res_mut = .true.
!
		   end if
		end if
	   end do
	end do
!
! 	Filling up a few things
!
	do i=1,size(mol)
         do j=1,mol(i)%mol_nrs
	   do k=1,mol(i)%res(j)%res_naa
              do l=1, mol(i)%res(j)%res_aas(k)%aa_nrt
                 do m=1, mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_nat
		    mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_n12 = 0
		    mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_n14 = 0
                 end do
	       end do 
	     end do
	   end do
	end do
!
!
	PRINT*, '        '
	PRINT*, '################################'
	PRINT*, '# SUMMARY  GET_PREV_STRUCT     #'
	PRINT*, '################################'
	PRINT*, '        '
	PRINT*, '  ... FINISHED GETTING PREV STRUCTURE '
	PRINT*, '        '
!
	END SUBROUTINE GET_PREV_STRUCT
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!@author  Yasemin Rudolph
!
    SUBROUTINE APPLY_BACKRUB_MAINCHAIN()
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
    USE MOLEC_M
    USE MUMBO_DATA_M
    USE MAX_DATA_M
!
    IMPLICIT NONE

    CHARACTER (LEN=4) :: atnm
    LOGICAL :: flag
    REAL, DIMENSION(3) :: temp_vector, temp_vector_back                             ! temporary vectors for rotation only
    REAL, DIMENSION(3) :: axis_point1, axis_point2, axis_point3                     ! CA atoms set up the axis
    REAL, DIMENSION(3) :: point1, point2, point3, point4, point5, point6, point7    ! atoms between the CA-axis
    REAL :: qrot, qrot2                                 ! qrot2 = -qrot, needed for second rotation
    INTEGER :: i, q
    INTEGER :: m1, m2, m3, m4
    INTEGER :: io_error
    
    REAL, PARAMETER :: err3 = 0.001



    PRINT*, '        '
    PRINT*, '###################################'
    PRINT*, '# START APPLY_BACKRUB_MAINCHAIN  #'
    PRINT*, '###################################'
    PRINT*, '        '



    IF (.NOT.backrub_flag) THEN
      RETURN
    END IF


! HERE IDENTIFY THE CA COORDS OF RESIDUE_i-1, CA COORDS OF RESIDUE_i, and CA COORDS OF RESIDUE_i+1 TO DEFINE THE 
! ROTATION AXIS FOR BACKRUB
!
! All atoms between res_i-1 and res_i+1 will be rotated around the axis between CA_i-1 and CA_i+1
! In the following two steps new rotation axis between CA_i-1 and CA_i  and  CA_i + CA_i+1 are set
! to rotate atoms counterwise qrot to make backbone torsions as little as possible (--> position of CA_i is altered)

    Loop1: DO i=2, mol_nmls     
      Loop2: IF ( mol(i)%res(1)%res_bbr ) THEN

        m1=1
        m2=mol(i)%res(1)%res_num
        m3=1
        m4=1


! mainchain has to be changed according to qrot only if qrot /= 0.
!        Loop3: IF ( mol(i)%res(1)%res_aas(1)%aa_rots(1)%rot_qrot /= 0. ) THEN     
        Loop3: IF ( abs(mol(i)%res(1)%res_aas(1)%aa_rots(1)%rot_qrot).gt.err3) THEN     

          qrot = mol(i)%res(1)%res_aas(1)%aa_rots(1)%rot_qrot
          qrot2 = -qrot                                        ! qrot2 can be changed to any value needed, e.g. 0.5*qrot
          
!          PRINT*, ' '
!          PRINT*, ' --- NEXT RESIDUE ---'
!          PRINT*, ' '
!          PRINT*, ' We are now looking at residue i = ', mol(i)%res(1)%res_nold

! Axis point 1 = residue_i-1
          Loop4a: DO q=1,mol(m1)%res(m2-1)%res_aas(m3)%aa_rots(m4)%rot_nat
            atnm=   mol(m1)%res(m2-1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_nam

              IF (atnm==ca_name) THEN         ! axis_point1 is CA of residue_i-1
                axis_point1(1) = mol(m1)%res(m2-1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(1)
                axis_point1(2) = mol(m1)%res(m2-1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(2)
                axis_point1(3) = mol(m1)%res(m2-1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(3)
              END IF
          END DO Loop4a

! Axis point 3 = residue_i+1
          Loop4b: DO q=1,mol(m1)%res(m2+1)%res_aas(m3)%aa_rots(m4)%rot_nat
            atnm=   mol(m1)%res(m2+1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_nam

              IF (atnm==ca_name) THEN         ! axis_point3 is CA of residue_i+1
                axis_point3(1) = mol(m1)%res(m2+1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(1)
                axis_point3(2) = mol(m1)%res(m2+1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(2)
                axis_point3(3) = mol(m1)%res(m2+1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(3)
              END IF
          END DO Loop4b
             
! Axis point 2 = residue_i
          Loop4c: DO q=1,mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_nat
            atnm=   mol(m1)%res(m2+1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_nam

              IF (atnm==ca_name) THEN         ! axis_point2 is CA of residue_i
                temp_vector(1) = mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(1)
                temp_vector(2) = mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(2)
                temp_vector(3) = mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(3)

                CALL BACKRUB_CALC( axis_point1, axis_point3, temp_vector, qrot, temp_vector_back ) 

                axis_point2(1) = temp_vector_back(1)
                axis_point2(2) = temp_vector_back(2)
                axis_point2(3) = temp_vector_back(3)

!                PRINT*, '--- TEST DEBUG TEST DEBUG TEST DEBUG ---'
!                PRINT*, 'axis_point1 = ', axis_point1
!                PRINT*, 'axis_point3 = ', axis_point3
!                PRINT*, ' Residue ', mol(m1)%res(m2)%res_num
!                PRINT*, 'temp_vector = CA before rotation = ', temp_vector
!                PRINT*, 'axis_point2 = CA after rotation = ', axis_point2
                                
              END IF
          END DO Loop4c


! NOW IDENTIFY ALL MAINCHAIN ATOMS AND ROTATE THEM qrot DEGREES AND THEN BACK BY -qrot DEGREES

          Loop5: DO q=1,mol(m1)%res(m2-1)%res_aas(m3)%aa_rots(m4)%rot_nat
            atnm=   mol(m1)%res(m2-1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_nam

              Loop5a: IF (atnm==c_name) THEN             ! C atom of res_i-1
                point1(1) = mol(m1)%res(m2-1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(1)
                point1(2) = mol(m1)%res(m2-1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(2)
                point1(3) = mol(m1)%res(m2-1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(3)

                  CALL BACKRUB_CALC( axis_point1, axis_point3, point1, qrot, temp_vector )

                  CALL BACKRUB_CALC( axis_point1, axis_point2, temp_vector, qrot2, temp_vector_back)

!                  PRINT*, ' '
!                  PRINT*, '--- TEST DEBUG TEST DEBUG TEST DEBUG ---'
!                  PRINT*, 'point1 = C atom of res_i-1   ', point1
!                  PRINT*, 'axes points are '
!                  PRINT*, 'axis_point1 = ', axis_point1
!                  PRINT*, 'axis_point2 = ', axis_point2
!                  PRINT*, 'axis_point3 = ', axis_point3                  
!                  PRINT*, 'temp_vector & qrot =         ', temp_vector, ' & ', qrot
!                  PRINT*, 'temp_vector_back & qrot2 =   ', temp_vector_back, ' & ', qrot2
!                  PRINT*, ' '

                  mol(m1)%res(m2-1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(1) = temp_vector_back(1)
                  mol(m1)%res(m2-1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(2) = temp_vector_back(2)
                  mol(m1)%res(m2-1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(3) = temp_vector_back(3)

              ELSE IF (atnm==o_name) THEN        ! O atom of res_i-1
                point2(1) = mol(m1)%res(m2-1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(1)
                point2(2) = mol(m1)%res(m2-1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(2)
                point2(3) = mol(m1)%res(m2-1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(3)

                  CALL BACKRUB_CALC( axis_point1, axis_point3, point2, qrot, temp_vector )

                  CALL BACKRUB_CALC( axis_point1, axis_point2, temp_vector, qrot2, temp_vector_back )

!                  PRINT*, ' '
!                  PRINT*, '--- TEST DEBUG TEST DEBUG TEST DEBUG ---'
!                  PRINT*, 'point2 = O atom of res_i-1   ', point2
!                  PRINT*, 'axes points are '
!                  PRINT*, 'axis_point1 = ', axis_point1
!                  PRINT*, 'axis_point2 = ', axis_point2
!                  PRINT*, 'axis_point3 = ', axis_point3  
!                  PRINT*, 'temp_vector & qrot =         ', temp_vector, ' & ', qrot
!                  PRINT*, 'temp_vector_back & qrot2 =   ', temp_vector_back, ' & ', qrot2
!                  PRINT*, ' '

                  mol(m1)%res(m2-1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(1) = temp_vector_back(1)
                  mol(m1)%res(m2-1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(2) = temp_vector_back(2)
                  mol(m1)%res(m2-1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(3) = temp_vector_back(3)

            END IF Loop5a
          END DO Loop5


          Loop6: DO q=1,mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_nat
            atnm=   mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_nam

              Loop6a: IF (atnm==n_name) THEN              ! N atom of res_i
                point3(1) = mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(1)
                point3(2) = mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(2)
                point3(3) = mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(3)

                  CALL BACKRUB_CALC( axis_point1, axis_point3, point3, qrot, temp_vector )

                  CALL BACKRUB_CALC( axis_point1, axis_point2, temp_vector, qrot2, temp_vector_back )

!                  PRINT*, ' '
!                  PRINT*, '--- TEST DEBUG TEST DEBUG TEST DEBUG ---'
!                  PRINT*, 'point3 = N atom of res_i     ', point3
!                  PRINT*, 'axes points are '
!                  PRINT*, 'axis_point1 = ', axis_point1
!                  PRINT*, 'axis_point2 = ', axis_point2
!                  PRINT*, 'axis_point3 = ', axis_point3  
!                  PRINT*, 'temp_vector & qrot =         ', temp_vector, ' & ', qrot
!                  PRINT*, 'temp_vector_back & qrot2 =   ', temp_vector_back, ' & ', qrot2
!                  PRINT*, ' '

                  mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(1) = temp_vector_back(1)
                  mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(2) = temp_vector_back(2)
                  mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(3) = temp_vector_back(3)

              ELSE IF (atnm==ca_name) THEN              ! CA atom of res_i  --> only rotated once!
                point4(1)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(1)
                point4(2)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(2)
                point4(3)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(3)

                  CALL BACKRUB_CALC( axis_point1, axis_point3, point4, qrot, temp_vector )

                  mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(1) = temp_vector(1)
                  mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(2) = temp_vector(2)
                  mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(3) = temp_vector(3)

!                  PRINT*, ' '
!                  PRINT*, '--- TEST DEBUG TEST DEBUG TEST DEBUG ---'
!                  PRINT*, 'point4 = CA atom of res_i    ', point4
!                  PRINT*, 'axes points are '
!                  PRINT*, 'axis_point1 = ', axis_point1
!                  PRINT*, 'axis_point2 = ', axis_point2
!                  PRINT*, 'axis_point3 = ', axis_point3  
!                  PRINT*, 'temp_vector & qrot =         ', temp_vector, ' & ', qrot.AND. 
!                  PRINT*, 'temp_vector_back & qrot2 =   ', temp_vector_back, ' & ', qrot2
!                  PRINT*, ' '

              ELSE IF (atnm==c_name) THEN                 ! C atom of res_i
                point5(1) = mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(1)
                point5(2) = mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(2)
                point5(3) = mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(3)

                  CALL BACKRUB_CALC( axis_point1, axis_point3, point5, qrot, temp_vector )

                  CALL BACKRUB_CALC( axis_point2, axis_point3, temp_vector, qrot2, temp_vector_back )

!                  PRINT*, ' '
!                  PRINT*, '--- TEST DEBUG TEST DEBUG TEST DEBUG ---'
!                  PRINT*, 'axes points are '
!                  PRINT*, 'axis_point1 = ', axis_point1
!                  PRINT*, 'axis_point2 = ', axis_point2
!                  PRINT*, 'axis_point3 = ', axis_point3  
!                  PRINT*, 'point5 = C atom of res_i     ', point5
!                  PRINT*, 'temp_vector & qrot =         ', temp_vector, ' & ', qrot
!                  PRINT*, 'temp_vector_back & qrot2 =   ', temp_vector_back, ' & ', qrot2
!                  PRINT*, ' '

                  mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(1) = temp_vector_back(1)
                  mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(2) = temp_vector_back(2)
                  mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(3) = temp_vector_back(3)

              ELSE IF (atnm==o_name) THEN                 ! O atom of res_i
                point6(1) = mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(1)
                point6(2) = mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(2)
                point6(3) = mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(3)

                  CALL BACKRUB_CALC( axis_point1, axis_point3, point6, qrot, temp_vector )

                  CALL BACKRUB_CALC( axis_point2, axis_point3, temp_vector, qrot2, temp_vector_back )

!                  PRINT*, ' '
!                  PRINT*, '--- TEST DEBUG TEST DEBUG TEST DEBUG ---'
!                  PRINT*, 'axes points are '
!                  PRINT*, 'axis_point1 = ', axis_point1
!                  PRINT*, 'axis_point2 = ', axis_point2
!                  PRINT*, 'axis_point3 = ', axis_point3  
!                  PRINT*, 'point6 = O atom of res_i     ' , point6
!                  PRINT*, 'temp_vector & qrot =         ', temp_vector, ' & ', qrot
!                  PRINT*, 'temp_vector_back & qrot2 =   ', temp_vector_back, ' & ', qrot2
!                  PRINT*, ' '

                  mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(1) = temp_vector_back(1)
                  mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(2) = temp_vector_back(2)
                  mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(3) = temp_vector_back(3)

            END IF Loop6a
          END DO Loop6


          Loop7: DO q=1,mol(m1)%res(m2+1)%res_aas(m3)%aa_rots(m4)%rot_nat
            atnm=   mol(m1)%res(m2+1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_nam

              Loop7a: IF (atnm==n_name) THEN             ! C atom of res_i+1
                point7(1) = mol(m1)%res(m2+1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(1)
                point7(2) = mol(m1)%res(m2+1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(2)
                point7(3) = mol(m1)%res(m2+1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(3)

                  CALL BACKRUB_CALC( axis_point1, axis_point3, point7, qrot, temp_vector )

                  CALL BACKRUB_CALC( axis_point2, axis_point3, temp_vector, qrot2, temp_vector_back )

                  mol(m1)%res(m2+1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(1) = temp_vector_back(1)
                  mol(m1)%res(m2+1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(2) = temp_vector_back(2)
                  mol(m1)%res(m2+1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(3) = temp_vector_back(3)

!                  PRINT*, ' '
!                  PRINT*, '--- TEST DEBUG TEST DEBUG TEST DEBUG ---'
!                  PRINT*, 'point7 = N atom of res_i+1   ', point7
!                  PRINT*, 'axes points are '
!                  PRINT*, 'axis_point1 = ', axis_point1
!                  PRINT*, 'axis_point2 = ', axis_point2
!                  PRINT*, 'axis_point3 = ', axis_point3  
!                  PRINT*, 'temp_vector & qrot =         ', temp_vector, ' & ', qrot
!                  PRINT*, 'temp_vector_back & qrot2 =   ', temp_vector_back, ' & ', qrot2
!                  PRINT*, ' '

            END IF Loop7a
          END DO Loop7

          ELSE
!          PRINT*, ' '
!          PRINT*, ' --- NEXT RESIDUE ---'
!          PRINT*, ' '
!          PRINT*, 'We are now looking at residue i = ', mol(i)%res(1)%res_nold
!          PRINT*, 'Even though this residue had the backrub option, MUMBO chose not to do a backrub here.'
!          PRINT*, '--> This residue was not rotated.'
!          PRINT*, 'Reason: For this residue ''GLY'' was specified in mumbo.inp '
!          PRINT*, ' '

        END IF Loop3     
      END IF Loop2
    END DO Loop1


OPEN( UNIT = 19, FILE = '0_bck_sum', STATUS = 'UNKNOWN', ACTION = 'WRITE', IOSTAT = io_error )
  IF ( io_error /= 0 ) THEN
    WRITE (*,*) 'Sorry, problems writing backrubbed residues into 0_bck_sum'
  ELSE
    DO i=2, mol_nmls
      IF (mol(i)%res(1)%res_bbr) then
      IF (abs(mol(i)%res(1)%res_aas(1)%aa_rots(1)%rot_qrot).gt.err3) THEN
      WRITE (19,*) mol(i)%res(1)%res_chid, mol(i)%res(1)%res_nold, mol(i)%res(1)%res_aas(1)%aa_rots(1)%rot_qrot
      END IF
      END IF
    END DO
  END IF
CLOSE( UNIT = 19 )




    PRINT*, '         ... DONE    '
    PRINT*, '        '


END SUBROUTINE APPLY_BACKRUB_MAINCHAIN




