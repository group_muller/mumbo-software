!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE GET_JOB_DESCRIPTION ()
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MUMBO_DATA_M
	USE MAX_DATA_M
!
	IMPLICIT NONE
!
	CHARACTER (LEN=132) :: string
	CHARACTER (LEN=1000) :: input_file
	CHARACTER (LEN=80) ::  word(40), boff, baff, wsrd(40)
	CHARACTER (LEN=20) ::  biff
	CHARACTER (LEN=10) :: echo
	LOGICAL :: inflag, chainid_flag, flag
	REAL     :: rnstp1, rnstp2
	INTEGER  :: nw, ios
	INTEGER  :: na,nb,nc,nd,ne,nf,ng,nh,ni,nj,nk,nl,nm,i,j,k
	INTEGER  :: nn, no, np, nq, nr , ns, nz, n1, n3
	INTEGER  :: nu5, nu6, nu7, nu8, nu9, nu10, nu11
	INTEGER  :: nx1, nx2, nx3, nx4, nx5, nx6, nx7, nx8, nx9, nx10 
	INTEGER  :: ny1, ny2, ny3, ny4, ny5, ny6, ny7, ny8, ny9, ny10
	INTEGER  :: nz1, nz2, nz3, nz4, nz5, nz6, nz7, nz8, nz9 
	INTEGER  :: nz10, nz11, nz12, nz13, nz14 
	INTEGER  :: nz15, nz16, nz17, nz18, nz19, nz20, nz21
	INTEGER  :: nt1, nt2, nt3, nt4, nt5, nt6, nt7, nt8, nt9, nt10
	INTEGER  :: ns1, ns2, ns3, ns4, ns5, ns6, ns7
	INTEGER  :: nr1, nr2, nr3, nr4, nr5, nr6, nr7, nr8
	INTEGER  :: nr9, nr10, nr11, nr12, nr13, nr14, nr15, nr16
	INTEGER  :: nw1, nw2, nw3, nw4, nw5, nw6, nw7, nw8, nw9, nw10
	INTEGER  :: nv1, nv2, nv3, nv4, nv5, nv6, nv7, nv8, nv9
	INTEGER  :: nsp  ! needed for conformational splitting
        INTEGER  :: nsa  ! needed for monte carlo/simulated annealing
	INTEGER  :: nbd  ! needed for bound dead end
	INTEGER  :: ndt, nde     !STUFF REGARDING DEL: qual_del_tuning, qual_del_eps 
!	INTEGER  :: ndr          !STUFF REGARDING DEL: qual_del_RMSD_MIN (not used anymore) 
	INTEGER  :: nwt, nwb, nwo, nwh  !STUFF REGARDING WATER: WATER_TUNING, H20_build_ene_min, Ox_min_dist, Hy_min_dist			
!	
        REAL, PARAMETER :: err2 = 0.01
!
	na= len_trim(qual_job)
	nb= len_trim(qual_job_p1)
	nc= len_trim(qual_job_p2)
	nd= len_trim(qual_job_p3)
	nq= len_trim(qual_job_p4)
	nz= len_trim(qual_job_p5)
	ns1=len_trim(qual_job_p6)
	ns3=len_trim(qual_job_p7)
	nsp=len_trim(qual_job_p8)
	nsa=len_trim(qual_job_p9)
	nt8=len_trim(qual_job_p10)
	nbd=len_trim(qual_job_p11)
	nz17=len_trim(qual_job_p12)
!
! Testing parallel DEE and GOLD 
!
	nz18=len_trim(qual_job_p13)
	nz19=len_trim(qual_job_p14)
!WATER AND DEL	
	nz20=len_trim(qual_job_p15)
	nz21=len_trim(qual_job_p16)
!
	n1= len_trim(qual_inpdb)
	ns7=len_trim(qual_outpdb)
!
	nz3=len_trim(qual_out_hyd)
!
	n3= len_trim(qual_rot_lib1)
 	nx1=len_trim(qual_rot_lib2)
	ne=len_trim(qual_connec)
	nf=len_trim(qual_nonbond)
	ng=len_trim(qual_param)
	nu5=len_trim(qual_surface)
!
	nh=len_trim(qual_en_mc)
	nw9=len_trim(qual_en_m1)
	nw10=len_trim(qual_en_m2)
!
	nz4=len_trim(qual_en_dee)
	nz5=len_trim(qual_en_d1)
	nz6=len_trim(qual_en_gold)
	nz7=len_trim(qual_en_g1)
	nz8=len_trim(qual_en_split)
	nz9=len_trim(qual_en_s1)
!
	nz10=len_trim(qual_en_sa)
	nz11=len_trim(qual_sa_Tmax)
	nz12=len_trim(qual_sa_Tmin)
	nz13=len_trim(qual_sa_steps)
        nz14=len_trim(qual_sa_mc)
        nz15=len_trim(qual_en_bound)
        nz16=len_trim(qual_en_bo)
!
	ni=len_trim(qual_energy)
	nj= len_trim(qual_en_p1)
	nk= len_trim(qual_en_p2)
	nl= len_trim(qual_en_p3)
	nm= len_trim(qual_en_p4)
	nx2=len_trim(qual_en_p5)
	nx3=len_trim(qual_en_p6)
	nt1=len_trim(qual_en_p7)
!
	nx4=len_trim(qual_weights)
	nx5=len_trim(qual_we_p1)
	nx6=len_trim(qual_we_p2)
	nx7=len_trim(qual_we_p3)
	nx8=len_trim(qual_we_p4)
	nx9=len_trim(qual_we_p5)
	nx10=len_trim(qual_we_p6)
	nt2=len_trim(qual_we_p7)
!
	nn=len_trim(qual_en_tuning)
	no=len_trim(qual_en_t1)
	np=len_trim(qual_en_t2)
	ny4=len_trim(qual_en_t3)
	ny5=len_trim(qual_en_t4)
	ny6=len_trim(qual_en_t5)
	ny7=len_trim(qual_en_t6)
	ny8=len_trim(qual_en_t7)
	ny9=len_trim(qual_en_t8)
	ny10=len_trim(qual_en_t9)
	ns2= len_trim(qual_en_t10)
	nr1= len_trim(qual_en_t11)
	nr2= len_trim(qual_en_t12)
	nr3= len_trim(qual_en_t13)
	nr4= len_trim(qual_en_t14)
	nr5= len_trim(qual_en_t15)
	nr6= len_trim(qual_en_t16)
	nr7= len_trim(qual_en_t17)
	nr8= len_trim(qual_en_t18)
	nr9=  len_trim(qual_en_t19)
	nr10= len_trim(qual_en_t20)
	nr11= len_trim(qual_en_t21)
	nr12= len_trim(qual_en_t22)
	nr13= len_trim(qual_en_t23)
	nr14= len_trim(qual_en_t24)
	nr15= len_trim(qual_en_t25)
	nr16= len_trim(qual_en_t26)
!
	nr= len_trim(qual_en_brt)
	ns= len_trim(qual_en_b1)
!	ns8=len_trim(qual_en_b2)
!
	ny1=len_trim(qual_rot_tuning)
	ny2=len_trim(qual_rot_t1)
	ny3=len_trim(qual_rot_t2)
	nz1=len_trim(qual_rot_t3)
	nz2=len_trim(qual_rot_t4)
	nu9=len_trim(qual_rot_t5)
	nu10=len_trim(qual_rot_t6)
	nu11=len_trim(qual_rot_t7)
!	
	nu6=len_trim(qual_backrub_tuning)
	nu7=len_trim(qual_back_t1)
	nu8=len_trim(qual_back_t2)
	
	nt3=len_trim(qual_xray)
	nt4=len_trim(qual_xray_a1)
	nt5=len_trim(qual_xray_a2)
!
	nt6=len_trim(qual_comp_pick)
	nt7=len_trim(qual_comp_pick_a1)
	nt9=len_trim(qual_comp_pick_a2)
	nt10=len_trim(qual_comp_pick_a3)
!
	nw1=len_trim(qual_lig_tuning)
	nw2=len_trim(qual_lig_t1)
	nw3=len_trim(qual_lig_t2)
	nw4=len_trim(qual_lig_t3)
	nw5=len_trim(qual_lig_t4)
	nw6=len_trim(qual_lig_t5)
	nw7=len_trim(qual_lig_t6)
	nw8=len_trim(qual_lig_t7)
	nv8=len_trim(qual_lig_t8)
	nv9=len_trim(qual_lig_t9)
!
	ns4=len_trim(qual_en_ana)
	ns5=len_trim(qual_ana_a1)
	ns6=len_trim(qual_ana_a2)
!
	nv1=len_trim(qual_log_tuning)
	nv2=len_trim(qual_log_t1)
	nv3=len_trim(qual_log_t2)
	nv4=len_trim(qual_log_t3)
	nv5=len_trim(qual_log_t4)
	nv6=len_trim(qual_log_t5)
	nv7=len_trim(qual_log_t6)
!	
	ndt=len_trim(qual_del_tuning) 
	nde=len_trim(qual_del_eps)
!	ndr=len_trim(qual_del_RMSD_MIN)
!	
	nwt=len_trim(qual_water_tuning)       
	nwb=len_trim(qual_water_build_ene_min)
	nwo=len_trim(qual_water_Ox_min_dist)  
	nwh=len_trim(qual_water_Hy_min_dist)  
!
!
!       HERE TAKING CARE OF SOME POTENTIAL INPUT GLITCHES
!
	mumbo_inpdb(1:1)          ='x'
!	mumbo_outpdb(1:1)         ='x'
	mumbo_librot(1:1)         ='x'
	mumbo_libpps(1:1)         ='x'
	mumbo_libcon(1:1)         ='x'
	mumbo_libpar(1:1)         ='x'
	mumbo_libnbo(1:1)         ='x'
	mumbo_libsrf(1:1)         ='x'
	mumbo_lig_in(1:1)         ='x'
	mumbo_ref_pdb(1:1)        ='x'
	mumbo_map_in(1:1)         ='x'
!
	do i=1,1000
	  input_file(i:i)=' '
	  mumbo_outpdb(i:i)=' ' 
	end do
!
!	DONE
!
	mumbo_outpdb(1:len_trim(qual_outpdb))=qual_outpdb
!
	input_file(1:len_trim(qual_input))=qual_input
	inflag=.true.
!
	call get_infn(input_file,inflag,filelabel_input_file)
!
	open(unit=14,file=input_file,status='old',form='formatted')
!
!	
	do, i=1, max_lines_input
	  read(14,fmt='(a)',iostat=ios) string
	  if (ios==eo_file) then
!	      print*,'END-OF-FILE REACHED IN FILE: '
!	      print*, input_file(1:len_trim(input_file))
!	      print*,'NO HARM DONE                 '
	      exit
	  end if
	  if (i==max_lines_input) then
		PRINT*, '    '
		PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
		PRINT*, '>>>>>>>  END-OF-FILE NOT REACHED:                    '
		PRINT*, '>>>>>>> ', input_file(1:len_trim(input_file))
		PRINT*, '>>>>>>>  INCREASE MAX_LINES_INPUT                     '
		PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
		PRINT*, '    '
		STOP
	  end if
	  call parser(string, word, nw)
	  do j=1,nw
	      wsrd(j)=word(j)
	  end do
	  baff=word(2)
	  call maxwords(word,nw)
	  boff=word(1)
!
	  if (boff(1:1)=='!') then
	       cycle
!
! JOB DESCRIPTION
! a little bit clumsy for historical reasons
!
!
	  else if (boff(1:na)==qual_job) then
                mumbo_njobs = 0
		do j=2,nw
		    boff=word(j)
		    if (boff(1:nb)==qual_job_p1) then
		         init_flag = .true.
		    else if (boff(1:nc)==qual_job_p2) then
			 mc_flag = .true.
		    else if (boff(1:nd)==qual_job_p3) then
			 dee_flag = .true.
		    else if (boff(1:nq)==qual_job_p4) then
			 brut_flag = .true.
		    else if (boff(1:nz)==qual_job_p5) then
			 gold_flag = .true.
		    else if (boff(1:ns1)==qual_job_p6) then
			 doub_flag = .true.
		    else if (boff(1:ns3)==qual_job_p7) then
			 ana_flag = .true.
		    else if (boff(1:nsp)==qual_job_p8) then
			 split_flag = .true.
                    else if (boff(1:nsa)==qual_job_p9) then
                         monsa_flag = .true.
                    else if (boff(1:nt8)==qual_job_p10) then
                         comp_flag = .true.
!                    else if (boff(1:nbd)==qual_job_p11) then
!                         bound_flag = .true.
                    else if (boff(1:nz17)==qual_job_p12) then
                         picknat_flag = .true.
                    else if (boff(1:nz18)==qual_job_p13) then
                         pdee_flag = .true.
                    else if (boff(1:nz19)==qual_job_p14) then
                         pgold_flag = .true.
	            else if (boff(1:nz20)==qual_job_p15) then
                         water_flag = .true.
                    else if (boff(1:nz21)==qual_job_p16) then
			 del_flag = .true.
		    else if (boff(1:1)=='!') then
			 exit 	
		    end if
                    mumbo_njobs = mumbo_njobs + 1
		end do
!
	        if (allocated(mumbo_jobs)) deallocate(mumbo_jobs)
	        allocate(mumbo_jobs(mumbo_njobs))
!
	        do j=1,20
                 biff(j:j)=' '
	        end do
!
	        do j=1,mumbo_njobs
	          mumbo_jobs(j)=biff                       
                end do
!
		do j=1,mumbo_njobs
		    boff=word(j+1)
	            mumbo_job='                    '
		    if (boff(1:nb)==qual_job_p1) then
                          mumbo_job(1:nb)= boff(1:nb)
		    else if (boff(1:nc)==qual_job_p2) then
                          mumbo_job(1:nc)= boff(1:nc)
		    else if (boff(1:nd)==qual_job_p3) then
                          mumbo_job(1:nd)= boff(1:nd)
		    else if (boff(1:nq)==qual_job_p4) then
                          mumbo_job(1:nq)= boff(1:nq)
		    else if (boff(1:nz)==qual_job_p5) then
                          mumbo_job(1:nz)= boff(1:nz)
		    else if (boff(1:ns1)==qual_job_p6) then
                          mumbo_job(1:ns1)= boff(1:ns1)
		    else if (boff(1:ns3)==qual_job_p7) then
                          mumbo_job(1:ns3)= boff(1:ns3)
		    else if (boff(1:nsp)==qual_job_p8) then
                          mumbo_job(1:nsp)= boff(1:nsp)
                    else if (boff(1:nsa)==qual_job_p9) then
                          mumbo_job(1:nsa)= boff(1:nsa)
                    else if (boff(1:nt8)==qual_job_p10) then
                          mumbo_job(1:nt8)= boff(1:nt8)
!                    else if (boff(1:nbd)==qual_job_p11) then
!                          mumbo_job(1:nbd)= boff(1:nbd)
                    else if (boff(1:nz17)==qual_job_p12) then
                          mumbo_job(1:nz17)= boff(1:nz17)
                    else if (boff(1:nz18)==qual_job_p13) then
                          mumbo_job(1:nz18)= boff(1:nz18)
                    else if (boff(1:nz19)==qual_job_p14) then
                          mumbo_job(1:nz19)= boff(1:nz19)
		    else if (boff(1:nz20)==qual_job_p15) then
                          mumbo_job(1:nz20)= boff(1:nz20)
                    else if (boff(1:nz21)==qual_job_p16) then
                          mumbo_job(1:nz21)= boff(1:nz21)                                           
                    end if
		    mumbo_jobs(j)=mumbo_job
		end do
!
!
!  FILE NAMES TO OPEN
!
	   else if (boff(1:n1)==qual_inpdb) then
		mumbo_inpdb = baff
!	
	   else if (boff(1:ns7)==qual_outpdb) then
		mumbo_outpdb = baff
!
		do j=3,nw
		    boff=word(j)
		    if (boff(1:nz3)==qual_out_hyd) then
		         mumbo_out_hyd = .true.
		    else if (boff(1:1)=='!') then
			 exit
		    end if
	        end do
!
	   else if (boff(1:ne)==qual_connec) then
		mumbo_libcon = baff
!
	   else if (boff(1:nu5)==qual_surface) then
		mumbo_libsrf = baff
!
	   else if (boff(1:nf)==qual_nonbond) then
		mumbo_libnbo = baff
!
	   else if (boff(1:ng)==qual_param) then
		mumbo_libpar = baff
!
 	   else if (boff(1:n3)==qual_rot_lib1) then
		mumbo_librot = baff
!
	   else if (boff(1:nx1)==qual_rot_lib2) then
		mumbo_libpps = baff
!
!  ENERGY RELATED STUFF
!
	   else if (boff(1:nh)==qual_en_mc) then
		do j=2,nw
		    boff=word(j)
		    if (boff(1:nw9)==qual_en_m1) then
			read (word(j+1),*) mumbo_en_cut
		    else if (boff(1:nw10)==qual_en_m2) then
			mumbo_en_mc_recover_flag = .true.
		    else if (boff(1:1)=='!') then
			 exit
		    end if
		end do
!
	   else if (boff(1:nz4)==qual_en_dee) then
		do j=2,nw
		    boff=word(j)
		    if (boff(1:nz5)==qual_en_d1) then
			read (word(j+1),*) mumbo_dee_ncyc 
		    else if (boff(1:1)=='!') then
			 exit
		    end if
		end do
!
	   else if (boff(1:nz6)==qual_en_gold) then
		do j=2,nw
		    boff=word(j)
		    if (boff(1:nz7)==qual_en_g1) then
			read (word(j+1),*) mumbo_gold_ncyc 
		    else if (boff(1:1)=='!') then
			 exit
		    end if
		end do
!
	   else if (boff(1:nz8)==qual_en_split) then
		do j=2,nw
		    boff=word(j)
		    if (boff(1:nz9)==qual_en_s1) then
			read (word(j+1),*) mumbo_split_ncyc 
		    else if (boff(1:1)=='!') then
			 exit
		    end if
		end do
!
           else if (boff(1:nz15)==qual_en_bound) then
                do j=2,nw
		    boff=word(j)
		    if (boff(1:nz16)==qual_en_bo) then
			read (word(j+1),*) mumbo_bound_ncyc 
                    else if (boff(1:1)=='!') then
			 exit
		    end if
		end do
!
	   else if (boff(1:nz10)==qual_en_sa) then
	        do j=2,nw
		   boff=word(j)
		   if (boff(1:nz11)==qual_sa_Tmax) then
		       read(word(j+1),*) mumbo_sa_Tmax
		   else if (boff(1:nz12)==qual_sa_Tmin) then
		       read(word(j+1),*) mumbo_sa_Tmin
		   else if (boff(1:nz13)==qual_sa_steps) then
		       read(word(j+1),*) mumbo_sa_steps
		   else if (boff(1:nz14)==qual_sa_mc) then
		       read(word(j+1),*) mumbo_sa_mc
		   else if (boff(1:1)=='!') then
		       exit
		   end if
		end do
!
	   else if (boff(1:ni)==qual_energy) then
		do j=2,nw
		    boff=word(j)
		    if (boff(1:nj)==qual_en_p1) then
		         vdw_flag = .true.
		    else if (boff(1:nk)==qual_en_p2) then
			 elec_flag = .true.
		    else if (boff(1:nl)==qual_en_p3) then
			 hbond_flag = .true.
		    else if (boff(1:nm)==qual_en_p4) then
			 asa_flag = .true.
		    else if (boff(1:nx2)==qual_en_p5) then
			 solv_flag = .true.
		    else if (boff(1:nx3)==qual_en_p6) then
			 rprob_flag = .true.
		    else if (boff(1:nt1)==qual_en_p7) then
			 xray_flag = .true.
		    else if (boff(1:1)=='!') then
			 exit
		    end if
		end do
!
	   else if (boff(1:nn)==qual_en_tuning) then
		do j=2,nw
		    boff=word(j)
		    if (boff(1:no)==qual_en_t1) then
		         read(word(j+1),*) mumbo_en_svdw
		    else if (boff(1:np)==qual_en_t2) then
			 read(word(j+1),*) mumbo_en_sdiel
		    else if (boff(1:ny4)==qual_en_t3) then
			 eswitch_flag=.true.
		    else if (boff(1:ny5)==qual_en_t4) then
			 eshift_flag=.true.
		    else if (boff(1:ny6)==qual_en_t5) then
			 read(word(j+1),*) mumbo_en_ctonnb
		    else if (boff(1:ny7)==qual_en_t6) then
			 read(word(j+1),*) mumbo_en_ctofnb
		    else if (boff(1:ny8)==qual_en_t7) then
			 edist_flag=.true.
		    else if (boff(1:ny9)==qual_en_t8) then
			 read(word(j+1),*) mumbo_en_solgref
		    else if (boff(1:ny10)==qual_en_t9) then
			 read(word(j+1),*) mumbo_en_sollam
		    else if (boff(1:ns2)==qual_en_t10) then
			 econt_flag=.true.
		    else if (boff(1:nr1)==qual_en_t11) then
			 mumbo_en_it_vdw = 3
		    else if (boff(1:nr2)==qual_en_t12) then
			 mumbo_en_it_vdw = 4
		    else if (boff(1:nr3)==qual_en_t13) then
			 mumbo_en_it_ele = 2
		    else if (boff(1:nr4)==qual_en_t14) then
			 mumbo_en_it_ele = 3
		    else if (boff(1:nr5)==qual_en_t15) then
			 mumbo_en_it_ele = 4
		    else if (boff(1:nr6)==qual_en_t16) then
			 mumbo_en_it_sol = 2 
		    else if (boff(1:nr7)==qual_en_t17) then
			 mumbo_en_it_sol = 3
		    else if (boff(1:nr8)==qual_en_t18) then
			 mumbo_en_it_sol = 4
		    else if (boff(1:nr9)==qual_en_t19) then
			 mumbo_en_hbd_geoflag = .true.
			 mumbo_en_hbd_empflag = .false.
		    else if (boff(1:nr10)==qual_en_t20) then
			 mumbo_en_hbd_geoflag = .false.
			 mumbo_en_hbd_empflag = .true.
		    else if (boff(1:nr11)==qual_en_t21) then
			 read(word(j+1),*) mumbo_en_hbd_ctof 
		    else if (boff(1:nr12)==qual_en_t22) then
			 read(word(j+1),*) mumbo_en_hbd_dzero
		    else if (boff(1:nr13)==qual_en_t23) then
			 read(word(j+1),*) mumbo_en_hbd_ezero
		    else if (boff(1:nr14)==qual_en_t24) then
			 mumbo_en_vdw_soft = .true.
		    else if (boff(1:nr15)==qual_en_t25) then
			 read(word(j+1),*) mumbo_en_solgfree
		    else if (boff(1:nr16)==qual_en_t26) then
			 mumbo_en_vdw_reponly = 0.0
		    else if (boff(1:1)=='!') then
			 exit
		    end if
		end do
!
	   else if (boff(1:nx4)==qual_weights) then
		do j=2,nw
		    boff=word(j)
		    if (boff(1:nx5)==qual_we_p1) then
		         read(word(j+1),*) mumbo_we_vdw
		    else if (boff(1:nx6)==qual_we_p2) then
		         read(word(j+1),*) mumbo_we_elec
		    else if (boff(1:nx7)==qual_we_p3) then
		         read(word(j+1),*) mumbo_we_rprob
		    else if (boff(1:nx8)==qual_we_p4) then
		         read(word(j+1),*) mumbo_we_solv
		    else if (boff(1:nx9)==qual_we_p5) then
		         read(word(j+1),*) mumbo_we_hbond
		    else if (boff(1:nx10)==qual_we_p6) then
		         read(word(j+1),*) mumbo_we_asa 
		    else if (boff(1:nt2)==qual_we_p7) then
		         read(word(j+1),*) mumbo_we_xray 
		    else if (boff(1:1)=='!') then
			 exit
		    end if
		end do
!
	   else if (boff(1:nr)==qual_en_brt) then
		do j=2,nw
		    boff=word(j)
		    if (boff(1:ns)==qual_en_b1) then
		         read(word(j+1),*) mumbo_en_bwr 	
!		    else if (boff(1:ns8)==qual_en_b2) then
!		         read(word(j+1),*) mumbo_en_bmxc
		    else if (boff(1:1)=='!') then
			 exit
		    end if
		end do
!
! ROTAMER RELATED STUFF
!
	   else if (boff(1:ny1)==qual_rot_tuning) then
		do j=2,nw
		    boff=word(j)
		    if (boff(1:ny2)==qual_rot_t1) then
		         phipsi_flag= .true. 
		    else if (boff(1:ny3)==qual_rot_t2) then
			 read(word(j+1),*) mumbo_fine_ndeg
		    else if (boff(1:nz1)==qual_rot_t3) then
			 read(word(j+1),*) rnstp1
			 mumbo_fine_nstp =nint(rnstp1)
			 if (mumbo_fine_nstp > 0) then
			    fine_flag=.true. 	
			 end if
		    else if (boff(1:nz2)==qual_rot_t4) then
			 read(word(j+1),*) mumbo_fine_limit
		    else if (boff(1:nu9)==qual_rot_t5) then
			 chi_one_flag=.true.
		    else if (boff(1:nu10)==qual_rot_t6) then
			 chi_all_flag=.true.		    
		    else if (boff(1:nu11)==qual_rot_t7) then
			 his_one_flag=.true.		    
		    else if (boff(1:1)=='!') then
			 exit
		    end if
		end do
!
! BACKBONE BACKRUB MOTION
!
	   else if (boff(1:nu6)==qual_backrub_tuning) then
		do j=2,nw
		    boff=word(j)
		    if (boff(1:nu7)==qual_back_t1) then
			 read(word(j+1),*) mumbo_back_ndeg
		    else if (boff(1:nu8)==qual_back_t2) then
			 read(word(j+1),*) rnstp2
			 mumbo_back_nstp =nint(rnstp2)
			 if (mumbo_back_nstp > 0) then
			    backrub_flag=.true. 	
			 end if
		    else if (boff(1:1)=='!') then
			 exit
		    end if
		end do
!
! 	electron density
!
	    else if (boff(1:nt3)==qual_xray) then
		do j=2,nw
			boff=word(j)
			if (boff(1:nt4)==qual_xray_a1) then
				mumbo_map_in =   wsrd(j+1) 
			else if (boff(1:nt5)==qual_xray_a2) then
				mumbo_map_type = word(j+1) 
			else if (boff(1:1)=='!') then
				exit
			end if
		end do
!
!	reference pdbfile
!
	    else if (boff(1:nt6)==qual_comp_pick) then
		do j=2,nw
			boff=word(j) 	
			if (boff(1:nt7)==qual_comp_pick_a1) then
				mumbo_ref_pdb =   wsrd(j+1) 
			else if (boff(1:nt9)==qual_comp_pick_a2) then
				mumbo_ref_beta_flag=.false.
			else if (boff(1:nt10)==qual_comp_pick_a3) then
				mumbo_swap_qnh_flag=.true.
			else if (boff(1:1)=='!') then
				exit
			end if
		end do
!
	    else if (boff(1:len_trim(qual_comp))==qual_comp) then
		call obsolete1
!
!	ligand stuff
!
	    else if (boff(1:nw1)==qual_lig_tuning) then
		mumbo_lig_flag=.true.
		do j=2,nw
			boff=word(j)
                        baff=word(j+1)
			if (boff(1:nw2)==qual_lig_t1) then
			    mumbo_lig_in =   wsrd(j+1)
			    mumbo_lig_pdb_flag = .true. 
			else if (boff(1:nw3)==qual_lig_t2) then
		            call chainid_present(baff,chainid_flag)
		            if (chainid_flag) then
			       mumbo_lig_chid = baff(1:1)
			       read(word(j+2),*) mumbo_lig_nold
			    else
			       read(word(j+1),*) mumbo_lig_nold
			    end if 	
!			    mumbo_lig_fle_flag= .true.
			else if (boff(1:nw4)==qual_lig_t3) then
			    read(word(j+1),*) mumbo_lig_rms
			else if (boff(1:nw5)==qual_lig_t4) then
			    read(word(j+1),*) mumbo_lig_ngen
			else if (boff(1:nw6)==qual_lig_t5) then
			    read(word(j+1),*) mumbo_lig_seed
			else if (boff(1:nw7)==qual_lig_t6) then
			    mumbo_lig_shift_flag = .false.
			else if (boff(1:nw8)==qual_lig_t7) then
			    mumbo_lig_rot_flag =   .false.
			else if (boff(1:nv8)==qual_lig_t8) then
			    mumbo_lig_fullrot_flag = .true.
			else if (boff(1:nv9)==qual_lig_t9) then
			    mumbo_lig_centre = .true.
			    mumbo_lig_ctan = baff(1:4)
			else if (boff(1:1)=='!') then
			    exit
			end if
		end do
!		
!          ***WATER RELATED STUFF***	
!
	   else if (boff(1:nwt)==qual_water_tuning) then
		do j=2,nw
		    boff=word(j)
		    if (boff(1:nwb)==qual_water_build_ene_min) then
			 read(word(j+1),*) H20_build_ene_min
		    else if (boff(1:nwo)==qual_water_Ox_min_dist) then
			 read(word(j+1),*) Ox_min_dist
		    else if (boff(1:nwh)==qual_water_Hy_min_dist) then
			 read(word(j+1),*) Hy_min_dist
		    else if (boff(1:1)=='!') then
			 exit
		    end if
		end do		
		
!          ***DEL WATER RELATED STUFF***	

	   else if (boff(1:ndt)==qual_del_tuning) then
		do j=2,nw
		    boff=word(j)
		    if (boff(1:nde)==qual_del_eps) then
			 read(word(j+1),*) DEL_eps
!		    else if (boff(1:ndr)==qual_del_RMSD_MIN) then
!			 read(word(j+1),*) RMSDmin
		    else if (boff(1:1)=='!') then
			 exit
		    end if
		end do		
!
	   else if (boff(1:ns4)==qual_en_ana) then
		do j=2,nw
		    boff=word(j)
		    if (boff(1:ns5)==qual_ana_a1) then
		         read(word(j+1),*) mumbo_ana_mxc
		    else if (boff(1:ns6)==qual_ana_a2) then
			 read(word(j+1),*) mumbo_ana_mxs
		    else if (boff(1:1)=='!') then
			 exit
		    end if
		end do
! 	PRINT*, '        '

	   else if (boff(1:nv1)==qual_log_tuning) then
		do j=2,nw
		    boff=word(j)
		    if (boff(1:nv2)==qual_log_t1) then
			 mumbo_log_ana_flag = .true.
		    else if (boff(1:nv3)==qual_log_t2) then
			 mumbo_log_ana_flag = .false.
		    else if (boff(1:nv4)==qual_log_t3) then
			 mumbo_log_mc_flag = .true.
		    else if (boff(1:nv5)==qual_log_t4) then
			 mumbo_log_mc_flag = .false.
		    else if (boff(1:nv6)==qual_log_t5) then
			 mumbo_log_dbase_flag = .true.
		    else if (boff(1:nv7)==qual_log_t6) then
			 mumbo_log_dbase_flag = .false.
		    else if (boff(1:1)=='!') then
			 exit
		    end if
		end do
!
	   end if
	end do
! 
	close(14)
!
! NOW CHECKING FOR FILES AND GET COMPLETE FILE NAMES IF NEEDED
!
!
	inflag=.true.
	call get_infn(mumbo_librot,inflag,filelabel_librot)
!
	if (init_flag) then
	    call get_infn(mumbo_inpdb,inflag,filelabel_inpdb)
	    if (phipsi_flag) then
	       call get_infn(mumbo_libpps,inflag,filelabel_libpps)
	    end if
	end if
!
	if (brut_flag.or.ana_flag) then
	    inflag=.false.
	    call get_infn(mumbo_outpdb,inflag,filelabel_dummy)
	    inflag=.true.
	end if
!
	call get_infn(mumbo_libpar,inflag,filelabel_libpar)
	call get_infn(mumbo_libnbo,inflag,filelabel_libnbo)
 	call get_infn(mumbo_libcon,inflag,filelabel_libcon)
!
!	if (asa_flag) then 
!	    call get_infn(mumbo_libsrf,inflag,filelabel_libsrf)
!	end if
!
	if (xray_flag) then 
		call get_infn(mumbo_map_in,inflag,filelabel_map_in)
	end if
!
	if (comp_flag.or.picknat_flag) then 
		call get_infn(mumbo_ref_pdb,inflag,filelabel_ref_pdb)
	end if
!
	if (mumbo_lig_pdb_flag) then 
		call get_infn(mumbo_lig_in,inflag,filelabel_lig_in)
	end if
!
	PRINT*, '        '
	PRINT*, '################################'
	PRINT*, '# SUMMARY GET_JOB_DESCRIPTION  #'
	PRINT*, '################################'
	PRINT*, '        '
	PRINT*, '  INIT    REQUESTED? ',echo(init_flag)
	PRINT*, '  WATER   REQUESTED? ',echo(water_flag)
	PRINT*, '  DEL     REQUESTED? ',echo(del_flag)
	PRINT*, '  MC      REQUESTED? ',echo(mc_flag)
	PRINT*, '  DEE     REQUESTED? ',echo(dee_flag)
	PRINT*, '  GOLD    REQUESTED? ',echo(gold_flag)
	PRINT*, '  SPLIT   REQUESTED? ',echo(split_flag),'  NCYC= ',mumbo_split_ncyc 
	PRINT*, '  DOUB    REQUESTED? ',echo(doub_flag)
        PRINT*, '  MONSA   REQUESTED? ',echo(monsa_flag)
!	PRINT*, '  BOUND   REQUESTED? ',echo(bound_flag),'  NCYC= ', mumbo_bound_ncyc
	PRINT*, '  BRUT    REQUESTED? ',echo(brut_flag)
	PRINT*, '  ANA     REQUESTED? ',echo(ana_flag)
	PRINT*, '  COMP    REQUESTED? ',echo(comp_flag)
	PRINT*, '  PICKNAT REQUESTED? ',echo(picknat_flag)
	PRINT*, '        '
	PRINT*, '  PDEE    REQUESTED? ',echo(pdee_flag)
	PRINT*, '  PGOLD   REQUESTED? ',echo(pgold_flag)
	PRINT*, '        '
	PRINT*, '  FLOWSCHEME REQUESTED:'
	PRINT*, '        '
	do j=1, mumbo_njobs-1
         PRINT*,'            ',mumbo_jobs(j)
         PRINT*,'             |        '
	end do
        PRINT*, '            ',mumbo_jobs(mumbo_njobs)  
	PRINT*, '        '
	PRINT*, '  ------------------------------------- '
	PRINT*, '     '
		if (init_flag) then
	PRINT*, '  STARTING MODEL WILL BE READ FROM FILE: '
	PRINT*, '          ',mumbo_inpdb(1:len_trim(mumbo_inpdb))
	print*, '    '
		end if
	PRINT*, '  RESIDUE PARAMETERS WILL BE READ FROM: '
	PRINT*, '          ',mumbo_libpar(1:len_trim(mumbo_libpar))
	print*, '    '
	PRINT*, '  NONBONDED PARAMETERS WILL BE READ FROM: '
	PRINT*, '          ',mumbo_libnbo(1:len_trim(mumbo_libnbo))
	print*, '    '
	PRINT*, '  CONNECTIVITY WILL BE READ FROM FILE: '
	PRINT*, '          ',mumbo_libcon(1:len_trim(mumbo_libcon))
	print*, '    '
!
!	  	if (asa_flag) then
!	PRINT*, '  SURFACE PARAMETERS WILL BE READ FROM FILE: '
!	PRINT*, '          ',mumbo_libsrf(1:len_trim(mumbo_libsrf))
!	print*, '    '
!	  	end if
!
	  	if (brut_flag.or.ana_flag) then
	PRINT*, '  FINAL MODEL WILL BE WRITTEN TO: '
	PRINT*, '          ',mumbo_outpdb(1:len_trim(mumbo_outpdb))
	print*, '    '
	  	end if
!
	  	if (xray_flag) then
	PRINT*, '  ELECTRON DENSITY WILL BE READ FROM FILE: '
	PRINT*, '          ',mumbo_map_in(1:len_trim(mumbo_map_in))
	PRINT*, '  ELECTRON DENSITY MAP TYPE : '
	PRINT*, '          ',mumbo_map_type
	print*, '    '
	  	end if
!
	  	if (comp_flag.or.picknat_flag) then
	PRINT*, '  THE REFERENCE STRUCTURE FOR EITHER THE COMP OR THE PICKNAT STEP  '
	PRINT*, '    WILL BE READ FROM FILE: '
	PRINT*, '          ',mumbo_ref_pdb(1:len_trim(mumbo_ref_pdb))
	PRINT*, '    '
		end if
!
		if (comp_flag) then 
		   flag=.true.
		   if (mumbo_ref_beta_flag) flag=.false. 
	PRINT*, '          NOCB SPECIFIED:', echo(flag)
		   flag=.false.
		   if (mumbo_swap_qnh_flag) flag=.true. 
	PRINT*, '          SWAP_Gln_Asn_His REQUESTED:', echo(flag)
	PRINT*, '    '
	  	end if
		
		
!
		if (init_flag) then
	PRINT*, '  ------------------------------------- '
	PRINT*, '     '
	PRINT*, '  ROTAMER HANDLING                      '
	PRINT*, '                                        '
!
	PRINT*, '  ROTAMERS BUILIDING INSTRUCTIONS READ IN FROM  FILE: '
	PRINT*, '          ',mumbo_librot(1:len_trim(mumbo_librot))
	PRINT*, '    '
	PRINT*, '  PHI-PSI-DEPENDENT ROTAMERS REQUESTED:', echo(phipsi_flag)
	PRINT*, '    '
!
		   if (.not.phipsi_flag) then
	print*, '    '
	print*, '  BACKBONE-INDEPENDENT ROTAMERS USED AND READ FROM FILE:   '
	PRINT*, '          ',mumbo_librot(1:len_trim(mumbo_librot))
	print*, '            REFERENCE: Lovell et al. (2000), Proteins 40, 389-408'
	print*, '    '
		   else 
	print*, '  PHI-PSI-DEPENDENT ROTAMERS USED AND READ FROM FILE:            '
	PRINT*, '          ',mumbo_libpps(1:len_trim(mumbo_libpps))
	print*, '            REFERENCE: Dunbrack & Cohen (1997), Protein Science 6, 1661-1681'
	print*, '    '
		   end if
!
		   if (fine_flag) then
	PRINT*, '  >> '
	PRINT*, '  >> ROTAMERS WILL BE EXPANDED                                        << '
	PRINT*, '  >> +,- ',mumbo_fine_nstp,' STEPS ADDED TO EACH DIHEDRAL ANGLE CHI         << '
	PRINT*, '  >> WITH ',mumbo_fine_ndeg,' DEGREE STEP WIDTH                         << '
	PRINT*, '  >> UNLESS NUMBER OF ROTAMERS EXCEEDS:',mumbo_fine_limit, '              << '
		      if (.not.chi_one_flag.and.(.not.chi_all_flag)) then 
		        chi_one_flag=.true.
		      end if
		      if (chi_one_flag.and.chi_all_flag) then
	PRINT*, '    '
	PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>>>>>>>>>>>>>>>>>>>>>>>>'
	PRINT*, '>> BOTH FINE_CHI_1_ONLY AND FINE_CHI_ALL ARE SET TRUE      '
	PRINT*, '>> PLEASE CHOSE TO EITHER EXPAND ALL CHIs OR               '                                    
	PRINT*, '>> CHI-1 ONLY                                              '
	PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>'
	PRINT*, '    '
			  stop
		      end if 
		      if (chi_one_flag) then
		        chi_all_flag=.false.
	PRINT*, '  >> '
	PRINT*, '  >> ONLY SIDE CHAIN DIHEDRAL CHI-1 WILL BE EXPANDED                  << '
	PRINT*, '  >> '
		      end if
		      if (chi_all_flag) then
		        chi_one_flag=.false.
	PRINT*, '  >> '
	PRINT*, '  >> ALL SIDE CHAIN DIHEDRAL ANGLES WILL BE EXPANDED                  << '
	PRINT*, '  >> '
		      end if
	PRINT*, '  >> CHI EXTENSIONS: '  
		   do k=1,(1+2*mumbo_fine_nstp)
	PRINT*, '  >>     DELTA CHI= ',((k-1)*(mumbo_fine_ndeg)-(mumbo_fine_nstp*mumbo_fine_ndeg)),' DEGREES' 
		   end do
	PRINT*, '  >> '
	print*, '    '
		   end if
!		   
		   if (his_one_flag) then
	print*, '  >>  '
	print*, '  >> ONLY ONE OUT OF THREE POSSIBLE HIS PROTONATION STATES WILL BE MODELLED   '
	PRINT*, '  >>  '
		   else 
	print*, '  >>  '
	print*, '  >> HIS RESIDUES WILL BE AUTOMATICALLY EXPANDED '
	print*, '  >>     TO MODEL ALL THREE POSSIBLE HIS PROTONATION STATES: '
	print*, '  >>     - PROTONATION AT ND1  '
	print*, '  >>     - PROTONATION AT NE2  '
	print*, '  >>     - PROTONATION AT BOTH ND1 AND NE2 + POSITIVELY CHARGED '
	print*, '  >>     THESE RESIDUES WILL BE CALLED INTERNALLY HIS, HJS AND HKS '
	PRINT*, '  >>  '
	           end if
	print*, '    '
!
		   if (backrub_flag) then
	PRINT*, '  ---------------------------------------------------------------- '
	PRINT*, '     '
	PRINT*, '  BACKBONE-BACKRUB MOTION REQUESTED                                '
	print*, '            REFERENCE: Davis et al. (2006), Structure 14, 265-274'
	PRINT*, '                                        '
	PRINT*, '  >> '
	PRINT*, '  >> BACKBONE/MAIN CHAIN STRUCTURE WILL BE ALTERED                    << '
	PRINT*, '  >> +,- ',mumbo_back_nstp,'  BACKRUB ROTATION STEPS AT EACH POSITION       << '
	PRINT*, '  >> WITH ',mumbo_back_ndeg,' DEGREE STEP WIDTH                         << '
	PRINT*, '  >> BACKRUB ROTATION STEPS:'  
		   do k=1,(1+2*mumbo_back_nstp)
	PRINT*, '  >>      BACKRUB ROTATION= ',((k-1)*(mumbo_back_ndeg)-(mumbo_back_nstp*mumbo_back_ndeg)),' DEGREES' 
		   end do
	PRINT*, '  >> '
	PRINT*, '  >> BACKBONE/MAIN CHAIN STRUCTURE WILL BE ALTERED                     << '
	PRINT*, '  >> '
	print*, '     '
		   end if
!
		end if
!
	        if (mumbo_lig_flag) then
	PRINT*, '  ------------------------------------- '
	PRINT*, '     '
	PRINT*, '  LIGAND HANDLING REQUESTED         '
	PRINT*, '                                    '
	   	  if (mumbo_lig_pdb_flag .and. mumbo_lig_fle_flag) then 
	            mumbo_lig_fle_flag= .false.
		  end if
		  if (mumbo_lig_pdb_flag) then 
	PRINT*, '  LIGAND CONFORMATIONS/ORIENTATIONS WILL BE READ FROM FILE: '
	PRINT*, '        ',mumbo_lig_in(1:len_trim(mumbo_lig_in))
	PRINT*, '    '
		  else if (mumbo_lig_fle_flag) then 
	PRINT*, '  LIGAND COORDINATES WILL BE READ FROM GENERAL COORDINATE FILE  '
	PRINT*, '  LIGAND IDENTIFIER IS:                                    '
	PRINT*, '        ', mumbo_lig_chid(1:len_trim(mumbo_lig_chid)), mumbo_lig_nold 
	PRINT*, '    '
		  end if
	PRINT*, '  LIGAND ORIENTATIONS WILL BE EXPANDED:   '
		   if (mumbo_lig_centre) then
	PRINT*, '       WHEN GENERATING ADDITIONAL LIGAND POSITIONS THEN ... '
	PRINT*, '           ... LIGAND WILL BE ROTATED/TRANSLATED AROUND ATOM:', mumbo_lig_ctan
		   else 
	PRINT*, '       WHEN GENERATING ADDITIONAL LIGAND POSITIONS THEN ... '
	PRINT*, '           ... LIGAND WILL BE ROTATED/TRANSLATED AROUND CENTRE OF MASS'
		   end if
	PRINT*, '       NGENERATE(LIG_NGEN):',  mumbo_lig_ngen
	PRINT*, '       RANDM_SEED(LIG_SEED):', mumbo_lig_seed
		   if (mumbo_lig_fullrot_flag) then
			mumbo_lig_shift_flag = .false.
			mumbo_lig_rot_flag =   .true.
	PRINT*, '       FULL ROTATION MODE REQUESTED:', echo(mumbo_lig_fullrot_flag)
		   else
	PRINT*, '       TARGET_RMS(LIG_RMS):',  mumbo_lig_rms
		   end if
		   flag=.true.
		   if (mumbo_lig_shift_flag) flag=.false.
	PRINT*, '       NOSHIFT  SPECIFIED:', echo(flag)
		   flag=.true.
		   if (mumbo_lig_rot_flag) flag=.false.
	PRINT*, '       NOROTATE SPECIFIED:', echo(flag)
	PRINT*, '                                          '
		    if (mumbo_lig_ngen .gt. max_nlig) then
	PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>>>>>>>>>>>>>>>>>>>>'
	PRINT*, '>> NUMBER OF LIGAND ORIENTATIONS REQUESTED EXCEEDS     '
	PRINT*, '>> MAXIMUM ALLOWED                                     '
	PRINT*, '>> LIG_NGEN: ',  mumbo_lig_ngen 
	PRINT*, '>> MAX_NLIG: ',  max_nlig 
	PRINT*, '>> INCREASE MAX_NLIG IN  > mmods.f90 <    AND RECOMPILE'
	PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>'
		      stop
		    end if
	        end if
	PRINT*, '  ------------------------------------- '
	PRINT*, '    '
!
	  	if (MONSA_flag) then
	PRINT*, '  MONSA STEP - MONTE-CARLO SIMULATED ANNEALING - STARTING TEMPERATURE '
	print*, '    ', mumbo_sa_Tmax
	PRINT*, '  MONSA STEP - END TEMPERATURE '
	print*, '    ', mumbo_sa_Tmin
	PRINT*, '  MONSA STEP - NUMBER OF TEMPERATURE STEPS '
	print*, '    ', mumbo_sa_steps
	PRINT*, '  MONSA STEP - NUMBER OF ITERATIONS PER TEMP STEP'
	print*, '    ', mumbo_sa_mc
	print*, '    '
	  	end if
!
	  	if (brut_flag) then
	PRINT*, '  BRUT STEP - ROTAMERS WILL BE KEPT IF TOTAL ENERGY OF CONFIGURATION <= : '
	PRINT*, '          ', mumbo_en_bwr
!	PRINT*, '  BRUT STEP - MAXIMUM NUMBER OF COMBINATIONS TO BE CALCULATED: '
!	PRINT*, '          ', mumbo_en_bmxc
	print*, '    '
	  	end if
!
	  	if (ana_flag) then
	PRINT*, '  ANALYSE STEP - MAXIMUM NUMBER OF COMBINATIONS TO BE ANALYSED: '
	PRINT*, '          ',mumbo_ana_mxc
	PRINT*, '  ANALYSE STEP - MAXIMUM NUMBER OF SOLUTIONS TO BE SORTED: '
	PRINT*, '          ',mumbo_ana_mxs
	print*, '    '
	  	end if
!
                if (del_flag) then
	PRINT*, '  DELETION STEP - SIMILAR/IDENTICAL ROTAMES WILL BE DELETED IF IN A PAIR OF'
	PRINT*, '                  ROTAMERS NO EQUIVALENT ATOM PAIRS EXIST THAT ARE MORE THAN' 
	PRINT*, '                  DEL_EPS ANGS. DISTANT FROM EACH OTHER.'
	print*, '    '
	PRINT*, '                  DEL_EPS = ', DEL_eps
	print*, '    '
	  	end if
!
	PRINT*, '  ENERGY PARAMETERS:  '
	PRINT*, '     VDW   ENERGY REQUESTED? ',echo(vdw_flag)
	PRINT*, '     ELEC  ENERGY REQUESTED? ',echo(elec_flag)
	PRINT*, '     RPROB ENERGY REQUESTED? ',echo(rprob_flag)
	PRINT*, '     SOLV  ENERGY REQUESTED? ',echo(solv_flag)
	PRINT*, '     HBOND ENERGY REQUESTED? ',echo(hbond_flag)
!	PRINT*, '     AAS   ENERGY REQUESTED? ',echo(asa_flag)
		if (xray_flag) then
	PRINT*, '     XRAY  ENERGY REQUESTED? ',echo(xray_flag)
		end if
	PRINT*, '        '
	PRINT*, '  ENERGY WEIGHTS:  '
	PRINT*, '     E VDW WEIGHT     : ', mumbo_we_vdw
	PRINT*, '     E ELEC WEIGHT    : ', mumbo_we_elec
	PRINT*, '     E RPROB WEIGHT   : ', mumbo_we_rprob
	PRINT*, '     E SOLV WEIGHT    : ', mumbo_we_solv
	PRINT*, '     E HBOND WEIGHT   : ', mumbo_we_hbond
!	PRINT*, '     E ASA WEIGHT     : ', mumbo_we_asa
!
		if (xray_flag) then
	PRINT*, '     E XRAY WEIGHT    : ', mumbo_we_xray
		end if
	PRINT*, '        '
	PRINT*, '  ENERGY CUTOFF: ', mumbo_en_cut, ' [kcal/mol] '
	PRINT*, '    (SETS THE UPPER LIMIT FOR THE INTERACTION ENERGY WITH '
	PRINT*, '    THE CONSTANT PART OF THE MOLECULE)'
	PRINT*, '        '
		if (mumbo_en_mc_recover_flag) then
	PRINT*, '  >> '
	PRINT*, '  >> AUTO_RECOVER OPTION TURNED ON FOR THE MC-STEP '
	PRINT*, '  >> ROTAMER WITH LOWEST ENERGY KEPT AT POSITION i '
        PRINT*, '  >> IF E-MC OF ALL ROTAMERS OF ALL AAS > ENERGY-CUTOFF ' 
	PRINT*, '  >> '
	PRINT*, '        '
		end if
	PRINT*, '  ENERGY TUNING PARAMETERS:  '
	PRINT*, '     VDW RADII SCALING: ', mumbo_en_svdw
	PRINT*, '     DIELECT. CONSTANT: ', mumbo_en_sdiel
	PRINT*, '     SHIFT FUNCTION FOR ELEC:    ', echo(eshift_flag)
	PRINT*, '     SWITCH FUNCTION FOR ELEC:   ', echo(eswitch_flag)
	PRINT*, '     DISTANCE FUNCTION FOR ELEC: ', echo(edist_flag)
	PRINT*, '     CONTINUOUS ELEC:            ', echo(econt_flag)
	PRINT*, '     VDW-INTER.  ONLY FOR AT-AT SEPARATION > ', mumbo_en_it_vdw
	PRINT*, '     ELE-INTER.  ONLY FOR AT-AT SEPARATION > ', mumbo_en_it_ele
	PRINT*, '     SOLV-CONTR. ONLY FOR AT-AT SEPARATION > ', mumbo_en_it_sol
	PRINT*, '     HBOND-INTER.ONLY FOR AT-AT SEPARATION > ', mumbo_en_it_hbd
	PRINT*, '        '
!
	   if (mumbo_en_vdw_soft) then
	PRINT*, '  >> '
	PRINT*, '  >> SOFT- VDW-REPULSION FUNCTION REQUESTED FOR DESIGN PURPOSES '
	PRINT*, '  >> REFERENCE: Pokala & Handel (2005), J. Mol. Biol. 347, 203-227 '
	PRINT*, '  >> '
	PRINT*, '        '
	   end if
!
!	   if (mumbo_en_vdw_reponly==0.0) then
	   if (abs(mumbo_en_vdw_reponly).lt.err2) then
	PRINT*, '  >> '
	PRINT*, '  >> VDW REPULSION CONSIDERED EXCLUSIVELY  '
	PRINT*, '  >> '
	PRINT*, '        '
	   end if
!
	   if (hbond_flag) then 
		if (.not.mumbo_en_hbd_empflag) then
		if (.not.mumbo_en_hbd_geoflag) then
	PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>>>>>>>>>>>>>>>>'
	PRINT*, '>> IF HBONDS REQUESTED, EITHER HBGEOM OR           '
	PRINT*, '>>          HBEMP MUST BE SPECIFIED                '  
	PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>'
		      stop
		end if
		end if
		if (mumbo_en_hbd_geoflag) then 
	PRINT*, '  >> '
	PRINT*, '  >> HBONDS:  CALCULATED ACCORDING TO: Gohlke et al. (2004), Proteins 56, 322-337 '
	PRINT*, '  >> HBONDS:  DISTANCE CUTOFF ..H-A..: ', mumbo_en_hbd_ctof
	PRINT*, '  >> HBONDS:  d(0)= ',mumbo_en_hbd_dzero,' E(0)= ',mumbo_en_hbd_ezero
	PRINT*, '  >> '
	PRINT*, '        '
		else if (mumbo_en_hbd_empflag) then
	PRINT*, '  >> '
	PRINT*, '  >> HBONDS:  CALCULATED USING AN EMPIRICAL COMPILATION '
	PRINT*, '  >> HBONDS:  REFERENCE: Kortemme et al. (2003), J. Mol. Biol. 326, 1239-1256 '
	PRINT*, '  >> HBONDS:  DISTANCE CUTOFF ..H-A..: ', mumbo_en_hbd_ctof
	PRINT*, '  >> '
	PRINT*, '        '
	        end if 
	   end if
!
	   if (elec_flag) then
	  	if (eswitch_flag) then
		    if (eshift_flag.or.edist_flag.or.econt_flag) then
	PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>>>>>>>>>>>>>>>>'
	PRINT*, '>> CAN ONLY APPLY EITHER ESHIFT, ESWITCH, EDIST OR '
	PRINT*, '>> ECONT, MUST EXIT !                                     '  
	PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>'
		      stop
		    else if (mumbo_en_ctonnb >= mumbo_en_ctofnb )then   
	PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>>>>>>>>>>'
	PRINT*, '>> CTONNB MUST BE SMALLER THEN CTOFNB        '
	PRINT*, '>> CTONNB: ', mumbo_en_ctonnb,' CTOFNB:',mumbo_en_ctofnb  
	PRINT*, '>> MUST EXIT !                               '  
	PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>'
		      stop
		    else
	PRINT*, '  >> '
	PRINT*, '  >> CTONNB AND CTOFNB FOR ESWITCH : '
	PRINT*, '  >> ', mumbo_en_ctonnb, mumbo_en_ctofnb 
	PRINT*, '  >> '
	PRINT*, '        '
		    end if
	  	else if (eshift_flag) then
		    if (eswitch_flag.or.edist_flag.or.econt_flag) then
	PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>>>>>>>>>>>>>>>>'
	PRINT*, '>> CAN ONLY APPLY EITHER ESHIFT, ESWITCH, EDIST OR '
	PRINT*, '>> ECONT, MUST EXIT !                                     '  
	PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>'
		      stop
		    else
	PRINT*, '  >> '
	PRINT*, '  >> CTOFNB FOR ESHIFT: '
	PRINT*, '  >> ', mumbo_en_ctofnb 
	PRINT*, '  >> '
	PRINT*, '        '
                    end if
	  	else if (edist_flag) then
		    if (eswitch_flag.or.eshift_flag.or.econt_flag) then
	PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>>>>>>>>>>>>>>>>'
	PRINT*, '>> CAN ONLY APPLY EITHER ESHIFT, ESWITCH, EDIST OR '
	PRINT*, '>> ECONT, MUST EXIT !                                     '  
	PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>'
		      stop
		    else
	PRINT*, '  >> '
	PRINT*, '  >> DISTANCE DEPENDENT DIELECTRIC APPLIED:     '
	PRINT*, '  >>    ', mumbo_en_sdiel, ' * DISTANCE(I,J)    '
	PRINT*, '  >> DIELECTRIC CONSTANT READ FORM DIELEC= CARD ' 
	PRINT*, '  >>   '
	PRINT*, '        '
		    end if
	  	else if (econt_flag) then
		    if (eswitch_flag.or.eshift_flag.or.edist_flag) then
	PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>>>>>>>>>>>>>>>>'
	PRINT*, '>> CAN ONLY APPLY EITHER ESHIFT, ESWITCH, EDIST OR '
	PRINT*, '>> ECONT, MUST EXIT !                                     '  
	PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>'
		      stop
		    else
	PRINT*, '  >>   '
	PRINT*, '  >> DIELECTRIC CONSTANT KEPT CONSTANT....      '
	PRINT*, '  >>   ', mumbo_en_sdiel, '                    '
	PRINT*, '  >>   '
	PRINT*, '       '
		    end if
		else
	PRINT*, '  >>   '
	PRINT*, '  >> NEITHER ESWITCH, EDIST, ESHIFT OR ECONT REQUESTED  '
	PRINT*, '  >>   '
	PRINT*, '       '
		end if
	  end if
!
		if (solv_flag) then
	PRINT*, '  >>  '
	PRINT*, '  >> SOLVATION MODEL FROM LAZARIDIS AND KARPLUS  '
	PRINT*, '  >> REFERENCE: Lazaridis & Karplus (1999), Proteins 35, 133-152'
	PRINT*, '  >> SOL_GREF  SET TO     : ', mumbo_en_solgref 
	PRINT*, '  >> SOL_GFREE SET TO     : ', mumbo_en_solgfree
	PRINT*, '  >> SOL_LAM   SET TO     : ', mumbo_en_sollam   
	PRINT*, '  >> '
	PRINT*, '        '
		end if
!
!
	PRINT*, '  LOG-FILE-TUNING:           '
	PRINT*, '     MC STEP      - DETAILED/LONG OUTPUT REQUESTED: ', echo(mumbo_log_mc_flag)
	PRINT*, '     ANALYSE STEP - DETAILED/LONG OUTPUT REQUESTED: ', echo(mumbo_log_ana_flag)
	PRINT*, '     DATABASE TRACKING - BACKING UP PREVIOUS FILES: ', echo(mumbo_log_dbase_flag)
	PRINT*, '        '
!
!	PRINT*, '  ACCESSIBLE SURFACE PARAMETERS:  '
!	PRINT*, '     SURFACE GRID (FRACTION OF ANG) : ', mumbo_asa_grd
!	PRINT*, '     SURFACE PROB RADIUS :            ', mumbo_asa_prb
!	PRINT*, '     SURFACE SHELL RADIUS :           ', mumbo_asa_shl
!	PRINT*, '        '
!
!
	END SUBROUTINE GET_JOB_DESCRIPTION  
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE GET_MUMBO_DATA()
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MUMBO_DATA_M
	USE MAX_DATA_M
!
	IMPLICIT NONE
!
	CHARACTER (LEN=132) :: string
	CHARACTER (LEN=1000) :: input_file
	CHARACTER (LEN=80) ::  word(40), boff, baff, biff
        CHARACTER (LEN=3), DIMENSION (:), allocatable :: list
        CHARACTER (LEN=60), DIMENSION (:), allocatable :: string_list 
        CHARACTER (LEN=60) :: string_blank
!        
    CHARACTER (LEN=3), DIMENSION (:), allocatable :: temp_mut_aas
    LOGICAL, DIMENSION (:), allocatable :: temp_flags
! 
	INTEGER  :: nw, nn, nmost, nstr_list, nnold, nnnew, ncount, nntrue, nnfalse
	INTEGER  :: na, i, j, k, l, m, nmum, ios, nnchar
	REAL     :: rr
	LOGICAL ::  chainid_flag 
	LOGICAL ::  inflag, flag
!
	na=len_trim(qual_mumbo_pos)
!
	do i=1,1000
	  input_file(i:i)=' '
	end do
!
	input_file(1:len_trim(qual_input))=qual_input
	inflag=.true.
	call get_infn(input_file,inflag,filelabel_input_file)
!
!	First count number of positions to be mamboed
!
	open(unit=14,file=input_file,status='old',form='formatted')
! 
	if (allocated(mumbo)) deallocate(mumbo)
!
	nmum=0
	do, i=1, max_lines_input 
	  read(14,fmt='(a)',iostat=ios) string
	  if (ios==eo_file) then
!	      print*,'END-OF-FILE REACHED IN FILE: '
!	      print*, input_file
!	      print*,'NO HARM DONE                 '
	      exit
	  end if
	  call parser(string, word, nw)
	  call maxwords(word,nw)
	  boff=word(1)
	  if (boff(1:na)==qual_mumbo_pos) then
		nmum=nmum+1
	  end if
	end do
!
	close(14)
	allocate(mumbo(nmum))
!
	open(unit=14,file=input_file,status='old',form='formatted')
!
	nmum=0
	do, i=1, max_lines_input 
	  read(14,fmt='(a)',iostat=ios) string
!	  print*, string
	  if (ios==eo_file) then
!	      print*,'END-OF-FILE REACHED IN FILE: '
!	      print*, input_file
!	      print*,'NO HARM DONE                 '
	      exit
	  end if
	  call parser(string, word, nw)
	  call maxwords(word,nw)
	  boff=word(1)
	  baff=word(2)
!
	  if (boff(1:na)==qual_mumbo_pos) then
!
		nmum=nmum+1
		mumbo(nmum)%mut_flag=.false.
		call chainid_present(baff,chainid_flag)
!
		nn=0
		do j=1,nw
		  biff=word(j)
		  if (biff(1:1)=='!') then
		    exit
		  end if
		  nn=nn+1
		end do
!
		nw=nn
!
		if (chainid_flag) then
			allocate (mumbo(nmum)%mut_aas(nw-3))
			mumbo(nmum)%mut_chid=baff(1:1)
			do j=3,nw
			  word(j-1)=word(j)
			end do
			baff=word(2) 
		else
			allocate (mumbo(nmum)%mut_aas(nw-2))
			mumbo(nmum)%mut_chid=' '
		end if
!
		read (baff,*) mumbo(nmum)%mut_nold
		do j=1,size(mumbo(nmum)%mut_aas)
		   baff=word(j+2)
		   mumbo(nmum)%mut_aas(j)=baff(1:3)
		end do
	  end if
!
	end do
!
	close(14)
!
!       now allow for some substitutions to be performed....     
!
!       first read in the substitution instructions as specified towards the end of MODULE MUMBO_DATA_M
!
        if (allocated(mumbosub)) deallocate(mumbosub)
        allocate(mumbosub(size(subs_instr)))
        do i=1,size(mumbosub)
            do j=1,132
               string(j:j)=' '
            end do
            nnchar=len(subs_instr(i))
            string(1:nnchar)=subs_instr(i)
	        call parser(string, word, nw)
	        call maxwords(word,nw)
!
!            print*, string(1:nnchar)
!            print*, nw, word	        	        
!	        
!	        
	        baff=word(1)
	        mumbosub(i)%mut_key=baff(1:3)
	        allocate(mumbosub(i)%mut_rep(nw-2))
            do j=1,nw-2
               baff=word(j+2)
               mumbosub(i)%mut_rep(j)=baff(1:3)
            end do
        end do
!
!        do i=1,size(mumbosub)
!          print*, 'LOOKING AT MUMBOSUB: ', i
!          print*, mumbosub(i)%mut_key
!          print*, size(mumbosub(i)%mut_rep), 'SIZE ' 
!          do j=1, size(mumbosub(i)%mut_rep)
!             print*,mumbosub(i)%mut_rep(j)
!          end do
!          print*, '  '
!        end do
!
!       EXPANDING RESIDUES LIKE REPLACING HIS BY HIS, HJS AND HKS SHOULD BE CODED HERE.... 
!       ONE COULD ALSO CODE THINGS LIKE USING THE KEYWORD ALL FOR ALL AMINO ACIDS...  
!
!
        do i=1,size(mumbosub)
!            print*, 'xxxxxxxxxxx'
!            print*, 'ITERATION ', mumbosub(i)%mut_key
!            print*, 'xxxxxxxxxxx'
!
            if (mumbosub(i)%mut_key.eq.'HIS') then      ! His-substitutions/expansions will be included later   
              cycle                                     ! to avoid not replacing/overlooking histidines introduced 
            end if                                      ! in substitutions introduced here such as replacing ALL with all AAs 
!
            do j=1,size(mumbo)
!            
               nnold=size(mumbo(j)%mut_aas)
!               
               if (allocated(temp_mut_aas)) deallocate(temp_mut_aas)  
               allocate(temp_mut_aas(nnold*200))        ! this is a hardcoded limit but should be OK
!
               ncount=0
               do k=1,nnold
                 if (mumbo(j)%mut_aas(k).eq.mumbosub(i)%mut_key) then 
                   do l=1, size(mumbosub(i)%mut_rep)
                      ncount=ncount+1
                      if (ncount.gt.(nnold*200)) then
!
        PRINT*, '    '
		PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
		PRINT*, '>>>>>>>  TO MANY AAs REQUESTED AT POSITION: ', mumbo(j)%mut_chid,' ',mumbo(j)%mut_nold 
		PRINT*, '>>>>>>>  SOMETHING WENT HORRIBLY WRONG, MUST STOP               '
		PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
		PRINT*, '    '
		STOP
!
                      end if
                      temp_mut_aas(ncount) = mumbosub(i)%mut_rep(l) 
                   end do
                 else
                   ncount=ncount+1
                   if (ncount.gt.(nnold*200)) then

        PRINT*, '    '
		PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
		PRINT*, '>>>>>>>  TO MANY AAs REQUESTED AT POSITION: ', mumbo(j)%mut_chid,' ',mumbo(j)%mut_nold 
		PRINT*, '>>>>>>>  SOMETHING WENT HORRIBLY WRONG, MUST STOP               '
		PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
		PRINT*, '    '
		STOP
                   end if
                   temp_mut_aas(ncount)=mumbo(j)%mut_aas(k)
!
    		     end if
!
               end do
!               
               deallocate(mumbo(j)%mut_aas)
               allocate(mumbo(j)%mut_aas(ncount))
!               
               do k=1,ncount
                    mumbo(j)%mut_aas(k)=temp_mut_aas(k)
               end do
!       
            end do
!            
        end do 
!
!        print*, '     '
!        print*, '  FINAL  '
!        do i=1,size(mumbo)
!          print*, mumbo(i)%mut_nold,'  ', mumbo(i)%mut_aas, '   ', size(mumbo(i)%mut_aas)
!        end do        
!
!       now a final loop replacing all HIS if not explictely stated otherwise (his_one_flag = true)
!        
        if (.not.his_one_flag) then
!        
        do i=1,size(mumbosub)
!            print*, 'xxxxxxxxxxx'
!            print*, 'ITERATION  ', mumbosub(i)%mut_key
!            print*, 'xxxxxxxxxxx'
        
            if (mumbosub(i)%mut_key.ne.'HIS') then     ! only performing His-substitutions here ...
              cycle                                       
            end if                                       
!
            do j=1,size(mumbo)
!            
               nnold=size(mumbo(j)%mut_aas)
!               print*, nnold , ' starting Size size(mumbo(j)%mut_aas)'
!               
               if (allocated(temp_mut_aas)) deallocate(temp_mut_aas)  
               allocate(temp_mut_aas(nnold*200))        ! this is a hardcoded limit but should be OK
!
               ncount=0
               do k=1,nnold
                 if (mumbo(j)%mut_aas(k).eq.mumbosub(i)%mut_key) then 
                   do l=1, size(mumbosub(i)%mut_rep)
                      ncount=ncount+1
                      if (ncount.gt.(nnold*200)) then
!
        PRINT*, '    '
		PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
		PRINT*, '>>>>>>>  TO MANY AAs REQUESTED AT POSITION: ', mumbo(j)%mut_chid,' ',mumbo(j)%mut_nold 
		PRINT*, '>>>>>>>  SOMETHING WENT HORRIBLY WRONG ==> MUST STOP               '
		PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
		PRINT*, '    '
		STOP
!
                      end if
                      temp_mut_aas(ncount) = mumbosub(i)%mut_rep(l) 
                   end do
                 else
                   ncount=ncount+1
                   if (ncount.gt.(nnold*200)) then

        PRINT*, '    '
		PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
		PRINT*, '>>>>>>>  TO MANY AAs REQUESTED AT POSITION: ', mumbo(j)%mut_chid,' ',mumbo(j)%mut_nold 
		PRINT*, '>>>>>>>  SOMETHING WENT HORRIBLY WRONG ==> MUST STOP               '
		PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
		PRINT*, '    '
		STOP
                   end if
                   temp_mut_aas(ncount)=mumbo(j)%mut_aas(k)
!
    		     end if
!
               end do
!               
               deallocate(mumbo(j)%mut_aas)
               allocate(mumbo(j)%mut_aas(ncount))
!               
               do k=1,ncount
                    mumbo(j)%mut_aas(k)=temp_mut_aas(k)
               end do
!       
            end do
!            
        end do 
!
        end if
!
!        print*, '     '
!        print*, '  FINAL *** FINAL '
!        do i=1,size(mumbo)
!          print*, mumbo(i)%mut_nold,'  ', mumbo(i)%mut_aas, '   ', size(mumbo(i)%mut_aas)
!        end do
!
!       CHECKING FOR AN ELIMINATING REDUNDANT AMINOACIDS.. NAMELY IF A CERTAIN TYPE OF AMINOCIDS 
!       IS REQUESTED MORE THAN ONCE AT A GIVEN POSITION ... 
!
        do i=1,size(mumbo)     ! loop over all positions
!        
           if (allocated(temp_flags)) deallocate(temp_flags)
           nnold=size(mumbo(i)%mut_aas)
           allocate(temp_flags(nnold))
           do j=1,nnold
             temp_flags(j)=.true.
           end do
           nnfalse=0
           do j=1,nnold
              do k=j+1,nnold
               if (.not.temp_flags(j)) cycle
!               print*, 'checking:', i, j, k
               if (mumbo(i)%mut_aas(j).eq.mumbo(i)%mut_aas(k)) then
                 temp_flags(j)=.false.
                 nnfalse=nnfalse+1
               end if
              end do
           end do
!
!          counting the number of unique amino acids 
!
           nntrue=0
           do j=1,nnold
             if (temp_flags(j)) nntrue=nntrue+1
           end do
!
!           
!          now backing up the unique amino acids into the array temp_mut_aas
!           
           if (allocated(temp_mut_aas)) deallocate(temp_mut_aas)  
           allocate(temp_mut_aas(nntrue))
!           
           nnnew=0
           do j=1,nnold
             if (temp_flags(j)) then
                nnnew=nnnew+1
                temp_mut_aas(nnnew)=mumbo(i)%mut_aas(j)
             end if
           end do
!           
           if (nnnew.ne.nntrue) print*, '>>> BUMMER 1 >>>'
           if ((nnold-nnfalse).ne.nnnew) print*, '>>> BUMMER 2 >>>'
!           
!
           deallocate(mumbo(i)%mut_aas)  
           allocate(mumbo(i)%mut_aas(nntrue))
           do j=1,nntrue
                mumbo(i)%mut_aas(j)=temp_mut_aas(j)
           end do           
!        
      end do
!
!        print*, '     '
!        print*, '  FINAL *** FINAL *** FINAL '
!        do i=1,size(mumbo)
!          print*, mumbo(i)%mut_nold,'  ', mumbo(i)%mut_aas, '   ', size(mumbo(i)%mut_aas)
!        end do
!
	do i=1,size(mumbo)
	   do j=i,size(mumbo)
		if (i==j) then
		   cycle
		else if (mumbo(i)%mut_nold == mumbo(j)%mut_nold .and.     &
     &          mumbo(i)%mut_chid == mumbo(j)%mut_chid) then
!		   
		PRINT*, '    '
		PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>>'
		PRINT*, '>>>>>>>  REQUESTED POSITION ', mumbo(i)%mut_nold
		PRINT*, '>>>>>>>  TO BE MUMBOED TWICE       '
		PRINT*, '>>>>>>>  MUST EXIT                           '
		PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>>>'
		PRINT*, '    '
		STOP
!
		end if 
	   end do
	end do
!			
!
!       Generating a list of unique residue types requested in qual_mumbo_pos
!       reslst(i)%aas     names of residues
!       reslst(i)%flag    flag - logical
!
        nmost=1
!        
	do i=1,size(mumbo)
           nmost = nmost + size(mumbo(i)%mut_aas)
	end do
!
        if (allocated(list)) deallocate(list)
        allocate(list(nmost))
        do i=1,nmost
           list(i)='   '
        end do
        nn=0
	do i=1,size(mumbo)
           do j=1,size(mumbo(i)%mut_aas)
              flag=.false.
              do k=1,nn
                if (mumbo(i)%mut_aas(j)==list(k)) then
                    flag=.true.
                end if 
              end do
              if (.not.flag) then
                  nn=nn+1
                  list(nn)=mumbo(i)%mut_aas(j)
              end if
           end do
        end do
!
	nn=0
        do i=1,nmost
           if (list(i)=='   ') then
              nn=i-1
              exit
           end if 
        end do
!
!       copying the list
!
        if (allocated(reslst)) deallocate(reslst)
        allocate (reslst(nn))
        do i=1, nn
           reslst(i)%aas =list(i)
           reslst(i)%flag=.true.
        end do
!
!       generating a user friendly output for the list 
!
        rr= size(reslst)
        rr = rr / 15
        rr = aint(rr)
        nstr_list= nint(rr)+1
!       
        if (allocated(string_list)) deallocate(string_list)
        allocate (string_list(nstr_list))
!
        do i=1,60
          string_blank(i:i)=' '
        end do
        do i=1,nstr_list
          string_list(i)=string_blank
        end do
!
        k=0
        m=0
        do i=1,size(reslst)
          m=m+1
          j=4*m-3
          flag=.false.
          write(string_blank(j:j+3),'(a3,a1)') reslst(i)%aas,' '    
          if(m.eq.15) then
              k=k+1
              string_list(k)=string_blank
              m=m-15
              do l=1,60
                 string_blank(l:l)=' '
              end do
          end if    
        end do
        k=k+1
        string_list(k)=string_blank
!
!
	PRINT*, '        '
	PRINT*, '################################'
	PRINT*, '# SUMMARY GET_MUMBO_DATA       #'
	PRINT*, '################################'
	PRINT*, '        '
!
	PRINT*,'  NUMBER OF POSITIONS TO BE MUMBOED', size(mumbo)
	PRINT*, '        '
!
	do i=1,size(mumbo)
	  PRINT*, '  POSITION: ', (mumbo(i)%mut_chid),(mumbo(i)%mut_nold)
	  PRINT*, '  NUMBER OF AAS:', (size(mumbo(i)%mut_aas))
	  do j=1,(size(mumbo(i)%mut_aas))
	      print*,'    REQUESTED AA: ',(mumbo(i)%mut_aas(j))
	  end do
	  PRINT*, '    '
        end do
!	
	PRINT*, '    '
	PRINT*, '  LIST OF DIFFERENT TYPES OF RESIDUES REQUESTED: '
	PRINT*, '  NUMBER OF RESIDUE TYPES: ', size(reslst)
	PRINT*, '  RESIDUE TYPES REQUESTED: ' 
	PRINT*, '    '
        do i=1,nstr_list
              Print*, '        ', string_list(i)
        end do
!	
	PRINT*, '        '
	PRINT*, '        '
!
	END SUBROUTINE GET_MUMBO_DATA
!
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!
	SUBROUTINE GET_INFN(filename,inflag,label)
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!
	USE MUMBO_DATA_M
	USE MAX_DATA_M
!
	IMPLICIT NONE
!
	CHARACTER (LEN=1000) :: filename, boff
	CHARACTER (LEN=35)   :: label, intlabel
	LOGICAL, SAVE 	:: first=.true. , flag=.true.
	LOGICAL :: inflag
	INTEGER :: i
	INTEGER :: nch, nsh, nup, np
!
	do i=1,1000
	  boff(i:i)=' '
	end do
	boff(1:len_trim(filename))=filename(1:len_trim(filename))
	do i=1,1000
	filename(i:i)=' '
	end do
	do i=1,35
	  intlabel(i:i)=' '
	end do
	intlabel(1:len_trim(label))=label(1:len_trim(label))
	
!
	if (first) then 
	   	call getcwd(mumbo_cwd)
	   	mumbo_ncwd=len_trim(mumbo_cwd)
	   	call getenv(qual_env(2:len_trim(qual_env)),mumbo_env)
	   	mumbo_nenv=len_trim(mumbo_env)
	   	first=.false.
	end if
!
!	PRINT*,filename
!
	nup= 0
	nsh= 0
	nch= 0
!
	if (boff(1:len_trim(qual_env))==qual_env) then
		filename = mumbo_env(1:mumbo_nenv) //                   &
     &                   '/' // boff((len_trim(qual_env)+2):len_trim(boff)) 
!
	else if (boff(1:3)=='../') then
	    do i = 1, 100
	      if (boff((3*nup+1):(3*nup+3))=='../') then
	      	nup=nup+1
		cycle
	      else
		exit
	      end if
	    end do
	    do i= mumbo_ncwd,1,-1
		nch=i
		if (mumbo_cwd(i:i)=='/') then
		   nsh=nsh+1
		   if (nsh==nup) then
		     exit
		   end if
		end if
	    end do		
	    filename = mumbo_cwd(1:(nch-1)) //                   &
     &                         '/' // boff((3*nup+1):len_trim(boff)) 
!
	else if (boff(1:1)=='/') then
            filename = boff(1:len_trim(boff))
!
	else
	    filename = mumbo_cwd(1:mumbo_ncwd) //                &
     &				'/' // boff(1:len_trim(boff))
!
	end if
!
!	NOW TOUCHDOWN
!
	if (inflag) then
	  inquire(file=filename,exist=flag)
	  if (.not.flag) then
		PRINT*, '    '
		PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>>'
		PRINT*, '>>>>>>>  FILE HANDLING ERROR FOR:                      '
		PRINT*, '>>>>>>> '
		PRINT*, '>>>>>>>  ',intlabel
		PRINT*, '>>>>>>> '
		PRINT*, '>>>>>>>  >>> FILE IS EITHER NOT SPECIFIED IN mumbo.inp    '
		PRINT*, '>>>>>>>  >>> OR WRONG FILE NAME / FILE DOES NOT EXIST     '
		PRINT*, '>>>>>>> '
		PRINT*, '>>>>>>>  COULD NOT LOCATE FILE: '
		PRINT*, '>>>>>>> ', filename(1:len_trim(filename))
		PRINT*, '>>>>>>>  MUST EXIT !                                  '
		PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>>'
		PRINT*, '    '
		STOP
	  end if
!
	else
	  open(15,file=filename,status='unknown',iostat=np)
	  if (np > 0) then
		PRINT*, '    '
		PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>>'
		PRINT*, '>>>>>>>  COULD NOT OPEN FILE FOR WRITING:             '
		PRINT*, '>>>>>>> ', filename(1:len_trim(filename))
		PRINT*, '>>>>>>>  MUST EXIT !                                  '
		PRINT*, '>>>>>>>  MOST LIKELY OUTPDB-FILE NOT SPECIFIED        '
		PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>>'
		PRINT*, '    '
		STOP
	  end if
	  close(15)
	end if 
!
	END SUBROUTINE GET_INFN
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE READ_NBOND 
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE AA_DATA_M
	USE MUMBO_DATA_M
	USE MAX_DATA_M
!
	IMPLICIT NONE
!
	CHARACTER (LEN=132) :: string, strong
	CHARACTER (LEN=80) ::  word(40)
	INTEGER  :: nw, nln, i, ios
!
	PRINT*,'  STARTING TO READ IN .....'
	PRINT*,'  ',mumbo_libnbo(1:len_trim(mumbo_libnbo))
!
	open(14,file=mumbo_libnbo,form='formatted',status='old')
	nln=0
	do i= 1, max_lines_input
	    read(14,fmt='(a)',iostat=ios) string 
	    if (ios==eo_file) then
!	      print*,'END-OF-FILE REACHED IN FILE: '
!	      print*, mumbo_libnbo
!	      print*,'NO HARM DONE                 '
	      exit
	    end if
	    if (i==max_lines_input) then
	      PRINT*, '    '
	      PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
	      PRINT*, '>>>>>>>  END-OF-FILE NOT REACHED:                    '
	      PRINT*, '>>>>>>> ', mumbo_libnbo(1:len_trim(mumbo_libnbo))
	      PRINT*, '>>>>>>>  INCREASE MAX_LINE_INPUT                     '
	      PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
	      PRINT*, '    '
	      STOP
	    end if
	    if (string(1:1)/='_') then
	      nln=nln+1
	    end if
	end do
	close(14)
!
	if (allocated(nbo)) deallocate (nbo)
	allocate (nbo(nln))
!
	nln=0
	open(14,file=mumbo_libnbo,form='formatted',status='old')
	nln=0
	do i= 1,max_lines_input
		read(14,fmt='(a)',iostat=ios) string 
		if (ios == eo_file) then
		   exit
		end if
		if (string(1:1)/='_') then
		   nln=nln+1
		   nbo(nln)%n_at_typ = string(1:4)
		   strong(1:5)='     '
		   strong(6:132)=string(6:132)
		   call parser(strong,word,nw)
		     read(word(1),*) nbo(nln)%n_at_ntyp
		     read(word(2),*) nbo(nln)%n_at_p1
		     read(word(3),*) nbo(nln)%n_at_p2
		     read(word(4),*) nbo(nln)%n_at_p3
		     read(word(5),*) nbo(nln)%n_at_p4
		     read(word(6),*) nbo(nln)%n_at_p5
		     read(word(7),*) nbo(nln)%n_at_p6
		end if
	end do 
	close(14)
!
	PRINT*,'  .....DONE'
	PRINT*,'  '
!
	END SUBROUTINE READ_NBOND
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE READ_CONNEC 
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
! BRITTANY : NOT SURE IF THE BELOW STILL WORKS 
!
	USE AA_DATA_M
	USE MUMBO_DATA_M
	USE MAX_DATA_M
!
	IMPLICIT NONE
!
	CHARACTER (LEN=300) :: string 
	CHARACTER (LEN=100)  :: word(100), boff, biff
	INTEGER  :: nw, nln, natm
	INTEGER  :: i,j,k, ios
!
	PRINT*,'  STARTING TO READ IN .....'
	PRINT*,'  ',mumbo_libcon(1:len_trim(mumbo_libcon))
!
	open(14,file=mumbo_libcon,form='formatted',status='old')
	nln=0
	do i= 1, max_lines_input
		read(14,fmt='(a)',iostat=ios) string 
!
!
		if (ios==eo_file) then
!		  print*,'END-OF-FILE REACHED IN FILE: '
!		  print*, mumbo_libcon
!		  print*,'NO HARM DONE                 '
		  exit
		end if
		if (i==max_lines_input) then
		  PRINT*, '    '
		  PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
		  PRINT*, '>>>>>>>  END-OF-FILE NOT REACHED:                    '
		  PRINT*, '>>>>>>> ', mumbo_libcon(1:len_trim(mumbo_libcon))
		  PRINT*, '>>>>>>>  INCREASE MAX_LINE_INPUT                     '
		  PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
		  PRINT*, '    '
		  STOP
		end if
		if (string(4:7)=='_CON') then
		     call parser2(string,word,nw)
		     if (nw > 1) then
		       nln=nln+1
		     end if
		end if
	end do 
	close(14)
!
	if (allocated(cs)) deallocate(cs)
	allocate (cs(nln))
!
	nln=0
	open(14,file=mumbo_libcon,form='formatted',status='old')
	do i= 1, max_lines_input
		read(14,fmt='(a)',iostat=ios) string 
		if (ios==eo_file) then
		  exit
		end if
		natm=0
		if (string(4:7)=='_CON') then
		  call parser2(string,word,nw)
!
		  if (nw > 1) then
			nln=nln+1
			cs(nln)%c_aan(1:3) = string(1:3)
			cs(nln)%c_aan(4:4) = ' '
			read (word(2),*) natm
			cs(nln)%c_num=natm
			allocate (cs(nln)%c_at(natm,natm))
			allocate (cs(nln)%c_nam(natm))
		    if (nw > 2) then
			  biff=word(3)
			  if (biff(1:2)=='XL') then
		Print *,'   '
		Print *,'  >>  XL-MATRIX FOUND FOR RESIDUE = ', cs(nln)%c_aan
		Print *,'  >>  MAX NUMBER OF ATOMS ALLOWED IN THIS FORMAT = 100 ATOMS'
		Print *,'   '
				do j=1,natm
				  read(14,fmt='(a)', iostat=ios) string
				  if (ios==eo_file) then
				    exit
				  end if
				  call parser2(string,word,nw)
				  boff=word(1)
				  biff=word(2)
!
!		PRINT*, 'DEBUG1**', biff
!
				  cs(nln)%c_nam(j)=boff(1:4)
!
				    do k=1,natm
!		read(word(k+1),*) itemp; cs(nln)%c_at(j,k)=.FALSE.;if (itemp.eq.1) cs(nln)%c_at(j,k)=.TRUE.
                                cs(nln)%c_at(j,k) = (biff(k:k)=='1')  ! a bit simpler than the above. Kay
				    end do
!
!		do k=1, natm
!		PRINT*, 'DEBUG3 ', cs(nln)%c_nam(j), cs(nln)%c_at(j,k), cs(nln)%c_nam(k)
!		end do
!
				end do
			  else 
			    PRINT *, 'ERROR IN READING IN CONNECTIVITY MATRIX'
			  end if
		    else if (nw == 2) then
			read(14,fmt='(a)',iostat=ios) string
				if (ios==eo_file) then
				  exit
				end if
!
!		PRINT*, 'DEBUG1', cs(nln)%c_aan
!
				do j=1,natm
!		PRINT*, 'DEBUG1*'
				  read(14,fmt='(a)', iostat=ios) string
!		PRINT*, 'DEBUG1**', string
				  if (ios==eo_file) then
				    exit
				  end if
				  call parser2(string,word,nw)
				  boff=word(1)
!
				  cs(nln)%c_nam(j)=boff(1:4)
!
!		PRINT*, 'DEBUG2', cs(nln)%c_aan, cs(nln)%c_nam(j), natm, j
!
				    do k=1,natm
!		read(word(k+1),*) itemp; cs(nln)%c_at(j,k)=.FALSE.;if (itemp.eq.1) cs(nln)%c_at(j,k)=.TRUE.
                                cs(nln)%c_at(j,k) = (word(k+1)=='1')  ! a bit simpler than the above. Kay
				    end do
!
!		PRINT*, 'DEBUG3', cs(nln)%c_aan, natm
!
				end do
		       end if 
		  end if
		end if
	end do
!		PRINT*, 'DEBUG4'
	close(14)
!
!	checking the symmetry of the connectivity matrices
!
	do i=1,size(cs)
	   do j=1, cs(i)%c_num
		do k=1, cs(i)%c_num
		 if (cs(i)%c_at(j,k).neqv.cs(i)%c_at(k,j)) then
		  PRINT*, '    '
		  PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>          '
		  PRINT*, '>>>>>>>  CONNECTIVITY TABLE READ FORM FILE IS NOT SYMMETRICAL  '
                  PRINT*, '>>>>>>>  FILE: ', mumbo_libcon(1:len_trim(mumbo_libcon))
		  PRINT*, '>>>>>>>  PROBLEMS ENCOUNTERED FOR RESIDUE: ', cs(i)%c_aan
		  PRINT*, '>>>>>>>  ATOMS: ', cs(i)%c_nam(j),'   ', cs(i)%c_nam(k)
		  PRINT*, '>>>>>>>  POSITION IN TABLE: ', j, k
		  PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
		  PRINT*, '    '
		  STOP
		 end if 
		end do
	   end do
	end do
!
!
	PRINT*,'  .....DONE '
	PRINT*,'  '
!
	END SUBROUTINE READ_CONNEC
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE READ_PARAMS
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
!
	USE AA_DATA_M
	USE MUMBO_DATA_M
	USE MAX_DATA_M
!
	IMPLICIT NONE
!
	CHARACTER (LEN=132) :: string 
	CHARACTER (LEN=80)  :: word(40), boff
	CHARACTER (LEN=4) :: aanam 
	INTEGER  :: naa, nat, nw, ios
	INTEGER  :: i
	LOGICAL  :: new
!
!  ASN_PAR           
!  1    N        NH1   7    -0.3500    10   
!  2    H        H     1     0.2500     1    
!  3    CA       CH1E  6     0.1000     4    
!  ....... 
!
	PRINT*,'  STARTING TO READ IN .....'
	PRINT*,'  ',mumbo_libpar(1:len_trim(mumbo_libpar))
!
	open(14,file=mumbo_libpar,form='formatted',status='old')
!
	naa=0
	do i=1, max_lines_input 
		read(14,fmt='(a)',iostat=ios) string
		if (ios==eo_file) then
!		     print*,'END-OF-FILE REACHED IN FILE: '
!		     print*, mumbo_libpar(1:len_trim(mumbo_libpar))
!		     print*,'NO HARM DONE                 '
		     exit
		end if
		if (i==max_lines_input) then
		  PRINT*, '    '
		  PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
		  PRINT*, '>>>>>>>  END-OF-FILE NOT REACHED:                    '
		  PRINT*, '>>>>>>> ', mumbo_libpar(1:len_trim(mumbo_libpar))
		  PRINT*, '>>>>>>>  INCREASE MAX_LINE_INPUT                     '
		  PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
		  PRINT*, '    '
		  STOP
		end if
		if (string(4:4)=='_'.and.string(5:7)=='PAR') then
		     naa=naa+1
		     cycle
		end if
	end do
	close(14)
!
	if (allocated(ps)) deallocate(ps)
	allocate(ps(naa))
!
	open(14,file=mumbo_libpar,form='formatted',status='old')
!
	naa=0
	nat=0
	new=.false.
	do i=1,max_lines_input
		read(14,fmt='(a)',iostat=ios) string
		if (ios==eo_file) then
		     exit
		end if
		if (string(4:4)=='_'.and.string(5:7)=='PAR') then
		     new=.true.
		     naa=naa+1
		     nat=0
		else if (string(4:4)=='_'.and.string(5:7)/='PAR') then
		     if (new) then
		     	new=.false.
		     	allocate (ps(naa)%p_at_nam(nat))
		     	allocate (ps(naa)%p_at_typ(nat))
		     	allocate (ps(naa)%p_at_charge(nat))
		     	allocate (ps(naa)%p_at_num(nat))
		     	allocate (ps(naa)%p_at_ntyp(nat))
		     	allocate (ps(naa)%p_at_noz(nat))
		     	allocate (ps(naa)%p_at_ab(nat))
		     	allocate (ps(naa)%p_at_hb(nat))
		      end if 
		else if (new) then
		     nat=nat+1
		end if
	end do
	close(14)
!
!	print*,'***********************************************'
!	do i=1,size(ps)
!	   print*,  size(ps)
!	   print*, '*****',size(ps(i)%p_at_num)			   
!	end do
!	print*,'***********************************************'
!
	open(14,file=mumbo_libpar,form='formatted',status='old')
!
	naa=0
	nat=0
	new=.false.
	aanam='    '
	do i=1, max_lines_input
		read(14,fmt='(a)', iostat=ios) string
		if (ios==eo_file) then
		     exit
		end if
		if (string(4:4)=='_'.and.string(5:7)=='PAR') then
		   new=.true.
		   naa=naa+1
		   aanam(1:3)= string(1:3)
		   ps(naa)%p_aan = aanam
		   nat=0
		else if (string(4:4)=='_'.and.string(5:7)/='PAR') then
		   new=.false.
		else if (new) then
		     nat=nat+1
		     ps(naa)%p_at_ncnt=nat
		     call parser(string,word,nw)		
		     read(word(1),*) ps(naa)%p_at_num(nat)
		     boff = word(2)
		     ps(naa)%p_at_nam(nat) = boff(1:4)
		     boff = word(3)
		     ps(naa)%p_at_typ(nat) = boff(1:4)
		     read(word(4),*) ps(naa)%p_at_noz(nat)
		     read(word(5),*) ps(naa)%p_at_charge(nat)
		     read(word(6),*) ps(naa)%p_at_ntyp(nat)
!
		     boff= word(7)
		     ps(naa)%p_at_hb(nat) = boff(1:1)
		     boff= word(8)
		     ps(naa)%p_at_ab(nat) = boff(1:4)
!
!ALA_PAR
!1    N        NH1   7    -0.3500    10   E   C
!2    H        H     1     0.2500     1   H   -
!3    CA       CH1E  6     0.1000     4   -   -
!4    C        C     6     0.5500     3   -   -
!5    O        O     8    -0.5500    15   B   C
!6    CB       CH3E  6     0.0000     6   -   -
!
		end if
	end do
	close(14)
!
!	checking reading in
!
!	print*,'***********************************************'
!	do i=1,size(ps)
!	   print*, '*****',ps(i)%p_aan
!	   print*, '*****',ps(i)%p_at_ncnt				   
!	   do j=1,ps(i)%p_at_ncnt
!		print*, 						&
!     &			ps(i)%p_at_num(j),		&
!     &			ps(i)%p_at_nam(j),		&
!     &			ps(i)%p_at_typ(j),		&
!     &			ps(i)%p_at_noz(j),		&
!     &			ps(i)%p_at_charge(j),ps(i)%p_at_ntyp(j)
!	   end do
!	end do
!	print*,'***********************************************'
!
	PRINT*,'  .....DONE '
	PRINT*,'  '
!
	END SUBROUTINE READ_PARAMS
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE READ_GEOM
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE AA_DATA_M
	USE MUMBO_DATA_M
	USE MAX_DATA_M
!
	IMPLICIT NONE
!
	INTEGER :: i,j,k,l, m, n, o,p 
	INTEGER :: nw, nres, nbos, nan, ndh, nrt, ngr, nat
	INTEGER :: ios
	CHARACTER (LEN=3), dimension(max_resid_type) :: cres
	CHARACTER (LEN=132) :: string
	CHARACTER (LEN=80)  :: word(40), boff
	CHARACTER (LEN=7)   :: keyword
	LOGICAL :: flag 
	INTEGER :: nnpt, npp
	INTEGER :: noff, nnp, nfine, nstp, nmi, nmo,nrot, ntot, nt
	REAL, DIMENSION(:), ALLOCATABLE :: xdil, xdpr
!
!
! GFORTRAN (maybe noff=1 would be a safer starting value)
	noff=0
!
!
	PRINT*, '        '
	PRINT*, '##########################################'
	PRINT*, '# READING ROTAMER BUILDING INSTRUCTIONS  #'
	PRINT*, '##########################################'
	PRINT*, '  '
	PRINT*, '  FIRST READING PHI_PSI INDEPENDENT ROTAMERS ...  '
!
	open(11,file=mumbo_librot,form='formatted',status='old')
!
	nres=0
	do i=1, max_lines_input
	    read(11,fmt='(a)',iostat=ios) string 
	    if (ios==eo_file) then
!	      print*,'END-OF-FILE REACHED IN FILE: '
!	      print*, mumbo_librot(1:len_trim(mumbo_librot))
!	      print*,'NO HARM DONE                 '
	      exit
	    end if
	    if (i==max_lines_input) then
	      PRINT*, '    '
	      PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
	      PRINT*, '>>>>>>>  END-OF-FILE NOT REACHED:                    '
	      PRINT*, '>>>>>>> ', mumbo_librot(1:len_trim(mumbo_librot))
	      PRINT*, '>>>>>>>  INCREASE MAX_LINE_INPUT                     '
	      PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
	      PRINT*, '    '
	      STOP
	    end if
	    flag=.true.
	    if (string(4:4)=='_'.and.string(1:3)/='___') then
		do j=1,nres
		  if (string(1:3)==cres(j)) then
		     flag=.false.
		  end if
		end do
	      if (flag) then
		  nres=nres+1
		  cres(nres)=string(1:3)
	      end if
          end if 
	end do
!
	close(11)
!
	if (allocated(geo)) deallocate(geo)
	allocate(geo(nres))
!
	do i=1,nres
	  geo(i)%g_aan= cres(i)//' '
!	  print*, geo(i)%g_aan, i
	end do
!
!	NOW START READ IN BUILDING INSTRUCTIONS FOR EACH RESIDUE
!
!
!	NOW START READING BOND INSTRUCTIONS
!
!	'ARG_BONDS        '
!	'CB  CG     1.540 '
!	'CG  CD     1.540 '
!	'CD  NE     1.440 '
!	'NE  CZ     1.440 '
!	
	nbos=0
	do i=1,size(geo)
	  keyword=cres(i)//'_BON'
! 
	  do l=1,2
!
	    if (l==2) then 
		geo(i)%g_nbo=nbos
!		if (allocated(geo(i)%bo)) deallocate(geo(i)%bo)
		allocate(geo(i)%bo(nbos))
	    end if
!
	    open(11,file=mumbo_librot,form='formatted',status='old')
	    flag = .false.
	    nbos = 0
	    do j=1, max_lines_input
		read(11,fmt='(a)',iostat=ios) string 
		if (ios==eo_file) then
!		  print*,'END-OF-FILE REACHED IN FILE: '
!		  print*, mumbo_librot
!		  print*,'NO HARM DONE                 '
		  exit
		end if
		if (.not.flag) then
		  if (string(1:7)== keyword) then
		    flag = .true.
		  end if
		  cycle
		else if (flag) then 
		  if (string(4:4).eq.'_') then
		    flag=.false.
		    exit
		  else 
		    nbos=nbos+1
		    if (l==2) then
			 call parser(string,word,nw)
			 boff=word(1)
			 geo(i)%bo(nbos)%at_bod(1)=boff(1:4)
			 boff=word(2)
			 geo(i)%bo(nbos)%at_bod(2)=boff(1:4)
			 read(word(3),*) geo(i)%bo(nbos)%at_bol
		    end if
		  end if
		end if
	    end do
	    close(11)
!
	    if (nbos == 0) then
!	       if (allocated(geo(i)%bo)) deallocate(geo(i)%bo)
	       geo(i)%g_nbo=nbos
	       exit
	    end if
!
	  end do
	end do
!
!	do i=1, size(geo)
!	print*, '****',geo(i)%g_aan
!		do l=1, geo(i)%g_nbo
!		  print*, geo(i)%bo(l)%at_bod(1), geo(i)%bo(l)%at_bod(2), geo(i)%bo(l)%at_bol
!		end do
!	end do
!
!	print*, 'DONE READING IN BONDS'
!
!
!	FINISHED READING IN BOND DATA
!	NOW READING IN ANGLE DATA
!
!	'ARG_ANGLES            | '(3a4,f8.3)'
!	'CA  CB  CG   109.500 '
!	'CB  CG  CD   109.500 '
!	'CG  CD  NE   109.500 '
!	'CD  NE  CZ   109.500 '
!	
	nan=0
	do i=1,size(geo)
	  keyword=cres(i)//'_ANG' 
!
	  do l=1,2
!
	    if (l==2) then 
		geo(i)%g_nan=nan
!		if (allocated(geo(i)%an)) deallocate(geo(i)%an)
		allocate(geo(i)%an(nan))
	    end if
!
	    open(11,file=mumbo_librot,form='formatted',status='old')
	    flag = .false.
	    nan = 0
	    do j=1, max_lines_input
		read(11,fmt='(a)',iostat=ios) string 
		if (ios==eo_file) then
!		  print*,'END-OF-FILE REACHED IN FILE: '
!		  print*, mumbo_librot
!		  print*,'NO HARM DONE                 '
		  exit
		end if
		if (.not.flag) then
		  if (string(1:7)== keyword) then
		    flag = .true.
		  end if
		  cycle
		else if (flag) then 
		  if (string(4:4).eq.'_') then
		    flag=.false.
		    exit
		  else 
		    nan=nan+1
		    if (l==2) then
			 call parser(string,word,nw)
			 boff=word(1)
			 geo(i)%an(nan)%at_ang(1)=boff(1:4)
			 boff=word(2)
			 geo(i)%an(nan)%at_ang(2)=boff(1:4)
			 boff=word(3)
			 geo(i)%an(nan)%at_ang(3)=boff(1:4)
			 read(word(4),*) geo(i)%an(nan)%at_agl
		    end if
		  end if
		end if
	    end do
	    close(11)
!
	    if (nan == 0) then
!	       if (allocated(geo(i)%an)) deallocate(geo(i)%an)
	       geo(i)%g_nan=nan
	       exit
	    end if
!
	  end do
!
	end do
!
!	do i=1, size(geo)
!	print*, '****',geo(i)%g_aan
!		do l=1, geo(i)%g_nan
!		  print*, geo(i)%an(l)%at_ang(1), geo(i)%an(l)%at_ang(2), &
!     &     geo(i)%an(l)%at_ang(3), geo(i)%an(l)%at_agl
!		end do
!	end do
!
!	print*, 'DONE READING IN ANGLES'
!
!	FINISHED READING IN ANGLE DATA
!	NOW READING IN DIHEDRAL DATA
!
!	'ARG_DIHEDRAL    '
!	'N   CA  CB  CG  '
!	'CA  CB  CG  CD  '
!	'CB  CG  CD  NE  '
!	'CG  CD  NE  CZ  '
!
	ndh=0
	do i=1, size(geo)
	  keyword=cres(i)//'_DIH' 
!
	  do l=1,2
!
	    if (l==2) then 
		geo(i)%g_ndh=ndh
!		if (allocated(geo(i)%dh)) deallocate(geo(i)%dh)
		allocate(geo(i)%dh(ndh))
	    end if
!
	    open(11,file=mumbo_librot,form='formatted',status='old')
	    flag = .false.
	    ndh = 0
	    do j=1, max_lines_input
		read(11,fmt='(a)',iostat=ios) string 
		if (ios==eo_file) then
!		  print*,'END-OF-FILE REACHED IN FILE: '
!		  print*, mumbo_librot
!		  print*,'NO HARM DONE                 '
		  exit
		end if
		if (.not.flag) then
		  if (string(1:7)== keyword) then
		    flag = .true.
		  end if
		  cycle
		else if (flag) then 
		  if (string(4:4).eq.'_') then
		    flag=.false.
		    exit
		  else 
		    ndh=ndh+1
		    if (l==2) then
			 call parser(string,word,nw)
			 boff=word(1)
			 geo(i)%dh(ndh)%at_dih(1)=boff(1:4)
			 boff=word(2)
			 geo(i)%dh(ndh)%at_dih(2)=boff(1:4)
			 boff=word(3)
			 geo(i)%dh(ndh)%at_dih(3)=boff(1:4)
			 boff=word(4)
			 geo(i)%dh(ndh)%at_dih(4)=boff(1:4)
		    end if
		  end if
		end if
	    end do
	    close(11)
!
	    if (ndh == 0) then
!	       if (allocated(geo(i)%dh)) deallocate(geo(i)%dh)
	       geo(i)%g_ndh=ndh
	       exit
	    end if
!
	  end do
	end do
!
!	do i=1, size(geo)
!	print*, '****',geo(i)%g_aan
!		do l=1, geo(i)%g_ndh
!		  print*, geo(i)%dh(l)%at_dih(1), geo(i)%dh(l)%at_dih(2), &
!     &     geo(i)%dh(l)%at_dih(3), geo(i)%dh(l)%at_dih(4)
!		end do
!	end do
!
!	print*, 'DONE READING IN DIHEDRALS'
!
!	FINISHED READING IN DIHEDRAL DATA
!	NOW READING IN ACTUAL DIHEDRAL VALUES
!
!	'ARG_ROTAMERS              | 1st value = pobability for given rotamer'
!	'1.00 180.000 180.000 180.000 180.000'
!	'0.50 120.000 180.000 180.000 120.000'
!	'0.75  60.000 120.000 120.000 120.000'
!	'0.25  60.000  60.000  60.000  60.000'
!
	nrt=0
	do i=1, size(geo)
!
	keyword=cres(i)//'_ROT' 
!
	    if (geo(i)%g_ndh==0) then
		cycle
	    end if
!
	    open(11,file=mumbo_librot,form='formatted',status='old')
	    flag = .false.
	    nrt = 0
	    do j=1, max_lines_input
		read(11,fmt='(a)',iostat=ios) string 
		if (ios==eo_file) then
!		  print*,'END-OF-FILE REACHED IN FILE: '
!		  print*, mumbo_librot
!		  print*,'NO HARM DONE                 '
		  exit
		end if
		if (.not.flag) then
		  if (string(1:7) == keyword) then
		    flag = .true.
		  end if
		  cycle
		else if (flag) then 
		  if (string(4:4).eq.'_') then
		    flag=.false.
		    exit
		  else 
		    nrt=nrt+1
		  end if
		end if
	    end do
	    close(11)
!
	    if (nrt == 0) then
		do j=1, geo(i)%g_ndh 
	           geo(i)%dh(j)%at_nrt = nrt
		end do
		cycle
	    else 
		if (fine_flag) then
		    do j= (mumbo_fine_nstp + 1), 1, -1
		       nstp = j - 1
		       nfine = (2 * nstp ) + 1
		       if (chi_all_flag.and.(.not.chi_one_flag))      then
		          noff = nfine**(geo(i)%g_ndh)
		          ntot = noff * nrt
		       else if (chi_one_flag.and.(.not.chi_all_flag)) then
		          noff = nfine
		          ntot = noff * nrt
		       else
	PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>>>>>>>>>>>>>>>>>>>>>>>>>'
	PRINT*, '>> SOMETHING MESSED UP WITH CHI_ONE AND CHI_ALL           >>'  
	PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>'
		          stop
		       end if
		       if (ntot <= mumbo_fine_limit) then
		         exit
		       end if
	PRINT*, '  FINE_NSTP AUTOMATICALLY REDUCED FOR AMINO_ACID  ',geo(i)%g_aan  
		    end do 	  
		else
		    noff = 1
		    nfine = 1
		    ntot = nrt
		end if
!
		nt = geo(i)%g_ndh
	PRINT*, '  NUMBER OF ROTAMERS FOUND FOR: ', geo(i)%g_aan, nrt 
		if (fine_flag) then
		    if (chi_all_flag.and.(.not.chi_one_flag))      then
	WRITE(*,'(A16,I2,A2,I2,A2,I3,A1,I5)')                          &
     &              '   EXPANDED TO:(',nfine,'**',nt,')*',nrt,'=',ntot
		    else if (chi_one_flag.and.(.not.chi_all_flag)) then
	WRITE(*,'(A16,I3,A3,I3,A3,I5)')                          &
     &              '   EXPANDED TO: ',nrt,' * ',nfine,' = ',ntot
		    end if
		end if
!
		do j=1, geo(i)%g_ndh 
		    geo(i)%dh(j)%at_nrt= ntot
	            allocate(geo(i)%dh(j)%at_dil(geo(i)%dh(j)%at_nrt))
	            allocate(geo(i)%dh(j)%at_dpr(geo(i)%dh(j)%at_nrt))
		end do
	    end if
!
	    open(11,file=mumbo_librot,form='formatted',status='old')
	    flag = .false.
	    nrt = 0
	    do j=1, max_lines_input
		read(11,fmt='(a)',iostat=ios) string 
		if (ios==eo_file) then
!		  print*,'END-OF-FILE REACHED IN FILE: '
!		  print*, mumbo_librot
!		  print*,'NO HARM DONE                 '
		  exit
		end if
		if (.not.flag) then
		  if (string(1:7) == keyword) then
		    flag = .true.
		  end if
		  cycle
		else if (flag) then 
		  if (string(4:4).eq.'_') then
		    flag=.false.
		    exit
		  else 
		    nrt=nrt+1
		    call parser(string,word,nw)
		    nnp = (noff*(nrt-1)) + 1
	            do k=1,(geo(i)%g_ndh)
			read (word(1),*)   geo(i)%dh(k)%at_dpr(nnp)
			read (word(k+1),*) geo(i)%dh(k)%at_dil(nnp) 
		    end do
		  end if
		end if
	    end do
	    close(11)
!
!DEBUG
!	    if (geo(i)%g_aan =='TYR ') then 
!	  	do k = 1, geo(i)%g_ndh
!		   do m = 1, size(geo(i)%dh(k)%at_dpr)
!		    print *, geo(i)%dh(k)%at_dil(m), geo(i)%dh(k)%at_dpr(m), k
!		   end do
!		end do
!	    end if
!DEBUG
!
	    if (fine_flag) then
!
		nstp = (nfine +1)/2 
	        nrot = geo(i)%dh(1)%at_nrt / noff 
		if (allocated(xdil)) deallocate(xdil)
		if (allocated(xdpr)) deallocate(xdpr)
		allocate(xdil(nfine))
		allocate(xdpr(nfine))
!
	     if (chi_all_flag.and.(.not.chi_one_flag))      then
!
	        nnp = 1
	        do p = 1, nrot 
	          nmo = noff / nfine
	          nmi = 1
	          do k = geo(i)%g_ndh, 1, -1
                     nnp = (noff*(p-1)) + 1 
		     do m = 1, nfine
		 xdil(m)= geo(i)%dh(k)%at_dil(nnp) + ((m-nstp)*  mumbo_fine_ndeg)
		 xdpr(m) = geo(i)%dh(k)%at_dpr(nnp)
		        if (xdil(m) < -180) then
			  xdil(m) = xdil(m) + 360
			else if (xdil(m) > 180) then
			  xdil(m) = xdil(m) - 360
			end if
		     end do
		     do m = 1, nmo
			do n = 1, nfine
			   do o = 1, nmi
!
			    geo(i)%dh(k)%at_dil(nnp) = xdil(n)
			    geo(i)%dh(k)%at_dpr(nnp) = xdpr(n)
!
			    nnp = nnp +1
!
			   end do
			end do
		     end do
		     nmo = nmo / nfine
		     nmi = nmi *  nfine
		   end do
		end do
!
	      else if (chi_one_flag.and.(.not.chi_all_flag)) then
!
		 npp=1
	         do k = 1, geo(i)%g_ndh
		   do p = 1, nrot
		     npp = (p-1) * nfine + 1
!
		     do m = 1, nfine
!
		       if (k==1) then
!		     
		 xdil(m) = geo(i)%dh(k)%at_dil(npp) + ((m-nstp)*  mumbo_fine_ndeg)
		 xdpr(m) = geo(i)%dh(k)%at_dpr(npp)
!		 
		          if (xdil(m) < -180) then
			    xdil(m) = xdil(m) + 360
			  else if (xdil(m) > 180) then
			    xdil(m) = xdil(m) - 360
			  end if
		       else
		 xdil(m) = geo(i)%dh(k)%at_dil(npp)
		 xdpr(m) = geo(i)%dh(k)%at_dpr(npp)
		       end if
		     end do
!
		     do m = 1, nfine
		 nnpt = npp + m - 1
		 geo(i)%dh(k)%at_dil(nnpt) = xdil(m)
		 geo(i)%dh(k)%at_dpr(nnpt) = xdpr(m)
		     end do
		   end do
		 end do
!
	    end if
	 end if
!
!	    if (geo(i)%g_aan =='TYR ') then 
!	  	do k = 1, geo(i)%g_ndh
!		   do m = 1, size(geo(i)%dh(k)%at_dpr)
!		    print *, geo(i)%dh(k)%at_dil(m), geo(i)%dh(k)%at_dpr(m), k
!		   end do
!		end do
!	    end if
!
	end do
!DEBUG
!	stop
!DEBUG
!
!	do i=1, size(geo)
!	print*, '****',geo(i)%g_aan
!		do l=1, geo(i)%g_ndh
!		  do k=1, geo(i)%dh(l)%at_nrt
!		    print*, geo(i)%dh(l)%at_dpr(k), geo(i)%dh(l)%at_dil(k)
!		  end do
!		end do
!	end do
!
!	print*, 'DONE READING IN DIHEDRAL VALUES'
!
!	FINISHED READING IN DIHEDRAL VALUES
!	NOW READING IN GROUPS
!
!	'ARG_GROUPS  '
!	'CD  NE  CZ  '
!       'ATOM   1045  CD  ARG   131      29.386  53.327  26.440  1.00 27.02     
!       'ATOM   1046  NE  ARG   131      28.980  54.215  27.530  1.00 28.02     
!       'ATOM   1047  CZ  ARG   131      29.323  54.075  28.815  1.00 28.47    
!       'ATOM   1048  NH1 ARG   131      30.087  53.058  29.223  1.00 22.21      
!       'ATOM   1049  NH2 ARG   131      28.940  55.014  29.690  1.00 28.87      
!
!GFORTRAN
	ngr=0
!GFORTRAN	
	do i=1, size(geo)
	  keyword=cres(i)//'_GRO' 
!	  print*, geo(i)%g_aan, keyword
!
	  do l=1,3
!
	    if (l==2) then 
		geo(i)%g_ngr=ngr
!		if (allocated(geo(i)%gr)) deallocate(geo(i)%gr)
		allocate(geo(i)%gr(ngr))
	    end if
!
	    if (l==3) then
		ngr=size(geo(i)%gr)
		do, j=1,ngr
			nat=geo(i)%gr(j)%at_num
			allocate(geo(i)%gr(j)%at_nam(nat))
			allocate(geo(i)%gr(j)%at_co1(nat))
			allocate(geo(i)%gr(j)%at_co2(nat))
			allocate(geo(i)%gr(j)%at_co3(nat))
		end do
		do, j=1,ngr
		  do k=1,3
		    geo(i)%gr(j)%at_com(k)='    '	
		  end do
		  do k=1,(geo(i)%gr(j)%at_num)
		    geo(i)%gr(j)%at_nam(k)= '    '
		  end do  
		end do
	    end if
!
	    open(11,file=mumbo_librot,form='formatted',status='old')
	    flag = .false.
	    ngr = 0
	    nat = 0
	    do j=1, max_lines_input
		read(11,fmt='(a)',iostat=ios) string 
		if (ios==eo_file) then
!		  print*,'END-OF-FILE REACHED IN FILE: '
!		  print*, mumbo_librot
!		  print*,'NO HARM DONE                 '
		  exit
		end if
		if (.not.flag) then
		    if (string(1:7)== keyword) then
		      flag = .true.
		    end if
		    cycle
		else if (flag) then 
		    if (string(4:4).eq.'_'.and.string(5:7).ne.'GRO') then
		      flag=.false.
		      exit
		    else if (string(1:4).ne.'ATOM'.and.string(4:4).ne.'_') then 
		      ngr=ngr+1
		      nat=0
		      if (l==3) then
			   call parser(string,word,nw)
			   boff=word(1)
			   geo(i)%gr(ngr)%at_com(1)=boff(1:4)
			   boff=word(2)
			   geo(i)%gr(ngr)%at_com(2)=boff(1:4)
			   boff=word(3)
			   geo(i)%gr(ngr)%at_com(3)=boff(1:4)
		      end if
		      cycle				
		    else if (string(1:4)=='ATOM') then 
		      nat=nat+1
		      if (l==2) then
			   geo(i)%gr(ngr)%at_num = nat
		      end if
		      if (l==3) then 
			   call parser(string,word,nw)
			   boff=word(3)
			   geo(i)%gr(ngr)%at_nam(nat) = boff(1:4)
			   read(word(6),*) geo(i)%gr(ngr)%at_co1(nat)  
			   read(word(7),*) geo(i)%gr(ngr)%at_co2(nat)  
			   read(word(8),*) geo(i)%gr(ngr)%at_co3(nat)
!			   read(string(14:18),fmt='(a4)')  geo(i)%gr(ngr)%at_nam(nat)
!			   read(string(31:38),fmt='(f8.3)')geo(i)%gr(ngr)%at_co1(nat)
!			   read(string(39:46),fmt='(f8.3)')geo(i)%gr(ngr)%at_co2(nat)
!			   read(string(47:54),fmt='(f8.3)')geo(i)%gr(ngr)%at_co3(nat)
		      end if
		    cycle
		    end if
		end if
	    end do
	    close(11)
!
	    if (ngr == 0) then
!	       if (allocated(geo(i)%gr)) deallocate(geo(i)%gr)
	       geo(i)%g_ngr=0
	       exit
	    end if
!
! 
! 	    if (l==3) then 
! 	      do j=1,ngr
! 		  print*, geo(i)%gr(j)%at_com(1), &
!     &                geo(i)%gr(j)%at_com(2), &
!     &                geo(i)%gr(j)%at_com(3)
! 		  nat=geo(i)%gr(j)%at_num
! 		  do k=1, nat
! 	          print*,geo(i)%gr(j)%at_nam(k), &
!     &                 geo(i)%gr(j)%at_co1(k), &
!     &                 geo(i)%gr(j)%at_co2(k), &
!     &                 geo(i)%gr(j)%at_co3(k)
! 		  end do
! 	      end do
! 	    end if
!
!
	  end do
	end do
!
	PRINT*, '  ... FINISHED READING IN PHI_PSI INDEPENDENT ROTAMER BUILDING INSTRUCTIONS'
	PRINT*, '  '
!
	END SUBROUTINE READ_GEOM
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE OBSOLETE1
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	IMPLICIT NONE
!
	PRINT*,'  '
	PRINT*,'>>>>>>>>> BUMMER >>>>>>>>>>>>>>>>>>>>>>>>>>>                     '
	PRINT*,'>>>                                                              '
	PRINT*,'>>> OBSOLETE KEYWORD IN MUMBO.INP ## COMP_TUNING ##              '
	PRINT*,'>>> PLEASE USE ## COMP_PICKNAT_TUNING ## INSTEAD                 '
	PRINT*,'>>> SORRY FOR THIS                                               '
	PRINT*,'>>>                                                              '
	PRINT*,'>>>>>>>>> BUMMER >>>>>>>>>>>>>>>>>>>>>>>>>>>                     '
	PRINT*,'  '
	stop
!	
	END SUBROUTINE OBSOLETE1
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE ANNOUNCE_START
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MUMBO_DATA_M
!
	IMPLICIT NONE
!	
!	PRINT*, '      '
	PRINT*, '###############################################################################'
	PRINT*, '# STARTING WITH: ',MUMBO_PROC,MUMBO_PROC,MUMBO_PROC,MUMBO_PROC, &
     &	MUMBO_PROC,MUMBO_PROC
	PRINT*, '# '
	PRINT*, '     '
!
        CALL CPU_TIME(proc_start)
!	
	END SUBROUTINE ANNOUNCE_START
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE ANNOUNCE_END
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MUMBO_DATA_M
!
	IMPLICIT NONE
!
        CALL CPU_TIME(proc_end)
!
        PRINT*, '      '
        PRINT*, '#      '
	PRINT*, '# ELAPSED TIME IN ', MUMBO_PROC,' : ', proc_end - proc_start, ' SECONDS'
        PRINT*, '#      '
	PRINT*, '# FINISHED WITH: ',MUMBO_PROC,MUMBO_PROC,MUMBO_PROC,MUMBO_PROC, &
     &	MUMBO_PROC,MUMBO_PROC
	PRINT*, '###############################################################################'
!	PRINT*, '     '
!
	END SUBROUTINE ANNOUNCE_END
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	FUNCTION ECHO(flag)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	IMPLICIT NONE
	CHARACTER(LEN=10) :: echo
	LOGICAL :: flag
!
	if (flag) then
	    echo = ' - YES -  '
	else
	    echo = '    no    '
	end if
!	
	END FUNCTION ECHO
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc








