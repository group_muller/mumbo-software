!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE FILL_IN_MISSING_DATA ()
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MOLEC_M
	USE AA_DATA_M
	USE MUMBO_DATA_M
	USE MAX_DATA_M
!
	IMPLICIT NONE
!
	CHARACTER (LEN=4)  :: atnam, aanam, aatyp
!
	INTEGER :: I,J,K,L,M,O,P
	INTEGER :: NQ
	LOGICAL :: flag
!
	real, dimension(3) :: xyz
	real :: rho
!
!	GFORTRAN
	nq=0
!	GFORTRAN
!
!
!	FILL IN ATOM_CHARGE, ATOM_TYPE, ATOM_NUMBER etc .....
!	FROM PARAMETER FILE
!	ALSO SET ROTAMER_ENERGY, ROTAMER_FLAG
!	ALSO SET PAIR_FLAG
!
	do i=1,size(mol)
	  do j=1,mol(i)%mol_nrs
	    do k=1,mol(i)%res(j)%res_naa
		aanam=mol(i)%res(j)%res_aas(k)%aa_nam
!
		do l=1, size(ps)
		      if (ps(l)%p_aan==aanam) then
			  nq=l
		      end if
	        end do
!
	     	do l=1, mol(i)%res(j)%res_aas(k)%aa_nrt
!
		   mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_flag=.true.
!
			do p=1,(size(mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_en))
		   mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_en(p)=0.0
			end do
!
		   do m=1, mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_nat
!
	atnam = mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_nam
!
			flag=.true.
			do o=1,(ps(nq)%p_at_ncnt)
			    if (atnam.eq.ps(nq)%p_at_nam(o)) then
!
	mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_num=ps(nq)%p_at_num(o)
	mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_ntyp=ps(nq)%p_at_ntyp(o)
	mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_typ=ps(nq)%p_at_typ(o)
	mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_cha=ps(nq)%p_at_charge(o)
	mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_noz=ps(nq)%p_at_noz(o)
	mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_ab=ps(nq)%p_at_ab(o)
	mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_hb=ps(nq)%p_at_hb(o)
!
	flag=.false.
	exit
			    end if
			end do
!
			if (flag) then
!
	PRINT*, '                   >>>>>> BUMMER >>>>>>'
	PRINT*, '  FAILED TO FIND PARAMETERS FOR ATOM:  ' 
	PRINT*, atnam,'  IN RESIDUE: ',aanam
	PRINT*, i, j, k, l ,m
	PRINT*, '                   >>>>>> BUMMER >>>>>>'
			    STOP 
			end if
!
                        flag=.true.
                        do o=1,size(nbo)
!
	aatyp=mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_typ
!		  
                           if (aatyp==(nbo(o)%n_at_typ)) then
	mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_eps=nbo(o)%n_at_p1
	mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_sig=nbo(o)%n_at_p2
	mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_svo=nbo(o)%n_at_p3
	mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_sla=nbo(o)%n_at_p4
 	mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_sgr=nbo(o)%n_at_p5
	mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_sgf=nbo(o)%n_at_p6
                               flag=.false.
                            end if
                        end do
!
                        if (flag) then
        PRINT*, '                   >>>>>>> BUMMER >>>>>>>>>> '
        PRINT*, ' ATOM TYPE NOT IDENTIFIED IN LIBRARY IN ENERGIZE '
        PRINT*, aatyp
        PRINT*, i, j, k, l ,m
        PRINT*, ' 		    >>>>>>> BUMMER >>>>>>>>>> '
                          STOP
                        end if
!
		  end do
	       end do
!
	    end do
	  end do
	end do
!
!
	PRINT*, '        '
	PRINT*, '################################'
	PRINT*, '# SUMMARY FILL_IN_MISSING_DATA #'
	PRINT*, '################################'
	PRINT*, '        '
	PRINT*, '  ... FINISHED FILLING IN MISSING DATA       '
	PRINT*, '  ... FINISHED FILLING IN ATOM PARAMETERS       '
!	PRINT*, '  ... FINISHED READING RESIDUE CONNECTIVITY  '
!
	if (xray_flag) then
	
	do i=2,size(mol)
	  do j=1,mol(i)%mol_nrs
	    do k=1,mol(i)%res(j)%res_naa
               do l=1,mol(i)%res(j)%res_aas(k)%aa_nrt
		  do m=1, mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_nat
!
	xyz(1)= mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_xyz(1)
	xyz(2)= mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_xyz(2)
	xyz(3)= mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_xyz(3)
!
	call GET_ATOM_DENS(XYZ(1),XYZ(2),XYZ(3),RHO)
	
	mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_ede = rho
!
		  end do
	       end do
	    end do
	  end do
	end do
!
	PRINT*, '  ... FINISHED FILLING IN ELECTRON DENSITY       '
	PRINT*, '        '
!
	end if
!
!DEBUG	
!	do i=1,size(mol)
!	  do j=1,mol(i)%mol_nrs
!	    do k=1,mol(i)%res(j)%res_naa
!!	       if (mol(i)%res(j)%res_aas(k)%aa_nam=='ALA ') then
!              do l=1,mol(i)%res(j)%res_aas(k)%aa_nrt
!
!		   call	dump(i,j,k,l,1)
!
!		end do
!!	       end if
!	    end do
!	  end do
!	end do
!
!	TESTING PROPER READING BELOW
!
!	do i=1,size(nbo)
!	 print*,nbo(i)%n_at_typ,nbo(i)%n_at_ntyp
!	 print*,nbo(i)%n_at_p1,nbo(i)%n_at_p2
!	 print*,nbo(i)%n_at_p3,nbo(i)%n_at_p4
!	 print*,nbo(i)%n_at_p5,nbo(i)%n_at_p6
!	end  do
!
!	do k=1, size(cs)
!
!	print*, cs(k)%c_aan, cs(k)%c_num
!
!	do i=1,size(cs(k)%c_nam)
!	      do j=1,size(cs(k)%c_nam)
!	         print*, cs(k)%c_nam(i), cs(k)%c_at(i,j)
!	      end do
!	end do
!	end do
!DEBUG
!
	CALL CONNEX_ATS
!
	PRINT*, '  ... FINISHED CONNECTING ALL ATOMS     '
	PRINT*, '        '
!
!
	END SUBROUTINE FILL_IN_MISSING_DATA
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE CONNEX_ATS
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MOLEC_M
	USE AA_DATA_M
	USE MUMBO_DATA_M
	USE MAX_DATA_M
!
	IMPLICIT NONE
!
	integer, dimension(:,:), allocatable :: conn12
	integer, dimension(:,:), allocatable :: tp, tq 
	integer :: nconn
	integer :: max_conn
!
	integer :: i,j,k,l,m,n,o,p,q,r
	integer :: n1, n2, n3, n4, n5, n6, n7, n9, naa
	integer :: n21, n22, n23, n24, n31, n32, n33, n34, n41, n42, n43, n44
	integer :: n11, n12, n13, n14, nall, ncnt, nq, nfx
	integer :: n51, n52, n53, n54, n61, n62, n63, n64, n55, n65
	logical :: flag
	real, dimension(3) :: yat1, yat2
	real :: dx,dy,dz,dr
	character(len=4) :: aa_nam, at_nam, at_nom, aa_nom
	character(len=1) :: c1, cfx
!
!	GFORTRAN (initialisation of parameters that might never be initialised else)
	naa=-1000
	n3=-1000
	n2=-1000
!	GFORTRAN
!
!	allocate a lot of space because this is needed for 
!	ca atoms of positions to be mumboed connected to all CB of residues at those
!	positions
! 
	max_conn = max_conn_12 + max_resid_type
	if (allocated(conn12)) deallocate(conn12)
	allocate (conn12(max_conn,4))
!
	do i=1,mol_nmls
!
        do j=1,mol(i)%mol_nrs
            do k=1,mol(i)%res(j)%res_naa
		  aa_nam = mol(i)%res(j)%res_aas(k)%aa_nam
!
!	looking up connectivity for aa being considered
!
 		   flag=.true. 
		   do l=1,size(cs)
			if (aa_nam == cs(l)%c_aan) then
			  n1= l 
			  naa = n1
			  flag=.false.
			end if
		   end do
	  	   if (flag) then
	     	     PRINT *, ' >>>>>>>>>>>>>>>>>>>>>>>> BUMMER '
	     	     PRINT *, '    NO CONNECTIVITY FOUND IN CONNEX_ATS'
	     	     PRINT *, '    FOR ', aa_nam
	     	     PRINT *, '    SORRY MUST EXIT-1'
	     	     PRINT *, ' >>>>>>>>>>>>>>>>>>>>>>>> BUMMER '
	     	     STOP
	  	   end if
!
               l=1
!
!	starting to gather atom connectivity
!
              do m=1, mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_nat
		        nconn = 0
			at_nam= mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_nam
		        aa_nam = mol(i)%res(j)%res_aas(k)%aa_nam
			n1 = naa
!
			flag=.true.
		      do n=1,cs(n1)%c_num
			  if (at_nam == cs(n1)%c_nam(n)) then
			     n2 = n
			     flag=.false.
			  end if
			end do
	  		if (flag) then
	     			PRINT *, ' >>>>>>>>>>>>>>>>>>>>>>>> BUMMER '
	     			PRINT *, '    NO CONNECTIVITY FOUND IN CONNEX_ATS'
	     			PRINT *, '    FOR:', aa_nam, at_nam
	     			PRINT *, '    AT POS: ',i,j
	     			PRINT *, '    SORRY MUST EXIT-2              '
	     			PRINT *, ' >>>>>>>>>>>>>>>>>>>>>>>> BUMMER '
	     			STOP
	  		end if
!
!	now look up atoms present in the same residue
!
			do n=1,mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_nat
			  at_nom = mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(n)%at_nam
!
			  n4=i
			  n5=j
			  n6=k
			  n7=n
!
			  flag=.true.
		        do o=1,cs(n1)%c_num
			    if (at_nom == cs(n1)%c_nam(o)) then
			      n3 = o
			      flag=.false.
			    end if
			  end do
	  		  if (flag) then
	     			PRINT *, ' >>>>>>>>>>>>>>>>>>>>>>>> BUMMER '
	     			PRINT *, '    NO CONNECTIVITY FOUND IN CONNEX_ATS'
	     			PRINT *, '    FOR:', aa_nam, at_nam
	     			PRINT *, '    AT POS: ',i,j
	     			PRINT *, '    SORRY MUST EXIT-3              '
	     			PRINT *, ' >>>>>>>>>>>>>>>>>>>>>>>> BUMMER '
	     			STOP
	  		  end if
!
!	now check if atoms are connected
!
			  if (n2/=n3 .and. cs(n1)%c_at(n2,n3)) then 
			    nconn = nconn +1
			    if (nconn > max_conn) then
	     			PRINT *, ' >>>>>>>>>>>>>>>>>>>>>>>> BUMMER '
	     			PRINT *, '    CONNECTIVITY LINKS EXCEEDING '
	     			PRINT *, '    MAX_CONN                     '
	     			PRINT *, '    ',nconn, max_conn
	     			PRINT *, '    SORRY MUST EXIT-4              '
	     			PRINT *, ' >>>>>>>>>>>>>>>>>>>>>>>> BUMMER '
	     			STOP
			    end if 
!
			    conn12(nconn,1)=n4
			    conn12(nconn,2)=n5
			    conn12(nconn,3)=n6
			    conn12(nconn,4)=n7
			  end if
!
!	done with looking up atoms in the same residue
!
			end do
!
!	in case of i == 1 still check whether residue is connected to 
!	residues preceding or residues after
!
			if (i==1 .and. at_nam == n_name) then	
			  if (j>1) then 
				do n=1, mol(1)%res(j-1)%res_aas(1)%aa_rots(1)%rot_nat 
				at_nom = mol(1)%res(j-1)%res_aas(1)%aa_rots(1)%rot_ats(n)%at_nam
				  if (at_nom == c_name) then
!
		yat1(1)=mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(1)
		yat1(2)=mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(2)
		yat1(3)=mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(3)
!
		yat2(1)=mol(1)%res(j-1)%res_aas(1)%aa_rots(1)%rot_ats(n)%at_xyz(1)
		yat2(2)=mol(1)%res(j-1)%res_aas(1)%aa_rots(1)%rot_ats(n)%at_xyz(2)
		yat2(3)=mol(1)%res(j-1)%res_aas(1)%aa_rots(1)%rot_ats(n)%at_xyz(3)
!
				    dx=yat1(1)-yat2(1)
				    dy=yat1(2)-yat2(2)
				    dz=yat1(3)-yat2(3)

				    dr=(dx**2)+(dy**2)+(dz**2)
				    dr=sqrt(dr)
!
				    if(dr < ch_break_dis) then
					nconn=nconn+1
					if (nconn > max_conn) then
					  PRINT *, ' >>>>>>>>>>>>>>>>>>>>>>>> BUMMER '
					  PRINT *, '    CONNECTIVITY LINKS EXCEEDING '
					  PRINT *, '    MAX_CONN                     '
					  PRINT *, '    ',nconn, max_conn
					  PRINT *, '    SORRY MUST EXIT-5            '
					  PRINT *, ' >>>>>>>>>>>>>>>>>>>>>>>> BUMMER '
					  STOP
					end if 
!
					conn12(nconn,1)=i
					conn12(nconn,2)=j-1
					conn12(nconn,3)=k
					conn12(nconn,4)=n
!
				    end if
				  end if
				end do
			  end if
			end if
!	
!     For i = 1, look up n_name in the residue that follows
!
			if (i==1 .and. at_nam == c_name) then	
			  if (j < mol(i)%mol_nrs) then 
!
				do n=1,mol(1)%res(j+1)%res_aas(1)%aa_rots(1)%rot_nat 
				at_nom=mol(1)%res(j+1)%res_aas(1)%aa_rots(1)%rot_ats(n)%at_nam
!
				  if (at_nom == n_name) then
!
		yat1(1)=mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(1)
		yat1(2)=mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(2)
		yat1(3)=mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(3)
!
		yat2(1)=mol(1)%res(j+1)%res_aas(1)%aa_rots(1)%rot_ats(n)%at_xyz(1)
		yat2(2)=mol(1)%res(j+1)%res_aas(1)%aa_rots(1)%rot_ats(n)%at_xyz(2)
		yat2(3)=mol(1)%res(j+1)%res_aas(1)%aa_rots(1)%rot_ats(n)%at_xyz(3)
!
				    dx=yat1(1)-yat2(1)
				    dy=yat1(2)-yat2(2)
				    dz=yat1(3)-yat2(3)

				    dr=(dx**2)+(dy**2)+(dz**2)
				    dr=sqrt(dr)
!
				    if(dr < ch_break_dis) then
					nconn=nconn+1
					if (nconn > max_conn) then
					  PRINT *, ' >>>>>>>>>>>>>>>>>>>>>>>> BUMMER '
					  PRINT *, '    CONNECTIVITY LINKS EXCEEDING '
					  PRINT *, '    MAX_CONN                     '
					  PRINT *, '    ',nconn, max_conn
					  PRINT *, '    SORRY MUST EXIT-6            '
					  PRINT *, ' >>>>>>>>>>>>>>>>>>>>>>>> BUMMER '
					  STOP
					end if 
!
					conn12(nconn,1)=i
					conn12(nconn,2)=j+1
					conn12(nconn,3)=k
					conn12(nconn,4)=n
!
				    end if
				  end if
				end do
			  end if
			end if
!	
!     now find out where the rotamers for this position are 
!     stored and connect all needed connections 
!     BE AWARE THAT N IS CONNECTED TO PRO CD 
!     ALL POSSIBLE CONNECTIONS SHOULD BE SEARCHED
!     possibly nconn becomes nconn + a lot ...
!
		   if (at_nam /= h_name) then 
			if(i==1 .and. mol(1)%res(j)%res_mut) then
			  nfx= mol(1)%res(j)%res_nold
			  cfx= mol(1)%res(j)%res_chid
			  do n=2,mol_nmls
		     if (nfx==mol(n)%res(1)%res_nold.and.cfx==mol(n)%res(1)%res_chid) then
!
!		     print*, 'Qualifier found '  
!		     print*,  n, nfx, cfx, mol(n)%res(1)%res_nold, mol(n)%res(1)%res_chid
!		     print*,  mol(1)%res(j)%res_nold,    mol(1)%res(j)%res_chid
!
				do o=1,mol(n)%res(1)%res_naa
				  aa_nom = mol(n)%res(1)%res_aas(o)%aa_nam
				  flag=.true. 
				  do p=1,size(cs)
				    if (aa_nom == cs(p)%c_aan) then
					n1=p 
					flag=.false.
				    end if
				  end do
!
			  	  if (flag) then
	     			    PRINT *, ' >>>>>>>>>>>>>>>>>>>>>>>> BUMMER '
	     			    PRINT *, '    NO CONNECTIVITY FOUND IN CONNEX_ATS'
	     			    PRINT *, '    FOR ', aa_nom
	     			    PRINT *, '    SORRY MUST EXIT-7            '
	   		  	    PRINT *, ' >>>>>>>>>>>>>>>>>>>>>>>> BUMMER '
	   		  	    STOP
	 		 	  end if
!
				  flag=.true.
		         	  do p=1,cs(n1)%c_num
			 	    if (at_nam == cs(n1)%c_nam(p)) then
			              n2=p
			              flag=.false.
			          end if
			         end do
!
	  			  if (flag) then
	     			    PRINT *, ' >>>>>>>>>>>>>>>>>>>>>>>> BUMMER '
	     			    PRINT *, '    NO CONNECTIVITY FOUND IN CONNEX_ATS'
	     			    PRINT *, '    FOR:', aa_nom, at_nam
	     			    PRINT *, '    AT POS: ',i, j
	     			    PRINT *, '    SORRY MUST EXIT-8            '
	     			    PRINT *, ' >>>>>>>>>>>>>>>>>>>>>>>> BUMMER '
	     			    STOP
	  		        end if
!
!	NOW SEARCH FOR CORRESPONDING ROTAMER ATOMS
!
				do q=1,mol(n)%res(1)%res_aas(o)%aa_rots(1)%rot_nat 
				  at_nom = mol(n)%res(1)%res_aas(o)%aa_rots(1)%rot_ats(q)%at_nam
				  flag=.true.
		         	  do p=1,cs(n1)%c_num
			 	    if (at_nom == cs(n1)%c_nam(p)) then
			              n3 = p
			              flag=.false.
			          end if
			        end do
	  			  if (flag) then
	     			    PRINT *, ' >>>>>>>>>>>>>>>>>>>>>>>> BUMMER '
	     			    PRINT *, '    NO CONNECTIVITY FOUND IN CONNEX_ATS'
	     			    PRINT *, '    FOR:', aa_nom, at_nom
	     			    PRINT *, '    AT POS: ',i,j
	     			    PRINT *, '    SORRY MUST EXIT-9            '
	     			    PRINT *, ' >>>>>>>>>>>>>>>>>>>>>>>> BUMMER '
	     			    STOP
	  		          end if
!
				  if (n2/=n3 .and. cs(n1)%c_at(n2,n3)) then 
			   	    nconn = nconn +1
			            if (nconn > max_conn) then
	     				PRINT *, ' >>>>>>>>>>>>>>>>>>>>>>>> BUMMER '
	     				PRINT *, '    CONNECTIVITY LINKS EXCEEDING '
	     				PRINT *, '    MAX_CONN                     '
	     				PRINT *, '    ',nconn, max_conn
	     				PRINT *, '    SORRY MUST EXIT-10           '
	     				PRINT *, ' >>>>>>>>>>>>>>>>>>>>>>>> BUMMER '
	     				STOP
			    	    end if 
!
			    	    conn12(nconn,1)=n
			    	    conn12(nconn,2)=1
			    	    conn12(nconn,3)=o
			    	    conn12(nconn,4)=q
			  	  end if
!
				  end do
!
				  end do
				end if
			    end do
			end if
		    end if
!
!	Now look up main chain atoms in case i > 1 and connect all residue 
!	atoms to the main chain, namely CB and CD(in case of Pro)
!
!
		if (i/=1) then 
!
		          n9= mol(i)%res(1)%res_nold
			  c1= mol(i)%res(1)%res_chid
			  do n=1, mol(1)%mol_nrs
			   if (n9==mol(1)%res(n)%res_nold .and. c1==mol(1)%res(n)%res_chid) then
				  flag=.true. 
				  do o=1,size(cs)
				    if (aa_nam == cs(o)%c_aan) then
					n1=o 
					flag=.false.
				    end if
				  end do
!
			  	  if (flag) then
	     			    PRINT *, ' >>>>>>>>>>>>>>>>>>>>>>>> BUMMER '
	     			    PRINT *, '    NO CONNECTIVITY FOUND IN CONNEX_ATS'
	     			    PRINT *, '    FOR ', aa_nam
	     			    PRINT *, '    SORRY MUST EXIT-12'
	   		  	    PRINT *, ' >>>>>>>>>>>>>>>>>>>>>>>> BUMMER '
	   		  	    STOP
	 		 	  end if
!
				  flag=.true.
		         	  do o=1,cs(n1)%c_num
			 	    if (at_nam == cs(n1)%c_nam(o)) then
			              n2=o
			              flag=.false.
			            end if
			          end do
!
	  			  if (flag) then
	     			    PRINT *, ' >>>>>>>>>>>>>>>>>>>>>>>> BUMMER '
	     			    PRINT *, '    NO CONNECTIVITY FOUND IN CONNEX_ATS'
	     			    PRINT *, '    FOR:', aa_nam, at_nam
	     			    PRINT *, '    AT POS: ',i, j
	     			    PRINT *, '    SORRY MUST EXIT-13           '
	     			    PRINT *, ' >>>>>>>>>>>>>>>>>>>>>>>> BUMMER '
	     			    STOP
	  		          end if
!
!	NOW SEARCH FOR CORRESPONDING ROTAMER ATOMS
!
				do o=1,mol(1)%res(n)%res_aas(1)%aa_rots(1)%rot_nat 
				 at_nom = mol(1)%res(n)%res_aas(1)%aa_rots(1)%rot_ats(o)%at_nam
				 aa_nom = mol(1)%res(n)%res_aas(1)%aa_nam
				 if (at_nom /= h_name) then 
				  flag=.true.
		         	  do p=1,cs(n1)%c_num
			 	    if (at_nom == cs(n1)%c_nam(p)) then
			              n3 = p
			              flag=.false.
			          end if
			         end do
	  			  if (flag) then
	     			    PRINT *, ' >>>>>>>>>>>>>>>>>>>>>>>> BUMMER '
	     			    PRINT *, '    NO CONNECTIVITY FOUND IN CONNEX_ATS'
	     			    PRINT *, '    FOR:', aa_nom, at_nom
	     			    PRINT *, '    AT POS: ',i,j
	     			    PRINT *, '    SORRY MUST EXIT-14           '
	     			    PRINT *, ' >>>>>>>>>>>>>>>>>>>>>>>> BUMMER '
	     			    STOP
	  		          end if
!
				    if (n2/=n3 .and. cs(n1)%c_at(n2,n3)) then 
			   	      nconn = nconn +1
			            if (nconn > max_conn) then
	     				 PRINT *, ' >>>>>>>>>>>>>>>>>>>>>>>> BUMMER '
	     				 PRINT *, '    CONNECTIVITY LINKS EXCEEDING '
	     				 PRINT *, '    MAX_CONN                     '
	     				 PRINT *, '    ',nconn, max_conn
	     				 PRINT *, '    SORRY MUST EXIT-15            '
	     				 PRINT *, ' >>>>>>>>>>>>>>>>>>>>>>>> BUMMER '
	     				 STOP
			    	    end if 
!
			    	    conn12(nconn,1)=1
			    	    conn12(nconn,2)=n
			    	    conn12(nconn,3)=1
			    	    conn12(nconn,4)=o
			  	   end if
!
				  end if
				  end do
!
				end if
			    end do
			end if
!
! DONE LOOKING UP ALL POSSIBLE CONNECTIVITIES
!
		mol(i)%res(j)%res_aas(k)%aa_rots(1)%rot_ats(m)%at_n12 = nconn
		allocate (mol(i)%res(j)%res_aas(k)%aa_rots(1)%rot_ats(m)%at_c12(nconn,5))
!
			do n=1,nconn
		mol(i)%res(j)%res_aas(k)%aa_rots(1)%rot_ats(m)%at_c12(n,1)=conn12(n,1) 
		mol(i)%res(j)%res_aas(k)%aa_rots(1)%rot_ats(m)%at_c12(n,2)=conn12(n,2) 
		mol(i)%res(j)%res_aas(k)%aa_rots(1)%rot_ats(m)%at_c12(n,3)=conn12(n,3) 
		mol(i)%res(j)%res_aas(k)%aa_rots(1)%rot_ats(m)%at_c12(n,4)=conn12(n,4) 
		mol(i)%res(j)%res_aas(k)%aa_rots(1)%rot_ats(m)%at_c12(n,5)=1 
!
			end do
!
		end do ! DONE ALL ATOMS IN AMINO ACID
	     end do ! DONE ALL AMINO ACIDS AT A GIVEN POSITION
	   end do ! DONE WITH ALL RESIDUES IN MOLECULE
	end do ! DONE WITH ALL MOLECULES.....
!
!
! DEBUG21
!	do i=1,mol_nmls
!	do j=1,mol(i)%mol_nrs
!	do k=1,mol(i)%res(j)%res_naa
!!	do l=1,mol(i)%res(j)%res_aas(k)%aa_nrt
!	l=1
!	do m=1,mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_nat
!
!	PRINT*,'DUMPING CONNECTIVITIES',mol(i)%res(j)%res_aas(k)%aa_nam
!	PRINT*, mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_nam,i,j,k,l,m
!	PRINT*, mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_n12
!	do n=1,mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_n12
!	PRINT*,(mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_c12(n,o), o=1,4)
!	end do
!	PRINT*,'    '
!!
!	end do
!!	end do
!	end do
!	end do 
!	end do
! DEBUG21
!
!
!     Now sum up 12, 13, 14 conectivities
!
	do i=1,mol_nmls
	do j=1,mol(i)%mol_nrs
	do k=1,mol(i)%res(j)%res_naa
	l=1
	do m=1,mol(i)%res(j)%res_aas(k)%aa_rots(1)%rot_nat
!
	n11=i
	n12=j
	n13=k
	n14=m
!
!	Now counting all possible 1-2, 1-3, 1-4 connectivities 
!
	nall = 0
	do n=1,  mol(n11)%res(n12)%res_aas(n13)%aa_rots(1)%rot_ats(n14)%at_n12 
	 n21   = mol(n11)%res(n12)%res_aas(n13)%aa_rots(1)%rot_ats(n14)%at_c12(n,1)
	 n22   = mol(n11)%res(n12)%res_aas(n13)%aa_rots(1)%rot_ats(n14)%at_c12(n,2)
	 n23   = mol(n11)%res(n12)%res_aas(n13)%aa_rots(1)%rot_ats(n14)%at_c12(n,3)
	 n24   = mol(n11)%res(n12)%res_aas(n13)%aa_rots(1)%rot_ats(n14)%at_c12(n,4)
	 do o=1,  mol(n21)%res(n22)%res_aas(n23)%aa_rots(1)%rot_ats(n24)%at_n12 
	  n31   = mol(n21)%res(n22)%res_aas(n23)%aa_rots(1)%rot_ats(n24)%at_c12(o,1)
	  n32   = mol(n21)%res(n22)%res_aas(n23)%aa_rots(1)%rot_ats(n24)%at_c12(o,2)
	  n33   = mol(n21)%res(n22)%res_aas(n23)%aa_rots(1)%rot_ats(n24)%at_c12(o,3)
	  n34   = mol(n21)%res(n22)%res_aas(n23)%aa_rots(1)%rot_ats(n24)%at_c12(o,4)
	  nall =  nall + mol(n31)%res(n32)%res_aas(n33)%aa_rots(1)%rot_ats(n34)%at_n12
	 end do
	 nall = nall + mol(n21)%res(n22)%res_aas(n23)%aa_rots(1)%rot_ats(n24)%at_n12 
	end do
	nall = nall + mol(n11)%res(n12)%res_aas(n13)%aa_rots(1)%rot_ats(n14)%at_n12	
!
	if (allocated(tp)) deallocate(tp)
	allocate(tp(nall,5))
!
	do n=1, nall
	  do o=1,4
	    tp(n,o)= 0
	  end do
	    tp(n,5)= 5
	end do
!
!	Now gathering all 1-2,1-3,1-4 connectivities and avoiding redundancies
!	and marking the corresponding connectivities with 2,3 or 4
!
	ncnt=0
!
	do n=1,mol(n11)%res(n12)%res_aas(n13)%aa_rots(1)%rot_ats(n14)%at_n12 
	 n21   = mol(n11)%res(n12)%res_aas(n13)%aa_rots(1)%rot_ats(n14)%at_c12(n,1)
	 n22   = mol(n11)%res(n12)%res_aas(n13)%aa_rots(1)%rot_ats(n14)%at_c12(n,2)
	 n23   = mol(n11)%res(n12)%res_aas(n13)%aa_rots(1)%rot_ats(n14)%at_c12(n,3)
	 n24   = mol(n11)%res(n12)%res_aas(n13)%aa_rots(1)%rot_ats(n14)%at_c12(n,4)
	 do o=1, mol(n21)%res(n22)%res_aas(n23)%aa_rots(1)%rot_ats(n24)%at_n12 
	  n31   = mol(n21)%res(n22)%res_aas(n23)%aa_rots(1)%rot_ats(n24)%at_c12(o,1)
	  n32   = mol(n21)%res(n22)%res_aas(n23)%aa_rots(1)%rot_ats(n24)%at_c12(o,2)
	  n33   = mol(n21)%res(n22)%res_aas(n23)%aa_rots(1)%rot_ats(n24)%at_c12(o,3)
	  n34   = mol(n21)%res(n22)%res_aas(n23)%aa_rots(1)%rot_ats(n24)%at_c12(o,4)
	  do p=1, mol(n31)%res(n32)%res_aas(n33)%aa_rots(1)%rot_ats(n34)%at_n12 
	   n41   = mol(n31)%res(n32)%res_aas(n33)%aa_rots(1)%rot_ats(n34)%at_c12(p,1)
	   n42   = mol(n31)%res(n32)%res_aas(n33)%aa_rots(1)%rot_ats(n34)%at_c12(p,2)
	   n43   = mol(n31)%res(n32)%res_aas(n33)%aa_rots(1)%rot_ats(n34)%at_c12(p,3)
	   n44   = mol(n31)%res(n32)%res_aas(n33)%aa_rots(1)%rot_ats(n34)%at_c12(p,4)
!
	flag=.true.
	do q=1,ncnt
	 if (tp(q,1)==n41 .and. tp(q,2)==n42 .and. tp(q,3)==n43 .and. tp(q,4)==n44) then
	   if (tp(q,5) > 4) then
	     tp(q,5)=4 
	   end if
	   flag=.false. 
	   exit	 
	 else  
          cycle 
	   flag=.true.
	 end if
	end do	
	if (flag) then 
	  ncnt=ncnt+1
	  tp(ncnt,1) = n41
	  tp(ncnt,2) = n42
	  tp(ncnt,3) = n43
	  tp(ncnt,4) = n44
	  tp(ncnt,5) = 4
	end if
!
	  end do 
!
	flag=.true.
	do q=1,ncnt
	 if (tp(q,1)==n31 .and. tp(q,2)==n32 .and. tp(q,3)==n33 .and. tp(q,4)==n34) then
	   if (tp(q,5) > 3) then
	     tp(q,5)=3 
	   end if
	   flag=.false. 
	   exit
	 else  
           cycle 
	   flag=.true.
	 end if
	end do	
	if (flag) then 
	  ncnt=ncnt+1
	  tp(ncnt,1) = n31
	  tp(ncnt,2) = n32
	  tp(ncnt,3) = n33
	  tp(ncnt,4) = n34
	  tp(ncnt,5) = 3
	end if
!
	 end do
!
	flag=.true.
	do q=1,ncnt
	 if (tp(q,1)==n21 .and. tp(q,2)==n22 .and. tp(q,3)==n23 .and. tp(q,4)==n24) then
	   if (tp(q,5) > 2) then
	     tp(q,5)=2 
	   end if
	   flag=.false. 
	   exit
	 else  
           cycle 
	   flag=.true.
	 end if
	end do	
	if (flag) then 
	  ncnt=ncnt+1
	  tp(ncnt,1) = n21
	  tp(ncnt,2) = n22
	  tp(ncnt,3) = n23
	  tp(ncnt,4) = n24
	  tp(ncnt,5) = 2
	end if
!
	end do
!
!	Some sorting here and removing 1-1 connections as well as connections
!	between aminoacids present at the same position
!
	nq=0
	do q=1, ncnt
	 if (tp(q,1)==i.and.tp(q,2)==j.and.tp(q,3)==k.and.tp(q,4)==m) then
	   cycle
	 else if (i/=1.and.tp(q,1)==i.and.tp(q,3)/=k) then
	   cycle
	 else
	   nq=nq+1
	 end if
	end do
!
	if (allocated(tq)) deallocate(tq)
	allocate(tq(nq,5))
!
	nq=0
	do q=1, ncnt
	 if (tp(q,1)==i.and.tp(q,2)==j.and.tp(q,3)==k.and.tp(q,4)==m) then
	   cycle
	 else if (i/=1.and.tp(q,1)==i.and.tp(q,3)/=k) then
	   cycle
	 else
	   nq=nq+1
	   tq(nq,1)=tp(q,1)
	   tq(nq,2)=tp(q,2)
	   tq(nq,3)=tp(q,3)
	   tq(nq,4)=tp(q,4)
	   tq(nq,5)=tp(q,5)
	 end if
	end do
!
	do q=1,nq
	  do r=1,nq-1
	    n51=tq(r,1)
	    n52=tq(r,2)
	    n53=tq(r,3)
	    n54=tq(r,4)
	    n55=tq(r,5)
!	     
	    n61=tq(r+1,1)
	    n62=tq(r+1,2)
	    n63=tq(r+1,3)
	    n64=tq(r+1,4)
	    n65=tq(r+1,5)
!
	    if (n61 > n51) then
		cycle
	    else if (n61 < n51) then
	    	tq(r,1) = n61
	    	tq(r,2) = n62
	    	tq(r,3) = n63
	    	tq(r,4) = n64
	    	tq(r,5) = n65
	    	tq(r+1,1) = n51
	    	tq(r+1,2) = n52
	    	tq(r+1,3) = n53
	    	tq(r+1,4) = n54
	    	tq(r+1,5) = n55
		cycle		
	    else if (n61==n51) then 
		if (n62 > n52) then
		    cycle
	        else if (n62 < n52) then
	    	    tq(r,1) = n61
	    	    tq(r,2) = n62
	    	    tq(r,3) = n63
	    	    tq(r,4) = n64
	          tq(r,5) = n65
	    	    tq(r+1,1) = n51
	    	    tq(r+1,2) = n52
	    	    tq(r+1,3) = n53
	    	    tq(r+1,4) = n54
		    tq(r+1,5) = n55
		    cycle		
	         else if (n62==n52) then 
		    if (n63 > n53) then
		       cycle
	            else if (n63 < n53) then
	    		tq(r,1) = n61
	    		tq(r,2) = n62	     
	    		tq(r,3) = n63	     
	    		tq(r,4) = n64
		        tq(r,5) = n65
			tq(r+1,1) = n51
			tq(r+1,2) = n52	     
			tq(r+1,3) = n53	     
			tq(r+1,4) = n54
		        tq(r+1,5) = n55
			cycle		
		   else if (n63==n53) then 
		     if (n64 >= n54) then
		       cycle
	             else 
	    		tq(r,1) = n61
	    		tq(r,2) = n62	     
	    		tq(r,3) = n63	     
	    		tq(r,4) = n64
		        tq(r,5) = n65
	    		tq(r+1,1) = n51
	    		tq(r+1,2) = n52	     
	    		tq(r+1,3) = n53	     
	    		tq(r+1,4) = n54
		        tq(r+1,5) = n55
			cycle		
		     end if
		   end if
		 end if
	    end if
!
	  end do 
	end do	
!
	mol(i)%res(j)%res_aas(k)%aa_rots(1)%rot_ats(m)%at_n14 = nq
	allocate (mol(i)%res(j)%res_aas(k)%aa_rots(1)%rot_ats(m)%at_c14(nq,5))
	do q=1,nq
		mol(i)%res(j)%res_aas(k)%aa_rots(1)%rot_ats(m)%at_c14(q,1)=tq(q,1) 
		mol(i)%res(j)%res_aas(k)%aa_rots(1)%rot_ats(m)%at_c14(q,2)=tq(q,2) 
		mol(i)%res(j)%res_aas(k)%aa_rots(1)%rot_ats(m)%at_c14(q,3)=tq(q,3) 
		mol(i)%res(j)%res_aas(k)%aa_rots(1)%rot_ats(m)%at_c14(q,4)=tq(q,4) 
		mol(i)%res(j)%res_aas(k)%aa_rots(1)%rot_ats(m)%at_c14(q,5)=tq(q,5) 
	end do
!
	end do   ! Done with atoms in rotamer 
	end do   ! Done with amino acids
	end do   ! Done with all residues
	end do   ! Done with all molecules
!
!	Now clean up all 12 connections
!
	do i=1, mol_nmls
	  do j=1, mol(i)%mol_nrs
	    do k=1,mol(i)%res(j)%res_naa
	      do m=1,mol(i)%res(j)%res_aas(k)%aa_rots(1)%rot_nat
!
	mol(i)%res(j)%res_aas(k)%aa_rots(1)%rot_ats(m)%at_n12 = 0
!	if (allocated(mol(i)%res(j)%res_aas(k)%aa_rots(1)%rot_ats(m)%at_c12)) then
	  deallocate(mol(i)%res(j)%res_aas(k)%aa_rots(1)%rot_ats(m)%at_c12)
!	end if
!
	   end do
	  end do
	 end do 
	end do
!
!!
! DEBUG22
!	do i=1, mol_nmls
!      do j=1, mol(i)%mol_nrs
!      do k=1,mol(i)%res(j)%res_naa
!!	do l=1,mol(i)%res(j)%res_aas(k)%aa_nrt
!	l=1
!	do m=1,mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_nat
!
!	PRINT*,'DUMPING ALL 1-4 CONNECTIVITIES ',mol(i)%res(j)%res_aas(k)%aa_nam
!	PRINT*, mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_nam,i,j,k,l,m
!	PRINT*, mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_n14
!	do n=1,mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_n14
!	PRINT*,(mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_c14(n,o), o=1,5)
!	end do
!	PRINT*,'    '
!
!	end do
!!	end do
!	end do
!	end do
!	end do
! DEBUG22
!
	END SUBROUTINE CONNEX_ATS
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc


