!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	subroutine dump(nm,nr,na,nt,naa)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MOLEC_M
!
	IMPLICIT NONE
!
	INTEGER:: nm,nr,na,nt,naa,i
	LOGICAL:: flag
!
	call check(nm,nr,na,nt,naa,flag)
!
	if (flag) then 
!
!	do i=1,mol(nm)%res(nr)%res_aas(na)%aa_rots(nt)%rot_nat
	do i=1,nt
!
	print*, 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
	print*, 'nmol=',nm,' nres=',nr,' naa=',na,' nrot=',nt,' nat=',i
	print*, 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
	print*, 'RESNUM: ',mol(nm)%res(nr)%res_num
	print*, 'RESNAM: ',mol(nm)%res(nr)%res_aas(na)%aa_nam
	print*, 'PROB: ',mol(nm)%res(nr)%res_aas(na)%aa_rots(nt)%rot_prob
	print*,'AT_NAME,_NUM,_TYP,_NTYP,AT_OZ ,AT_CHARGE'
	print*, mol(nm)%res(nr)%res_aas(na)%aa_rots(nt)%rot_ats(i)%at_nam
	print*, mol(nm)%res(nr)%res_aas(na)%aa_rots(nt)%rot_ats(i)%at_num
	print*, mol(nm)%res(nr)%res_aas(na)%aa_rots(nt)%rot_ats(i)%at_typ
	print*, mol(nm)%res(nr)%res_aas(na)%aa_rots(nt)%rot_ats(i)%at_ntyp
	print*, mol(nm)%res(nr)%res_aas(na)%aa_rots(nt)%rot_ats(i)%at_noz
	print*, mol(nm)%res(nr)%res_aas(na)%aa_rots(nt)%rot_ats(i)%at_cha
	print*,'COORD '
	print*, mol(nm)%res(nr)%res_aas(na)%aa_rots(nt)%rot_ats(i)%at_xyz(1)
	print*, mol(nm)%res(nr)%res_aas(na)%aa_rots(nt)%rot_ats(i)%at_xyz(2)
	print*, mol(nm)%res(nr)%res_aas(na)%aa_rots(nt)%rot_ats(i)%at_xyz(3)
	print*, mol(nm)%res(nr)%res_aas(na)%aa_rots(nt)%rot_ats(i)%at_occ
	print*, mol(nm)%res(nr)%res_aas(na)%aa_rots(nt)%rot_ats(i)%at_bfa
	print*,'SOLVAT '
	print*, mol(nm)%res(nr)%res_aas(na)%aa_rots(nt)%rot_ats(i)%at_svo
	print*, mol(nm)%res(nr)%res_aas(na)%aa_rots(nt)%rot_ats(i)%at_sla
	print*, mol(nm)%res(nr)%res_aas(na)%aa_rots(nt)%rot_ats(i)%at_sgr
	print*, mol(nm)%res(nr)%res_aas(na)%aa_rots(nt)%rot_ats(i)%at_sgf
	print*,'EPS  SIG'
	print*, mol(nm)%res(nr)%res_aas(na)%aa_rots(nt)%rot_ats(i)%at_eps
	print*, mol(nm)%res(nr)%res_aas(na)%aa_rots(nt)%rot_ats(i)%at_sig

!
	end do
!
	else 
		print*,'REQUESTED ATOM DOES NOT EXIST'
	end if
!
	END SUBROUTINE DUMP
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	subroutine dump_pdb(nm,nr,na,nt)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MOLEC_M
!
	IMPLICIT NONE
!
	INTEGER:: nm,nr,na,nt,nnrs
	INTEGER:: i,j,k,l,m
	INTEGER, SAVE :: natn=0
	CHARACTER(LEN=4) :: name='    '
	real :: xboff
	CHARACTER(LEN=1) :: achid
!
	i=nm
	j=nr
	k=na
	l=nt
!
	if (i==1) then
	    nnrs=  mol(i)%res(j)%res_nold
	    achid= mol(i)%res(j)%res_chid
	else
	    nnrs=  mol(i)%res(j)%res_nold
	    achid= mol(i)%res(j)%res_chid
	end if
!
        do m=1, mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_nat
          natn=natn+1
          xboff = mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_prob
          name=mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_nam
!
          if (name(4:4)==' ') then
!
          write(*,fmt=2010)                                             &
     &  'ATOM  ',                                                       &
     &      natn,                                                       &
     &  mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_nam,          &
     &      mol(i)%res(j)%res_aas(k)%aa_nam,                            &
     &      achid,                                                      &
     &      nnrs,                                                       &
     &  mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_xyz(1),       &
     &  mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_xyz(2),       &
     &  mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_xyz(3),       &
     &  xboff,                                                          &
     &  mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_bfa
!
          write(15,fmt=2010)                                            &
     &  'ATOM  ',                                                       &
     &      natn,                                                       &
     &  mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_nam,          &
     &      mol(i)%res(j)%res_aas(k)%aa_nam,                            &
     &      achid,                                                      &
     &      nnrs,                                                       &
     &  mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_xyz(1),       &
     &  mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_xyz(2),       &
     &  mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_xyz(3),       &
     &  xboff,                                                          &
     &  mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_bfa
!
          else
!
          write(*,fmt=2011)                                             &
     &  'ATOM  ',                                                       &
     &      natn,                                                       &
     &  mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_nam,          &
     &      mol(i)%res(j)%res_aas(k)%aa_nam,                            &
     &      achid,                                                      &
     &      nnrs,                                                       &
     &  mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_xyz(1),       &
     &  mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_xyz(2),       &
     &  mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_xyz(3),       &
     &  xboff,                                                          &
     &  mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_bfa
!
          write(15,fmt=2011)                                            &
     &  'ATOM  ',                                                       &
     &      natn,                                                       &
     &  mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_nam,          &
     &      mol(i)%res(j)%res_aas(k)%aa_nam,                            &
     &      achid,                                                      &
     &      nnrs,                                                       &
     &  mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_xyz(1),       &
     &  mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_xyz(2),       &
     &  mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_xyz(3),       &
     &  xboff,                                                          &
     &  mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_bfa
!
          end if
!
                  end do
!
2010    format(a6,i5,2x,a4,a4,a,i4,4x,3f8.3,2f6.2)
2011    format(a6,i5,1x,a4,1x,a4,a,i4,4x,3f8.3,2f6.2)
!
	END SUBROUTINE DUMP_PDB
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	subroutine check(nm,nr,na,nt,naa,flag)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MOLEC_M
!
	IMPLICIT NONE
!
	INTEGER:: nm,nr,na,nt,naa
	INTEGER:: mm,mr,ma,mt,maa
	LOGICAL:: flag
!
	flag=.false.
	mm=size(mol)	
	if (mm >= nm) then
	   mr=mol(nm)%mol_nrs
	   if (mr >= nr) then
	       ma=mol(nm)%res(nr)%res_naa
	       if (ma >= na) then
	           mt=mol(nm)%res(nr)%res_aas(na)%aa_nrt
		   if (mt >= nt) then
		      maa=mol(nm)%res(nr)%res_aas(na)%aa_rots(nt)%rot_nat
		      if (maa >= naa) then
			flag=.true.
		      end if
		   end if
	       end if			
	   end if
	end if
! 
	END SUBROUTINE CHECK 
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE JUST_CHECKING()
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MOLEC_M
	USE EP_DATA_M
	USE AA_DATA_M
	USE MUMBO_DATA_M
	USE SU_DATA_M
	USE MAX_DATA_M
!
	IMPLICIT NONE
!
	INTEGER :: i,j,k,l,m
	INTEGER:: nm,nr,na,nt,naa
!
	PRINT*, '        '
	PRINT*, '################################'
	PRINT*, '# SUMMARY  JUST_CHEKING        #'
	PRINT*, '################################'
	PRINT*, '        '
!
	print*, '      NUMBER OF MOLECULES IN DATA STRUCTURE:'
	print*, '      ', (size(mol)-1)
!
	do i=1,size(mol)
!
	if (i/=1) then 
			PRINT *, '   '
			PRINT *,' RESIDUE= ', mol(i)%res(1)%res_nold,        &
     &    mol(i)%res(1)%res_chid,'  /  INTERNAL NUMB: ',mol(i)%res(1)%res_num       
			print *,                                              &
     &    '    DATA-MOLECULE  #RESIDUES  #AMINOACIDS_IN_RES',           &
     &    '          #ROTAMERS_IN_AA      #ATOMS_IN_ROT   ROT_PROB(FIRST ROTA.)'
			PRINT *, '   '
	end if
!
         do j=1,mol(i)%mol_nrs
            do k=1,mol(i)%res(j)%res_naa
!               do l=1, mol(i)%res(j)%res_aas(k)%aa_nrt
!                 do m=1, mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_nat
!
		if (i/=1) then 
!
                        print *,                                        &
     &                  (i-1),                                          &
     &                  mol(i)%mol_nrs,                                 &
     &                  mol(i)%res(j)%res_naa,                          &
     &                  mol(i)%res(j)%res_aas(k)%aa_nrt,                &
     &                  mol(i)%res(j)%res_aas(k)%aa_rots(1)%rot_nat,    &
     &                  mol(i)%res(j)%res_aas(k)%aa_rots(1)%rot_prob
!
		end if
!
!                 end do
!               end do
            end do
          end do
        end do
!
!
!	do i=1,100
!		print *,'    '
!		print *,' NMOL , NRES, NAAC, NROT , NAT ??'
!		read *,nm,nr,na,nt,naa
!		CALL DUMP(nm,nr,na,nt,naa)
!	end do
!
!	do i= 1, mol(1)%mol_nrs) 
!	print*, i, size(mol(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats)
!	end do
!
	PRINT*, '        '
	PRINT*, ' FINISHED JUST CHECKING '
	PRINT*, '        '
!
!
	END SUBROUTINE JUST_CHECKING
!