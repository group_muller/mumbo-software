!
!	PROGRAM MUMBO
!	MUMBO VERSION: March   2021 
!	
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!
	MODULE MOLEC_M
!
	TYPE ATOM_T
		CHARACTER (LEN=4) ::  at_nam    ! Atom_name
		CHARACTER (LEN=4) ::  at_typ    ! Atom_type
		CHARACTER (LEN=4) ::  at_ab     ! Atom_ab in hydrogen bonding
		CHARACTER (LEN=1) ::  at_hb     ! Atom_hydrogen_bond_type
		REAL, DIMENSION(3) :: at_xyz    ! Atom_coordinates
		REAL ::    at_bfa               ! Atom_bfactors
		REAL ::    at_occ       ! Atom_occupancy
		REAL ::    at_cha       ! Atom_charge
		REAL ::    at_sig       ! Atom_sig (for VDW)
		REAL ::    at_eps       ! Atom_eps (for VDW)
		REAL ::    at_svo       ! Atom_svo (for SOLV)
		REAL ::    at_sla       ! Atom_sla (for SOLV)
		REAL ::    at_sgr       ! Atom_sgr (for SOLV)
		REAL ::    at_sgf       ! Atom_sgf (for SOLV)
		REAL ::    at_ede       ! Atom_electron_density
		INTEGER :: at_noz       ! Atom_atomic_number
		INTEGER :: at_ntyp      ! Atom_typ_number
		INTEGER :: at_num       ! Atom_number
		INTEGER, DIMENSION(:,:), pointer :: at_c12, at_c14  ! At_conn 
		INTEGER :: at_n12, at_n14
		LOGICAL :: at_flag      ! Atom_flag
!
!		LOGICAL, DIMENSION(:,:,:),pointer :: at_su_at   !At_surfaces_Main_chain
!		LOGICAL, DIMENSION(:,:,:),pointer :: at_su_mc   !At_surfaces_Main_chain 	
!		LOGICAL, DIMENSION(:,:,:),pointer :: at_su_rt   !At_surfaces_Eigen
!		INTEGER, DIMENSION(3) :: at_su_st
!		INTEGER, DIMENSION(3) :: at_su_ex
!
	END TYPE ATOM_T
!
	TYPE DEE_PAIR_T
		INTEGER, DIMENSION(3) :: dee_rot
	END TYPE DEE_PAIR_T
!
	TYPE PAIR_LIST_T
          	TYPE (DEE_PAIR_T), DIMENSION(:), ALLOCATABLE :: pair_list
		INTEGER :: max_size=0  ! max number of list elements
		INTEGER :: tip=0       ! actual list-position
	END TYPE PAIR_LIST_T
!
	TYPE ROTAMER_T
		INTEGER :: rot_nat            ! Rotamer_number_of_atoms
		REAL ::    rot_prob           ! Rotamer_probability
		REAL ::    rot_qrot           ! Rotamer_backbone_backrub_rotation
		REAL, DIMENSION(15) :: rot_en ! Rotamer_energy
		LOGICAL :: rot_flag           ! Rotamer_flag 
		LOGICAL :: rot_flag2          ! Rotamer_flag 
!		                        ! Rotamer will be kept if flag=true
!		                        ! rot_flag2 = Rotamer of global ener min
		TYPE (ATOM_T), DIMENSION(:), pointer :: rot_ats
!		                        ! Rotamer_atoms_of_rotamer
                TYPE (PAIR_LIST_T) :: dee_friends   
		
	END TYPE ROTAMER_T
!
	TYPE AA_T
		INTEGER :: aa_nrt             ! Amino_acid_number_of rotamers
		CHARACTER (LEN=4) :: aa_nam   ! Amino_acid_name
		TYPE (ROTAMER_T), DIMENSION(:), pointer :: aa_rots
!		                              ! Amino_acid_rotamers	     
	END TYPE AA_T
!
	TYPE RESIDUE_T
		INTEGER :: res_num                ! Residue_number
		INTEGER :: res_nold               ! Residue_old_number
		CHARACTER (LEN=1) :: res_chid     ! Residue_chain_id
		LOGICAL :: res_mut                ! Residue_to_be_mutated=.true.
		LOGICAL :: res_lig                ! True if residue = ligand
		LOGICAL :: res_pps                ! True if phi,psi values can be calculated
		REAL, DIMENSION(2) :: res_phi_psi ! Phi, Psi values
		LOGICAL :: res_bbr                ! True if backrub motion can be performed, i.e. if residues -1 and +1 exist
!		LOGICAL :: res_wtr                ! True if potential water positions are being searched for in this residue
		INTEGER :: res_naa                ! Residue_number_of_amino_acids
!		
		TYPE (ATOM_T), DIMENSION(:),pointer :: res_atom_mc
!		                                  ! Residue_main_chain_atoms
		TYPE (AA_T), DIMENSION(:), pointer :: res_aas
!		                                  ! Residue_amino_acids
	END TYPE RESIDUE_T
!	
	TYPE MOLECULE_T
		INTEGER :: mol_nrs           ! Molecule_number_of_residues
		                             ! _in_molecule
		TYPE (RESIDUE_T), DIMENSION(:), pointer :: res
!		                             ! Molecule_residues
	END TYPE MOLECULE_T
!
	TYPE (MOLECULE_T), DIMENSION(:), allocatable :: mol     !Molecules
	TYPE (MOLECULE_T), DIMENSION(:), allocatable :: ori     !Comparison/Ref coordinates
!
	INTEGER ::    mol_nmls
!
	END MODULE MOLEC_M
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
!	ENERGY OF ROTAMER PAIRS
!
	MODULE EP_DATA_M
!
	TYPE EP_G_T
		REAL, DIMENSION(15)   :: ep_en     ! ep_energy
		LOGICAL               :: ep_flag   ! ep_flag
	        LOGICAL               :: pair_flag ! true if dee pair
		REAL                  :: pair_max  ! E(ir)+E(js)+E(ir,js)+
!                                                  !    sum(k){max(t)[E(ir,kt)+E(js,kt)]}
		REAL                  :: pair_min  ! E(ir)+E(js)+E(ir,js)+sum(k)
!                                                  !           {min(t)[E(ir,kt)+E(js,kt)]}
		REAL                  :: ep_min_split
	END TYPE EP_G_T
!
	TYPE EP_F_T
	     TYPE (EP_G_T), DIMENSION(:), pointer :: ef
	END TYPE EP_F_T
!
	TYPE EP_E_T
	     TYPE (EP_F_T), DIMENSION(:), pointer :: ee
	END TYPE EP_E_T
!
	TYPE EP_D_T
	     TYPE (EP_E_T), DIMENSION(:), pointer :: ed
	END TYPE EP_D_T
!
	TYPE EP_C_T
	     TYPE (EP_D_T), DIMENSION(:), pointer :: ec
	END TYPE EP_C_T
!
	TYPE EP_B_T
	     TYPE (EP_C_T), DIMENSION(:), pointer :: eb
	END TYPE EP_B_T
!
	TYPE (EP_B_T), DIMENSION(:), allocatable :: ea
!
	END MODULE EP_DATA_M
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!        
!       module MB_M stores the magic bullet pair at position ij
!       and the corresponding energy

	MODULE MB_M
	
	TYPE PAIR_T
	     REAL :: max_energy
	     INTEGER :: pair_aa1
	     INTEGER :: pair_rot1
	     INTEGER :: pair_aa2
	     INTEGER :: pair_rot2
        END TYPE PAIR_T
	
	     TYPE(PAIR_T), DIMENSION(:,:), ALLOCATABLE :: p_ij
	     	
	END MODULE MB_M
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
!       module SPLIT_M stores the energy minima, needed in subroutine
!       CONF_SPLIT
!
        MODULE SPLIT_M
	  
	TYPE SP_A_T
	  REAL :: minima
	END TYPE SP_A_T

	TYPE SP_B_T
	  TYPE (SP_A_T), DIMENSION(:), POINTER :: sf
	END TYPE SP_B_T
	
	TYPE SP_C_T
	  TYPE (SP_B_T), DIMENSION(:), POINTER :: se
	END TYPE SP_C_T
	
	TYPE SP_D_T
	  TYPE (SP_C_T), DIMENSION(:), POINTER :: sd
	END TYPE SP_D_T

	TYPE SP_E_T
	  TYPE (SP_D_T), DIMENSION(:), POINTER :: sc
	END TYPE SP_E_T
	
	TYPE SP_F_T
	  TYPE (SP_E_T), DIMENSION(:), POINTER :: sb
	END TYPE SP_F_T
	
	TYPE (SP_F_T), DIMENSION(:), ALLOCATABLE :: sa


	END MODULE SPLIT_M
!
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
!       module SPLIT_POS_M stores the split positions


        MODULE SPLIT_POS_M
        
        TYPE SPLIT_T
          INTEGER, DIMENSION(:), POINTER :: split
        END TYPE SPLIT_T
        
        TYPE RO_T
          TYPE(SPLIT_T), DIMENSION(:), POINTER :: ro
        END TYPE RO_T

        TYPE AAS_T
          TYPE(RO_T), DIMENSION(:), POINTER :: aa
        END TYPE AAS_T
        
        TYPE (AAS_T), DIMENSION(:), ALLOCATABLE :: pos

        END MODULE SPLIT_POS_M
!
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
!	OBERFLAECHEN VON RESTEN IM LICHTE ANDERER ROTAMERE, 
!	SOLLEN BEI DEN PAARWEISEN ENERGIEN BERECHNET WERDEN
!
!	(sa%sb%sc)%(sd%se%sf)%sg(i)%su_pa  ===
!	surface of atom i of rotamer sa%sb%sc im Lichte von Rotamer sd%se%sf	
!
!	(sa%sb%sc)%(sd%se%sf)%sg(i)%su_ac  ===
!	flag if surface atom i of rotamer sa%sb%sc is influenced by rotamer 
!	sd%se%sf	
!
!	SURFACE PAIRS
!
	MODULE SU_DATA_M
!
!	TYPE SU_H_T
!	      LOGICAL, DIMENSION(:,:,:), pointer	:: su_pa  !SURFACE PER ATOM PER ROTAMER
!	      LOGICAL	:: su_ac	! FLAG IF ATOM PRESENT OR NOT
!	END TYPE SU_H_T
!
!	TYPE SU_G_T
!	      TYPE (SU_H_T), DIMENSION(:), pointer :: sg
!	END TYPE SU_G_T
!
!	TYPE SU_F_T
!	     TYPE (SU_G_T), DIMENSION(:), pointer :: sf
!	END TYPE SU_F_T
!
!	TYPE SU_E_T
!	     TYPE (SU_F_T), DIMENSION(:), pointer :: se
!	END TYPE SU_E_T
!
!	TYPE SU_D_T
!	     TYPE (SU_E_T), DIMENSION(:), pointer :: sd
!	END TYPE SU_D_T
!
!	TYPE SU_C_T
!	     TYPE (SU_D_T), DIMENSION(:), pointer :: sc
!	END TYPE SU_C_T
!
!	TYPE SU_B_T
!	     TYPE (SU_C_T), DIMENSION(:), pointer :: sb
!	END TYPE SU_B_T
!
!	TYPE (SU_B_T), DIMENSION(:), allocatable :: sa
!
	END MODULE SU_DATA_M
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	MODULE AA_DATA_M
!
	TYPE BONDS_DATA_T
		CHARACTER (LEN=4), dimension(2)   ::  at_bod    ! Atom_bonded
		REAL  :: at_bol                                 ! Bond_length
	END TYPE BONDS_DATA_T
!
	TYPE ANGLE_DATA_T
		CHARACTER (LEN=4), dimension(3)   ::  at_ang    ! Atom_angled
		REAL  :: at_agl                                 ! Angle_length
	END TYPE ANGLE_DATA_T
!
	TYPE DIHE_DATA_T
		CHARACTER (LEN=4), DIMENSION(4) ::  at_dih     ! Atom_dihedraled
		REAL, DIMENSION(:), pointer     ::  at_dil     ! Dihedral_angle
		REAL, DIMENSION(:), pointer     ::  at_dpr     ! Dihed._probability
		INTEGER                         ::  at_nrt     ! Nr_of_rotamers
	END TYPE DIHE_DATA_T
!
	TYPE GROUP_DATA_T
		INTEGER    :: at_num                               ! Nr_atoms_group
		CHARACTER (LEN=4), dimension(3)          :: at_com ! Atom_common
		CHARACTER (LEN=4), dimension(:), pointer :: at_nam ! Atom_name
		REAL, DIMENSION (:), pointer :: at_co1             ! Atom_coord
		REAL, DIMENSION (:), pointer :: at_co2             ! Atom_coord
		REAL, DIMENSION (:), pointer :: at_co3             ! Atom_coord
	END TYPE GROUP_DATA_T
!
	TYPE GEOM_DATA_T
		CHARACTER(LEN=4) :: g_aan 
		INTEGER :: g_nbo, g_nan, g_ndh, g_ngr 
		TYPE (BONDS_DATA_T), dimension(:), pointer :: bo
		TYPE (ANGLE_DATA_T), dimension(:), pointer :: an
		TYPE (DIHE_DATA_T),  dimension(:), pointer :: dh
		TYPE (GROUP_DATA_T), dimension(:), pointer :: gr
	END TYPE GEOM_DATA_T
!
	TYPE (GEOM_DATA_T), DIMENSION(:), allocatable :: geo
!
	TYPE PARAM_DATA_T
		CHARACTER (LEN=4) :: p_aan                  ! Residue_name
		CHARACTER (LEN=4), dimension(:), pointer :: p_at_nam
		CHARACTER (LEN=4), dimension(:), pointer :: p_at_typ  
		CHARACTER (LEN=4), dimension(:), pointer :: p_at_ab 
		CHARACTER (LEN=1), dimension(:), pointer :: p_at_hb  
		REAL, dimension(:), pointer ::    p_at_charge
		INTEGER, dimension(:), pointer :: p_at_num
		INTEGER, dimension(:), pointer :: p_at_ntyp
		INTEGER, dimension(:), pointer :: p_at_noz
		INTEGER :: p_at_ncnt
	END TYPE PARAM_DATA_T
!
	TYPE (PARAM_DATA_T), DIMENSION(:), allocatable :: ps
!
	TYPE CONN_DATA_T
		CHARACTER (LEN=4) :: c_aan                ! Connec_residue_name
		INTEGER           :: c_num                ! Connec_num_atoms
		CHARACTER (LEN=4), dimension(:), pointer :: c_nam 
!		                                          ! Connec_name of atom
		LOGICAL, dimension(:,:), pointer :: c_at  ! Connec_atom_connec
	END TYPE CONN_DATA_T
!
	TYPE (CONN_DATA_T), DIMENSION(:), allocatable :: cs
!
	TYPE NB_DATA_T
		CHARACTER (LEN=4) :: n_at_typ  
		INTEGER :: n_at_ntyp  
		REAL  :: n_at_p1
		REAL  :: n_at_p2
		REAL  :: n_at_p3
		REAL  :: n_at_p4
		REAL  :: n_at_p5
		REAL  :: n_at_p6
	END TYPE NB_DATA_T
!
	TYPE (NB_DATA_T), DIMENSION(:), allocatable :: nbo
!
	END MODULE AA_DATA_M
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	MODULE MAX_DATA_M
!
	INTEGER, PARAMETER  :: max_npos = 999      ! Max. positions to be mumboed
	INTEGER, PARAMETER  :: max_resid_type= 400 ! Max. number of different 
!	                                           ! residue types 
	INTEGER, PARAMETER  :: max_lines_input=  10000000 
	INTEGER, PARAMETER  :: max_lines_output=   200000 
	                                           ! Max. lines in any input file /
						   ! output file
	INTEGER, PARAMETER  :: max_nlig =  2000     ! Max. number of ligand orientations
!
	INTEGER, PARAMETER  :: max_conn_12 =  8    ! related to atom connectivity_12
	INTEGER, PARAMETER  :: max_conn_14 = 64    !                 connectivity_14
!
	INTEGER, PARAMETER  :: max_ntmp = 99999    ! highest atom number in any 
!					             output pdb_file				
!
!
	INTEGER, PARAMETER  :: eo_file= -1     ! End of file integer
	INTEGER, PARAMETER  :: eo_line= -2     ! End of line integer
!
	CHARACTER(LEN=4), PARAMETER :: lig_name= 'LIG '
	CHARACTER(LEN=4), PARAMETER :: gly_name= 'GLY '
	CHARACTER(LEN=4), PARAMETER :: ala_name= 'ALA '
	CHARACTER(LEN=4), PARAMETER :: pro_name= 'PRO '
	CHARACTER(LEN=4), PARAMETER :: pep_name= 'PEP '
	CHARACTER(LEN=4), PARAMETER :: g2a_name= 'G2A ' ! Gly_to_Ala in add_groups
	CHARACTER(LEN=4), PARAMETER :: cah_name= 'CAH ' ! C-alpha-H atom in add_groups
	CHARACTER(LEN=4), PARAMETER :: bbr_name= 'BBR '
	CHARACTER(LEN=4), PARAMETER :: wtr_name= 'WTR '
	
					    ! USED TO IDENTIFY RESIDUES WITH BACKBONE 
!					    ! BACKRUB MOTION and BUILD_WATERS in 
					    ! file 0_mch_sum
!
	CHARACTER(LEN=4), PARAMETER :: ca_name= 'CA  '
	CHARACTER(LEN=4), PARAMETER :: c_name=  'C   '
	CHARACTER(LEN=4), PARAMETER :: n_name=  'N   '
	CHARACTER(LEN=4), PARAMETER :: o_name=  'O   '
	CHARACTER(LEN=4), PARAMETER :: h_name=  'H   '
	CHARACTER(LEN=4), PARAMETER :: cb_name= 'CB  '
	CHARACTER(LEN=4), PARAMETER :: cd_name= 'CD  '
!
!	hydrogen_bonding_related
!
	CHARACTER(LEN=1), PARAMETER :: a_sp3_name= 'A' 
	CHARACTER(LEN=1), PARAMETER :: a_sp2_name= 'B' 
	CHARACTER(LEN=1), PARAMETER :: d_sp3_name= 'D' 
	CHARACTER(LEN=1), PARAMETER :: d_sp2_name= 'E' 
	CHARACTER(LEN=1), PARAMETER :: b_sp3_name= 'Z' 
 	CHARACTER(LEN=1), PARAMETER :: b_sp2_name= 'W' 
	CHARACTER(LEN=1), PARAMETER :: h_sp_name=  'H' 
	CHARACTER(LEN=1), PARAMETER :: h2_sp_name= 'I' 
	CHARACTER(LEN=1), PARAMETER :: h3_sp_name= 'J' 
!
	REAL, PARAMETER :: ch_break_dis= 1.8
	REAL, PARAMETER :: pro_cd_c_dis= 2.7
	REAL, PARAMETER :: picknat_rms_tolerance = 0.01
!
	END MODULE MAX_DATA_M
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	MODULE MUMBO_DATA_M
!
	CHARACTER (LEN=40)  :: mumbo_ver=   'MUMBO VERSION: March   2021             '
!	                                    '1234567890123456789012345678901234567890'
	CHARACTER (LEN=63)  :: mumbo_ref=   'REFERENCE: Stiebritz and Muller (2006), Acta Cryst D62, 648-658'
	REAL                :: mumbo_start,  mumbo_end, proc_start, proc_end
	CHARACTER (LEN=9)   :: qual_input=  'mumbo.inp'
	CHARACTER (LEN=10)  :: qual_env=    '$MUMBO_LIB'
!
	INTEGER :: mumbo_njobs = 0
	CHARACTER (LEN=20), DIMENSION(:), ALLOCATABLE :: mumbo_jobs, mumbo_job_list  
	CHARACTER (LEN=20)  :: mumbo_job, mumbo_part_job  
	LOGICAL :: mumbo_conv_flag = .false.
!
	CHARACTER (LEN=3)   :: qual_job=       'JOB'
	CHARACTER (LEN=4)   :: qual_job_p1=    'INIT'
	CHARACTER (LEN=2)   :: qual_job_p2=    'MC'
	CHARACTER (LEN=3)   :: qual_job_p3=    'DEE'
	CHARACTER (LEN=4)   :: qual_job_p4=    'BRUT'
	CHARACTER (LEN=4)   :: qual_job_p5=    'GOLD'
	CHARACTER (LEN=4)   :: qual_job_p6=    'DOUB'
	CHARACTER (LEN=3)   :: qual_job_p7=    'ANA'
	CHARACTER (LEN=5)   :: qual_job_p8=    'SPLIT'
        CHARACTER (LEN=5)   :: qual_job_p9=    'MONSA'
        CHARACTER (LEN=4)   :: qual_job_p10=   'COMP'
	CHARACTER (LEN=5)   :: qual_job_p11=   'BOUND'
	CHARACTER (LEN=7)   :: qual_job_p12=   'PICKNAT'
!
	CHARACTER (LEN=5)   :: qual_job_p13=   'PDEE'
	CHARACTER (LEN=7)   :: qual_job_p14=   'PGOLD'
!	
	CHARACTER (LEN=5)   :: qual_job_p15=   'WATER'
	CHARACTER (LEN=3)   :: qual_job_p16=   'DEL'
!		
!
        CHARACTER (LEN=9)   :: mumbo_proc=     '---------'
!
	LOGICAL :: init_flag    = .false.
	LOGICAL :: mc_flag      = .false.
	LOGICAL :: dee_flag     = .false.
	LOGICAL :: brut_flag    = .false.
	LOGICAL :: gold_flag    = .false.
	LOGICAL :: doub_flag    = .false.
	LOGICAL :: ana_flag     = .false.
	LOGICAL :: split_flag   = .false.
        LOGICAL :: monsa_flag   = .false.
        LOGICAL :: comp_flag    = .false.
        LOGICAL :: bound_flag   = .false.
        LOGICAL :: picknat_flag = .false.
!	
        LOGICAL :: pdee_flag    = .false.
        LOGICAL :: pgold_flag   = .false.
        LOGICAL :: water_flag   = .false.
        LOGICAL :: del_flag     = .false.
!
	LOGICAL :: mumbo_db_conv = .false.
	INTEGER :: mumbo_db_maxit = 8
	INTEGER :: mumbo_db_nit  = 0
!
	CHARACTER (LEN=11)  :: qual_energy=    'ENERGY_CALC'
	CHARACTER (LEN=3)   :: qual_en_p1=     'VDW'
	CHARACTER (LEN=4)   :: qual_en_p2=     'ELEC'
	CHARACTER (LEN=5)   :: qual_en_p3=     'HBOND'
	CHARACTER (LEN=3)   :: qual_en_p4=     'ASA'
	CHARACTER (LEN=4)   :: qual_en_p5=     'SOLV'
	CHARACTER (LEN=5)   :: qual_en_p6=     'RPROB'
	CHARACTER (LEN=4)   :: qual_en_p7=     'XRAY'
!
	LOGICAL :: vdw_flag =   .false.
	LOGICAL :: elec_flag =  .false.
	LOGICAL :: hbond_flag=  .false.
	LOGICAL :: asa_flag=    .false.
	LOGICAL :: solv_flag=   .false.
	LOGICAL :: rprob_flag=  .false.
	LOGICAL :: xray_flag=   .false.
!
	CHARACTER (LEN=13)  :: qual_en_tuning=  'ENERGY_TUNING'
	CHARACTER (LEN=9)   :: qual_en_t1=  'VDW_RADII'
	CHARACTER (LEN=6)   :: qual_en_t2=  'DIELEC'
	CHARACTER (LEN=7)   :: qual_en_t3=  'ESWITCH'
	CHARACTER (LEN=6)   :: qual_en_t4=  'ESHIFT'
	CHARACTER (LEN=6)   :: qual_en_t5=  'CTONNB'
	CHARACTER (LEN=6)   :: qual_en_t6=  'CTOFNB'
	CHARACTER (LEN=5)   :: qual_en_t7=  'EDIST'
	CHARACTER (LEN=8)   :: qual_en_t8=  'SOL_GREF'
	CHARACTER (LEN=7)   :: qual_en_t9=  'SOL_LAM'
	CHARACTER (LEN=5)   :: qual_en_t10= 'ECONT'
	CHARACTER (LEN=5)   :: qual_en_t11= 'VDW13'
	CHARACTER (LEN=5)   :: qual_en_t12= 'VDW14'
	CHARACTER (LEN=5)   :: qual_en_t13= 'ELE12'
	CHARACTER (LEN=5)   :: qual_en_t14= 'ELE13'
	CHARACTER (LEN=5)   :: qual_en_t15= 'ELE14'
	CHARACTER (LEN=5)   :: qual_en_t16= 'SOL12'
	CHARACTER (LEN=5)   :: qual_en_t17= 'SOL13'
	CHARACTER (LEN=5)   :: qual_en_t18= 'SOL14'
	CHARACTER (LEN=6)   :: qual_en_t19= 'HBGEOM'
	CHARACTER (LEN=5)   :: qual_en_t20= 'HBEMP'
	CHARACTER (LEN=7)   :: qual_en_t21= 'HB_CTOF'
	CHARACTER (LEN=5)   :: qual_en_t22= 'HB_DZ'
	CHARACTER (LEN=5)   :: qual_en_t23= 'HB_EZ'
	CHARACTER (LEN=8)   :: qual_en_t24= 'VDW_SOFT'
	CHARACTER (LEN=9)   :: qual_en_t25= 'SOL_GFREE'
	CHARACTER (LEN=11)  :: qual_en_t26= 'VDW_REPONLY'
	CHARACTER (LEN=7)   :: qual_en_t27= 'EDIS_ON'
	CHARACTER (LEN=8)   :: qual_en_t28= 'ECST_DIS'
!
	REAL  :: mumbo_en_svdw =        1.0
	REAL  :: mumbo_en_sdiel =      20.0
	REAL  :: mumbo_en_ctonnb =      6.5
	REAL  :: mumbo_en_ctofnb =     10.5
	REAL  :: mumbo_en_solgref =     0.0
	REAL  :: mumbo_en_sollam =      1.0
	REAL  :: mumbo_en_solgfree =    1.0
	REAL  :: mumbo_en_vdw_reponly = 1.0
	REAL  :: mumbo_en_edison      = 0.0
	REAL  :: mumbo_en_ecstdis     = 0.0
!
	INTEGER :: mumbo_en_it_vdw = 4
	INTEGER :: mumbo_en_it_ele = 4
	INTEGER :: mumbo_en_it_sol = 3
	INTEGER :: mumbo_en_it_hbd = 4
!
	real    :: mumbo_en_hbd_ctof=   4.0
	real    :: mumbo_en_hbd_dzero=  2.8
	real    :: mumbo_en_hbd_ezero=  8.0
	logical :: mumbo_en_hbd_geoflag = .true.
	logical :: mumbo_en_hbd_empflag = .false.
!
	logical :: mumbo_en_vdw_soft = .false.
!
	logical   :: eswitch_flag=  .false.   
	logical   :: eshift_flag=   .false.   
	logical   :: edist_flag=    .false.   
	logical   :: econt_flag=    .false.   
!
	CHARACTER (LEN=14)  :: qual_weights=   'ENERGY_WEIGHTS'
	CHARACTER (LEN=3)   :: qual_we_p1=     'VDW'
	CHARACTER (LEN=4)   :: qual_we_p2=     'ELEC'
	CHARACTER (LEN=5)   :: qual_we_p3=     'RPROB'
	CHARACTER (LEN=4)   :: qual_we_p4=     'SOLV'
	CHARACTER (LEN=5)   :: qual_we_p5=     'HBOND'
	CHARACTER (LEN=3)   :: qual_we_p6=     'ASA'
	CHARACTER (LEN=3)   :: qual_we_p7=     'XRAY'
!
	REAL  :: mumbo_we_vdw    = 1.0
	REAL  :: mumbo_we_elec   = 1.0
	REAL  :: mumbo_we_rprob  = 3.0
	REAL  :: mumbo_we_solv   = 1.0
	REAL  :: mumbo_we_hbond  = 1.0
	REAL  :: mumbo_we_asa    = 1.0
	REAL  :: mumbo_we_xray   = 1.0
!
	CHARACTER (LEN=11)  :: qual_en_brt=  'ENERGY_BRUT'
	CHARACTER (LEN=4)   :: qual_en_b1=   'KEEP'
!	CHARACTER (LEN=7)   :: qual_en_b2=   'MAXCOMB'
!
	INTEGER  :: mumbo_en_bt  =  0
	REAL     :: mumbo_en_bwr = -10000
	INTEGER,PARAMETER :: INT_LONG = selected_int_kind(18) 
!	INTEGER (KIND=INT_LONG) :: mumbo_en_bmxc 
!
	CHARACTER (LEN=9)   :: qual_en_mc=  'ENERGY_MC'
	CHARACTER (LEN=6)   :: qual_en_m1=  'CUTOFF'
	CHARACTER (LEN=12)  :: qual_en_m2=  'AUTO_RECOVER'
	REAL    :: mumbo_en_cut =  40.0
	LOGICAL :: mumbo_en_mc_recover_flag = .false.
!
	CHARACTER (LEN=10)  :: qual_en_ana= 'ENERGY_ANA'
	CHARACTER (LEN=7)   :: qual_ana_a1= 'MAXCOMB'
	CHARACTER (LEN=7)   :: qual_ana_a2= 'MAXSORT'
!
	INTEGER (KIND=8) ::    mumbo_ana_mxc = 100000
	INTEGER ::    mumbo_ana_mxs = 100
!
	CHARACTER (LEN=12)  :: qual_en_sa    = 'ENERGY_MONSA'
	CHARACTER (LEN=10)  :: qual_sa_Tmax  = 'TEMP_START'
	CHARACTER (LEN=8)   :: qual_sa_Tmin  = 'TEMP_END'
	CHARACTER (LEN=10)  :: qual_sa_steps = 'TEMP_STEPS'
	CHARACTER (LEN=10)  :: qual_sa_mc    = 'TEMP_NUMIT'
	REAL                :: mumbo_sa_Tmax =  5000.0     ! [K] starting temperature for simulated annealing
	REAL                :: mumbo_sa_Tmin =  293.73     ! [K] final temperature for simulated annealing
	INTEGER             :: mumbo_sa_steps=  500        ! [K] temperature decrement
	INTEGER             :: mumbo_sa_mc   =  1000       ! number of iterations per step in monte carlo subroutine
!
	INTEGER, DIMENSION(:), ALLOCATABLE :: mumbo_sim_conf_min
!
	CHARACTER (LEN=10)  :: qual_en_dee=    'ENERGY_DEE'
	CHARACTER (LEN=4)   :: qual_en_d1=      'NCYC'
	INTEGER    :: mumbo_dee_ncyc =   1.0
!
	CHARACTER (LEN=11)  :: qual_en_gold=   'ENERGY_GOLD'
	CHARACTER (LEN=4)   :: qual_en_g1=      'NCYC'
	INTEGER    :: mumbo_gold_ncyc =  1.0
!
	CHARACTER (LEN=9)   :: qual_en_split=  'ENERGY_SPLIT'
	CHARACTER (LEN=6)   :: qual_en_s1=     'NCYC'
	INTEGER    :: mumbo_split_ncyc = 3.0
!
	CHARACTER (LEN=12)  :: qual_en_bound=  'ENERGY_BOUND'
	CHARACTER (LEN=4)   :: qual_en_bo=     'NCYC'
	INTEGER             :: mumbo_bound_ncyc = 10.0
!
	CHARACTER (LEN=5)   :: qual_inpdb=     'INPDB'
	CHARACTER (LEN=6)   :: qual_outpdb=    'OUTPDB'
	CHARACTER (LEN=15)  :: qual_rot_lib1=  'ROTAMER_LIB_ALL'
	CHARACTER (LEN=18)  :: qual_rot_lib2=  'ROTAMER_LIB_PHIPSI'
!	CHARACTER (LEN=16)  :: qual_rot_lib3=  'ROTAMER_LIB_BETA'
	CHARACTER (LEN=10)  :: qual_connec=    'CONNEC_LIB'
	CHARACTER (LEN=13)  :: qual_nonbond=   'NONBONDED_LIB'
	CHARACTER (LEN=9)   :: qual_param=     'PARAM_LIB'
	CHARACTER (LEN=11)  :: qual_surface=   'SURFACE_LIB'
!
	character (LEN=1000) :: mumbo_inpdb
	character (LEN=1000) :: mumbo_outpdb
	character (LEN=1000) :: mumbo_librot
	character (LEN=1000) :: mumbo_libpps
	character (LEN=1000) :: mumbo_libcon
	character (LEN=1000) :: mumbo_libpar
	character (LEN=1000) :: mumbo_libnbo
	character (LEN=1000) :: mumbo_libsrf
!
	character (LEN=1000) :: mumbo_cwd
	character (LEN=1000) :: mumbo_env
	integer :: mumbo_ncwd, mumbo_nenv
!
	CHARACTER (LEN=5)   :: qual_out_hyd=    'HYDRO'
	LOGICAL :: mumbo_out_hyd = .false.
!
	CHARACTER (LEN=14)  :: qual_rot_tuning=  'ROTAMER_TUNING'
	CHARACTER (LEN=10)  :: qual_rot_t1=    'PHIPSI_DEP'
	CHARACTER (LEN=9)   :: qual_rot_t2=    'FINE_NDEG'
	CHARACTER (LEN=9)   :: qual_rot_t3=    'FINE_NSTP'
	CHARACTER (LEN=10)  :: qual_rot_t4=    'FINE_LIMIT'
	CHARACTER (LEN=15)  :: qual_rot_t5=    'FINE_CHI_1_ONLY'
	CHARACTER (LEN=12)  :: qual_rot_t6=    'FINE_CHI_ALL'
	CHARACTER (LEN=12)  :: qual_rot_t7=    'HIS_ONE_ONLY'
!
	logical   :: phipsi_flag= .false. 
	logical   :: fine_flag= .false.
	logical   :: chi_one_flag= .false.
	logical   :: chi_all_flag= .false.
	logical   :: his_one_flag= .false.                        
	REAL      ::    mumbo_fine_ndeg =  5.0
	INTEGER   ::    mumbo_fine_nstp =  0
	REAL      ::    mumbo_fine_limit = 300
!
	CHARACTER (LEN=14)  :: qual_backrub_tuning=  'BACKRUB_TUNING'
	CHARACTER (LEN=9)   :: qual_back_t1=   'BACK_NDEG'
	CHARACTER (LEN=9)   :: qual_back_t2=   'BACK_NSTP'
	logical   ::    backrub_flag= .false.
	REAL      ::    mumbo_back_ndeg =  3.0
	INTEGER   ::    mumbo_back_nstp =  0.0
!
	CHARACTER (LEN=11) :: qual_xray=     'XRAY_TUNING'
	CHARACTER (LEN=5)  :: qual_xray_a1=   'MAPIN'
	CHARACTER (LEN=7)  :: qual_xray_a2=   'MAPTYPE'
!
	CHARACTER (LEN=1000) :: mumbo_map_in
	CHARACTER (LEN=1000) :: mumbo_map_type
!
	CHARACTER (LEN=11) :: qual_comp=      'COMP_TUNING'
!
	CHARACTER (LEN=19) :: qual_comp_pick=      'COMP_PICKNAT_TUNING'
	CHARACTER (LEN=6)  :: qual_comp_pick_a1=   'REFPDB'
	CHARACTER (LEN=4)  :: qual_comp_pick_a2=   'NOCB'
	CHARACTER (LEN=8)  :: qual_comp_pick_a3=   'SWAP_QNH'
!
	CHARACTER (LEN=1000) :: mumbo_ref_pdb
	LOGICAL :: mumbo_ref_beta_flag = .true.
	LOGICAL :: mumbo_swap_qnh_flag = .false.
!
	CHARACTER (LEN=13)  :: qual_lig_tuning= 'LIGAND_TUNING'
	CHARACTER (LEN=7)   :: qual_lig_t1=  'LIGFILE'
	CHARACTER (LEN=7)   :: qual_lig_t2=  'LIGNAME'
	CHARACTER (LEN=6)   :: qual_lig_t3=  'LIGRMS'
	CHARACTER (LEN=7)   :: qual_lig_t4=  'LIGNGEN'
	CHARACTER (LEN=7)   :: qual_lig_t5=  'LIGSEED'
	CHARACTER (LEN=7)   :: qual_lig_t6=  'NOSHIFT'
	CHARACTER (LEN=5)   :: qual_lig_t7=  'NOROT'
	CHARACTER (LEN=10)  :: qual_lig_t8=  'LIGFULLROT'
	CHARACTER (LEN=9)   :: qual_lig_t9=  'LIGCENTRE'
!
	CHARACTER (LEN=1000)  :: mumbo_lig_in
	CHARACTER (LEN=1)     :: mumbo_lig_chid= ' '
	CHARACTER (LEN=4)     :: mumbo_lig_ctan= '    '
	INTEGER               :: mumbo_lig_nold= 0
	INTEGER               :: mumbo_lig_seed = 12345
	INTEGER               :: mumbo_lig_ngen  = 1
	REAL                  :: mumbo_lig_rms  = 3.0
	LOGICAL               :: mumbo_lig_pdb_flag=.false.
	LOGICAL               :: mumbo_lig_fle_flag=.false.
	LOGICAL               :: mumbo_lig_flag=.false.
	LOGICAL               :: mumbo_lig_shift_flag = .true.
	LOGICAL               :: mumbo_lig_rot_flag = .true.
	LOGICAL               :: mumbo_lig_fullrot_flag = .false.
	LOGICAL               :: mumbo_lig_centre = .false.
!	
	CHARACTER (LEN=10)                 :: qual_del_tuning   = 'DEL_TUNING'
	CHARACTER (LEN=7)                  :: qual_del_eps      = 'DEL_EPS'
!	CHARACTER (LEN=12)                 :: qual_del_RMSD_MIN = 'DEL_RMSD_MIN'
!	
	real                               :: DEL_eps      = 0.0
!	real                               :: RMSDmin      = 0.0
!         
        CHARACTER (LEN=17)  :: qual_water_tuning              =  'WATER_TUNING' 
        CHARACTER (LEN=17)  :: qual_water_build_ene_min       =  'H2O_BUILD_ENE_MIN'
        CHARACTER (LEN=11)  :: qual_water_Ox_min_dist         =  'OX_MIN_DIST'
        CHARACTER (LEN=11)  :: qual_water_Hy_min_dist         =  'HY_MIN_DIST'
        CHARACTER (LEN=9)   :: qual_water_chainid             =  'WCHID_OUT'
        CHARACTER (LEN=9)   :: qual_water_name                =  'WNAME_OUT'
!        
        real                              :: H20_build_ene_min =  -1.0
        real                              :: Ox_min_dist       =   0.1
        real                              :: Hy_min_dist       =   0.1
        CHARACTER(LEN=4)                  :: WHOH_name_out = 'HOH '
        CHARACTER(LEN=1)                  :: WchainID_out  = 'W'
!                             
!
!	CHARACTER (LEN=10)  :: qual_asa_tuning=  'ASA_TUNING'
!
!	CHARACTER (LEN=4) ::    qual_asa_a1= 'GRID'
!	CHARACTER (LEN=5) ::    qual_asa_a2= 'PROBE'
!	CHARACTER (LEN=5) ::    qual_asa_a3= 'SHELL'
!
!	REAL ::    mumbo_asa_grd = 5
!	REAL ::    mumbo_asa_prb = 1.4
!	REAL ::    mumbo_asa_shl = 0.4
!
	CHARACTER (LEN=14)  :: qual_log_tuning=  'LOGFILE_TUNING'
!
	CHARACTER (LEN=8)  ::   qual_log_t1= 'ANA_LONG'
	CHARACTER (LEN=9)  ::   qual_log_t2= 'ANA_SHORT'
	CHARACTER (LEN=7)  ::   qual_log_t3= 'MC_LONG'
	CHARACTER (LEN=8)  ::   qual_log_t4= 'MC_SHORT'
	CHARACTER (LEN=10) ::   qual_log_t5= 'DBASE_LONG'
	CHARACTER (LEN=11) ::   qual_log_t6= 'DBASE_SHORT'
	CHARACTER (LEN=10) ::   qual_log_t7= 'ENE2_CTOFF'

!
	LOGICAL        ::    mumbo_log_ana_flag   = .false.
	LOGICAL        ::    mumbo_log_mc_flag    = .false.
	LOGICAL        ::    mumbo_log_dbase_flag = .false.
        REAL           ::    mumbo_log_ene2_ctoff =  0.0005
!
	CHARACTER (LEN=9)   :: qual_mumbo_pos= 'MUMBO_POS'
!
!       HERE SOME LABELS TO BETTER ECHO PROBLEMS OCCURING DURING FILE READING 
!                                                      12345678901234567890123456789012345
!
	character (LEN=35) :: filelabel_input_file  = 'MUMBO INPUT FILE                   '
	character (LEN=35) :: filelabel_inpdb       = 'INPDB FILE                         '
	character (LEN=35) :: filelabel_outpdb      = 'OUTPDB FILE                        '
	character (LEN=35) :: filelabel_librot      = 'ROTAMER_LIB_ALL FILE               '
	character (LEN=35) :: filelabel_libpps      = 'ROTAMER_LIB_PHIPSI FILE            '
	character (LEN=35) :: filelabel_libcon      = 'CONNEC_LIB FILE                    '
	character (LEN=35) :: filelabel_libpar      = 'PARAM_LIB FILE                     '
	character (LEN=35) :: filelabel_libnbo      = 'NONBONDED_LIB                      '  
	character (LEN=35) :: filelabel_libsrf      = 'SURFACE_LIB FILE                   '
!
	character (LEN=35) :: filelabel_ref_pdb     = 'COMP_PICKNAT_TUNING/REFPDB FILE    '
	character (LEN=35) :: filelabel_lig_in      = 'LIGAND_TUNING/LIGFILE FILE         '
	character (LEN=35) :: filelabel_map_in      = 'XRAY_TUNING/MAPIN FILE             '
	character (LEN=35) :: filelabel_dummy       = '12345678901234567890123456789012345'
!
!   here some data allowing for automatic substitutions to be performed at mumbo_pos
!
!                                                          string_length has to be smaller or equal 132 
	character (LEN=100), dimension(5) :: subs_instr =                                  &               
        &(/'HIS 2b_repl HIS HJS HKS                                                                             ', &
        &  'POL 2b_repl SER THR ASN GLN TYR TRP                                                                 ', &
        &  'HYD 2b_repl ALA VAL LEU ILE PHE MET                                                                 ', &
        &  'CHA 2b_repl ASP GLU LYS ARG HIS                                                                     ', &
        &  'ALL 2b_repl ALA ARG ASN ASP CYS GLN GLU GLY HIS ILE LEU LYS MET PHE PRO SER THR TRP TYR VAL         '/) 
!           1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
!                   10        20        30        40        50        60        70        80        90       100
!
!       Data read in with MUMBO_POS
!
	TYPE MUT_T
		CHARACTER (LEN=3), DIMENSION (:), pointer :: mut_aas
		INTEGER   :: mut_nold
		INTEGER   :: mut_nnew
		LOGICAL   :: mut_flag
		CHARACTER (LEN=1)  :: mut_chid
	END TYPE MUT_T
!
	TYPE (MUT_T), DIMENSION(:), allocatable :: mumbo
!
	TYPE MUT_R
		CHARACTER (LEN=3), DIMENSION (:), pointer :: mut_rep
		CHARACTER (LEN=3)  :: mut_key
	END TYPE MUT_R
!
	TYPE (MUT_R), DIMENSION(:), allocatable :: mumbosub
!
!       UNQUE LIST OF RESIDUES TYPES TO BE BUILD IN MUMBO with MUMBO_POS
!
!       reslst(i)%aas     names of residues
!       reslst(i)%flag    flag - logical
!
        TYPE RLS_T
		CHARACTER (LEN=3)  :: aas
                LOGICAL            :: flag
        END TYPE RLS_T
!
        TYPE (RLS_T), DIMENSION(:), allocatable  :: reslst
!
	END MODULE MUMBO_DATA_M
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
!       module WATER_DATA stores some data for building solvated Rotamers
!
        MODULE WATER_DATA
!
        logical                            :: Water_between_AS_SC_flag = .TRUE.
        logical                            :: Water_between_AS_MC_flag = .TRUE.
        
!        CHARACTER(LEN=4),dimension(20)     :: ASNAMES  =  (/'ALA ','ARG ','ASN ','ASP ','CYS ','GLN ','GLU ','GLY ','HIS ','ILE '&
!                                                         &,'LEU ','LYS ','MET ','PHE ','PRO ','SER ','THR ','TRP ','TYR ','VAL '/)
!        
!        CHARACTER(LEN=4),dimension(20)     :: ASWNAMES = (/'ALW ','ARW ','ANW ','APW ','CYW ','GNW ','GUW ','GYW ','HIW ','ILW '& 
!                                                         &,'LEW ','LYW ','MEW ','PHW ','POW ','SEW ','THW ','TRW ','TYW ','VAW '/)
!
        CHARACTER(LEN=4),dimension(22)  :: ASNAMES  = (/'ALA ','ARG ','ASN ','ASP ','CYS ','GLN ','GLU ','GLY ','HIS ','HJS ',&
                                        & 'HKS ','ILE ','LEU ','LYS ','MET ','PHE ','PRO ','SER ','THR ','TRP ','TYR ','VAL '/)
        
        CHARACTER(LEN=4),dimension(22)  :: ASWNAMES = (/'ALW ','ARW ','ANW ','APW ','CYW ','GNW ','GUW ','GYW ','HIW ','HJW ',&
                                        & 'HKW ','ILW ','LEW ','LYW ','MEW ','PHW ','POW ','SEW ','THW ','TRW ','TYW ','VAW '/)
!
!
        CHARACTER(LEN=4)                   :: WOx_name = 'OW  ' 
        CHARACTER(LEN=4)                   :: WH1_name = 'HW1 '
        CHARACTER(LEN=4)                   :: WH2_name = 'HW2 '
!
        CHARACTER(LEN=4)                   :: WOx_name_out  = 'O   ' 
        CHARACTER(LEN=4)                   :: WH_name_out   = 'H   '
!       
        real                               :: H2O_mindistAOD= 3.09
        real                               :: H2O_maxdistAOD= 5.20
!             
        real                               :: DIST_POLAR_WATER = 2.80
        real                               :: DIST_N_WATER     = 2.95
!        
! HERE SOME PARAMETER TO ONLY BUILD BURRIED WATERS 
!
!        real                               :: buried_radius = 0.0
!        integer                            :: buried_num_CA = 0.0
!
	TYPE EP_G_T
		REAL, DIMENSION(1)    :: ep_en     ! ep_energy
		!LOGICAL               :: ep_flag   ! ep_flag
	        !LOGICAL               :: pair_flag ! true if dee pair
		!REAL                  :: pair_max  ! E(ir)+E(js)+E(ir,js)+
!                                                  !    sum(k){max(t)[E(ir,kt)+E(js,kt)]}
		!REAL                  :: pair_min  ! E(ir)+E(js)+E(ir,js)+sum(k)
!                                                  !           {min(t)[E(ir,kt)+E(js,kt)]}
		!REAL                  :: ep_min_split
	END TYPE EP_G_T
!
	TYPE EP_F_T
	     TYPE (EP_G_T), DIMENSION(:), pointer :: ef
	END TYPE EP_F_T
!
	TYPE EP_E_T
	     TYPE (EP_F_T), DIMENSION(:), pointer :: ee
	END TYPE EP_E_T
!
	TYPE EP_D_T
	     TYPE (EP_E_T), DIMENSION(:), pointer :: ed
	END TYPE EP_D_T
!
	TYPE EP_C_T
	     TYPE (EP_D_T), DIMENSION(:), pointer :: ec
	END TYPE EP_C_T
!
	TYPE EP_B_T
	     TYPE (EP_C_T), DIMENSION(:), pointer :: eb
	END TYPE EP_B_T
!
	TYPE (EP_B_T), DIMENSION(:), allocatable :: ea
!
         integer,dimension(:,:), allocatable          :: BACKsetsglobal
         real,   dimension(:,:), allocatable          :: BACKcordglobal
         character(len=4), dimension(:), allocatable  :: BACKnameglobal
!
         integer,dimension(:,:), allocatable          :: DATAsetspos, BACKsetspos
         real,   dimension(:,:), allocatable          :: DATAcordpos, BACKcordpos
         character(len=4), dimension(:), allocatable  :: DATAnamepos, BACKnamepos
!
         integer,dimension(:,:), allocatable          :: DATAsetslocal
         real,   dimension(:,:), allocatable          :: DATAcordlocal
         character(len=4), dimension(:), allocatable  :: DATAnamelocal
!
         logical                                      :: first_DATApos= .TRUE.
!
!$omp threadprivate (BACKsetsglobal,BACKcordglobal,BACKnameglobal,DATAsetspos,DATAcordpos,DATAnamepos, &
!$omp&               BACKsetspos,BACKcordpos,BACKnamepos,DATAsetslocal,DATAcordlocal,DATAnamelocal,    &
!$omp&               first_DATApos)
!
        END MODULE WATER_DATA
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
      
 



