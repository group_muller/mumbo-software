!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	subroutine dump(nm,nr,na,nt,naa)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MOLEC_M
!
	IMPLICIT NONE
!
	INTEGER:: nm,nr,na,nt,naa,i
	LOGICAL:: flag
!
	call check(nm,nr,na,nt,naa,flag)
!
	if (flag) then 
!
!	do i=1,mol(nm)%res(nr)%res_aas(na)%aa_rots(nt)%rot_nat
	do i=1,nt
!
	print*, 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
	print*, 'nmol=',nm,' nres=',nr,' naa=',na,' nrot=',nt,' nat=',i
	print*, 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
	print*, 'RESNUM: ',mol(nm)%res(nr)%res_num
	print*, 'RESNAM: ',mol(nm)%res(nr)%res_aas(na)%aa_nam
	print*, 'PROB: ',mol(nm)%res(nr)%res_aas(na)%aa_rots(nt)%rot_prob
	print*,'AT_NAME,_NUM,_TYP,_NTYP,AT_OZ ,AT_CHARGE'
	print*, mol(nm)%res(nr)%res_aas(na)%aa_rots(nt)%rot_ats(i)%at_nam
	print*, mol(nm)%res(nr)%res_aas(na)%aa_rots(nt)%rot_ats(i)%at_num
	print*, mol(nm)%res(nr)%res_aas(na)%aa_rots(nt)%rot_ats(i)%at_typ
	print*, mol(nm)%res(nr)%res_aas(na)%aa_rots(nt)%rot_ats(i)%at_ntyp
	print*, mol(nm)%res(nr)%res_aas(na)%aa_rots(nt)%rot_ats(i)%at_noz
	print*, mol(nm)%res(nr)%res_aas(na)%aa_rots(nt)%rot_ats(i)%at_cha
	print*,'COORD '
	print*, mol(nm)%res(nr)%res_aas(na)%aa_rots(nt)%rot_ats(i)%at_xyz(1)
	print*, mol(nm)%res(nr)%res_aas(na)%aa_rots(nt)%rot_ats(i)%at_xyz(2)
	print*, mol(nm)%res(nr)%res_aas(na)%aa_rots(nt)%rot_ats(i)%at_xyz(3)
	print*, mol(nm)%res(nr)%res_aas(na)%aa_rots(nt)%rot_ats(i)%at_occ
	print*, mol(nm)%res(nr)%res_aas(na)%aa_rots(nt)%rot_ats(i)%at_bfa
	print*,'SOLVAT '
	print*, mol(nm)%res(nr)%res_aas(na)%aa_rots(nt)%rot_ats(i)%at_svo
	print*, mol(nm)%res(nr)%res_aas(na)%aa_rots(nt)%rot_ats(i)%at_sla
	print*, mol(nm)%res(nr)%res_aas(na)%aa_rots(nt)%rot_ats(i)%at_sgr
	print*, mol(nm)%res(nr)%res_aas(na)%aa_rots(nt)%rot_ats(i)%at_sgf
	print*,'EPS  SIG'
	print*, mol(nm)%res(nr)%res_aas(na)%aa_rots(nt)%rot_ats(i)%at_eps
	print*, mol(nm)%res(nr)%res_aas(na)%aa_rots(nt)%rot_ats(i)%at_sig
!
	end do
!
	else 
		print*,'REQUESTED ATOM DOES NOT EXIST'
	end if
!
	END SUBROUTINE DUMP
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	subroutine dump_pdb(nm,nr,na,nt)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MOLEC_M
	USE MAX_DATA_M
!
	IMPLICIT NONE
!
	INTEGER:: nm,nr,na,nt,nnrs
	INTEGER:: i,j,k,l,m
	INTEGER, SAVE :: natn=0
	CHARACTER(LEN=4) :: name='    '
	real :: xboff, yboff
	CHARACTER(LEN=1) :: achid
!
	i=nm
	j=nr
	k=na
	l=nt
!
	nnrs=  mol(i)%res(j)%res_nold
	achid= mol(i)%res(j)%res_chid
!
	if (i/=1 .and. (mol(i)%res(j)%res_aas(k)%aa_nam) == gly_name) then 
!
          natn=natn+1
          xboff = 9.9
	  yboff = 99.99
	  do m=1, size(mol(i)% res(1)%res_atom_mc)
!
		    if (ca_name == (mol(i)%res(1)%res_atom_mc(m)%at_nam)) then
!
		      write(*,fmt=2010)                              &
     &		             'ATOM  ',                                &
     &                       natn,                                    &
     &                       ca_name,                                 &
     &                       gly_name,                                &
     &                       achid,                                   &
     &                       nnrs,                                    &
     &                       mol(i)%res(1)%res_atom_mc(m)%at_xyz(1),  &
     &                       mol(i)%res(1)%res_atom_mc(m)%at_xyz(2),  &
     &                       mol(i)%res(1)%res_atom_mc(m)%at_xyz(3),  &
     &                       xboff,                                   &
     &                       yboff   
!
		      write(15,fmt=2010)                              &
     &		             'ATOM  ',                                &
     &                       natn,                                    &
     &                       ca_name,                                 &
     &                       gly_name,                                &
     &                       achid,                                   &
     &                       nnrs,                                    &
     &                       mol(i)%res(1)%res_atom_mc(m)%at_xyz(1),  &
     &                       mol(i)%res(1)%res_atom_mc(m)%at_xyz(2),  &
     &                       mol(i)%res(1)%res_atom_mc(m)%at_xyz(3),  &
     &                       xboff,                                   &
     &                       yboff   
!
		    end if
	end do
!
!	GENERAL CASE - NO MUMBOED GLY....
!
	else
!
        do m=1, mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_nat
          natn=natn+1
          xboff = mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_prob
          name=mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_nam
!
          if (name(4:4)==' ') then
!
          write(*,fmt=2010)                                             &
     &  'ATOM  ',                                                       &
     &      natn,                                                       &
     &  mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_nam,          &
     &      mol(i)%res(j)%res_aas(k)%aa_nam,                            &
     &      achid,                                                      &
     &      nnrs,                                                       &
     &  mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_xyz(1),       &
     &  mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_xyz(2),       &
     &  mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_xyz(3),       &
     &  xboff,                                                          &
     &  mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_bfa
!
          write(15,fmt=2010)                                            &
     &  'ATOM  ',                                                       &
     &      natn,                                                       &
     &  mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_nam,          &
     &      mol(i)%res(j)%res_aas(k)%aa_nam,                            &
     &      achid,                                                      &
     &      nnrs,                                                       &
     &  mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_xyz(1),       &
     &  mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_xyz(2),       &
     &  mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_xyz(3),       &
     &  xboff,                                                          &
     &  mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_bfa
!
          else
!
          write(*,fmt=2011)                                             &
     &  'ATOM  ',                                                       &
     &      natn,                                                       &
     &  mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_nam,          &
     &      mol(i)%res(j)%res_aas(k)%aa_nam,                            &
     &      achid,                                                      &
     &      nnrs,                                                       &
     &  mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_xyz(1),       &
     &  mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_xyz(2),       &
     &  mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_xyz(3),       &
     &  xboff,                                                          &
     &  mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_bfa
!
          write(15,fmt=2011)                                            &
     &  'ATOM  ',                                                       &
     &      natn,                                                       &
     &  mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_nam,          &
     &      mol(i)%res(j)%res_aas(k)%aa_nam,                            &
     &      achid,                                                      &
     &      nnrs,                                                       &
     &  mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_xyz(1),       &
     &  mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_xyz(2),       &
     &  mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_xyz(3),       &
     &  xboff,                                                          &
     &  mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_bfa
!
          end if
!
                  end do
!
	end if
!
2010    format(a6,i5,2x,a4,a4,a,i4,4x,3f8.3,2f6.2)
2011    format(a6,i5,1x,a4,1x,a4,a,i4,4x,3f8.3,2f6.2)
!
	END SUBROUTINE DUMP_PDB
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	subroutine check(nm,nr,na,nt,naa,flag)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MOLEC_M
!
	IMPLICIT NONE
!
	INTEGER:: nm,nr,na,nt,naa
	INTEGER:: mm,mr,ma,mt,maa
	LOGICAL:: flag
!
	flag=.false.
	mm=size(mol)	
	if (mm >= nm) then
	   mr=mol(nm)%mol_nrs
	   if (mr >= nr) then
	       ma=mol(nm)%res(nr)%res_naa
	       if (ma >= na) then
	           mt=mol(nm)%res(nr)%res_aas(na)%aa_nrt
		   if (mt >= nt) then
		      maa=mol(nm)%res(nr)%res_aas(na)%aa_rots(nt)%rot_nat
		      if (maa >= naa) then
			flag=.true.
		      end if
		   end if
	       end if			
	   end if
	end if
! 
	END SUBROUTINE CHECK 
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE JUST_CHECKING()
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MOLEC_M
	USE MUMBO_DATA_M
	USE MAX_DATA_M
!
	IMPLICIT NONE
!
	INTEGER :: i,j,k
!
	PRINT*, '        '
	PRINT*, '################################'
	PRINT*, '# SUMMARY  JUST_CHECKING        #'
	PRINT*, '################################'
	PRINT*, '        '
!
	print*, '      NUMBER OF MOLECULES IN DATA STRUCTURE:'
	print*, '      ', (size(mol)-1)
!
	do i=1,size(mol)
!
	if (i/=1) then 
			PRINT *, '   '
			PRINT *,' RESIDUE= ', mol(i)%res(1)%res_nold,        &
     &    mol(i)%res(1)%res_chid,'  /  INTERNAL NUMB: ',mol(i)%res(1)%res_num       
			print *,                                              &
     &    '    DATA-MOLECULE  #RESIDUES  #AMINOACIDS_IN_RES',           &
     &    '          #ROTAMERS_IN_AA      #ATOMS_IN_ROT   ROT_PROB(FIRST ROTA.)'
			PRINT *, '   '
	end if
!
         do j=1,mol(i)%mol_nrs
            do k=1,mol(i)%res(j)%res_naa
!               do l=1, mol(i)%res(j)%res_aas(k)%aa_nrt
!                 do m=1, mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_nat
!
		if (i/=1) then 
!
                        print *,                                        &
     &                  (i-1),                                          &
     &                  mol(i)%mol_nrs,                                 &
     &                  mol(i)%res(j)%res_naa,                          &
     &                  mol(i)%res(j)%res_aas(k)%aa_nrt,                &
     &                  mol(i)%res(j)%res_aas(k)%aa_rots(1)%rot_nat,    &
     &                  mol(i)%res(j)%res_aas(k)%aa_rots(1)%rot_prob
!
		end if
!
!                 end do
!               end do
            end do
          end do
        end do
!
!
!	do i=1,100
!		print *,'    '
!		print *,' NMOL , NRES, NAAC, NROT , NAT ??'
!		read *,nm,nr,na,nt,naa
!		CALL DUMP(nm,nr,na,nt,naa)
!	end do
!
!	do i= 1, mol(1)%mol_nrs) 
!	print*, i, size(mol(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats)
!	end do
!
	PRINT*, '        '
	PRINT*, ' FINISHED JUST CHECKING '
	PRINT*, '        '
!
!
	END SUBROUTINE JUST_CHECKING
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!
	SUBROUTINE CLEAN_UP()
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MOLEC_M
	USE MUMBO_DATA_M
	USE MAX_DATA_M
	USE EP_DATA_M
        USE SPLIT_M
        USE SPLIT_POS_M
!
	IMPLICIT NONE 
!
	integer :: i,j,k,l,m, ncheck
!
	if (allocated(ea)) then
!
	do i=1,size(ea)
	  do j=1, size(ea(i)%eb)
	    do k=1, size(ea(i)%eb(j)%ec)
	       do l=1, size(ea(i)%eb(j)%ec(k)%ed)
		 do m=1, size(ea(i)%eb(j)%ec(k)%ed(l)%ee)
		  deallocate(ea(i)%eb(j)%ec(k)%ed(l)%ee(m)%ef,stat=ncheck)
		  if(ncheck/=0) stop '1Fehler beim Deallokieren. Programmabbruch'
	         end do
		 deallocate(ea(i)%eb(j)%ec(k)%ed(l)%ee,stat=ncheck)
		 if(ncheck/=0) stop '2Fehler beim Deallokieren. Programmabbruch'
	       end do
	       deallocate(ea(i)%eb(j)%ec(k)%ed,stat=ncheck)
	       if(ncheck/=0) stop '3Fehler beim Deallokieren. Programmabbruch'
	    end do
	    deallocate(ea(i)%eb(j)%ec,stat=ncheck)
	    if(ncheck/=0) stop '4Fehler beim Deallokieren. Programmabbruch'
	  end do
	  deallocate(ea(i)%eb,stat=ncheck)
	  if(ncheck/=0) stop '5Fehler beim Deallokieren. Programmabbruch'
	end do
	deallocate(ea,stat=ncheck)
        if(ncheck/=0) stop '6Fehler beim Deallokieren. Programmabbruch'
	end if
!
        if (allocated(sa)) then
!        
        do i=1,size(sa)
          do j=1,size(sa(i)%sb)
            do k=1,size(sa(i)%sb(j)%sc)
              do l=1,size(sa(i)%sb(j)%sc(k)%sd)
                do m=1,size(sa(i)%sb(j)%sc(k)%sd(l)%se)
                  deallocate(sa(i)%sb(j)%sc(k)%sd(l)%se(m)%sf,stat=ncheck)
		  if(ncheck/=0) stop '1Fehler beim Deallokieren. Programmabbruch'                
                end do
                deallocate(sa(i)%sb(j)%sc(k)%sd(l)%se,stat=ncheck)
                if(ncheck/=0) stop '2Fehler beim Deallokieren. Programmabbruch'
              end do
              deallocate(sa(i)%sb(j)%sc(k)%sd,stat=ncheck)
              if(ncheck/=0) stop '3Fehler beim Deallokieren. Programmabbruch'
            end do
            deallocate(sa(i)%sb(j)%sc,stat=ncheck)
            if(ncheck/=0) stop '4Fehler beim Deallokieren. Programmabbruch'
          end do
          deallocate(sa(i)%sb,stat=ncheck)
          if(ncheck/=0) stop '5Fehler beim Deallokieren. Programmabbruch'
        end do
        deallocate(sa,stat=ncheck)
        if(ncheck/=0) stop '6Fehler beim Deallokieren. Programmabbruch'
!        
        end if
!        
        if (allocated(pos)) then
!        
        do i=1,size(pos)
          do j=1,size(pos(i)%aa)
            do k=1,size(pos(i)%aa(j)%ro)
              deallocate(pos(i)%aa(j)%ro(k)%split,stat=ncheck)
              if(ncheck/=0) stop '1Fehler beim Deallokieren. Programmabbruch'
            end do
            deallocate(pos(i)%aa(j)%ro,stat=ncheck)
            if(ncheck/=0) stop '2Fehler beim Deallokieren. Programmabbruch' 
          end do
          deallocate(pos(i)%aa,stat=ncheck)
          if(ncheck/=0) stop '3Fehler beim Deallokieren. Programmabbruch' 
        end do
        deallocate(pos,stat=ncheck)
        if(ncheck/=0) stop '4Fehler beim Deallokieren. Programmabbruch'

        end if
!
	if (allocated(mol)) then
!
        do i=1,size(mol)
          do j=1,mol(i)%mol_nrs
            do k=1,mol(i)%res(j)%res_naa
               do l=1, mol(i)%res(j)%res_aas(k)%aa_nrt
  		 deallocate(mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats,stat=ncheck)
		   if(ncheck/=0) stop '7Fehler beim Deallokieren. Programmabbruch'
               end do
	       deallocate(mol(i)%res(j)%res_aas(k)%aa_rots,stat=ncheck)
		   if(ncheck/=0) stop '8Fehler beim Deallokieren. Programmabbruch'
             end do
	     deallocate(mol(i)%res(j)%res_aas,stat=ncheck)
		   if(ncheck/=0) stop '9Fehler beim Deallokieren. Programmabbruch'
           end do
	   deallocate(mol(i)%res,stat=ncheck)
		   if(ncheck/=0) stop '10Fehler beim Deallokieren. Programmabbruch'
        end do
	deallocate(mol,stat=ncheck)
	end if
!
!
	PRINT*, '  ... FINISHED CLEANING UP MEMORY '
	PRINT*, '        '
!
	END SUBROUTINE CLEAN_UP
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!
	SUBROUTINE add_dee_pair(rotamer, res, aa, rot)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	  USE MOLEC_M

	  IMPLICIT NONE
	  INTEGER,         INTENT (in)    :: res
	  INTEGER,         INTENT (in)    :: aa
	  INTEGER,         INTENT (in)    :: rot
	  TYPE(ROTAMER_T), INTENT (inout) :: rotamer
	  INTEGER,         PARAMETER      :: free_space = 10
	  INTEGER                         :: cur_tip
	  LOGICAL                         :: first_call
	  INTEGER,SAVE                    :: call_number

	  call_number = call_number + 1
	  first_call = .false.
	  IF (.NOT.(ALLOCATED(rotamer%dee_friends%pair_list)))THEN
	    first_call = .true.
	    CALL resize_pair_list(rotamer%dee_friends, free_space, first_call)
	  END IF
	  first_call = .false.
	  IF (rotamer%dee_friends%tip == rotamer%dee_friends%max_size)THEN
	    CALL resize_pair_list(rotamer%dee_friends, free_space, first_call)
	  END IF

	  rotamer%dee_friends%tip = rotamer%dee_friends%tip + 1
	  cur_tip = rotamer%dee_friends%tip
!	  print *,'feldposition in routine add_dee_pair ', cur_tip
!	  print *, 'call_number ', call_number
	  rotamer%dee_friends%pair_list(cur_tip)%dee_rot(1) = res
	  rotamer%dee_friends%pair_list(cur_tip)%dee_rot(2) = aa
	  rotamer%dee_friends%pair_list(cur_tip)%dee_rot(3) = rot
!
	END SUBROUTINE add_dee_pair
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE resize_pair_list(list_in, free_space, first_call)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	  USE MOLEC_M

	  IMPLICIT NONE
	  TYPE(PAIR_LIST_T), INTENT(inout) :: list_in
	  TYPE(PAIR_LIST_T)                :: list_temp
	  INTEGER,           INTENT(in)    :: free_space
	  LOGICAL,           INTENT(in)    :: first_call
	!  print *, 'resize aufgerufen'
	  IF(first_call) THEN
	      ALLOCATE(list_in%pair_list(free_space))
	      list_in%max_size = free_space
	      list_in%tip = 0 
	      RETURN
	  END IF
          IF (ALLOCATED(list_temp%pair_list)) THEN
	      DEALLOCATE(list_temp%pair_list)
	  END IF
	      ALLOCATE(list_temp%pair_list(list_in%max_size))
	      list_temp%pair_list(1:list_in%max_size) = list_in%pair_list(1:list_in%max_size)
	      list_temp%max_size = list_in%max_size
	      list_temp%tip = list_in%tip
	      DEALLOCATE(list_in%pair_list)
	      list_in%max_size = list_in%max_size + free_space
	      ALLOCATE(list_in%pair_list(list_in%max_size))
	      list_in%pair_list(1:list_in%tip) = list_temp%pair_list(1:list_temp%tip) 
	      DEALLOCATE(list_temp%pair_list)
!
	END SUBROUTINE resize_pair_list
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE fit_pair_list(list_in)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	  USE MOLEC_M

	  IMPLICIT NONE
	  TYPE(PAIR_LIST_T), INTENT(inout) :: list_in
	  TYPE(PAIR_LIST_T)             :: list_temp

        IF (ALLOCATED(list_temp%pair_list)) THEN
	      DEALLOCATE(list_temp%pair_list)
	END IF
	      ALLOCATE(list_temp%pair_list(list_in%tip))
              list_temp%tip = list_in%tip
              list_temp%max_size = list_in%tip
	      list_temp%pair_list(1:list_temp%max_size) = list_in%pair_list(1:list_temp%max_size)
	      DEALLOCATE(list_in%pair_list)
	      list_in%max_size = list_in%tip
	      ALLOCATE(list_in%pair_list(list_in%max_size))
	      list_in%pair_list(1:list_in%tip) = list_temp%pair_list(1:list_temp%tip) 
	      DEALLOCATE(list_temp%pair_list)
!
	END SUBROUTINE fit_pair_list
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
! 	THE SUBROUTINES BELOW DO NOT USE THE MODULE MOLEC_M AND CAN BE 
!	COMPILED SEPARATELY
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!
	subroutine maxwords(word,nw)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	IMPLICIT NONE
	integer       nw, i, j, k
	integer       nmaxchar, nwlength
	character*42  max_abc, min_abc
	character*80  new, word(nw)
!
	data nmaxchar/42/
	data nwlength/80/
!
	data max_abc/'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.-/!$_'/
	data min_abc/'abcdefghijklmnopqrstuvwxyz0123456789.-/!$_'/
!	              1234567890123456789012345678901234567890123
!
	do i=1,nw
	  do j=1,nwlength
            new(j:j)=' '
	  end do
 	  new=word(i)
	  do j=1,nwlength
	     do k=1,nmaxchar
	        if ( new(j:j)== min_abc(k:k)) then
                     new(j:j)= max_abc(k:k)
	        end if
             end do
	  end do
	  word(i)=new
	end do
!	
	END SUBROUTINE maxwords
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE get_nresid(filenm,nr,nmax,cid)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	IMPLICIT NONE
	character(LEN=1000) :: filenm
	integer :: nr, n5, nold, nw, i, nmax, io_err, ncode
	character(LEN=132) :: string
	character(LEN=80), dimension(40) :: word
	character(LEN=80) :: boff
	character(LEN=1) :: cold= ' ', cnow= ' ', cid
	LOGICAL	:: chainid_flag
!	
	open(14,file=filenm,status='old',form='formatted')
!
	nr=0
	nold=nmax
	ncode=0
        cid=' '
!
		do i=1, nmax
			read(14,fmt='(a)',end=60) string
!
			if (i.eq.nmax) then
	PRINT*, '    '
	PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
	PRINT*, '>>>>>>>  END-OF-FILE NOT REACHED:                    '
	PRINT*, '>>>>>>> ', filenm(1:len_trim(filenm))
	PRINT*, '>>>>>>>  IN SUBROUTINE GET_NRESID                    '
	PRINT*, '>>>>>>>  POSSIBLY INCREASE MAX_LINES_INPUT           '
	PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
	PRINT*, '    '
	STOP
			end if
!
	if (string(1:6)=='ATOM  '.or.string(1:6)=='HETATM') then
!
			call parser(string, word, nw)
			chainid_flag=.false.
			call chainid_present(word(5),chainid_flag)
!
			if (chainid_flag) then
			        boff = word(5)
				cnow = boff(1:1)
				cid  = cnow
				read(word(6),*,iostat=io_err) n5
				if (io_err > 0 ) then
				     ncode=3
				     call flame(string,ncode)
				end if
!				
					if (n5 /= nold .or. cnow /= cold) then
				 		nold = n5
						cold = cnow   
						nr = nr+1
					end if
!
			else if (.not.chainid_flag) then
!
				read(word(5),*,iostat=io_err) n5
				if (io_err > 0 ) then
				     ncode=3
				     call flame(string,ncode)
				end if
				if (n5 /= nold) then
				 	nold = n5   
					nr = nr+1
				end if
!
			end if
!
	 end if
		end do
60	continue
!
	close(14)
!
	END SUBROUTINE get_nresid
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE chainid_present(text,id_flag)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	IMPLICIT NONE
	CHARACTER(LEN=80) :: text
	CHARACTER(LEN=1) :: letter
	LOGICAL  ::   id_flag
	CHARACTER(LEN=26) :: maxabc=  'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
	CHARACTER(LEN=26) :: minabc=  'abcdefghijklmnopqrstuvwxyz'
	INTEGER :: I
!
	letter=text(1:1)
	id_flag=.false.
	do i = 1, 26
	  if (letter == maxabc(i:i)) then
		id_flag=.true.
	  else if (letter == minabc(i:i)) then
		id_flag=.true.
	  end if
	end do
!
	END SUBROUTINE chainid_present
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE get_natom(filenm,nr,natom,cnam,nmax)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	IMPLICIT NONE
	integer :: nr, i, nw, n5, nat, nx, nmax
	integer :: nold, io_err, ncode
	logical :: chainid_flag
	integer, dimension(nr) :: natom
	character(LEN=4), dimension(nr) :: cnam
	character(LEN=80):: boff, baff
	character(LEN=1000) :: filenm
	character(LEN=132) :: string
	character(LEN=80), dimension(40) :: word
	character(LEN=1) :: cold=' ', cnow=' '
!
	open(14,file=filenm,status='old',form='formatted')
!
	do i=1,nr
	     natom(i)=0
	     cnam(i)='    '
	end do
!
	nold= nmax
	nx = 0
	nat = 0
	ncode = 0
!
	do i=1,nmax
		read(14,fmt='(a)',end=60) string
!
			if (i.eq.nmax) then
	PRINT*, '    '
	PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
	PRINT*, '>>>>>>>  END-OF-FILE NOT REACHED:                    '
	PRINT*, '>>>>>>> ', filenm(1:len_trim(filenm))
	PRINT*, '>>>>>>>  IN SUBROUTINE GET_NATM                      '
	PRINT*, '>>>>>>>  POSSIBLY INCREASE MAX_LINES_INPUT           '
	PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
	PRINT*, '    '
	STOP
			end if
!	
		if (string(1:6)=='ATOM  '.or.string(1:6)=='HETATM') then
!
		chainid_flag= .false.
		call parser(string, word, nw)
		call chainid_present(word(5),chainid_flag)
!
		if (chainid_flag) then
!
			boff= word(4)
			baff= word(5)
			cnow= baff(1:1)
			read(word(6),*,iostat=io_err) n5
!
			if (io_err > 0 ) then
			     ncode=3
			     call flame(string,ncode)
			end if
!
			if (n5 /= nold .or. cnow /= cold) then
				nold=n5
				cold=cnow   
				nx=nx+1
				nat=1
				natom(nx)=nat
				cnam(nx)=boff(1:4)
!
			else if (n5 == nold .and. cnow == cold) then
				nat=nat+1
				natom(nx)=nat
!			
			else 
				STOP 'SOMETHING TERRIBLY WRONG IN GET_NATOM'
!
			end if
!
	      else if (.not.chainid_flag) then
!
			read(word(5),*,iostat=io_err) n5
!			
			if (io_err > 0 ) then
			     ncode=3
			     call flame(string,ncode)
			end if
!
			boff=word(4)
!
			if (n5 /= nold) then
				nold=n5   
				nx=nx+1
				nat=1
				natom(nx)=nat
				cnam(nx)=boff(1:4)
!
			else if (n5 == nold) then
				nat=nat+1
				natom(nx)=nat
!			
			end if
!
		end if
	 end if
	end do
!
60 	continue
	close(14)
!
	END SUBROUTINE get_natom
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	subroutine get_mcinfo (maxnamc,maxnpo,natmc,npos)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
        IMPLICIT NONE
	integer		maxnamc, maxnpo
	integer		natmc, npos
!
	character*1000  filenm(5)
	character*132   string
	character*80    word(40), baff
	character*4     atom
	integer         na,nb,nw, nrnm, nrnm_old
	integer         i,j
	character*1     chid, chid_old
	logical         chainid_flag
!
	na=0
	call get_filnms(na,filenm)
!
!	GET NUMBER OF POSITIONS TO BE MUMBOED FROM MC-ATOM-FILE
! 
	open(14,file=filenm(1),status='old',form='formatted')
	nb=maxnpo*7
!
	npos=0
	nrnm=0
	nrnm_old=0
	chid='0'
	chid_old='0'
!
	do 10, i=1,nb
		read(14,'(a)',end=20) string
		call parser(string,word,nw)
		call chainid_present(word(5),chainid_flag)
		if (chainid_flag) then
		    baff=word(5)
		    chid=baff(1:1)
		    do j=6,40
		        word(j-1)=word(j)
		    end do
		else if (.not.chainid_flag) then
		    chid=' '
		end if
!
		read (word(5),*) nrnm
		if (nrnm.ne.nrnm_old.or.chid.ne.chid_old) then
		     npos=npos+1
		end if
		nrnm_old=nrnm
		chid_old=chid
10	continue
20	continue
!
!	print*,npos
	close(14)
!
!	GET TOTAL NUMBER OF ATOMS IN MAIN-CHAIN /CONSTANT PORTION
!
	open(14,file=filenm(2),status='old',form='formatted')
!
	natmc=0
!
	do 30, i=1,maxnamc
		read(14,'(a)',end=40) atom
		natmc=natmc+1
30	continue
40	continue
!
	close(14)
!
	return
	end subroutine get_mcinfo
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	subroutine get_filnms (nmn,filenm)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	IMPLICIT NONE
	character*1000 	filenm(5), boff, prefix
	integer		nmn, n1, i
	character*20      word
!	
	do 5, i=1,1000
  	      boff(i:i)=' '
5	continue
!
	do 10, i=1,5
		filenm(i)=boff
10	continue
!
	call getcwd(prefix)
	n1=len_trim(prefix)
!
	if (nmn.eq.0) then
	      filenm(1)= prefix(1:n1) // '/0_mch_sum '
	      filenm(2)= prefix(1:n1) // '/0_atm_sum '
!
	else if (nmn.gt.0.and.nmn.le.9) then
		word=' _res_sum           '
		write(word(1:1),'(i1)') nmn
		filenm(1)=prefix(1:n1) // '/' // word
!
		word=' _rot_sum           '
		write(word(1:1),'(i1)') nmn
		filenm(2)=prefix(1:n1) // '/' // word
!
		word=' _atm_sum           '
		write(word(1:1),'(i1)') nmn
		filenm(3)=prefix(1:n1) // '/' // word
!
	else if (nmn.gt.9.and.nmn.le.99) then
		word='  _res_sum          '
		write(word(1:2),'(i2)') nmn
		filenm(1)=prefix(1:n1) // '/' // word
!
		word='  _rot_sum          '
		write(word(1:2),'(i2)') nmn
		filenm(2)=prefix(1:n1) // '/' // word
!
		word='  _atm_sum          '
		write(word(1:2),'(i2)') nmn
		filenm(3)=prefix(1:n1) // '/' // word
!
	else if (nmn.gt.99.and.nmn.le.999) then
		word='  _res_sum          '
		write(word(1:3),'(i3)') nmn
		filenm(1)=prefix(1:n1) // '/' // word
!
		word='  _rot_sum          '
		write(word(1:3),'(i3)') nmn
		filenm(2)=prefix(1:n1) // '/' // word
!
		word='  _atm_sum          '
		write(word(1:3),'(i3)') nmn
		filenm(3)=prefix(1:n1) // '/' // word
!
	else if (nmn.gt.999.and.nmn.le.9999) then
		word='  _res_sum          '
		write(word(1:4),'(i4)') nmn
		filenm(1)=prefix(1:n1) // '/' // word
!
		word='  _rot_sum          '
		write(word(1:4),'(i4)') nmn
		filenm(2)=prefix(1:n1) // '/' // word
!
		word='  _atm_sum          '
		write(word(1:4),'(i4)') nmn
		filenm(3)=prefix(1:n1) // '/' // word
!
	else
!
	PRINT*, '    '
	PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>  '
	PRINT*, '>>>>>>>  ERROR in GET_FILNMS                           '
	PRINT*, '>>>>>>>  ASKING FOR MORE THEN 9999 DIFFERENT FILENAMES '
	PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
	PRINT*, '    '
!
	end if
!
	return
	end
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	subroutine flame(line,ncode)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	IMPLICIT NONE
	character*132 line
	integer       ncode
!
	PRINT*, '    '
	PRINT*, '    '
        PRINT*, ' PLEASE NOTE: '
	PRINT*, '    Most problems with MUMBO occur while reading the original PDB file.        '
	PRINT*, '    MUMBO expects all items in the PDB file to be separated by at least one    '
	PRINT*, '    blank/empty space. Therfore MUMBO does NOT LIKE the presence of alternative'
	PRINT*, '    conformations as in:                                                       '
	PRINT*, '    ATOM     47  OD1AASP A  63     -11.378  32.034   0.167  0.50 16.89         '
	PRINT*, '    or residue numbers > 999 as in:                                            '
	PRINT*, '    ATOM     47  OD1 ASP A1001     -11.378  32.034   0.167  1.00 16.89         '
	PRINT*, '    or B-factors higher then 99.99 as in:                                      '
	PRINT*, '    ATOM     47  OD1 ASP A  63     -11.378  32.034   0.167  1.00120.00         '
	PRINT*, '    and so forth.                                                              '
	PRINT*, '    For reasons unknown to the authors, MUMBO will not accept negative         '
	PRINT*, '    residue numbers. If MUMBO stops before the next step then please take a    '
	PRINT*, '    close look at the input PDB file.                                          '
	PRINT*, '        '
	PRINT*, '    '
	PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>  '
	PRINT*, '>>>>>>>  ERROR DURING PDB READING                                         '
	PRINT*, '>>>>>>>  THE ERROR IS MOST LIKELY CAUSED BY THE FOLLOWING PDB INPUT LINE  '
	PRINT*, '    '
	PRINT*, line
	if (ncode==1) then
!	PRINT*, 'ATOM     33  CB  LEU A  27     -46.502  25.309   6.313  1.00 60.28           C'
	PRINT*, '             ///\\\                                                       '
	PRINT*, '>>>>>> LIKELY PROBLEM: PRESENCE OF ALTERNATIVE SIDE CHAIN ORIENTATIONS IN '
	PRINT*, '>>>>>> FILE. PLEASE DELETE ALTERNATIVE ORIENTATIONS IN INPUT FILE.        '
	PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>  '
!	
	else if (ncode==2) then
!	PRINT*, 'ATOM     33  CB  LEU A  27     -46.502  25.309   6.313  1.00 60.28           C'
	PRINT*, '                   ///\\\                                                 '
	PRINT*, '>>>>>> LIKELY PROBLEM: COULD NOT PROPERLY DETERMINE WETHER A CHAIN ID IS  '
	PRINT*, '>>>>>> PRESENT OR NOT.                                                    '
	PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>  '
!	
	else if (ncode==3) then
!	PRINT*, 'ATOM     33  CB  LEU A  27     -46.502  25.309   6.313  1.00 60.28           C'
	PRINT*, '                     ///\\\                                               '
	PRINT*, '>>>>>> LIKELY PROBLEM: COULD NOT PROPERLY READ RESIDUE NUMBER             '
	PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>  '
!	
	else if (ncode==4) then
!	PRINT*, 'ATOM     33  CB  LEU A  27     -46.502  25.309   6.313  1.00 60.28           C'
	PRINT*, '               ///\\\                                                     '
	PRINT*, '>>>>>> LIKELY PROBLEM: RESIDUE NAME HAS MORE THAN 4 CHARACTERS            '
	PRINT*, '>>>>>> NOT MORE THAN 4 CHARACTERS ARE EXPECTED.                           '
	PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>  '
!
	else if (ncode==5) then
!	PRINT*, 'ATOM     33  CB  LEU A  27     -46.502  25.309   6.313  1.00 60.28           C'
	PRINT*, '           ///\\\                                                         '
	PRINT*, '>>>>>> LIKELY PROBLEM: ATOM NAME HAS MORE THAN 4 CHARACTERS               '
	PRINT*, '>>>>>> NOT MORE THAN 4 CHARACTERS ARE EXPECTED.                           '
	PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>  '
!
	else if (ncode==6) then
!	PRINT*, 'ATOM     33  CB  LEU A  27     -46.502  25.309   6.313  1.00 60.28           C'
	PRINT*, '                                ///\\\                                    '
	PRINT*, '>>>>>> LIKELY PROBLEM: COULD NOT PROPERLY READ X-COORDINATE               '
	PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>  '
!	
	else if (ncode==7) then
!	PRINT*, 'ATOM     33  CB  LEU A  27     -46.502  25.309   6.313  1.00 60.28           C'
	PRINT*, '                                         ///\\\                           '
	PRINT*, '>>>>>> LIKELY PROBLEM: COULD NOT PROPERLY READ Y-COORDINATE               '
	PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>  '
!	
	else if (ncode==8) then
!	PRINT*, 'ATOM     33  CB  LEU A  27     -46.502  25.309   6.313  1.00 60.28           C'
	PRINT*, '                                                 ///\\\                   '
	PRINT*, '>>>>>> LIKELY PROBLEM: COULD NOT PROPERLY READ Z-COORDINATE               '
	PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>  '
!
	else if (ncode==9) then
!	PRINT*, 'ATOM     33  CB  LEU A  27     -46.502  25.309   6.313  1.00 60.28           C'
	PRINT*, '                                                       ///\\\             '
	PRINT*, '>>>>>> LIKELY PROBLEM: COULD NOT PROPERLY READ OCCUPANCIES                '
	PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>  '
!	
	else if (ncode==10) then
!	PRINT*, 'ATOM     33  CB  LEU A  27     -46.502  25.309   6.313  1.00 60.28           C'
	PRINT*, '                                                            ///\\\        '
	PRINT*, '>>>>>> LIKELY PROBLEM: COULD NOT PROPERLY READ B-FACTORS                  '
	PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>  '
!	
!	else if (ncode==99) then
!	PRINT*, 'ATOM     33  CB  LEU A  27     -46.502  25.309   6.313  1.00 60.28           C'
!	PRINT*, '                                                 ///\\\                   '
!	PRINT*, '>>>>>> LIKELY PROBLEM: COULD NOT PROPERLY READ Z-COORDINATE               '
!	PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>  '
	
	else if (ncode==99) then
	
	end if
!
!
	stop
	end subroutine flame
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	subroutine parser(line,word,nw)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
!
	IMPLICIT NONE
	character*1   char,cboff
	integer       nw, nch, i, j
	integer       nmaxchar, nwlength, nllength
	logical lchar, lword
!
	data nmaxchar/42/
	data nwlength/80/
	data nllength/132/
	character*42  max_abc, min_abc
	character*132 line
	character*80  new, word(40)
!
	data max_abc/'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.-/!$_'/
	data min_abc/'abcdefghijklmnopqrstuvwxyz0123456789.-/!$_'/
!                     1234567890123456789012345678901234567890123
!
	cboff=' '
!
	do 10, j=1,nwlength
		new(j:j)=cboff
10	continue
!
	do 20, i=1,40
		word(i)=new
20       continue
!
	nw=0
	nch=0
	lword=.true.
	lchar=.false.
!
	do 30,i=1,nllength
		read(line(i:i),'(a)',end=60) char
		if (char.eq.' '.or.char.eq.'=') then
		   lword=.true.
		   lchar=.false.
		   nch=0
		else
		   do 40,j=1,nmaxchar
		      if (char.eq.max_abc(j:j).or.char.eq.min_abc(j:j)) then
		         lchar=.true.
		      end if
40		   continue
		end if
!
		if (lchar)then
		    if(lword) then
				do 50, j=1,nwlength
					new(j:j)=cboff
50				continue
			nw=nw+1
			nch=nch+1
 	 		new(nch:nch)=char
			word(nw)=new
			lword=.false.
		    else 
			nch=nch+1
 	 		new(nch:nch)=char
			word(nw)=new
		    end if
		end if
!
30	continue
60	continue
!	
	return
	end
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	subroutine parser2(line,word,nw)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
!
	IMPLICIT NONE
	character*1   char,cboff
	integer       nw, nch, i, j
	integer       nmaxchar, nwlength, nllength
	logical lchar, lword
!
	data nmaxchar/42/
	data nwlength/100/
	data nllength/300/
	character*42  max_abc, min_abc
	character*300 line
	character*100  new, word(100)
!
	data max_abc/'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.-/!$_'/
	data min_abc/'abcdefghijklmnopqrstuvwxyz0123456789.-/!$_'/
!                     1234567890123456789012345678901234567890123
!
	cboff=' '
!
	do 10, j=1,nwlength
		new(j:j)=cboff
10	continue
!
	do 20, i=1,80
		word(i)=new
20       continue
!
	nw=0
	nch=0
	lword=.true.
	lchar=.false.
!
	do 30,i=1,nllength
		read(line(i:i),'(a)',end=60) char
		if (char.eq.' '.or.char.eq.'=') then
		   lword=.true.
		   lchar=.false.
		   nch=0
		else
		   do 40,j=1,nmaxchar
		      if (char.eq.max_abc(j:j).or.char.eq.min_abc(j:j)) then
		         lchar=.true.
		      end if
40		   continue
		end if
!
		if (lchar)then
		    if(lword) then
				do 50, j=1,nwlength
					new(j:j)=cboff
50				continue
			nw=nw+1
			nch=nch+1
 	 		new(nch:nch)=char
			word(nw)=new
			lword=.false.
		    else 
			nch=nch+1
 	 		new(nch:nch)=char
			word(nw)=new
		    end if
		end if
!
30	continue
60	continue
!	
	return
	end
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	subroutine build_atom(yat1,yat2,yat3,yat4,ybln,ybng,ybdh)
!
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
!
!	BEFORE ATOM 4 IS BUILT, ATOM 1,2 AND 3 ARE ALIGNED SUCH THAT
!
!	ATOM 1 IS TRANSLATED ONTO THE ORIGINE (NX=0, NY=0, NZ=0)
!	ATOM 2 PLACED ONTO THE -X AXIS (NX < 0, NY=0, NZ=0) 
!	ATOM 3 IN THE XZ PLANE (NX > 0, NY = 0, NZ > 0)
!	
!	ATOM TO BE BUILT IS ATTACHED TO ATOM 2 AND BACKTRANSFORMED COORDINATES RETURNED
!
!	                      Z
!	                     /|\ 
!	             4        |   3
!	              \       | /  
!	               \      |/
!	                2-----1-----> x
!	                      |
!	                      |
!
!
	IMPLICIT NONE
	real*4 xat1(3),xat2(3),xat3(3),xat4(3)
	real*4 yat1(3),yat2(3),yat3(3),yat4(3)
	real*4 ybln,ybng,ybdh
	real*4 unit(3,3),rmat(3,3),tra(3),notra(3)
	real*4 rphi, rchi, dx, dyz, pi
	integer i,j
	data   notra/0.0,0.0,0.0/
	data   pi/3.1415927/
!
!	TRANSFER COORDINATES SO NOT TO CHANGE THEM
!
	do 10, i=1,3
		xat1(i) = yat1(i)
		xat2(i) = yat2(i)
		xat3(i) = yat3(i)
10    	continue
!
	call align_plane(xat1,xat2,xat3,rmat,tra)
!
	call inimat(unit)
!	 
	call applytransform(xat1,unit,tra)
	call applytransform(xat2,unit,tra)
	call applytransform(xat3,unit,tra)
!
 	call applytransform(xat1,rmat,notra)
	call applytransform(xat2,rmat,notra)
	call applytransform(xat3,rmat,notra)
!
!	GENERATE COORDINATES FOR NEW ATOM
!
	rphi= (ybng * pi)/180
	rchi= (ybdh * pi)/180
!
	dx=  cos(rphi)*ybln
	dyz= sin(rphi)*ybln
!
	xat4(1)=xat2(1)+dx
!
!	APPLYING DIHEDRAL, DIRECTION MIGHT BE WRONG!!!!!!!
!
!
	xat4(2)=dyz*sin(rchi)
	xat4(3)=dyz*cos(rchi)
!
	call invomat(rmat)             
!
	do 20,j=1,3
		tra(j)= -tra(j)	
20	continue	
!
	call applytransform(xat4,rmat,tra)
!
!	TRANSFER COORDINATES	
	do 30, i=1,3
		yat4(i)=xat4(i)
30      continue
	return
	end
!
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	subroutine align_plane(yat1,yat2,yat3,rmat,tra)
!
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
!
!	CALCULATES THE TRANSLATION AND MATRIX NEEDED TO ORIENTE THREE ATOMS SUCH
!	THAT:
!	ATOM 1 IS TRANSLATED ONTO THE ORIGINE (NX=0, NY=0, NZ=0)
!	ATOM 2 PLACED ONTO THE -X AXIS (NX < 0, NY=0, NZ=0) 
!	ATOM 3 IN THE XZ PLANE (NX > 0, NY = 0, NZ > 0)
!
!	                      Z
!	                     /|\ 
!	                      |   3
!	                      | /  
!	                      |/
!	                2-----1-----> x
!	                      |
!	                      |
!
!	CALLS FOR THE SUBROUTINES:
!	INIMAT, APPLYTRANSFORM, MATMULT
!
	IMPLICIT NONE
	real, dimension(3)  ::  yat1, yat2, yat3, yac
	real, dimension(3)  ::  xat1, xat2, xat3, tra
	real, dimension(3,3) ::  rta, rtb, rtc, rtd, rmat  
	real :: tx, ty, tz, th, tq, phi
	real, parameter :: pi = 3.141592654
	integer ::  i, nq
	real, parameter :: err7= 0.0000001 
!
	yac(1) = 0.0
	yac(2) = 0.0
	yac(3) = 0.0
!
!	TRANSFER COORDINATES	
	do i=1,3
		xat1(i) = yat1(i)
		xat2(i) = yat2(i)
		xat3(i) = yat3(i)
	end do
!
	call inimat(rta)
!
!	TRANSLATION vector from atom1
!
	do i=1,3
		tra(i) = -xat1(i)
	end do
!
	call applytransform(xat1,rta,tra)
	call applytransform(xat2,rta,tra)
	call applytransform(xat3,rta,tra)
!
!	FIRST ROTATION AROUND X ONTO X,Y PLANE WITH Y >= 0
!
	ty=xat2(2)
	tz=xat2(3)
	th=sqrt(ty**2+tz**2) 
!
!	PROBLEM IF ATOM2 ALREADY ALIGNED ALONG X THEN th.eq.0 
!
!	if (th.eq.0) then 
        if (abs(th).le.err7) then
          phi=0.0
	else
!	   tq=  (ty/th) * 1000000
!	   nq=  nint(tq)
!	   tq = nq
!	   tq = tq / 1000000
!          if (tq == 1) then
	   tq=  (ty/th) 
!	   if (tq == 1) then
	   if (abs(tq-1).lt.err7) then
	       phi = 0.0
!	   else if (tq == -1) then
	   else if (abs(tq+1).lt.err7) then
	       phi = pi
	   else
	       phi=acos(tq)
	   end if
	end if
!
	if (tz.le.0) then 
	   phi=(2*pi)-phi
	end if
!
	rta(1,1)=1
	rta(1,2)=0
	rta(1,3)=0
	rta(2,1)=0
	rta(2,2)=cos(phi)
	rta(2,3)=sin(phi)
	rta(3,1)=0
	rta(3,2)=-sin(phi)
	rta(3,3)=cos(phi)
!
	call applytransform(xat1,rta,yac)
	call applytransform(xat2,rta,yac)
	call applytransform(xat3,rta,yac)
!
!DEBUG	PRINT*, 'XAT21', xat2
!
	call inimat(rtb)
!
!	NO ROTATE ONTO -X DIRECTION
!
	tx=xat2(1)
	ty=xat2(2)
     	th=sqrt(tx**2+ty**2)
!
!	if (th.eq.0) then 
	if (abs(th).le.err7) then 
	    phi=0.0
	else
	   tq= (tx/th) 
!	   if (tq == 1) then
	   if (abs(tq-1).le.err7) then
	       phi = 0
!	   else if (tq == -1) then
	   else if (abs(tq+1).le.err7) then
	       phi = pi
	   else
	       phi=acos(tq)
	   end if
	end if
!
	if (ty.le.0) then 
	phi=(2*pi)-phi
	end if
!
	phi=phi+pi
!
!	ROTATION AROUND Z
!
	rtb(1,1)=cos(phi)
	rtb(1,2)=sin(phi)
	rtb(1,3)=0
	rtb(2,1)=-sin(phi)
	rtb(2,2)=cos(phi)
	rtb(2,3)=0
	rtb(3,1)=0
	rtb(3,2)=0
	rtb(3,3)=1
!
	call applytransform(xat1,rtb,yac)
	call applytransform(xat2,rtb,yac)
	call applytransform(xat3,rtb,yac)
!
!DEBUG	PRINT*, 'XAT22', xat2
!
	call inimat(rtc)
!
!	ROTATION AROUND X SO THAT 3 ATOM in X-Z PLANE
!
	ty=xat3(2)
	tz=xat3(3)
	th=sqrt(ty**2+tz**2)
!
	if (abs(th).lt.err7) then 
	    phi=0.0
	else
	   tq= (ty/th)
!	   if (tq == 1) then
	   if (abs(tq-1).lt.err7) then
	       phi = 0
!	   else if (tq == -1) then
	   else if (abs(tq+1).lt.err7) then
	       phi = pi
	   else
	       phi=acos(tq)
	   end if
	end if
!
	if (tz.le.0) then 
	phi=(2*pi)-phi
	end if
!
	phi=phi-(pi/2)
!
	rtc(1,1)= 1
	rtc(1,2)= 0
	rtc(1,3)= 0
	rtc(2,1)= 0
	rtc(2,2)= cos(phi)
	rtc(2,3)= sin(phi)
	rtc(3,1)= 0
	rtc(3,2)= -sin(phi)
	rtc(3,3)= cos(phi)
!
	call applytransform(xat1,rtc,yac)
	call applytransform(xat2,rtc,yac)
	call applytransform(xat3,rtc,yac)
!
!DEBUG	print*, 'XAT3', xat3
!
	call inimat(rmat)
	call inimat(rtd)
!
	call matmult(rtb,rta,rtd)
	call matmult(rtc,rtd,rmat)
!
	return
!
	end
!
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	subroutine inimat(umat)
!
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
!	INITIALISES A MATRIX AND GENERATES THE E-MATRIX
!
	IMPLICIT NONE
	real*4  umat(3,3)
	integer	i, j
!
	do 10, i=1,3
        do 20, j=1,3
	    if (i.eq.j) then 
		umat(i,j) = 1
	    else  
	      umat(i,j) = 0
	    end if
20	  continue
10	continue
!
	return
	end
!
!
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
 	subroutine applytransform(zat,zmat,ztra)
!
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
!	APPLIES A PROVIDED MATRIX AND TRANSLATION TO A SET OF COORDINATES
!
	IMPLICIT NONE
	real*4    zat(3),ztra(3),zmat(3,3),xt(3)
!
	xt(1)=(zmat(1,1)*zat(1)) + (zmat(1,2)*zat(2)) + (zmat(1,3)*zat(3))
	xt(2)=(zmat(2,1)*zat(1)) + (zmat(2,2)*zat(2)) + (zmat(2,3)*zat(3))
	xt(3)=(zmat(3,1)*zat(1)) + (zmat(3,2)*zat(2)) + (zmat(3,3)*zat(3))
!	
	zat(1)=xt(1)+ztra(1)
	zat(2)=xt(2)+ztra(2)
	zat(3)=xt(3)+ztra(3)
!
	return
	end
!
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	subroutine matmult(umat,vmat,wmat)
!
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
!	MULTIPLIES TWO MATRICES
!
	IMPLICIT NONE
	real*4  umat(3,3),vmat(3,3),wmat(3,3)
!
	wmat(1,1)=umat(1,1)*vmat(1,1)+umat(1,2)*vmat(2,1)+umat(1,3)*vmat(3,1)
	wmat(2,1)=umat(2,1)*vmat(1,1)+umat(2,2)*vmat(2,1)+umat(2,3)*vmat(3,1)
	wmat(3,1)=umat(3,1)*vmat(1,1)+umat(3,2)*vmat(2,1)+umat(3,3)*vmat(3,1)
!
	wmat(1,2)=umat(1,1)*vmat(1,2)+umat(1,2)*vmat(2,2)+umat(1,3)*vmat(3,2)
	wmat(2,2)=umat(2,1)*vmat(1,2)+umat(2,2)*vmat(2,2)+umat(2,3)*vmat(3,2)
	wmat(3,2)=umat(3,1)*vmat(1,2)+umat(3,2)*vmat(2,2)+umat(3,3)*vmat(3,2)
!
	wmat(1,3)=umat(1,1)*vmat(1,3)+umat(1,2)*vmat(2,3)+umat(1,3)*vmat(3,3)
	wmat(2,3)=umat(2,1)*vmat(1,3)+umat(2,2)*vmat(2,3)+umat(2,3)*vmat(3,3)
	wmat(3,3)=umat(3,1)*vmat(1,3)+umat(3,2)*vmat(2,3)+umat(3,3)*vmat(3,3)
!
	return
	end
!
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	subroutine invomat(tmat)             
!
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
!	INVOMAT INVERTS AN OTHOGONAL MATRIX AND CALCULATES THE TRANSPOSED MATRIX
!
	IMPLICIT NONE
	real*4  smat(3,3), tmat(3,3)
	integer i,j
!
	do 20, i=1,3
        do 10, j=1,3
	    smat(i,j)=tmat(j,i)
10	  continue
20	continue
!
	do 40, i=1,3
        do 30, j=1,3
	    tmat(i,j)=smat(i,j)
30	  continue
40	continue
!
	return
	end
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE ANGLE (d,e,f,ang)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	IMPLICIT NONE
	REAL, dimension(3), intent(in) :: d, e, f
	REAL, dimension(3)  :: a, b, c
	REAL :: ang, da, dc, dp, dq
	INTEGER :: i
!
	do i=1,3
	  a(i) = d(i) 
	  b(i) = e(i)
	  c(i) = f(i)
	end do
!
	do i=1,3
	  a(i) = a(i) - b(i)
	  c(i) = c(i) - b(i)
	end do
!
	da = 0
	dc = 0
	dp = 0
	dq = 0
!
	da=sqrt(sum(a**2))
	dc=sqrt(sum(c**2))
        dp=DOT_PRODUCT(a,c) 
!
	dq=dp/(da*dc)
	if (dq.gt.0.99999999)  dq = 0.99999999
	if (dq.lt.(-0.99999999)) dq = -0.99999999
!	
	ang = acos(dq)
!
	END SUBROUTINE ANGLE
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE VEKPRO(d,e,f,g)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	IMPLICIT NONE
	REAL, dimension(3), intent(in) :: d,e,f 
	REAL, dimension(3) :: a,b,c,g
	INTEGER :: i
!
	do i=1,3
	  a(i) = d(i) 
	  b(i) = e(i)
	  c(i) = f(i)
	end do
!
	do i=1,3
	  a(i) = a(i) - b(i)
	  c(i) = c(i) - b(i)	  
	end do
!
	g(1) = ( a(2)* c(3) ) - ( a(3)*c(2) )
	g(2) = ( a(3)* c(1) ) - ( a(1)*c(3) )
	g(3) = ( a(1)* c(2) ) - ( a(2)*c(1) )
!
!	print*, 'CALCULATING VECTOR PRODUCT'
!	print*, 'CAL_d ', d
!	print*, 'CAL_e ', e
!	print*, 'CAL_f ', f
!	print*, 'CAL_a ', a
!	print*, 'CAL_c ', c
!	print*, 'CAL_g ', g
!
!	write(*,'(a,3f8.3,a)')'ATOM      1  O   HOH A   1    ', &
!     &  a(1), a(2), a(3), '  1.00 99.99 '
!	write(*,'(a,3f8.3,a)')'ATOM      2  O   HOH A   2    ', &
!     &  c(1), c(2), c(3), '  1.00 99.99 '
!	write(*,'(a,3f8.3,a)')'ATOM      3  O   HOH A   3    ', &
!     &  b(1), b(2), b(3), '  1.00 99.99 '
!	write(*,'(a,3f8.3,a)')'ATOM      4  O   HOH A   4    ', &
!     &  g(1), g(2), g(3), '  1.00 99.99 '
!
!
	END SUBROUTINE VEKPRO
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE DIHEDRAL(e,f,g,h,chi)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	IMPLICIT NONE
	REAL, dimension(3), intent(in) :: e,f,g,h
	REAL, dimension(3) :: a,b,c,d
	REAL, intent(out) :: chi
	REAL :: dy, dz
	REAL, DIMENSION(3,3) :: rmat, umat
	REAL, DIMENSION(3)   :: tra,  notra
	REAL, PARAMETER :: pi = 3.1415927, err7=0.0000001
	INTEGER :: I 
!
	do i=1,3
	 a(i) = e(i)
	 b(i) = f(i)
	 c(i) = g(i)
	 d(i) = h(i)
	 notra(i)=0.0
	end do
!
	call align_plane(c,b,d,rmat,tra)
	call inimat(umat)
!
	call applytransform(a,umat,tra)
	call applytransform(a,rmat,notra)
	call applytransform(b,umat,tra)
	call applytransform(b,rmat,notra)
	call applytransform(c,umat,tra)
	call applytransform(c,rmat,notra)
	call applytransform(d,umat,tra)
	call applytransform(d,rmat,notra)
!
	dy= a(2)
	dz= a(3)
!
	if (abs(dz).lt.err7) then
	   if (dy > 0) then
	      chi= 90
	   else
	      chi= -90
	   end if
	else
	   chi=atan(dy/dz)
	   chi=(chi*180)/pi
	   if (dz > 0) then
		chi = chi
	   else if (dz < 0) then
	      chi = 180 + chi 
	   end if
	end if
!
	if (chi > 180) then
	   chi = chi - 360 
	end if
!
	if (chi < -180) then
	   chi = chi + 360 
	end if
!
	END SUBROUTINE DIHEDRAL
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE get_lflag(filenm,nr,natom,flag,xname) 
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	IMPLICIT NONE
	integer, dimension(nr) :: natom
	logical, dimension(nr) :: flag
	character(LEN=1000) :: filenm
	character(LEN=132) :: string
	character(LEN=80), dimension(40) :: word
	integer :: nr, nw, i, j, k
	logical :: chainid_flag
	character(LEN=4) :: xname
	character(LEN=80) :: boff
	
	open(14,file=filenm,status='old',form='formatted')
!
	do i=1,nr
	     flag(i)=.false.
	end do
	
	do i=1,nr
	   do j=1,natom(i)
		read(14,fmt='(a)',end=60) string
		if (string(1:6)=='ATOM  '.or.string(1:6)=='HETATM') then
		  call parser(string, word, nw)
		  do k=1,nw
		    boff=word(k)
	            if (boff(1:len_trim(boff))==xname(1:len_trim(xname))) then 
			 flag(i)=.true.
		    end if
		  end do 
                end if
           end do
        end do
!
60 	continue
	close(14)
!
	END SUBROUTINE get_lflag
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!@author  Yasemin Rudolph
!>
!> Kronecker delta
!> equals 1 for i==j, 0 else
!>
!> @note http://de.wikipedia.org/wiki/Kronecker-Delta
!>
!> @return                  Kronecker delta, i.e. 0 or 1
!> @param[in]       i       first index
!> @param[in]       j       second index
!>
FUNCTION delta(i,j)
IMPLICIT NONE
    INTEGER :: delta
    INTEGER, INTENT(IN) :: i, j

    IF( i == j ) THEN
        delta = 1
    ELSE
        delta = 0
    END IF

END FUNCTION delta
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!@author  Yasemin Rudolph
!>
!> Levi-Civita-symbol (totally anti-symmetric tensor of rank 3)
!> equals +1 for even permutations of i, j, k,
!>        -1 for uneven permutations, and
!>         0 else.
!>
!> @note http://de.wikipedia.org/wiki/Levi-Civita-Symbol
!>
!> @return                  Cevi-Livita-Symbol, i.e. -1, 0 or 1
!> @param[in]       i       first index
!> @param[in]       j       second index
!> @param[in]       k       third index
!>
FUNCTION levi(i,j,k)
IMPLICIT NONE
    INTEGER :: levi
    INTEGER, INTENT(IN) :: i, j, k

    levi = -(j-i)*(k-j)*(i-k)/2

END FUNCTION levi
!>
!> rotate a point around the axis given by two other points by a given angle
!>
!> @param[in]       a           1st point for determination of axis of rotation
!> @param[in]       b           2nd point for determination of axis of rotation
!> @param[in]       c           point(s) to rotate around the axis given by a and b
!> @param[in]       alpha_deg   angle of rotation
!> @param[out]      c_new       new coordinates of point(s) c after rotation by alpha_deg
!>
!> @remarks
!> matrix @f$ \mathbf{R} @f$ of rotation in tensorial notation is given by
!> @f[
!>  R_{ij} = u_i u_j + \cos(\alpha) (\delta_{ij} - u_i u_j) - \epsilon_{ijk} u_k \sin(\alpha)
!> @f]
!> whereas @f$ \mathbf{u} @f$ is the directional unit vector of the axis of rotation, @f$ \alpha @f$ is the angle of
!> rotation and @f$ \delta_{ij}, \epsilon_{ijk} @f$ are the Kronecker delta and Epsilon tensor respectively
!>
!> @note http://de.wikipedia.org/wiki/Drehmatrix
!>
!> @todo account for possibly degenerate points (not highly important as this should not happen really)
!>
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!>@author  Yasemin Rudolph
!
    SUBROUTINE backrub_calc(a, b, c, alpha_deg, c_new)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

IMPLICIT NONE

    REAL, DIMENSION(3), INTENT(IN) :: a, b
    REAL, DIMENSION(3), INTENT(IN) :: c
    REAL, INTENT(IN) :: alpha_deg
    REAL, DIMENSION(3), INTENT(OUT) :: c_new

    INTEGER :: delta                                 !< function Kronecker delta
    INTEGER :: levi                                  !< Levi-Civita-symbol

    REAL, PARAMETER :: PI = 3.141593                 !< obviously @f$ \pi = 3.14\ldots @f$
    REAL, PARAMETER :: DEG_TO_RAD = PI/180.          !< conversion factor for angles @f$ [deg \rightarrow rad] @f$

    REAL, DIMENSION(3) :: n                          !< unit vector ab
    REAL, DIMENSION(3) :: c_trans                    !< translated point of origin
    REAL :: alpha_rad                                !< angle of rotation in degrees

    REAL, DIMENSION(3,3) :: r, tmp                   !< matrix of rotation
    INTEGER :: i, j, k                               !< loop indices
    REAL :: eps                                      !< epsilon tensor


    ! conversion of angle from degrees to radians
    alpha_rad = DEG_TO_RAD * alpha_deg


    ! unit vector from a to b
    n = b - a
    n = n / SQRT(SUM(n**2))


    ! determination of rotation matrix with axis ab and angle alpha
    DO i=1, 3
      DO j=1, 3

        eps = 0
        DO k=1, 3
          eps = eps + levi(i,j,k) * n(k)
        END DO

        r(i,j) = n(i)*n(j) + COS(alpha_rad) * (delta(i,j) - n(i)*n(j)) - SIN(alpha_rad) * eps
      END DO
    END DO

    tmp = r
    r = TRANSPOSE(tmp)


    ! translate point of origin to first point
    c_trans = c - a


    ! application of rotation matrix on point(s) c (i.e. actually rotating the point(s))
    DO i=1, 3
      c_new(i) = 0
      DO j=1, 3
        c_new(i) = c_new(i) + r(i,j) * c_trans(j)
      END DO
    END DO


    ! translate back to previous coord sys
    c_new = c_new + a


      END SUBROUTINE backrub_calc
!





