!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
      SUBROUTINE ANALYSE ()
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MOLEC_M
	USE MUMBO_DATA_M
	USE MAX_DATA_M
!
	IMPLICIT NONE
!     
      integer, dimension(:),     allocatable     :: num_nco
      logical, dimension(size(mol)-1)    :: new
      integer(kind=INT_LONG), dimension(:),allocatable:: ncomb_plus, ncomb_minus
      integer(kind=INT_LONG) :: ncomb_tot, AAN, BBN, CCN, i
      integer, dimension(:,:,:), allocatable     :: nco
!
      integer :: j,k, na, nao, nb, nbo ,n7, n8
      integer :: nmax, ncnt, ncut, nnpos
      integer (kind=INT_LONG) :: nct
!
      real, dimension(:,:), allocatable :: enlist
      integer, dimension(size(mol),4)       :: hkl      
      integer, dimension(size(mol),3:4)     :: hkl_old
      real            ::  entot, toten
      real            :: en_lowest = 1000000
      logical :: fflag
!
      real, dimension(max_npos)             :: en_up_to
!     
!     NUM_NCO(i) = NUMBER OF AA * ROTAMERS AT EACH POSITION (i)	
!     NMAX = MAX NUMBER ROTAMERS AT ANY POSITION
!     NCO(i, ncnt, 1-2) = STORES POINTERS FOR AA and ROTAMERS
!
      nnpos=size(mol)-1
      if (allocated(num_nco)) deallocate(num_nco)
      allocate(num_nco(nnpos))
      if (allocated(ncomb_plus)) deallocate(ncomb_plus)
      allocate(ncomb_plus(nnpos))
      if (allocated(ncomb_minus)) deallocate(ncomb_minus)
      allocate(ncomb_minus(nnpos))
!
!      allocate(num_nco(size(mol)-1))
!      allocate(ncomb_plus(size(mol)-1))
!      allocate(ncomb_minus(size(mol)-1))
!
      nmax=0
      fflag=.true.
!
      do i=1,nnpos
            num_nco(i)=0
            do j=1,mol(i+1)%res(1)%res_naa
               num_nco(i)=num_nco(i)+(mol(i+1)%res(1)%res_aas(j)%aa_nrt)
               if (nmax < num_nco(i)) then
                   nmax=num_nco(i)
               end if
               do k=1, mol(i+1)%res(1)%res_aas(j)%aa_nrt
!
         mol(i+1)%res(1)%res_aas(j)%aa_rots(k)%rot_flag=.false.
         mol(i+1)%res(1)%res_aas(j)%aa_rots(k)%rot_flag2=.false.
!
               end do
            end do
        end do
!
!      allocate(nco((size(num_nco)),nmax,2))
      allocate(nco(nnpos,nmax,2))
!
      do i=1,nnpos
            ncnt=0
            do j=1,mol(i+1)%res(1)%res_naa
                do k=1,mol(i+1)%res(1)%res_aas(j)%aa_nrt
               ncnt=ncnt+1
               nco(i,ncnt,1)=j
               nco(i,ncnt,2)=k
            end do
           end do
      end do
!
      ncomb_tot=1     
!
      do i=1, nnpos
             ncomb_minus(i)=  ncomb_tot
             ncomb_tot=       ncomb_tot*num_nco(i)
             ncomb_plus(i)=   ncomb_tot
      end do
!     
	PRINT*, '        '
	PRINT*, '################################'
	PRINT*, '#    SUMMARY ANALYSE           #'
	PRINT*, '################################'
	PRINT*, '          '
!	
	PRINT*, '          '
	PRINT*, '  NUMBER OF COMBINATIONS TO TEST: '
	PRINT*, '          '
	DO i=1, nnpos
	PRINT*, '  COMBINATION LIMITS:  ',ncomb_minus(i), ncomb_plus(i)
	END DO
	PRINT*, '      ' 
	PRINT*, '  TOTAL NUMBER OF COMBINATIONS TO TEST: ', ncomb_tot 
	PRINT*, '      ' 
	PRINT*, '  LIMIT SPECIFIED IN INPUT FOR TESTING: MAXCOMB = ',mumbo_ana_mxc
	PRINT*, '  LIMIT SPECIFIED FOR SORTING: MAXSORT = ',mumbo_ana_mxs 
	PRINT*, '  LIMIT SPECIFIED FOR GLOBAL ENERGY MINIMUM: EN_BWR = ',mumbo_en_bwr 
!
	if (ncomb_tot > mumbo_ana_mxc) then
!
	PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>    '
	PRINT*, '>> NUMBER OF COMBINATIONS > MAXCOMB                      '
	PRINT*, '>> YOU MAY INCREASE MAXCOMB, BUT BE AWARE OF INCREASE    '
	PRINT*, '>> OF COMPUTER TIME NEEDED                               '
	PRINT*, '>> OR RERUN BRUT WITH A LOWER ENERGY_BRUT, KEEP - VALUE  '
	PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>    '
!	
		stop
	end if
!
	allocate(enlist(mumbo_ana_mxs,2))
	do i= 1, mumbo_ana_mxs
	   enlist(i,1) = real(2 * ncomb_tot) 
	   enlist(i,2) = mumbo_en_bwr + 3 * abs(mumbo_en_bwr)
	end do   
!	
!
	do i=1,ncomb_tot
          do j=1,nnpos
!
              new(j)=.true.
!
              AAN = mod(i,ncomb_plus(j))
              if (AAN==0) then
                 AAN=ncomb_plus(j)
              end if
!
              BBN=int(AAN/(ncomb_minus(j)))
              CCN=mod(i,ncomb_minus(j))
                hkl(j,1)=j+1
                hkl(j,2)=1
              if (CCN > 0) then
                hkl(j,3)=nco(j,(BBN+1),1)
                hkl(j,4)=nco(j,(BBN+1),2)
              else
                hkl(j,3)=nco(j,(BBN),1)
                hkl(j,4)=nco(j,(BBN),2)
              end if
!
          end do
!
          do j=nnpos,1,-1
          na=hkl(j,3)
          nao=hkl_old(j,3)
          nb=hkl(j,4)
            nbo=hkl_old(j,4)
              if (na==nao.and.nb==nbo) then
                  new(j)=.false.
                  cycle
              else
                  goto 100
              end if
          end do
100      continue
!
          do j=1,nnpos
               hkl_old(j,3)=hkl(j,3)
               hkl_old(j,4)=hkl(j,4)
          end do      
!
          call tot_energy(hkl,new,entot,fflag,en_up_to)
          fflag=.false.
!          
          if (entot < en_lowest) then
            en_lowest = entot
          end if
!
	if (entot < mumbo_en_bwr) then 
	  ncut = mumbo_ana_mxs
	  do j = mumbo_ana_mxs,1,-1
	    if ( entot < enlist(j,2) ) then
	      ncut=j-1
	      cycle	
	    else 
	      exit
	    end if
	  end do
!
	  if (ncut <= (mumbo_ana_mxs-2)) then 
	     do j= mumbo_ana_mxs, (ncut+2), -1
		enlist(j,1) = enlist((j-1),1) 
		enlist(j,2) = enlist((j-1),2) 
	     end do
	  end if
!
	  if (ncut <= (mumbo_ana_mxs-1)) then 
		  enlist(ncut+1,1) = real(i)
		  enlist(ncut+1,2) = entot
	  end if
	end if 
!
	end do
!
!	do i = 1, mumbo_ana_mxs
!	   PRINT*, i, enlist(i,1), enlist(i,2)
!	end do
!
!	NOW STARTING TO ANALYSE THE VARIOUS ENERGIES
!	PER RESIDUE
!

!
	do i = 1, mumbo_ana_mxs
	  if (enlist(i,2) < mumbo_en_bwr) then
	    nct = int(enlist(i,1),kind=INT_LONG)
	    do j=1,nnpos
!
		AAN = mod(nct,ncomb_plus(j))
		if (AAN==0) then
		  AAN=ncomb_plus(j)
		end if
!
		BBN=int(AAN/(ncomb_minus(j)))
		CCN=mod(nct,ncomb_minus(j))
		hkl(j,1)=j+1
		hkl(j,2)=1
		if (CCN > 0) then
		   hkl(j,3)=nco(j,(BBN+1),1)
		   hkl(j,4)=nco(j,(BBN+1),2)
		else
		   hkl(j,3)=nco(j,(BBN),1)
		   hkl(j,4)=nco(j,(BBN),2)
		end if
!
 	    end do
!
!	HAVING DETERMINED THE CONFIGURATION, GET ALL THE ENERGIES AND WRITE TO FILE
!
		toten = enlist(i,2)
		call write_energies(toten,hkl)
!
            	do j=1,nnpos
            	    n7=hkl(j,3)
                    n8=hkl(j,4)
            	    mol(j+1)%res(1)%res_aas(n7)%aa_rots(n8)%rot_flag=.true.
            	end do
!
!	        FLAG THE BEST SOLUTION FOR WRITING
!
	        if (i==1) then
!
            do j=1,nnpos
            n7=hkl(j,3)
            n8=hkl(j,4)
            mol(j+1)%res(1)%res_aas(n7)%aa_rots(n8)%rot_flag2=.true.
            end do
!
		end if
!
	  else 
	    exit
	  end if 
!
	end do
!
	close(15)
	close(16)
	if (mumbo_log_ana_flag) close(17)
!
!
	if (en_lowest >  mumbo_en_bwr) then
            PRINT*,  '                                                  '
            PRINT*,  ' >>>>>>>>>>>>>>>>>>>>>>>>>>  BUMMER >>>>>>>>>>>>>>'
            PRINT*,  ' >>  NO COMBINATIONS FOUND WITH E < ',mumbo_en_bwr  
            PRINT*,  ' >>  LOWEST ENERGY FOUND = ', en_lowest 
            PRINT*,  ' >>                                               '
            PRINT*,  ' >>  NOTHING TO ANALAZE IN - ANA STEP -           ' 
            PRINT*,  ' >>  YOU MIGHT WANT TO INCREASE THE KEEP VALUE    '
            PRINT*,  ' >>  IN:  ENERGY_BRUT=   KEEP= -300               '
            PRINT*,  ' >>                                               '
            PRINT*,  ' >>  MUST STOP                                    '
            PRINT*,  ' >>>>>>>>>>>>>>>>>>>>>>>>>>  BUMMER >>>>>>>>>>>>>>'
            PRINT*,  '                                                  '
	    STOP
	end if
!
	PRINT*, '  ' 
	PRINT*, '  RESULTS WRITTEN TO FILE: mumbo.ene     '
	PRINT*, '  AND FILE: mumbo.lis ' 
	PRINT*, '  ' 
	PRINT*, '  ... FINISHED WITH ANALYSE   '
!
	END SUBROUTINE ANALYSE
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE LOAD_REF_STRUCT 
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MOLEC_M
	USE AA_DATA_M
	USE MUMBO_DATA_M
	USE MAX_DATA_M
!
	IMPLICIT NONE
!
	INTEGER :: i,j,k,l
	INTEGER :: n1, n2, n3
	INTEGER :: nres, ios, nn, nats, nw
	CHARACTER (LEN=132) :: string
	CHARACTER (LEN=80)  ::  word(40)
	CHARACTER (LEN=80)  ::  boff 
	CHARACTER (LEN=1)   ::  cid
	LOGICAL :: chainid_flag, flag
	REAL ::  xbfa, xx, xy, xz, xocc
!
	INTEGER, DIMENSION(:), allocatable :: natom
	CHARACTER(LEN=4), DIMENSION(:), allocatable :: cnam
	CHARACTER(LEN=4) :: aanm, atnm
	LOGICAL, DIMENSION(:), allocatable :: strange_flag

	if (allocated(natom)) deallocate (natom)
	if (allocated(cnam))  deallocate (cnam)
	if (allocated(strange_flag)) deallocate (strange_flag)
	if (allocated(ori))   deallocate (ori)
!
	PRINT*, '        '
	PRINT*, '#######################################'
	PRINT*, '# STARTING LOADING REFERENCE STRUCT   #'
	PRINT*, '#######################################'
	PRINT*, '        '
!
!	IF LIGAND OPTIMISATION REQUESTED THEN CREATE SPACE FOR LIGAND
!
	allocate (ori(1))
!
	call get_nresid(mumbo_ref_pdb,nres,max_lines_input,cid)
	allocate(natom(nres),cnam(nres),strange_flag(nres))
	call get_natom(mumbo_ref_pdb,nres,natom,cnam,max_lines_input)
!
	allocate (ori(1)%res(nres))
	ori(1)%mol_nrs=nres
!
	do,i=1,nres
	   allocate(ori(1)%res(i)%res_aas(1))
	   ori(1)%res(i)%res_naa=1
	   ori(1)%res(i)%res_mut=.false.
	   allocate(ori(1)%res(i)%res_aas(1)%aa_rots(1))
	   ori(1)%res(i)%res_aas(1)%aa_nrt=1
	   ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_prob=1.0
	end do
!
!	NOW GET RESIDUE NAMES FROM PDB-FILE
!	AND ALLOCATE RESIDUES
!
	open(unit=14,file=mumbo_ref_pdb,status='old',form='formatted')
!
	n1=0
	n2=0
	n3=0
        ReadPDBRef: do 
!
	   read(14,fmt='(a)',iostat=ios) string
           if (ios /= 0) exit
!
	   if (string(1:6)=='ATOM  ' .or. string(1:6)=='HETATM') then
	      n1=n1+1
	      n3=n3+1
	   else
	      cycle
	   end if
!
	   call parser(string, word, nw)
	   call chainid_present(word(5),chainid_flag)
!
	   if (n1==1) then
	     n2=n2+1
	   end if
!
	   if (chainid_flag) then
	      boff=word(5)
	      if (n1==1) then 
	          ori(1)%res(n2)%res_chid=boff(1:1)
	      end if
	      do k=6,40
	         word(k-1)=word(k)
	      end do
	   else if (.not.chainid_flag) then
	      if (n1==1) then 
	          ori(1)%res(n2)%res_chid=' '
	      end if
	   end if
!
	   if (n1==1) then
	     strange_flag(n2)=.true.
	     boff=word(4)
	     read(word(5),*) nn
	     ori(1)%res(n2)%res_aas(1)%aa_nam=boff(1:4)
	     ori(1)%res(n2)%res_aas(1)%aa_rots(1)%rot_flag= .false.
	     ori(1)%res(n2)%res_aas(1)%aa_rots(1)%rot_flag2=.false.
	     ori(1)%res(n2)%res_num=n2
	     ori(1)%res(n2)%res_nold=nn
!
	     aanm= ori(1)%res(n2)%res_aas(1)%aa_nam
	     do k=1,(size(ps))
	        if (ps(k)%p_aan == aanm) then
	          strange_flag(n2)=.false.
	          ori(1)%res(n2)%res_aas(1)%aa_rots(1)%rot_flag2=.true.
		  nats = ps(k)%p_at_ncnt
	          allocate(ori(1)%res(n2)%res_aas(1)%aa_rots(1)%rot_ats(nats))
	          ori(1)%res(n2)%res_aas(1)%aa_rots(1)%rot_nat=nats
	          do l=1, ps(k)%p_at_ncnt
	            atnm= ps(k)%p_at_nam(l) 
	            ori(1)%res(n2)%res_aas(1)%aa_rots(1)%rot_ats(l)%at_nam=atnm
	            ori(1)%res(n2)%res_aas(1)%aa_rots(1)%rot_ats(l)%at_flag=.false.
                  end do
	        end if
	     end do
!
! RESIDUES NOT PRESENT IN PARAMETER-FILE ARE ALLOCATED HERE
!
	     if (strange_flag(n2)) then
 	        allocate(ori(1)%res(n2)%res_aas(1)%aa_rots(1)%rot_ats(natom(n2)))
	        ori(1)%res(n2)%res_aas(1)%aa_rots(1)%rot_nat=natom(n2)
	        ori(1)%res(n2)%res_aas(1)%aa_rots(1)%rot_flag2=.false.
	     end if
	   end if
!
	   if (n1==natom(n2)) then
		n1=0
	   end if
	end do ReadPDBRef 
	close(14)
!
         PRINT *, '  NUMBER OF ATOMS FOUND IN REFERENCE PDB FILE   :', N3
         PRINT *, '  NUMBER OF RESIDUES FOUND IN REFERENCE PDB FILE:', N2
!
         if (n3 == 0) then
          PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>>'
          PRINT*, '>>>>>>>  FOUND NO ATOMS IN INPUT PDB               '
          PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>> '
          PRINT*, '    '
          stop
         endif
!
!	do, i=1,ori(1)%mol_nrs
!	 do j=1, ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_nat
!   	    ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_xyz(1) = 0
!           ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_xyz(2) = 0
!           ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_xyz(3) = 0
!           ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_occ   = 0
!           ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_bfa   = 0
!	 end do
!	end do
!
!
! CHECKING IF RESIDUE NUMBERS ARE UNIQUE IN INPUT PDB FILE
!
	do i=1,ori(1)%mol_nrs
	   do j= i,ori(1)%mol_nrs
	     if (i==j) then
	        cycle
	     else if (ori(1)%res(i)%res_nold == ori(1)%res(j)%res_nold) then

	        if (ori(1)%res(i)%res_chid == ori(1)%res(j)%res_chid) then

		PRINT*, '    '
		PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>> '
		PRINT*, '>>>>>>>  FOUND MORE THEN 1 RESIDUE WITH                '
		PRINT*, '>>>>>>>  RESIDUE NUMBER : ', ori(1)%res(i)%res_chid,   &
    &		    ori(1)%res(i)%res_nold
		PRINT*, '>>>>>>>  IN REFERNCE COORDINATE FILE                      '
		PRINT*, '>>>>>>>  MUST EXIT                                     '
		PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>>>'
		PRINT*, '    '

	        stop
	        end if
	     end if
	   end do
	end do
!
!
! NOW READ IN COORDINATES FROM PDB-FILE
!
	open(unit=14,file=mumbo_ref_pdb,status='old',form='formatted')
	PRINT*, '  READING IN REF PDB-FILE ..... '
!
	n1=0
	n2=0
        ReadPDBCoord: do 
	   read(14,fmt='(a)',iostat=ios) string
!
	   if (ios /=0 ) then
	      exit
	   end if
!
	   if (string(1:6)=='ATOM  ' .or. string(1:6)=='HETATM') then
	      n1=n1+1
	   else
	      cycle
	   end if
!
	   call parser(string, word, nw)
	   call chainid_present(word(5),chainid_flag)
!
	   if (chainid_flag) then
	      do k=6,40
	         word(k-1)=word(k)
	      end do
	   end if
!
	   if (n1==1) then
	     n2=n2+1
	   end if
!
	   boff=word(3)
	   atnm=boff(1:4)
	   read(word(6),*)  xx
	   read(word(7),*)  xy
	   read(word(8),*)  xz
!
! 	Taking care of non-existent information in pdb file
!
           if (word(9) == ' ') then
               xocc = 1.0
           else
               read(word(9),*)  xocc
           endif
           if (word(10) == ' ') then
               xbfa = 20.0
           else
               read(word(10),*) xbfa
           endif
!
	   if (strange_flag(n2)) then
	      ori(1)%res(n2)%res_aas(1)%aa_rots(1)%rot_ats(n1)%at_nam=atnm
	      ori(1)%res(n2)%res_aas(1)%aa_rots(1)%rot_ats(n1)%at_xyz(1)=xx
	      ori(1)%res(n2)%res_aas(1)%aa_rots(1)%rot_ats(n1)%at_xyz(2)=xy
	      ori(1)%res(n2)%res_aas(1)%aa_rots(1)%rot_ats(n1)%at_xyz(3)=xz
	      ori(1)%res(n2)%res_aas(1)%aa_rots(1)%rot_ats(n1)%at_occ=xocc
	      ori(1)%res(n2)%res_aas(1)%aa_rots(1)%rot_ats(n1)%at_bfa=xbfa
	      ori(1)%res(n2)%res_aas(1)%aa_rots(1)%rot_ats(n1)%at_flag=.true.
	   else
	      do j=1, ori(1)%res(n2)%res_aas(1)%aa_rots(1)%rot_nat
	if (ori(1)%res(n2)%res_aas(1)%aa_nam=='ILE '.and.atnm=='CD  ') then
	  atnm='CD1 '
	end if	
	if (atnm==ori(1)%res(n2)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_nam) then
	flag=ori(1)%res(n2)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_flag
	if (.not.flag) then
	  ori(1)%res(n2)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_xyz(1)=xx
	  ori(1)%res(n2)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_xyz(2)=xy
	  ori(1)%res(n2)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_xyz(3)=xz
	  ori(1)%res(n2)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_occ=xocc
	  ori(1)%res(n2)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_bfa=xbfa
	  ori(1)%res(n2)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_flag=.true.
!
	 end if
	end if
	      end do
	   end if
	   if (n1==natom(n2)) then
		n1=0
	   end if
        end do ReadPDBCoord
	close(14)
        PRINT*, '                            ..... DONE'
!
!
!	PRINT*,'***',ori(1)%mol_nrs 
!	do, i=1,ori(1)%mol_nrs
!	 PRINT*,'****',ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_nat
!	 do j=1, ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_nat
!	PRINT*, ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_nam
!	PRINT*, ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_xyz(1)
!	PRINT*, ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_xyz(2)
!	PRINT*, ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_xyz(3)
!	PRINT*, ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_occ 
!	PRINT*, ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_bfa 
!	 end do
!	end do
!
!	Writing everything out 
!
!	open(15,file='z.lis',form='formatted',status='unknown')
!
!	nats=0
!	do i=1, ori(1)%mol_nrs 
!       do j=1, ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_nat
!         nats=nats+1
!	 if (ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_flag) then 
!          atnm=ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_nam
!          if (atnm(4:4)==' ') then
!
!          write(15,fmt=2010)                                            &
!     &  'ATOM  ',                                                       &
!     &      nats,                                                       &
!     &  ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_nam,          &
!     &  ori(1)%res(i)%res_aas(1)%aa_nam,                                &
!     &  ori(1)%res(i)%res_chid,                                         &
!     &  ori(1)%res(i)%res_nold,                                         &
!     &  ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_xyz(1),       &
!     &  ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_xyz(2),       &
!     &  ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_xyz(3),       &
!     &  ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_occ,          &
!     &  ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_bfa
!
!          else
!
!          write(15,fmt=2011)                                            &
!     &  'ATOM  ',                                                       &
!     &      nats,                                                       &
!     &  ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_nam,          &
!     &  ori(1)%res(i)%res_aas(1)%aa_nam,                                &
!     &  ori(1)%res(i)%res_chid,                                         &
!     &  ori(1)%res(i)%res_nold,                                         &
!     &  ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_xyz(1),       &
!     &  ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_xyz(2),       &
!     &  ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_xyz(3),       &
!     &  ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_occ,          &
!     &  ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_bfa
!!
!          end if
!	 end if
!        end do
!	end do
!
!2010    format(a6,i5,2x,a4,a4,a,i4,4x,3f8.3,2f6.2)
!2011    format(a6,i5,1x,a4,1x,a4,a,i4,4x,3f8.3,2f6.2)
!
!	close(15)
!
	END SUBROUTINE LOAD_REF_STRUCT  
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE COMP_ANA_STRUCT 
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MOLEC_M
	USE AA_DATA_M
	USE MUMBO_DATA_M
	USE MAX_DATA_M
!
	IMPLICIT NONE
!
	INTEGER :: i, j, k, l, m, nat1, nat2, nct
	LOGICAl :: flag1, flag2, swap_flag1
	REAL :: rms1, rms2, sx, sy, sz, dis1, dis2, dis3
	REAL, DIMENSION(3) :: xat, yat
	REAL, DIMENSION(4) :: dis
	CHARACTER (LEN=4) :: atnm1, atnm2, aanm1
	CHARACTER (LEN=4), DIMENSION(:), ALLOCATABLE :: aan_swap
	REAL, DIMENSION(:,:), ALLOCATABLE :: xyz_swap
!
	PRINT*, '        '
	PRINT*, '################################'
	PRINT*, '# SUMMARY  COMP_ANA_STRUCT     #'
	PRINT*, '################################'
	PRINT*, '        '
!
!	Checking if rotamers are unique in molecular structure
!
	flag1=.true.
	flag2=.true.
!
	if (.not.allocated(mol)) then
	  flag1=.false.
	end if 
!
	if (flag1) then 
	 do i=2,size(mol)
	    if (mol(i)%res(1)%res_naa==1) then
	       if (mol(i)%res(1)%res_aas(1)%aa_nrt/=1) then
                 flag2=.false.
	       end if
	    else
	       flag2=.false.
	    end if
	 end do
	end if
!
	if (.not.flag1) then
	  PRINT*, '        '
	  PRINT*, '  NO MOLECULAR STRUCTURE PRESENT                         '
	  PRINT*, '  CAN ONLY FOCUS ON REFERENCE STRUCTURE RIGHT NOW        '
	  PRINT*, '        '
	end if
!
	if (.not.flag2) then
	  PRINT*, '        '
	  PRINT*, '  MORE THEN ONE ROTAMER PRESENT AT CERTAIN POSITIONS     '
          PRINT*, '  CAN ONLY FOCUS ON REFERENCE STRUCTURE RIGHT NOW        '
	  PRINT*, '        '
	end if
!
	if (flag1.and.flag2) then
!
	 do i=1,ori(1)%mol_nrs
	   do j=2,size(mol)   
	      if ((ori(1)%res(i)%res_chid)==(mol(j)%res(1)%res_chid)) then
	      if ((ori(1)%res(i)%res_nold)==(mol(j)%res(1)%res_nold)) then
	      if ((ori(1)%res(i)%res_aas(1)%aa_nam)==(mol(j)%res(1)%res_aas(1)%aa_nam)) then
	        aanm1 = ori(1)%res(i)%res_aas(1)%aa_nam
		swap_flag1=.false.
	        if (allocated(aan_swap)) deallocate(aan_swap)
	        if (allocated(xyz_swap)) deallocate(xyz_swap)
		if (aanm1=='PHE '.or.aanm1=='TYR ') then	         
		  allocate (aan_swap(4))
		  allocate (xyz_swap(8,3))
		  aan_swap(1)='CD1'
		  aan_swap(2)='CD2'
		  aan_swap(3)='CE1'
		  aan_swap(4)='CE2'
		  swap_flag1=.true.
		else if (aanm1=='GLU ') then
		  allocate (aan_swap(2))
		  allocate (xyz_swap(4,3))
		  aan_swap(1)='OE1'
		  aan_swap(2)='OE2'
		  swap_flag1=.true.
		else if (aanm1=='ASP ') then
		  allocate (aan_swap(2))
		  allocate (xyz_swap(4,3))
		  aan_swap(1)='OD1'
		  aan_swap(2)='OD2'
		  swap_flag1=.true.
		end if
!
	        if (mumbo_swap_qnh_flag) then
		  if (aanm1=='GLN ') then
		    allocate (aan_swap(2))
		    allocate (xyz_swap(4,3))
		    aan_swap(1)='OE1'
		    aan_swap(2)='NE2'
		    swap_flag1=.true.
		  else if (aanm1=='ASN ') then
		    allocate (aan_swap(2))
		    allocate (xyz_swap(4,3))
		    aan_swap(1)='OD1'
		    aan_swap(2)='ND2'
		    swap_flag1=.true.
		  else if (aanm1=='HIS ') then
		    allocate (aan_swap(4))
		    allocate (xyz_swap(8,3))
		    aan_swap(1)='ND1'
		    aan_swap(2)='CD2'
		    aan_swap(3)='CE1'
		    aan_swap(4)='NE2'
		    swap_flag1=.true.
	          end if
		end if
!
	        if (swap_flag1) then
	          nct = size(aan_swap)
		  do k=1, nct
		    do m=1,ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_nat 
		     if (aan_swap(k) == ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_nam) then
			 xyz_swap(k,1) = ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(1) 
			 xyz_swap(k,2) = ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(2) 
			 xyz_swap(k,3) = ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(3) 
		     end if
		    end do
		    do m=1,mol(j)%res(1)%res_aas(1)%aa_rots(1)%rot_nat
		     if (aan_swap(k) == mol(j)%res(1)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_nam) then
			 xyz_swap(nct+k,1) = mol(j)%res(1)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(1) 
			 xyz_swap(nct+k,2) = mol(j)%res(1)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(2) 
			 xyz_swap(nct+k,3) = mol(j)%res(1)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(3) 
		     end if
		    end do
		  end do
!
		  dis1 = 0
		  dis2 = 0
		  do k=1, nct  
		     dis3 = 0
		     dis3 = (xyz_swap(k,1) - xyz_swap(nct+k,1))**2
		     dis3 = dis3 + (xyz_swap(k,2) - xyz_swap(nct+k,2))**2
		     dis3 = dis3 + (xyz_swap(k,3) - xyz_swap(nct+k,3))**2
		     dis3 = sqrt(dis3)
		     dis1 = dis1 + dis3
		     dis3 = 0
		     if (mod(k,2)==1) then
		      dis3 = (xyz_swap(k,1) - xyz_swap(nct+k+1,1))**2
		      dis3 = dis3 + (xyz_swap(k,2) - xyz_swap(nct+k+1,2))**2
		      dis3 = dis3 + (xyz_swap(k,3) - xyz_swap(nct+k+1,3))**2
		      dis3 = sqrt(dis3)
		     else 
		      dis3 = (xyz_swap(k,1) - xyz_swap(nct+k-1,1))**2
		      dis3 = dis3 + (xyz_swap(k,2) - xyz_swap(nct+k-1,2))**2
		      dis3 = dis3 + (xyz_swap(k,3) - xyz_swap(nct+k-1,3))**2
		      dis3 = sqrt(dis3)
		     end if
		     dis2 = dis2 + dis3
		  end do
		  if (dis2 < dis1 ) then
	    PRINT*, '  SIDE CHAIN SWAPPING FOR RESIDUE:   ',      &
     &                          ori(1)%res(i)%res_aas(1)%aa_nam,            &
     &                          ori(1)%res(i)%res_chid, ori(1)%res(i)%res_nold
	    PRINT*, '  SUM_DIS1== ', dis1, 'SUM_DIS2== ', dis2
		    sx = 9999
		    sy = 9999
		    sz = 9999
		    do k = 1, nct  
		     if (mod(k,2)==1) then
		      sx = xyz_swap(k,1)
		      sy = xyz_swap(k,2)
		      sz = xyz_swap(k,3)
		      xyz_swap(k,1) = xyz_swap(k+1,1)
		      xyz_swap(k,2) = xyz_swap(k+1,2)
		      xyz_swap(k,3) = xyz_swap(k+1,3)
		     else 
		      xyz_swap(k,1) = sx
		      xyz_swap(k,2) = sy
		      xyz_swap(k,3) = sz
		     end if
                    end do
!
		    do k=1, nct
		      do m=1,ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_nat 
		       if (aan_swap(k) == ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_nam) then
			 ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(1) = xyz_swap(k,1) 
			 ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(2) = xyz_swap(k,2)
			 ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(3) = xyz_swap(k,3)
		       end if
		      end do
		    end do
		  end if
!
		end if  
	      end if
	      end if
	      end if
	  end do
	 end do
!
	PRINT*, '  '
!
!	Now calculating rms deviations ....
!
	rms1=0
	nat1=0
!
	 do i=1, ori(1)%mol_nrs
	   do j=1,3
	      xat(j)=0.0
	      yat(j)=0.0
	   end do
	   do j=1,4
	      dis(j)=0.0
	   end do
	   rms2=0
	   nat2=0
	   do j=2,size(mol)   
	      if ((ori(1)%res(i)%res_chid)==(mol(j)%res(1)%res_chid)) then
	      if ((ori(1)%res(i)%res_nold)==(mol(j)%res(1)%res_nold)) then
	      if ((ori(1)%res(i)%res_aas(1)%aa_nam)/=(mol(j)%res(1)%res_aas(1)%aa_nam)) then
	PRINT*, 'MISMATCH AMINO ACID AT ',ori(1)%res(i)%res_aas(1)%aa_nam,    &
     &                      ori(1)%res(i)%res_chid, ori(1)%res(i)%res_nold
	PRINT*, '  AA IN MOLEC. STRUCT CALLED ', mol(j)%res(1)%res_aas(1)%aa_nam
	      end if
!
		do k=1,ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_nat 
	         ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(k)%at_bfa=0.0
!
		 if (.not.mumbo_ref_beta_flag) then		 
	    	    if (ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(k)%at_nam==cb_name) cycle
		 end if
!
	         if (ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(k)%at_flag) then
		  atnm1= ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(k)%at_nam
	          xat(1) = ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(k)%at_xyz(1) 
	          xat(2) = ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(k)%at_xyz(2) 
	          xat(3) = ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(k)%at_xyz(3)
!
                  do l=1,mol(j)%res(1)%res_aas(1)%aa_rots(1)%rot_nat 
	            atnm2= mol(j)%res(1)%res_aas(1)%aa_rots(1)%rot_ats(l)%at_nam
		    if (atnm1==atnm2) then
!
	yat(1) = mol(j)%res(1)%res_aas(1)%aa_rots(1)%rot_ats(l)%at_xyz(1) 
	yat(2) = mol(j)%res(1)%res_aas(1)%aa_rots(1)%rot_ats(l)%at_xyz(2) 
	yat(3) = mol(j)%res(1)%res_aas(1)%aa_rots(1)%rot_ats(l)%at_xyz(3)
!
	dis(1) = (xat(1) - yat(1))**2
	dis(2) = (xat(2) - yat(2))**2
	dis(3) = (xat(3) - yat(3))**2
	dis(4) = sqrt(dis(1) + dis(2) + dis(3))
!
	rms2=rms2 + (dis(4)**2)
	rms1=rms1 + (dis(4)**2)
	nat2=nat2 + 1
	nat1=nat1 + 1
!
	ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(k)%at_bfa = dis(4)
!
		    end if
!
		  end do 

		 end if
!
		end do
!
	      end if
	      end if
	   end do 
!
	if (nat2 > 0) then
!
	 rms2= rms2/nat2
	 rms2= sqrt(rms2)
!	
	 write(*,'(A,2x,A4,2x,A1,2x,I5,A,F8.3)')                                    &
     &         '  RMS DEVIATION SIDE CHAIN OF ',ori(1)%res(i)%res_aas(1)%aa_nam,    &
     &         ori(1)%res(i)%res_chid, ori(1)%res(i)%res_nold, ' == ', rms2
!
	else 
	 write(*,'(A,2x,A4,2x,A1,2x,I5,A,A)')                                       &
     &         '  RMS DEVIATION SIDE CHAIN OF ',ori(1)%res(i)%res_aas(1)%aa_nam,    &
     &         ori(1)%res(i)%res_chid, ori(1)%res(i)%res_nold, ' == ','      --'
	end if
!
	 end do
!
	if (nat1 > 0) then
!	
	 rms1= rms1/nat1
	 rms1= sqrt(rms1)
!
	PRINT*, '        '
	WRITE(*,'(A,F7.3)')'NUM13: RMS DEVIATIONS ALL SIDE CHAINS ', rms1 
	PRINT*, '        '
	!
	else 

	PRINT*, '        '
	WRITE(*,'(A,F7.3)')'NUM13: RMS DEVIATIONS ALL SIDE CHAINS   == ' 
	PRINT*, '        '
	!
	end if
!
	end if
!
!	Writing out everyrthing
!
!	open(15,file='y.lis',form='formatted',status='unknown')
!
!	nats=0
!	do i=1, ori(1)%mol_nrs 
!        do j=1, ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_nat
!          nats=nats+1
!	 if (ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_flag) then 
!          aanm=ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_nam
!          if (aanm(4:4)==' ') then
!
!          write(15,fmt=2010)                                            &
!     &  'ATOM  ',                                                       &
!     &      nats,                                                       &
!     &  ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_nam,          &
!     &  ori(1)%res(i)%res_aas(1)%aa_nam,                                &
!     &  ori(1)%res(i)%res_chid,                                         &
!     &  ori(1)%res(i)%res_nold,                                         &
!     &  ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_xyz(1),       &
!     &  ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_xyz(2),       &
!     &  ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_xyz(3),       &
!     &  ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_occ,          &
!     &  ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_bfa
!
!          else
!
!          write(15,fmt=2011)                                            &
!     &  'ATOM  ',                                                       &
!     &      nats,                                                       &
!     &  ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_nam,          &
!     &  ori(1)%res(i)%res_aas(1)%aa_nam,                                &
!     &  ori(1)%res(i)%res_chid,                                         &
!     &  ori(1)%res(i)%res_nold,                                         &
!     &  ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_xyz(1),       &
!     &  ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_xyz(2),       &
!     &  ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_xyz(3),       &
!     &  ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_occ,          &
!     &  ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_bfa
!
!          end if
!	 end if
!        end do
!	end do
!
!2010    format(a6,i5,2x,a4,a4,a,i4,4x,3f8.3,2f6.2)
!2011    format(a6,i5,1x,a4,1x,a4,a,i4,4x,3f8.3,2f6.2)
!
!	close(15)
!
	END SUBROUTINE COMP_ANA_STRUCT   
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE PICKNAT_STRUCT 
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MOLEC_M
	USE AA_DATA_M
	USE MUMBO_DATA_M
	USE MAX_DATA_M
!
	IMPLICIT NONE
!
	INTEGER :: i, j, k, l, m, n, nat, nct, nlist, ndt, q, naver
	REAL :: rms, rmsmin, rmsaver
	REAL, DIMENSION(3) :: xat, yat
	REAL, DIMENSION(4) :: dis
	REAL, DIMENSION(:), ALLOCATABLE :: rotrms
	CHARACTER (LEN=4) :: atnm1, atnm2, atnm3, aanm1, aanm2
	CHARACTER (LEN=4), DIMENSION(:), ALLOCATABLE :: atomlist, aan_swap
	LOGICAL :: matchflag, swap_flag
!
	PRINT*, '        '
	PRINT*, '################################'
	PRINT*, '# SUMMARY  PICKNAT_STRUCT      #'
	PRINT*, '################################'
	PRINT*, '        '
	PRINT*, '  PICKING THE BEST MATCH.....'
	PRINT*, '        '
	PRINT*, '   (Please note that sometimes more than one rotamer is retained. It is quite'
	PRINT*, '    possible that the rotamers only differ by the positions of the hydrogens.' 
	PRINT*, '    These might well be absent in the reference structure)'
	PRINT*, '        '
!
!       SET ALL ROTAMER FLAGS TO FALSE  
!
	do j=2,size(mol)  
	     do k= 1, mol(j)%res(1)%res_naa
	        do l=1,mol(j)%res(1)%res_aas(k)%aa_nrt
		  mol(j)%res(1)%res_aas(k)%aa_rots(l)%rot_flag=.false.
	        end do
	     end do
	end do
!
!
	rmsaver=0
	naver=0
	do i=1,ori(1)%mol_nrs
!
!	Taking care of possible atom swapping problems such as OD1 and OD2 in Asp etc.. 
!	Everything is duplicated even non-swapped residues. 
!	This should only slightly increase cpu time
!	  
	   nlist=ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_nat*2
	   if (allocated(atomlist)) deallocate(atomlist)
	   allocate(atomlist(nlist))
!	   
	   ndt=0
	   do j=1,2
	   do k=1,ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_nat
	   ndt=ndt+1
	     atomlist(ndt)=ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(k)%at_nam	   
	   end do
	   end do
!	   
	   aanm1=ori(1)%res(i)%res_aas(1)%aa_nam
	   swap_flag=.false.
!	   
	   if (allocated(aan_swap)) deallocate(aan_swap)
           allocate (aan_swap(1))
	   if (aanm1=='PHE '.or.aanm1=='TYR ') then
	          if (allocated(aan_swap)) deallocate(aan_swap)
		  allocate (aan_swap(4))
		  aan_swap(1)='CD1'
		  aan_swap(2)='CD2'
		  aan_swap(3)='CE1'
		  aan_swap(4)='CE2'
		  swap_flag=.true.
	   else if (aanm1=='GLU ') then
	          if (allocated(aan_swap)) deallocate(aan_swap)
		  allocate (aan_swap(2))
		  aan_swap(1)='OE1'
		  aan_swap(2)='OE2'
		  swap_flag=.true.
	   else if (aanm1=='ASP ') then
	          if (allocated(aan_swap)) deallocate(aan_swap)
		  allocate (aan_swap(2))
		  aan_swap(1)='OD1'
		  aan_swap(2)='OD2'
		  swap_flag=.true.
	   end if
!	   
	   if (mumbo_swap_qnh_flag) then
		if (aanm1=='GLN ') then
	          if (allocated(aan_swap)) deallocate(aan_swap)
		  allocate (aan_swap(2))
		  aan_swap(1)='OE1'
		  aan_swap(2)='NE2'
		  swap_flag=.true.
		else if (aanm1=='ASN ') then
	          if (allocated(aan_swap)) deallocate(aan_swap)
		  allocate (aan_swap(2))
		  aan_swap(1)='OD1'
		  aan_swap(2)='ND2'
		  swap_flag=.true.
		else if (aanm1=='HIS ') then
	          if (allocated(aan_swap)) deallocate(aan_swap)
		  allocate (aan_swap(4))
		  aan_swap(1)='ND1'
		  aan_swap(2)='CD2'
		  aan_swap(3)='CE1'
		  aan_swap(4)='NE2'
		  swap_flag=.true.
	        end if
	   end if
!
	   if (swap_flag) then
!
	   do j=((nlist/2)+1),nlist
	      atnm3=atomlist(j)
!
	      do k=1,size(aan_swap)
		if (atnm3==aan_swap(k)) then
!		
		    if (mod(k,2)==0) then 
		       atomlist(j)=aan_swap(k-1)
!		PRINT*, 'SWAPPING   ',aan_swap(k),aan_swap(k-1),' in ', aanm1  
		    else if (mod(k,2)==1) then 
		       atomlist(j)=aan_swap(k+1)
!		PRINT*, 'SWAPPING   ',aan_swap(k),aan_swap(k+1),' in ', aanm1  
		    end if
!		
		end if
	      end do
!
	   end do
!
	   end if
!	
	   do j=2,size(mol)   
	      if ((ori(1)%res(i)%res_chid)==(mol(j)%res(1)%res_chid)) then
	      if ((ori(1)%res(i)%res_nold)==(mol(j)%res(1)%res_nold)) then
!	      
	     matchflag=.false. 
!
	     do k= 1, mol(j)%res(1)%res_naa
!
	      aanm2 = mol(j)%res(1)%res_aas(k)%aa_nam
!
	      if (aanm1==aanm2) then
	      
	        matchflag=.true.
	        
	        if (aanm1==gly_name) then 
	           mol(j)%res(1)%res_aas(k)%aa_rots(1)%rot_flag=.true.
	 write(*,'(A,2x,A4,2x,A1,2x,I5,A)')                                         &
     &         '  BEST MATCH, SIDE CHAIN, RMS DEVIATION:', aanm2,                   &
     &          mol(j)%res(1)%res_chid, mol(j)%res(1)%res_nold,' ==    0.000  '
		  cycle
		end if
!
	        if (allocated(rotrms)) deallocate(rotrms)
		allocate (rotrms(mol(j)%res(1)%res_aas(k)%aa_nrt*2))
!		
! Now calculate rmsd for each rotamer
!
		do q=1,2
		do l=1,mol(j)%res(1)%res_aas(k)%aa_nrt
		rms = 0
		nat = 0
		do m=1,ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_nat 
	         if (ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_flag) then
		  atnm1= atomlist(((q-1)*(nlist/2))+m)
!
!		  atnm1= ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_nam
!
		  xat(1) = ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(1) 
	          xat(2) = ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(2) 
	          xat(3) = ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(3)
!
                  do n=1,mol(j)%res(1)%res_aas(k)%aa_rots(l)%rot_nat 
	            atnm2= mol(j)%res(1)%res_aas(k)%aa_rots(l)%rot_ats(n)%at_nam
		    
		    if (atnm1==atnm2) then
!
	yat(1) = mol(j)%res(1)%res_aas(k)%aa_rots(l)%rot_ats(n)%at_xyz(1) 
	yat(2) = mol(j)%res(1)%res_aas(k)%aa_rots(l)%rot_ats(n)%at_xyz(2) 
	yat(3) = mol(j)%res(1)%res_aas(k)%aa_rots(l)%rot_ats(n)%at_xyz(3)
!
!DEBUG
!	PRINT*, xat
!	PRINT*, yat
!	PRINT*, aanm1, atnm1, atnm2
!		    
	dis(1) = (xat(1) - yat(1))**2
	dis(2) = (xat(2) - yat(2))**2
	dis(3) = (xat(3) - yat(3))**2
	dis(4) = sqrt(dis(1) + dis(2) + dis(3))
!
	rms=rms + (dis(4)**2)
	nat=nat + 1
!	
		    end if
		    
		  end do
		 end if
		end do 
		if (nat==0.and.aanm1/=gly_name) then 
	PRINT*,'  '
	PRINT*,'>>>>>>>>> BUMMER >>>>>>>>>>>>>>>>>>>>>>>>>>>                     '
	PRINT*,'>>>                                                              '
	PRINT*,'>>> NO MATCHING ATOMS FOUND ALTHOUGH RESIDUE NAMES ARE IDENTICAL '
	PRINT*,'>>> IN MUMBO STRUCTURE AND REFERENCE STRUCTURE                   '
	PRINT*,'>>>                MUST STOP    (SUBROUTINE PICK_NAT_STRCT)      '                            
	PRINT*,'>>>                                                              '
	PRINT*,'>>>>>>>>> BUMMER >>>>>>>>>>>>>>>>>>>>>>>>>>>                     '
	PRINT*,'  '
			stop
		else if (nat==0.and.aanm1==gly_name) then 
		rotrms(((q-1)*mol(j)%res(1)%res_aas(k)%aa_nrt)+l)=0
		else if (nat/=0) then 
		rotrms(((q-1)*mol(j)%res(1)%res_aas(k)%aa_nrt)+l)=sqrt(rms/nat)
		end if
	       end do
	       end do
!
! Now identify the rotamer with the lowest rmsd
!
		rmsmin = 999999
		do l=1,(mol(j)%res(1)%res_aas(k)%aa_nrt*2)
		  if (rotrms(l).lt.rmsmin) then  
		    rmsmin = rotrms(l) 
		  end if
		end do 
!
! Now flag the rotamers for writting out 
!
		rmsmin = rmsmin + picknat_rms_tolerance
		nct=0
		do q=1,2
		do l=1,mol(j)%res(1)%res_aas(k)%aa_nrt
		  nct=((q-1)*mol(j)%res(1)%res_aas(k)%aa_nrt)+l
		  if (rotrms(nct).le.rmsmin) then
!		  
		   mol(j)%res(1)%res_aas(k)%aa_rots(l)%rot_flag=.true.
		   rmsaver=rmsaver + rotrms(nct)
		   naver=naver+1
!
		    if (q==1) then 
	 write(*,'(A,2x,A4,2x,A1,2x,I5,A,F8.3)')                                    &
     &         '  BEST MATCH, SIDE CHAIN, RMS DEVIATION:', aanm2,                   &
     &          mol(j)%res(1)%res_chid, mol(j)%res(1)%res_nold,' == ', rotrms(nct)
		    else if (q==2.and.swap_flag) then
	 write(*,'(A,2x,A4,2x,A1,2x,I5,A,F8.3,A)')                                  &
     &         '  BEST MATCH, SIDE CHAIN, RMS DEVIATION:', aanm2,                   &
     &          mol(j)%res(1)%res_chid, mol(j)%res(1)%res_nold,' == ', rotrms(nct), &
     &         ' AFTER ATOM SWAPPING '
		    
		    end if
		  end if
		end do
		end do
!
	      end if
	      end do
!
	      if (matchflag.eqv..false.) then 
	PRINT*,'  '
	PRINT*,'>>>>>>>>> BUMMER >>>>>>>>>>>>>>>>>>>>>>>>>>>                     '
	PRINT*,'>>>                                                              '
	PRINT*,'>>> NO MATCHING RESIDUES / LIGANDS FOUND FOR POSITION            '
	PRINT*,'>>> ', mol(j)%res(1)%res_chid, mol(j)%res(1)%res_nold 
	PRINT*,'>>> PLEASE CHECK - REFERENCE STRUCTURE- OR -MUMBO.INP-           '
	PRINT*,'>>>                                                              '
	PRINT*,'>>>          MUST STOP                                           '                            
	PRINT*,'>>>                                                              '
	PRINT*,'>>>>>>>>> BUMMER >>>>>>>>>>>>>>>>>>>>>>>>>>>                     '
	PRINT*,'  '
	         stop
	      end if
!	      
	      end if
	      end if
	   end do 
	   end do
	PRINT*, '        '
	WRITE(*,'(A,2x,F8.3,x,A)') '   AVERAGE SIDE CHAIN RMS =',rmsaver/naver,'Angs.'
	PRINT*, '        '
	PRINT*, '        '
	PRINT*, '      .... DONE PICKING BEST MATCHES'
	PRINT*, '        '
!
	END SUBROUTINE PICKNAT_STRUCT   
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!


